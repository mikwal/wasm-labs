import { WASIMemory, WASIModule } from '/js/wasi';

export async function init(programUrl: string) {

    const canvas = document.getElementById('screen')!;
    const fpsCounter = document.getElementById('fps-counter')!;

    console.log(`Loading and compiling '${programUrl}'...`);

    const module = new WASIModule();
    try {
        await module.load(programUrl, { 
            imports: {
                app: {
                    'show_fps': (fps: number) => {
                        fpsCounter.innerText = `FPS=${Number(fps).toFixed(0)}`;
                    }
                }
            },
            plugins: [
                ['/js/GLES2', { canvas }]
            ]
        });
    }
    catch (error) {
        console.error(`Failed to load and compile '${programUrl}':`, error);
        return;
    }

    console.log(`Done, running program...`);

    const code = await module.start();

    console.log(`Program exited with code ${code}.`);
}
