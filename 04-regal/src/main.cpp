#include <GL/Regal.h>
#include <browser.h>
#include <cmath>
#include <cstdlib>

extern "C" {
    void show_fps(float fps) __attribute__((__import_module__("app"), __import_name__("show_fps")));
}

double get_time() {
    timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
	return t.tv_sec * 1000.0 + t.tv_nsec / 1000000.0;
}

int main(int argc, char **argv) {
    RegalMakeCurrent((RegalSystemContext)1);

    static GLuint textureID = 0;
    if (textureID == 0) {
        const int texWidth = 4;
        const int texHeight = 4;
        uint32_t *pixels = (uint32_t *)malloc(sizeof(uint32_t) * texWidth * texHeight);
        for (int i = 0; i < texWidth; i++) {
            for (int j = 0; j < texHeight; j++) {
                uint32_t color = 0xFFFFFFFF;
                if ((j >= 2 && i < 2) || (j < 2 && i >= 2)) {
                    color = 0xFFCCCCCC;
                }
                pixels[j * 4 + i] = color;
            }
        }
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &textureID);
        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, &pixels[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glEnable(GL_TEXTURE_2D);

    double t0 = get_time();
    double time = 0;
    const auto max_run_time = 4000; //2*60000.0;
    int frameCount = 0;

    while (time < max_run_time) {
        if (time > 0) {
            show_fps((float)frameCount / (time / 1000.0f));
        }
        frameCount++;

        __proxy_batch_mode(true);

        browser::wait_for_animation_frame();

        glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glBegin(GL_TRIANGLES);

        GLfloat red = cosf(time / 10000.0f);
        GLfloat green = sinf(time / 10000.0f);
        GLfloat blue = 0.5 * sinf(time / 20000.0f) + 0.5 * cosf(time / 40000.0f);

        GLfloat a = time / 5000.0f;

        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(0, 0, 0);
        glColor3f(red, green, blue);
        glTexCoord2f(20.0, 0.0);
        glVertex3f(sinf(a), cosf(a), 0);
        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 20.0);
        glVertex3f(-cosf(a), sinf(a), 0);

        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(0, 0, 0);
        glColor3f(red, green, blue);
        glTexCoord2f(20.0, 0.0);
        glVertex3f(-sinf(a), -cosf(a), 0);
        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 20.0);
        glVertex3f(cosf(a), -sinf(a), 0);

        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(0, 0, 0);
        glColor3f(red, green, blue);
        glTexCoord2f(20.0, 0.0);
        glVertex3f(sinf(a), cosf(a), 0);
        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 20.0);
        glVertex3f(cosf(a), -sinf(a), 0);

        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(0, 0, 0);
        glColor3f(red, green, blue);
        glTexCoord2f(20.0, 0.0);
        glVertex3f(-sinf(a), -cosf(a), 0);
        glColor3f(red, green, blue);
        glTexCoord2f(0.0, 20.0);
        glVertex3f(-cosf(a), sinf(a), 0);

        glEnd();

        __proxy_batch_mode(false);

        time = get_time() - t0;
    }

    __proxy_batch_mode(false);

    char infoLog[4000];
    glGetProgramInfoLog(1, 4000, NULL, infoLog);
    printf("InfoLOG: \n%s\n----\n", infoLog);
    glDeleteTextures(1, &textureID);
    RegalDestroyContext((RegalSystemContext)1);
     __proxy_batch_mode(false);

}