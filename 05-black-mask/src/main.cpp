#include <browser.h>
#include <cstdlib>
#include <gl/gl.h>

int glCreateWnd(const char* , uint32_t ,uint32_t ,uint32_t ,uint32_t);
long glWndProc(int, unsigned int, unsigned int, long);
int WinMain(int hInstance,int hPrevInstance,void *lpCmdLine,int nCmdShow);

extern "C" void __show_fps(double fps) __attribute__((__import_module__("app"), __import_name__("show_fps")));

int window_new_width = 0;
int window_new_height = 0;
void on_resize_window(int new_width, int new_height) __attribute__((__export_name__("on_resize_window"))) {
    window_new_width = new_width;
    window_new_height = new_height;
}

void glClear() {
    __proxy_batch_mode(true);
    browser::wait_for_animation_frame();
    rglClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
 
void glSwapBuf() {

    __show_fps(fps);
    __proxy_batch_mode(false);

    if (window_new_width && window_new_height) {
        glWndProc(0, WM_SIZE, 0, (window_new_width & 0xFFFF) | ((window_new_height & 0xFFFF) << 16));
        window_new_width = window_new_height = 0;
    }
}


int main(int argc, char **argv) {
    uint32_t width = 800;
    uint32_t height = 600;
    if (argc >= 2) {
        width = atoi(argv[0]);
        height = atoi(argv[1]);
    }
    RegalMakeCurrent((RegalSystemContext)1);
    browser::load_file_async("/globject.mdf");
    browser::load_file_async("/gltexture.mdf");
    browser::load_file_async("/H2O.XM");
    browser::wait_for_async();
    glCreateWnd(NULL, width, height, 0, 0);
    return WinMain(0, 0, NULL, 0);
}