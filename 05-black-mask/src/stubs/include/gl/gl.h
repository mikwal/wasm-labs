#ifndef STUBS_GL_H
#define STUBS_GL_H

#include <GL/Regal.h>
#include <windows.h>

#undef glClear

void glClear();

void glSwapBuf();

#endif