#ifndef STUBS_MIDASDLL_H
#define STUBS_MIDASDLL_H

#include <cstdint>

typedef uint32_t MIDASmodule;
typedef uint32_t MIDASmodulePlayHandle;

void MIDASstartup();
void MIDASinit();
void MIDASsetAmplification(int );
void MIDASstartBackgroundPlay(int );
MIDASmodule MIDASloadModule(const char* );
MIDASmodulePlayHandle MIDASplayModule(MIDASmodule, bool );
void MIDASstopModule(MIDASmodulePlayHandle );
void MIDASfreeModule(MIDASmodule);
void MIDASstopBackgroundPlay();
void MIDASclose();

#endif