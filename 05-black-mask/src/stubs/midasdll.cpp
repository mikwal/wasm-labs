#include <midasdll.h>

extern "C" {
    void __xm_init() __attribute__((__import_module__("app"), __import_name__("xm_init")));
    void __xm_load(const char *) __attribute__((__import_module__("app"), __import_name__("xm_load")));
    void __xm_play() __attribute__((__import_module__("app"), __import_name__("xm_play")));
    void __xm_stop() __attribute__((__import_module__("app"), __import_name__("xm_stop")));
}

void MIDASstartup() {}
void MIDASinit() { __xm_init(); }
void MIDASsetAmplification(int ) {}
void MIDASstartBackgroundPlay(int ) {}
MIDASmodule MIDASloadModule(const char* filename) { __xm_load(filename); return 0; }
MIDASmodulePlayHandle MIDASplayModule(MIDASmodule, bool ) { __xm_play(); return 0; }
void MIDASstopModule(MIDASmodulePlayHandle ) {}
void MIDASfreeModule(MIDASmodule) {}
void MIDASstopBackgroundPlay() { __xm_stop(); }
void MIDASclose() {}
