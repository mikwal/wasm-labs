#ifndef GLENV_H
#define GLENV_H

#include "glMath.h"

#define GL_MAXLIGHTSRC 8

class GL_CAMERA {
private:


public:
	GL_VECTOR3D pos;
	float head;
	float pitch;
	float roll;
	
	UINT vp_x;
	UINT vp_y;
	UINT vp_w;
	UINT vp_h;
	float fov;
	float zs;
	float ze;
	float _dx,_nx_x,_nx_z;
	float _dy,_ny_y,_ny_z;

	GL_VECTOR3D *target;
	GL_VECTOR3D dir;
	GL_VECTOR3D up;
	GL_MATRIX mtx;

	void reset();
	void updmatrix();
	void updview();
	
	inline void settarget(GL_VECTOR3D *_target) {target=_target;}
};


class GL_LIGHTSRC {
public:
	UINT ID;

	GL_VECTOR3D pos;
	float pos_4;
	
	GL_VECTOR3D s_dir;
	GL_VECTOR3D *s_target;
	float s_exp;
	float s_ang;

	float c_atn;
	float l_atn;
	float q_atn;
	float ints;

	GL_RGBACOLOR amb;
	GL_RGBACOLOR dif;
	GL_RGBACOLOR spec;
	float ints_amb;
	float ints_dif;
	float ints_spec;


	inline void on() {glEnable(ID);}
	inline void off() {glDisable(ID);}
	inline UCHAR ison() {return glIsEnabled(ID);}

	void reset();
	inline void updpos() {glLightfv(ID,GL_POSITION,(float*)&pos);}
	void updspot();
	void updcolor();
	void updall();

	inline void settarget(GL_VECTOR3D *_target) {s_target=_target;}
};


extern GL_CAMERA glCam;
extern GL_LIGHTSRC glLight[GL_MAXLIGHTSRC];


struct GL_FOGVECTOR {
	float d;
	float zs;
	float ze;
};

extern GL_FOGVECTOR glFog;
extern GL_RGBACOLOR glBgColor;
extern GL_RGBACOLOR glAmbLight;
void glSetFog(float ,float ,float );
void glUpdFog();
void glSetBgColor(float ,float ,float ,float );
void glUpdBgColor();
void glSetAmbLight(float ,float ,float ,float );
void glUpdAmbLight();


extern float glFrameTime;
float glTime();
inline void glSetFps(float fps) {glFrameTime=1000.0f/fps;}
#define SECVAL(v) ((v)*(glFrameTime/1000.0f))


void glInitEnv();


extern void glSetRenderMode(UINT ,float size=1.0f);
#define GLR_POINTS		0x0000
#define GLR_LINES		0x0001
#define GLR_FILLED		0x0002

inline void glZBufOn() {glEnable(GL_DEPTH_TEST);}
inline void glZBufOff() {glDisable(GL_DEPTH_TEST);}
inline UCHAR glIsZBufOn() {return glIsEnabled(GL_DEPTH_TEST);}
inline void glZBufMask(bool mask) {glDepthMask(mask);}
inline void glTextureOn() {glEnable(GL_TEXTURE_2D);}
inline void glTextureOff() {glDisable(GL_TEXTURE_2D);}
inline UCHAR glIsTextureOn() {return glIsEnabled(GL_TEXTURE_2D);}
inline void glSetTexture(UINT _text) {glBindTexture(GL_TEXTURE_2D,_text);}
inline void glSMapOn() {glEnable(GL_TEXTURE_GEN_S);glEnable(GL_TEXTURE_GEN_T);}
inline void glSMapOff() {glDisable(GL_TEXTURE_GEN_S);glDisable(GL_TEXTURE_GEN_T);}
inline UCHAR glIsSMapOn() {return glIsEnabled(GL_TEXTURE_GEN_S)&glIsEnabled(GL_TEXTURE_GEN_T);}
inline void glBlendOn() {glEnable(GL_BLEND);}
inline void glBlendOff() {glDisable(GL_BLEND);}
inline UCHAR glIsBlendOn() {return glIsEnabled(GL_ALPHA_TEST);}
inline void glAlphaOn() {glEnable(GL_ALPHA_TEST);}
inline void glAlphaOff() {glDisable(GL_ALPHA_TEST);}
inline UCHAR glIsAlphaOn() {return glIsEnabled(GL_ALPHA_TEST);}
inline void glLightOn() {glEnable(GL_LIGHTING);}
inline void glLightOff() {glDisable(GL_LIGHTING);}
inline UCHAR glIsLightOn() {return glIsEnabled(GL_LIGHTING);}
inline void glFogOn() {glEnable(GL_FOG);}
inline void glFogOff() {glDisable(GL_FOG);}
inline UCHAR glIsFogOn() {return glIsEnabled(GL_FOG);}
inline void glCFaceOn() {glEnable(GL_CULL_FACE);}
inline void glCFaceOff() {glDisable(GL_CULL_FACE);}
inline UCHAR glIsCFaceOn() {return glIsEnabled(GL_CULL_FACE);}
inline void glPolySmoothOn() {glEnable(GL_POLYGON_SMOOTH);}
inline void glPolySmoothOff() {glDisable(GL_POLYGON_SMOOTH);}
inline UCHAR glIsPolySmoothOn() {glIsEnabled(GL_POLYGON_SMOOTH);}
inline void glLineSmoothOn() {glEnable(GL_LINE_SMOOTH);}
inline void glLineSmoothOff() {glDisable(GL_LINE_SMOOTH);}
inline UCHAR glIsLineSmoothOn() {glIsEnabled(GL_LINE_SMOOTH);}
inline void glPointSmoothOn() {glEnable(GL_POINT_SMOOTH);}
inline void glPointSmoothOff() {glDisable(GL_POINT_SMOOTH);}
inline UCHAR glIsPointSmoothOn() {glIsEnabled(GL_POINT_SMOOTH);}

#endif