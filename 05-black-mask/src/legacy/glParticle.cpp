#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glaux.h>
#include "glEnv.h"
#include "glParticle.h"



void GL_PARTICLESYSTEM::_initp(UINT i)
{
	GL_VECTOR3D _rot;

	if(flags&GLP_SPHEREINIT)
	{
		_rot.x=randf()*RAD_CIRCLE;
		_rot.y=randf()*RAD_CIRCLE;
		_rot.z=randf()*RAD_CIRCLE;

		p[i].pos.x=(-pos_dif.x)+((randf()*2.0f)*pos_dif.x);
		p[i].pos.y=0.0f;
		p[i].pos.z=0.0f;

		p[i].pos.zrotate(_rot.z);
		p[i].pos.yrotate(_rot.y);
		
		p[i].pos+=pos;

		p[i].vel.x=0.0f;
		p[i].vel.y=vel.y-vel_dif.y+((randf()*2.0f)*vel_dif.y);
		p[i].vel.z=0.0f;

		p[i].vel.xrotate(_rot.x);
		p[i].vel.zrotate(_rot.z);
		p[i].vel.yrotate(_rot.y);
		

	}
	else
	{
		p[i].pos.x=pos.x-pos_dif.x+(randf()*pos_dif.x*2.0f);
		p[i].pos.y=pos.y-pos_dif.y+(randf()*pos_dif.y*2.0f);
		p[i].pos.z=pos.z-pos_dif.z+(randf()*pos_dif.z*2.0f);
		p[i].vel.x=vel.x-vel_dif.x+(randf()*vel_dif.x*2.0f);
		p[i].vel.y=vel.y-vel_dif.y+(randf()*vel_dif.y*2.0f);
		p[i].vel.z=vel.z-vel_dif.z+(randf()*vel_dif.z*2.0f);
	}
	p[i].col.r=col.r-col_dif.r+(randf()*col_dif.r*2.0f);
	p[i].col.g=col.g-col_dif.g+(randf()*col_dif.g*2.0f);
	p[i].col.b=col.b-col_dif.b+(randf()*col_dif.b*2.0f);
	p[i].col.a=col.a-col_dif.a+(randf()*col_dif.a*2.0f);
	p[i].fade.r=fade.r-fade_dif.r+(randf()*fade_dif.r*2.0f);
	p[i].fade.g=fade.g-fade_dif.g+(randf()*fade_dif.g*2.0f);
	p[i].fade.b=fade.b-fade_dif.b+(randf()*fade_dif.b*2.0f);
	p[i].fade.a=fade.a-fade_dif.a+(randf()*fade_dif.a*2.0f);
}


void GL_PARTICLESYSTEM::move()
{
	UINT i;
	GL_VECTOR3D _dir;

	if(isdead())
		return;

	if(p_used<n_p)
	{
		i=p_used;
		_pc+=pps;
		p_used=UINT(_pc);

		while(i<p_used&&i<n_p)
		{
			_initp(i);
			p[i].alive=true;
			i++;
		}

		if(p_used>=n_p)
			p_used=n_p;
	}
		

	for(i=0;i<p_used;i++)
		if(p[i].alive)
		{
			if(p[i].col.a<0.0f||(flags&GLP_BOUNDINGBOX&&p[i].pos>bbox))
				if(flags&GLP_REGENERATE)
					_initp(i);
				else
				{
					p_dead++;
					p[i].alive=false;
					continue;
				}

			p[i].pos+=p[i].vel;
			p[i].col+=p[i].fade;

			if(flags&GLP_GRAVITY)
				if((flags&GLP_GRAVCENTER)&~GLP_GRAVITY)
				{
					_dir=p[i].pos-pos;
					p[i].vel-=_dir*(!grav/!_dir);
				}
				else
					p[i].vel+=grav;


		}
}


void GL_PARTICLESYSTEM::render()
{
	UINT i;
	GL_VECTOR3D _pos;
	GL_RGBACOLOR bg(0.0f);

	if(isdead())
		return;

	glPushMatrix();
	glLoadIdentity();
	
	glBindTexture(GL_TEXTURE_2D,text);

	glClearColor(bg.r,bg.g,bg.b,bg.a);
	glFogfv(GL_FOG_COLOR,(float*)&bg);
	
	glBegin(GL_TRIANGLES);
	
	for(i=0;i<p_used;i++)	
		if(p[i].alive)
		{
			_pos=p[i].pos*glCam.mtx;
			
			if(flags&GLP_LINEARALPHA)
				glColor4f(p[i].col.r,p[i].col.g,p[i].col.b,(p[i].col.a-!(p[i].pos-glCam.pos))/p[i].col.a);
			else	
				glColor4fv((float*)&p[i].col);


			glTexCoord2f(1.0f,1.0f);
			glVertex3f(_pos.x+size,_pos.y+size,_pos.z);
			glTexCoord2f(0.0f,1.0f);
			glVertex3f(_pos.x-size,_pos.y+size,_pos.z);
			glTexCoord2f(1.0f,0.0f);
			glVertex3f(_pos.x+size,_pos.y-size,_pos.z);

			glTexCoord2f(1.0f,0.0f);
			glVertex3f(_pos.x+size,_pos.y-size,_pos.z);
			glTexCoord2f(0.0f,1.0f);
			glVertex3f(_pos.x-size,_pos.y+size,_pos.z);
			glTexCoord2f(0.0f,0.0f);
			glVertex3f(_pos.x-size,_pos.y-size,_pos.z);	
		}
	glEnd();
	glUpdBgColor();
	glPopMatrix();
}

void GL_PARTICLESYSTEM::explode(float s,float ints,bool die)
{
	UINT i;

	if(isdead())
		return;

	if(die)
		flags&=~GLP_REGENERATE;

	for(i=0;i<p_used;i++)	
		if(p[i].alive)
		{
			p[i].pos=pos;
			p[i].vel.x=(-1.0f+randf()*2.0f)*s;
			p[i].vel.y=(-1.0f+randf()*2.0f)*s;
			p[i].vel.z=(-1.0f+randf()*2.0f)*s;
			p[i].col*=ints;
			p[i].fade.a/=ints;
		}
}


