#ifndef GLPRINTF_H
#define GLPRINTF_H

#define GLF_RIGHT	0x00
#define GLF_CENTER	0x01
#define GLF_LEFT	0x02
#define GLF_TOP		GLF_RIGHT
#define GLF_BOTTOM	GLF_LEFT


void glTextModeOn(float scale=100.0f);
void glTextModeOff();

class GL_FONT {
	void _print(float ,float ,float,char *);

public:
	UINT face;
	float width,height;
	float hspace,vspace;
	UINT halign,valign;
	GL_RGBACOLOR color;

	void init(UINT ,float ,float );
	inline void setsize(float _width,float _height) {width=_width;height=_height;}	
	inline void setalign(UINT _halign,UINT _valign) {halign=_halign&0x0F;valign=_valign&0x0F;}
	inline void setspace(float _hspace,float _vspace) {hspace=_hspace;vspace=_vspace;}
	inline void setcolor(float r,float g,float b,float a) {color=GL_RGBACOLOR(r,g,b,a);}
	inline void setcolor(const GL_RGBACOLOR &c) {color=c;}		
	void printf(float x,float y,char *,...);
	void printf(GL_VECTOR3D ,GL_VECTOR3D ,char *,...);
};

#endif