#ifndef GLTEXTURE_H
#define GLTEXTURE_H

#include "DATFILE.h"

#define GLT_NEAREST		0x0000
#define GLT_LINEAR		0x0001 
#define GLT_MIPMAP		0x0002

extern UINT glTextureFilter;

void glCreateTexture(UINT *,const void *,UINT ,UINT ,UINT );
int glLoadTexture(UINT *,const char *);
void glFreeTexture(UINT *);

#include "DATFILE.h"

void glTFLoadProc(DATOBJ *,PACKFILE *);
void glTFFreeProc(DATOBJ *);

class GL_TEXTUREFILE : public DATFILE {
public:
	inline GL_TEXTUREFILE():DATFILE() {}
	inline GL_TEXTUREFILE(const char *filename):DATFILE() {open(filename);}
	inline int open(const char *filename) {return ((DATFILE*)this)->open(filename,true,glTFLoadProc,glTFFreeProc);}

	UINT obj(int );
	inline UINT obj(const char *name) {return obj(index(name));}
	inline UINT operator[](int i) {return obj(i);}
	inline UINT operator[](const char *name) {return obj(index(name));}
};

#endif