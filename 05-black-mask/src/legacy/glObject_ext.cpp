#include <windows.h>
#include <stdio.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "glEnv.h"
#include "glObject.h"


void _gl_fix_ASE_v3d(GL_VECTOR3D *);


int GL_BONE::load(const char *filename)
{
	FILE *fp;
	UINT i,ii;	

	unload();

	if(glObjectFile)
		return load(filename,*glObjectFile);

	fp=fopen(filename,"rb");
	if(fp==NULL)
		return -1;

	fread(&n_bone,sizeof(UINT),1,fp);
	bone=new GL_BONESECTOR[n_bone];
	memset(bone,0,sizeof(GL_BONESECTOR)*n_bone);
	
	for(i=0;i<n_bone;i++)
	{	
		fread(&bone[i].n_pt,sizeof(UINT),1,fp);
		bone[i].pt=new GL_VECTOR3D[bone[i].n_pt];
		fread(bone[i].pt,sizeof(GL_VECTOR3D),bone[i].n_pt,fp);
		for(ii=0;ii<bone[i].n_pt;ii++)
			_gl_fix_ASE_v3d(&bone[i].pt[ii]);
	}

	fclose(fp);

	return 0;
}

int GL_BONE::load(const char *filename,GL_DATFILE &df)
{
	PACKFILE *pfp;
	UINT i,ii;	

	unload();

	pfp=df.lock(filename);
	if(pfp==NULL)
		return -1;
	
	pfp->read(&n_bone,sizeof(UINT));
	bone=new GL_BONESECTOR[n_bone];
	memset(bone,0,sizeof(GL_BONESECTOR)*n_bone);
	
	for(i=0;i<n_bone;i++)
	{	
		pfp->read(&bone[i].n_pt,sizeof(UINT));
		bone[i].pt=new GL_VECTOR3D[bone[i].n_pt];
		pfp->read(bone[i].pt,sizeof(GL_VECTOR3D)*bone[i].n_pt);
		for(ii=0;ii<bone[i].n_pt;ii++)
			_gl_fix_ASE_v3d(&bone[i].pt[ii]);
	}

	df.unlock();

	return 0;
}

void GL_BONE::unload()
{
	UINT i;

	if(bone)
		for(i=0;i<n_bone;i++)
		{
			delete [] bone[i].pt;
			delete [] bone[i].vtx;
			delete [] bone[i].nv;
			delete [] bone[i].bval;
		}
	delete [] bone;	
	memset(this,0,sizeof(GL_BONE));
}


void GL_BONE::attach(GL_OBJECT &obj,float r1,float r2,UINT flags)
{
	float *bval;
	UINT *bref;
	UINT n_vtx,bi,pi,ri,oi,vi,nvi;
	GL_VECTOR3D dif;
	float dist;


	for(n_vtx=0,oi=0;oi<obj.n_sobj;oi++)
		n_vtx+=obj[oi].n_vtx;

	bval=new float[n_vtx];
	bref=new UINT[n_vtx];

	for(ri=0;ri<n_vtx;ri++)
	{
		bval[ri]=r2;
		bref[ri]=0;
	}

	for(bi=0;bi<n_bone;bi++)
		for(ri=0,oi=0;oi<obj.n_sobj;oi++)
		{
			for(pi=0;pi<bone[bi].n_pt;pi++)
				for(vi=0;vi<obj[oi].n_vtx;vi++)
				{
					dif=bone[bi].pt[pi]-obj[oi].vtx[vi];
					switch(flags&3)
					{
					case GLB_XYPLANE:
						dist=sqrtf(dif.x*dif.x+dif.y*dif.y);
						break;
					case GLB_XZPLANE:
						dist=sqrtf(dif.x*dif.x+dif.z*dif.z);
						break;
					case GLB_YZPLANE:
						dist=sqrtf(dif.y*dif.y+dif.z*dif.z);
						break;
					default:
						dist=sqrtf(dif.x*dif.x+dif.y*dif.y+dif.z*dif.z);
					}

					if(dist<bval[ri+vi])
					{
						bval[ri+vi]=dist;
						bref[ri+vi]=bi;
					}
				}
			ri+=obj[oi].n_vtx;
		}
	
	for(ri=0;ri<n_vtx;ri++)
	{
		if(bval[ri]<r1)
			bval[ri]=1.0f;
		else
			if(bval[ri]<r2)
				bval[ri]=(r2-bval[ri])/(r2-r1);
			else
				bval[ri]=0.0f;
	}
	
	for(bi=0;bi<n_bone;bi++)
	{
		for(ri=0;ri<n_vtx;ri++)
			if(bref[ri]==bi&&bval[ri]>0.0f)
				bone[bi].n_vtx++;

		bone[bi].vtx=new GL_VECTOR3D*[bone[bi].n_vtx];
		bone[bi].nv=new GL_VECTOR3D*[bone[bi].n_vtx];
		bone[bi].bval=new float[bone[bi].n_vtx];
	
		for(nvi=0,ri=0,oi=0;oi<obj.n_sobj;oi++)
		{
			for(vi=0;vi<obj[oi].n_vtx;vi++)
				if(bref[ri+vi]==bi&&bval[ri+vi]>0.0f)
				{
					bone[bi].vtx[nvi]=&obj[oi].vtx[vi];
					bone[bi].nv[nvi]=&obj[oi].nv[vi];
					bone[bi].bval[nvi]=bval[ri+vi];
					nvi++;
				}
			ri+=obj[oi].n_vtx;
		}

	}

	obj.flags&=~GLO_CULLOBJ;
	
	delete [] bref;
	delete [] bval;
}


void GL_BONE::translate(UINT bi,float tx,float ty,float tz)
{
	UINT i;

	for(i=0;i<bone[bi].n_vtx;i++)	
		(*bone[bi].vtx[i])+=GL_VECTOR3D(tx,ty,tz)*bone[bi].bval[i];
}

void GL_BONE::translate(UINT bi,const GL_VECTOR3D &tv)
{
	UINT i;

	for(i=0;i<bone[bi].n_vtx;i++)	
		(*bone[bi].vtx[i])+=tv*bone[bi].bval[i];
}

void GL_BONE::xrotate(UINT bi,GL_VECTOR3D c,float r)
{
	UINT i;

	for(i=0;i<bone[bi].n_vtx;i++)	
	{
		(*bone[bi].vtx[i])-=c;
		(*bone[bi].vtx[i]).xrotate(r*bone[bi].bval[i]);
		(*bone[bi].vtx[i])+=c;
		(*bone[bi].nv[i]).xrotate(r);
	}
}

void GL_BONE::yrotate(UINT bi,GL_VECTOR3D c,float r)
{
	UINT i;

	for(i=0;i<bone[bi].n_vtx;i++)	
	{
		bone[1];
		(*bone[bi].vtx[i])-=c;
		(*bone[bi].vtx[i]).yrotate(r*bone[bi].bval[i]);
		(*bone[bi].vtx[i])+=c;
		(*bone[bi].nv[i]).yrotate(r);
	}
}

void GL_BONE::zrotate(UINT bi,GL_VECTOR3D c,float r)
{
	UINT i;

	for(i=0;i<bone[bi].n_vtx;i++)	
	{
		bone[1];
		(*bone[bi].vtx[i])-=c;
		(*bone[bi].vtx[i]).zrotate(r*bone[bi].bval[i]);
		(*bone[bi].vtx[i])+=c;
		(*bone[bi].nv[i]).zrotate(r);
	}
}



int GL_PATH::load(const char *filename)
{
	FILE *fp;
	UINT i,ii;	

	unload();

	if(glObjectFile)
		return load(filename,*glObjectFile);

	fp=fopen(filename,"rb");
	if(fp==NULL)
		return -1;

	fread(&n_path,sizeof(UINT),1,fp);
	path=new GL_PATHSECTOR[n_path];
	memset(path,0,sizeof(GL_PATHSECTOR)*n_path);
	
	for(i=0;i<n_path;i++)
	{	
		fread(&path[i].n_pt,sizeof(UINT),1,fp);
		path[i].pt=new GL_VECTOR3D[path[i].n_pt];
		fread(path[i].pt,sizeof(GL_VECTOR3D),path[i].n_pt,fp);
		for(ii=0;ii<path[i].n_pt;ii++)
			_gl_fix_ASE_v3d(&path[i].pt[ii]);
	}


	fclose(fp);

	return 0;
}

int GL_PATH::load(const char *filename,GL_DATFILE &df)
{
	PACKFILE *pfp;
	UINT i,ii;	

	unload();

	pfp=df.lock(filename);
	if(pfp==NULL)
		return -1;
	
	pfp->read(&n_path,sizeof(UINT));
	path=new GL_PATHSECTOR[n_path];
	memset(path,0,sizeof(GL_PATHSECTOR)*n_path);
	
	for(i=0;i<n_path;i++)
	{	
		pfp->read(&path[i].n_pt,sizeof(UINT));
		path[i].pt=new GL_VECTOR3D[path[i].n_pt];
		pfp->read(path[i].pt,sizeof(GL_VECTOR3D)*path[i].n_pt);
		for(ii=0;ii<path[i].n_pt;ii++)
			_gl_fix_ASE_v3d(&path[i].pt[ii]);
	}

	df.unlock();

	return 0;
}

void GL_PATH::unload()
{
	UINT i;

	if(path)
		for(i=0;i<n_path;i++)
			delete [] path[i].pt;
	delete [] path;	
	memset(this,0,sizeof(GL_PATH));
}

void GL_PATH::travel(UINT pi,float v)
{
	path[pi].c_pt+=v;
	if(path[pi].c_pt>float(path[pi].n_pt-1))
		path[pi].c_pt=float(path[pi].n_pt-1);
	if(path[pi].c_pt<0.0f)
		path[pi].c_pt=0.0f;
}

GL_VECTOR3D GL_PATH::getpos(UINT pi)
{
	UINT p1,p2;

	p1=UINT(path[pi].c_pt);
	p2=p1+1;
	if(p2>(path[pi].n_pt-1))
		return path[pi].pt[p1];
	else
		return path[pi].pt[p1]+(path[pi].pt[p2]-path[pi].pt[p1])*(path[pi].c_pt-float(p1));
}
