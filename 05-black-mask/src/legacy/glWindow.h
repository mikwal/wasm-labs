#ifndef GLWINDOW_H
#define GLWINDOW_H

#define GL_EXIT(msg) glDestroyWnd(msg)

#define GL_WCLASSNAME "OpenGL"

#define GLW_ACTIVE		0x000100
#define GLW_FULLSCREEN	0x000001

extern HWND glWnd;
extern HINSTANCE glInstance;
extern HDC glDC;
extern HGLRC glRC;

extern UINT glWWidth;
extern UINT glWHeight;
extern UINT glWBpp;
extern UINT glWFlags;

extern UCHAR glKey[256];

int glCreateWnd(const char* ,UINT ,UINT ,UINT ,UINT);
int glDestroyWnd(const char *msg=NULL);
// inline void glClear() {glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);}
// inline void glSwapBuf() { SwapBuffers(glDC); }
LRESULT CALLBACK glWndProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam);


#endif
