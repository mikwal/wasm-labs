/*****************************************/
/***   LZSS compression file format    ***/
/*****************************************/

#ifndef PACKFILE_H
#define PACKFILE_H

#include <stdio.h>
#include <string.h>

#define PF_N 4096
#define PF_F 18
#define PF_THRESHOLD 2

class PACKFILE {
private:
	FILE *_file;
	unsigned long _textsize, _codesize;
	unsigned char _mode,_pos;
	int _lson[PF_N+1];
	int _rson[PF_N+257];
	int _dad[PF_N+1];
	int	_match_position, _match_length, _len;
	int _i, _j, _c, _k, _r, _s;
	unsigned int _flags;
	int _last_match_length, _code_buf_ptr;
	unsigned char _code_buf[17], _mask, _text_buf[PF_N+PF_F-1];
	bool _eof,_vfile;

	void _init_tree();
	void _insert_node(int r);
	void _delete_node(int p);

public:
	inline PACKFILE() {memset(this,0,sizeof(PACKFILE));}
	inline PACKFILE(const char *filename,char mode) {memset(this,0,sizeof(PACKFILE)); open(filename,mode);}
	inline PACKFILE(FILE *parent,char mode) {memset(this,0,sizeof(PACKFILE)); open(parent,mode);}
	inline ~PACKFILE() {close();}

	int open(const char *,char );
	int open(FILE *,char );
	void close();

	int get();
	void put(int );
	size_t read(void *,size_t );
	size_t write(const void *,size_t );

	inline bool eof() {return _eof;}
	inline bool good() {return (_file==NULL)?false:true;}
	inline unsigned long size(bool packed) {return packed?_codesize:_textsize;}
};

#endif