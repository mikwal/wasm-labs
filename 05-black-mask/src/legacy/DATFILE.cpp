/*****************************************/
/***    MDF Dat Archive fileformat     ***/
/*****************************************/

#include "DATFILE.h"


int DATFILE::open(const char *filename,bool _load,void (*_loadproc)(DATOBJ *,PACKFILE *),void (*_freeproc)(DATOBJ *))
{
	unsigned long id;
	unsigned short n;
	unsigned char sl;
	int i;

	
	close();

	if((_file=fopen(filename,"rb"))==NULL)
		return -1;

	fread(&id,sizeof(unsigned long),1,_file);
	fread(&n,sizeof(unsigned short),1,_file);
	if(id!=*((unsigned long*)"MDF#")||n==0)
	{
		fclose(_file);
		_file=NULL;
		return 1;
	}

	_obj=new DATOBJ[n];

	for(i=0;i<n;i++)
	{
		fread(&_obj[i].offset,sizeof(unsigned long),1,_file);
		fread(&_obj[i].size,sizeof(unsigned long),1,_file);
		_obj[i].data=NULL;
	}

	for(i=0;i<n;i++)
	{
		sl=getc(_file);
		_obj[i].name=new char[sl+1];
		fread(_obj[i].name,sl,1,_file);
		_obj[i].name[sl]=0;
	}
	_nobj=n;
	_cobj=-1;
	
	if(_loadproc)
		loadproc=_loadproc;
	else
		loadproc=DefaultDFLoadProc;

	if(_freeproc)
		freeproc=_freeproc;
	else
		freeproc=DefaultDFFreeProc;

	if(_load)
		for(i=0;i<n;i++)
			load(i);

	return 0;
}

void DATFILE::close()
{
	int i;

	if(good())
	{
		if(_cobj!=-1)
			unlock();
		for(i=0;i<_nobj;i++)
		{
			unload(i);
			delete [] _obj[i].name;
		}
		delete [] _obj;
		fclose(_file);
		memset(this,0,sizeof(DATFILE));
	}
}

int DATFILE::index(const char *name)
{
	if(!good())
		return -1;

	for(int i=0;i<_nobj;i++)
		if(strcmpi(name,_obj[i].name)==0)
			return i;

	return -1;
}

PACKFILE *DATFILE::lock(const int i)
{
	if(!good())
		return NULL;

	if(_cobj!=-1)
		if(_cobj==i)
			return &_packfile;
		else
			return NULL;

	if(i>=0&&i<_nobj)
	{
		fseek(_file,_obj[i].offset,SEEK_SET);
		if(_packfile.open(_file,'r'))
			return NULL;
		else
		{
			_cobj=i;
			return &_packfile;
		}
	}
	else
		return NULL;
}

int DATFILE::unlock()
{
	if(_cobj!=-1)
	{
		_cobj=-1;
		_packfile.close();
		return 0;
	}
	else
		return 1;
}

void *DATFILE::load(const int i)
{
	PACKFILE *pf;

	if(!good())
		return NULL;

	if(i>=0&&i<_nobj)
	{
		if(_obj[i].data)
			return _obj[i].data;

		if(_obj[i].size==0)
			return NULL;
		
		pf=lock(i);
		if(!pf)
			return NULL;	
		loadproc(&_obj[i],pf);
		unlock();

		return &_obj[i].data;
	}
	else
		return NULL;
}

void DATFILE::unload(const int i)
{
	if(i>=0&&i<_nobj)
		if(_obj[i].data)
			freeproc(&_obj[i]);
}

void DefaultDFLoadProc(DATOBJ *o,PACKFILE *pf)
{
	o->data=new unsigned char[o->size];
	pf->read((void *)o->data,o->size);
}

void DefaultDFFreeProc(DATOBJ *o)
{
	delete [] o->data;
}
