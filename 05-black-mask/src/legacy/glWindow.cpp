#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "glEnv.h"
#include "glWindow.h"

HWND glWnd=NULL;
HINSTANCE glInstance=NULL;
HDC glDC=NULL;
HGLRC glRC=NULL;

UCHAR glKey[256];

UINT glWWidth;
UINT glWHeight;
UINT glWBpp;
UINT glWFlags;

DEVMODE _dmode;


int glCreateWnd(const char* title,UINT width,UINT height,UINT bpp,UINT flags)
{
	WNDCLASS wclass;
	DWORD wstyle;
	DWORD wstylex;
	RECT wrect;
	PIXELFORMATDESCRIPTOR pfd;
	int pf;

	glWWidth=width;
	glWHeight=height;
	glWBpp=bpp;
	glWFlags=flags;

// 	glInstance=GetModuleHandle(NULL);

// 	wclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
// 	wclass.lpfnWndProc=(WNDPROC)glWndProc;
// 	wclass.cbClsExtra=0;
// 	wclass.cbWndExtra=0;
// 	wclass.hInstance=glInstance;
// 	wclass.hIcon=LoadIcon(NULL,IDI_WINLOGO);
// 	wclass.hCursor=LoadCursor(NULL,IDC_ARROW);
// 	wclass.hbrBackground=NULL;
// 	wclass.lpszMenuName=NULL;
// 	wclass.lpszClassName=GL_WCLASSNAME;

// 	if(!RegisterClass(&wclass))
// 		return GL_EXIT("Failed To Register The Window Class.");
	
// 	if(flags&GLW_FULLSCREEN)
// 	{
// 		memset(&_dmode,0,sizeof(_dmode));
// 		_dmode.dmSize=sizeof(_dmode);
// 		_dmode.dmPelsWidth=width;
// 		_dmode.dmPelsHeight=height;
// 		_dmode.dmBitsPerPel=bpp;
// 		_dmode.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

// 		if(ChangeDisplaySettings(&_dmode,CDS_FULLSCREEN))
// 			return GL_EXIT("The Requested Fullscreen Mode Is Not Supported");

// 		wstylex=WS_EX_APPWINDOW;
// 		wstyle=WS_POPUP|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;
// 	}
// 	else
// 	{
// 		wstylex=WS_EX_APPWINDOW|WS_EX_WINDOWEDGE;
// 		wstyle=WS_OVERLAPPEDWINDOW|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;
// 	}

// 	SetRect(&wrect,0,0,width,height);
// 	AdjustWindowRectEx(&wrect,wstyle,0,wstylex);

// 	glWnd=CreateWindowEx(
// 		wstylex,GL_WCLASSNAME,title,wstyle,
// 		CW_USEDEFAULT,CW_USEDEFAULT,
// 		wrect.right-wrect.left,wrect.bottom-wrect.top,
// 		NULL,NULL,glInstance,NULL);

// 	if(glWnd==NULL)
// 		return GL_EXIT("Cant Create Window.");
 
// 	glDC=GetDC(glWnd);
// 	if(glDC==NULL)
// 		return GL_EXIT("Can't Create A GL Device Context.");
	
// 	memset(&pfd,0,sizeof(pfd));
//     pfd.nSize=sizeof(pfd); 
//     pfd.nVersion=1; 
//     pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
// 	pfd.iPixelType=PFD_TYPE_RGBA;
//     pfd.dwLayerMask=PFD_MAIN_PLANE; 
//     pfd.cColorBits=bpp; 
//     pfd.cDepthBits=16; 
	
// 	pf=ChoosePixelFormat(glDC,&pfd);
// 	if(pf==0)
// 		return GL_EXIT("Can't Find A Suitable PixelFormat.");

// 	if(SetPixelFormat(glDC,pf,&pfd)==0)
// 		return GL_EXIT("Can't Set The PixelFormat.");

// 	glRC=wglCreateContext(glDC);
// 	if(glRC==NULL)
// 		return GL_EXIT("Can't Create A GL Rendering Context.");

// 	if(wglMakeCurrent(glDC,glRC)==0)
// 		return GL_EXIT("Can't Activate The GL Rendering Context.");

// 	ShowWindow(glWnd,SW_SHOW);
// 	SetForegroundWindow(glWnd);
// 	SetFocus(glWnd);

	glWFlags|=GLW_ACTIVE;
	
	return 0;
}

int glDestroyWnd(const char *msg)
{
// 	if(glRC)
// 	{
// 		wglMakeCurrent(NULL,NULL);
// 		wglDeleteContext(glRC);
// 		glRC=NULL;
// 	}

// 	if(glDC)
// 	{
// 		ReleaseDC(glWnd,glDC);
// 		glDC=NULL;
// 	}

// 	if(glWnd)
// 	{
// 		ReleaseCapture();
// 		DestroyWindow(glWnd);
// 		glWnd=NULL;
// 	}

// 	UnregisterClass(GL_WCLASSNAME,glInstance);

// 	if(glWFlags&GLW_FULLSCREEN)
// 		ChangeDisplaySettings(NULL,0);

// 	glWWidth=0;
// 	glWHeight=0;
// 	glWBpp=0;
// 	glWFlags=0;
// 	glInstance=NULL;

// 	if(msg)
// 	{
// 		MessageBox(NULL,msg,"ERROR",MB_OK|MB_ICONEXCLAMATION);
// 		return -1;
// 	}
// 	else
		return 0;
}


LRESULT CALLBACK glWndProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)
{
	switch (msg)
	{
// 	case WM_ACTIVATE:
// 		if(HIWORD(wparam))
// 		{
// 			glWFlags&=~GLW_ACTIVE;
// 			if(glWFlags&GLW_FULLSCREEN)
// 				ChangeDisplaySettings(NULL,0);
// 		}	
// 		else
// 		{
// 			glWFlags|=GLW_ACTIVE;
// 			if(glWFlags&GLW_FULLSCREEN)
// 				if(ChangeDisplaySettings(&_dmode,CDS_FULLSCREEN))
// 					return GL_EXIT("The Requested Fullscreen Mode Is Not Supported");
// 		}
// 		break;

// 	case WM_SYSCOMMAND:
// 		switch (wparam)	
// 		{
// 		case SC_SCREENSAVE:
// 		case SC_MONITORPOWER:
// 			return 0;
// 		}
// 		break;

// 	case WM_KEYDOWN:
// 		glKey[wparam]=0x01;				
// 		break;

// 	case WM_KEYUP:
// 		glKey[wparam]=0x00;
// 		break;

// 	case WM_CLOSE:
// 		return 1;

	case WM_SIZE:
		glCam.vp_x=0;
		glCam.vp_y=0;
		glCam.vp_w=glWWidth=LOWORD(lparam);
		glCam.vp_h=glWHeight=HIWORD(lparam);
		glCam.updview();
		break;
	}

 	return DefWindowProc(hwnd,msg,wparam,lparam);
}
