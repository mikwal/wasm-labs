#ifndef GLOBJECT_H
#define GLOBJECT_H


#include "glTexture.h"
typedef DATFILE GL_DATFILE;


struct GL_FACE {
	GL_VERTEX *vtx[3];
	GL_VECTOR3D *nv[3];
	GL_TEXCOORD *map[3];
	GL_RGBACOLOR *col[3];
};

struct GL_MATERIAL {
	char name[32];
	UINT text;
	GL_RGBACOLOR amb;
	GL_RGBACOLOR dif;
	GL_RGBACOLOR spec;
	GL_RGBACOLOR self;
	float exp;
};

struct GL_SUBOBJECT {
	char name[32];
	GL_VECTOR3D pos;
	GL_VECTOR3D rot;
	GL_MATERIAL *mat;

	//GL_VECTOR3D bbox[8];
	GL_VECTOR3D bv_center;
	float bv_radius;

	UINT n_vtx;
	UINT n_map;
	UINT n_face;
	GL_VERTEX *vtx;
	GL_VECTOR3D *nv;
	GL_RGBACOLOR *col;
	GL_TEXCOORD *map;
	GL_FACE *face;
};

extern GL_DATFILE *glObjectFile;
extern GL_TEXTUREFILE *glTextureFile;
inline void glSetObjectFile(GL_DATFILE *df) {glObjectFile=df;}
inline void glSetTextureFile(GL_TEXTUREFILE *tf) {glTextureFile=tf;}


#define GLO_CULLOBJ 1

class GL_OBJECT {
private:
	void _loadsub(FILE *fp,GL_SUBOBJECT *);
	void _loadsub(PACKFILE *pfp,GL_SUBOBJECT *);
	void _calcboxsub(GL_SUBOBJECT *);
	void _rendersub(GL_SUBOBJECT *);
	
public:
	char name[32];
	UINT flags;
	GL_VECTOR3D pos;
	GL_VECTOR3D rot;
	GL_VECTOR3D scl;

	UINT n_mat;
	UINT n_sobj;
	GL_MATERIAL *mat;
	GL_SUBOBJECT *sobj;
	
	inline GL_OBJECT() {memset(this,0,sizeof(GL_OBJECT));}
	inline GL_OBJECT(const char* filename) {memset(this,0,sizeof(GL_OBJECT));load(filename);}
	inline GL_OBJECT(const char* filename,GL_DATFILE &df) {memset(this,0,sizeof(GL_OBJECT));load(filename,df);}
	inline ~GL_OBJECT() {unload();}
	int load(const char*);
	int load(const char* ,GL_DATFILE &);
	void unload();
	void render(UINT s,UINT n=1);

	inline GL_SUBOBJECT &operator[](const int i) {return sobj[i];}
};



#define GLB_XYPLANE		1
#define GLB_XZPLANE		2
#define GLB_YZPLANE		3

struct GL_BONESECTOR {
	UINT n_pt;
	GL_VECTOR3D *pt;
	
	UINT n_vtx;
	GL_VECTOR3D **vtx;
	GL_VECTOR3D **nv;
	float *bval;
};

class GL_BONE {
public:
	UINT n_bone;
	GL_BONESECTOR *bone;

	inline GL_BONE() {memset(this,0,sizeof(GL_BONE));}
	inline GL_BONE(const char* filename) {memset(this,0,sizeof(GL_BONE));load(filename);}
	inline GL_BONE(const char* filename,GL_DATFILE &df) {memset(this,0,sizeof(GL_BONE));load(filename,df);}
	inline ~GL_BONE() {unload();}
	int load(const char*);
	int load(const char* ,GL_DATFILE &);
	void unload();
	
	void attach(GL_OBJECT &,float ,float ,UINT );

	void translate(UINT ,float ,float ,float );
	void translate(UINT ,const GL_VECTOR3D &);
	
	void xrotate(UINT ,GL_VECTOR3D ,float );
	inline void xrotate_first(UINT bi,float r) {xrotate(bi,bone[bi].pt[0],r);}
	inline void xrotate_last(UINT bi,float r) {xrotate(bi,bone[bi].pt[bone[bi].n_pt-1],r);}
	void yrotate(UINT ,GL_VECTOR3D ,float );
	inline void yrotate_first(UINT bi,float r) {yrotate(bi,bone[bi].pt[0],r);}
	inline void yrotate_last(UINT bi,float r) {yrotate(bi,bone[bi].pt[bone[bi].n_pt-1],r);}
	void zrotate(UINT ,GL_VECTOR3D ,float );
	inline void zrotate_first(UINT bi,float r) {zrotate(bi,bone[bi].pt[0],r);}
	inline void zrotate_last(UINT bi,float r) {zrotate(bi,bone[bi].pt[bone[bi].n_pt-1],r);}

};


struct GL_PATHSECTOR {
	UINT n_pt;
	float c_pt;
	GL_VECTOR3D *pt;
};

class GL_PATH {
public:
	UINT n_path;
	GL_PATHSECTOR *path;

	inline GL_PATH() {memset(this,0,sizeof(GL_PATH));}
	inline GL_PATH(const char* filename) {memset(this,0,sizeof(GL_PATH));load(filename);}
	inline GL_PATH(const char* filename,GL_DATFILE &df) {memset(this,0,sizeof(GL_PATH));load(filename,df);}
	inline ~GL_PATH() {unload();}

	int load(const char*);
	int load(const char* ,GL_DATFILE &);
	void unload();
	
	inline void reset(UINT pi) {path[pi].c_pt=0.0f;}
	void travel(UINT ,float );
	GL_VECTOR3D getpos(UINT );
};

#endif