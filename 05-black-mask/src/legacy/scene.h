#ifndef SCENE_H
#define SCENE_H

struct GL_DEMOSCENE {
	void (*load)();
	void (*free)();
	void (*init)();
	bool (*move)();
	void (*render)();
};

void s0load();
void s0free();
void s0init();
bool s0move();
void s0render();

void s1load();
void s1free();
void s1init();
bool s1move();
void s1render();

void s2load();
void s2free();
void s2init();
bool s2move();
void s2render();

void s3load();
void s3free();
void s3init();
bool s3move();
void s3render();

void s4load();
void s4free();
void s4init();
bool s4move();
void s4render();



void movecam();

#endif