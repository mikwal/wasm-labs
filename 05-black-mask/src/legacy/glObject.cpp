#include <windows.h>
#include <stdio.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "glEnv.h"
#include "glObject.h"


float _gldefmaterial[]={0,0,0,0,0,0,0,0,0,0.2f,0.2f,0.2f,1.0f,0.8f,0.8f,0.8f,1.0f,0,0,0,1.0f,0,0,0,1.0f,0};


DATFILE *glObjectFile=NULL;
GL_TEXTUREFILE *glTextureFile=NULL;

struct _glface {
	UINT vtx[3];
	UINT map[3];
	UINT col[3];
};


#define GL_ASE_XYZSCALE 0.1f
void _gl_fix_ASE_v3d(GL_VECTOR3D *v)
{
	float x,y,z;
	
	x=v->x;
	y=v->y;
	z=v->z;

	v->x=x*GL_ASE_XYZSCALE;
	v->y=z*GL_ASE_XYZSCALE;
	v->z=-y*GL_ASE_XYZSCALE;
}
#undef GL_ASE_XYZSCALE


void GL_OBJECT::_loadsub(FILE *fp,GL_SUBOBJECT *so)
{
	GL_RGBACOLOR *col;
	GL_VECTOR3D nv;
	_glface _face;	
	UINT n_col,_mat,i;

	memset(so,0,sizeof(GL_SUBOBJECT));

	fread(&so->name,sizeof(char)*32,1,fp);
	fread(&so->pos,sizeof(GL_VECTOR3D),1,fp);
	_gl_fix_ASE_v3d(&so->pos);
	fread(&_mat,sizeof(UINT),1,fp);
	if(_mat)
		so->mat=&mat[_mat-1];
	fread(&so->n_vtx,sizeof(UINT),1,fp);
	fread(&so->n_map,sizeof(UINT),1,fp);
	fread(&n_col,sizeof(UINT),1,fp);
	fread(&so->n_face,sizeof(UINT),1,fp);	
	
	so->vtx=new GL_VERTEX[so->n_vtx];
	fread(so->vtx,sizeof(GL_VERTEX),so->n_vtx,fp);
	for(i=0;i<so->n_vtx;i++)
		_gl_fix_ASE_v3d(&so->vtx[i]);

	so->nv=new GL_VECTOR3D[so->n_vtx];
	memset(so->nv,0,sizeof(GL_VECTOR3D)*so->n_vtx);
	
	if(so->n_map)
	{
		so->map=new GL_TEXCOORD[so->n_map];	
		fread(so->map,sizeof(GL_TEXCOORD),so->n_map,fp);
	}

	if(n_col)
	{
		col=new GL_RGBACOLOR[n_col];
		fread(col,sizeof(GL_RGBACOLOR),n_col,fp);
		so->col=new GL_RGBACOLOR[so->n_vtx];
	}

	so->face=new GL_FACE[so->n_face];
	for(i=0;i<so->n_face;i++)
	{
		memset(&so->face[i],0,sizeof(GL_FACE));
		fread(&_face,sizeof(_glface),1,fp);
		so->face[i].vtx[0]=&so->vtx[_face.vtx[0]];
		so->face[i].vtx[1]=&so->vtx[_face.vtx[1]];
		so->face[i].vtx[2]=&so->vtx[_face.vtx[2]];
		so->face[i].nv[0]=&so->nv[_face.vtx[0]];
		so->face[i].nv[1]=&so->nv[_face.vtx[1]];
		so->face[i].nv[2]=&so->nv[_face.vtx[2]];
		if(n_col)
		{
			memcpy(&so->col[_face.vtx[0]],&col[_face.col[0]],sizeof(GL_RGBACOLOR));
			memcpy(&so->col[_face.vtx[1]],&col[_face.col[1]],sizeof(GL_RGBACOLOR));
			memcpy(&so->col[_face.vtx[2]],&col[_face.col[2]],sizeof(GL_RGBACOLOR));
			so->face[i].col[0]=&so->col[_face.vtx[0]];
			so->face[i].col[1]=&so->col[_face.vtx[1]];
			so->face[i].col[2]=&so->col[_face.vtx[2]];
		}
		if(so->n_map)
		{
			so->face[i].map[0]=&so->map[_face.map[0]];
			so->face[i].map[1]=&so->map[_face.map[1]];
			so->face[i].map[2]=&so->map[_face.map[2]];
		}
		nv=(so->vtx[_face.vtx[1]]-so->vtx[_face.vtx[0]])^(so->vtx[_face.vtx[2]]-so->vtx[_face.vtx[0]]);
		nv/=!nv;
		so->nv[_face.vtx[0]]+=nv;
		so->nv[_face.vtx[1]]+=nv;
		so->nv[_face.vtx[2]]+=nv;
	}

	for(i=0;i<so->n_vtx;i++)
		so->nv[i]/=!so->nv[i];

	if(n_col)
		delete [] col;
}


void GL_OBJECT::_loadsub(PACKFILE *pfp,GL_SUBOBJECT *so)
{
	GL_RGBACOLOR *col;
	GL_VECTOR3D nv;
	_glface _face;	
	UINT n_col,_mat,i;

	memset(so,0,sizeof(GL_SUBOBJECT));

	pfp->read(&so->name,sizeof(char)*32);
	pfp->read(&so->pos,sizeof(GL_VECTOR3D));
	_gl_fix_ASE_v3d(&so->pos);
	pfp->read(&_mat,sizeof(UINT));
	if(_mat)
		so->mat=&mat[_mat-1];
	pfp->read(&so->n_vtx,sizeof(UINT));
	pfp->read(&so->n_map,sizeof(UINT));
	pfp->read(&n_col,sizeof(UINT));
	pfp->read(&so->n_face,sizeof(UINT));	
	
	so->vtx=new GL_VERTEX[so->n_vtx];
	pfp->read(so->vtx,sizeof(GL_VERTEX)*so->n_vtx);
	for(i=0;i<so->n_vtx;i++)
		_gl_fix_ASE_v3d(&so->vtx[i]);

	so->nv=new GL_VECTOR3D[so->n_vtx];
	memset(so->nv,0,sizeof(GL_VECTOR3D)*so->n_vtx);
	
	if(so->n_map)
	{
		so->map=new GL_TEXCOORD[so->n_map];	
		pfp->read(so->map,sizeof(GL_TEXCOORD)*so->n_map);
	}

	if(n_col)
	{
		col=new GL_RGBACOLOR[n_col];
		pfp->read(col,sizeof(GL_RGBACOLOR)*n_col);
		so->col=new GL_RGBACOLOR[so->n_vtx];
	}

	so->face=new GL_FACE[so->n_face];
	for(i=0;i<so->n_face;i++)
	{
		memset(&so->face[i],0,sizeof(GL_FACE));
		pfp->read(&_face,sizeof(_glface));
		so->face[i].vtx[0]=&so->vtx[_face.vtx[0]];
		so->face[i].vtx[1]=&so->vtx[_face.vtx[1]];
		so->face[i].vtx[2]=&so->vtx[_face.vtx[2]];
		so->face[i].nv[0]=&so->nv[_face.vtx[0]];
		so->face[i].nv[1]=&so->nv[_face.vtx[1]];
		so->face[i].nv[2]=&so->nv[_face.vtx[2]];
		if(n_col)
		{
			memcpy(&so->col[_face.vtx[0]],&col[_face.col[0]],sizeof(GL_RGBACOLOR));
			memcpy(&so->col[_face.vtx[1]],&col[_face.col[1]],sizeof(GL_RGBACOLOR));
			memcpy(&so->col[_face.vtx[2]],&col[_face.col[2]],sizeof(GL_RGBACOLOR));
			so->face[i].col[0]=&so->col[_face.vtx[0]];
			so->face[i].col[1]=&so->col[_face.vtx[1]];
			so->face[i].col[2]=&so->col[_face.vtx[2]];
		}
		if(so->n_map)
		{
			so->face[i].map[0]=&so->map[_face.map[0]];
			so->face[i].map[1]=&so->map[_face.map[1]];
			so->face[i].map[2]=&so->map[_face.map[2]];
		}
		nv=(so->vtx[_face.vtx[1]]-so->vtx[_face.vtx[0]])^(so->vtx[_face.vtx[2]]-so->vtx[_face.vtx[0]]);
		nv/=!nv;
		so->nv[_face.vtx[0]]+=nv;
		so->nv[_face.vtx[1]]+=nv;
		so->nv[_face.vtx[2]]+=nv;
	}

	for(i=0;i<so->n_vtx;i++)
		so->nv[i]/=!so->nv[i];

	if(n_col)
		delete [] col;
}


void GL_OBJECT::_calcboxsub(GL_SUBOBJECT *so)
{
	GL_BOX3D box;
	UINT i;

	box.p1=box.p2=so->vtx[0];

	for(i=1;i<so->n_vtx;i++)
	{
		if(so->vtx[i].x<box.p1.x)
			box.p1.x=so->vtx[i].x;

		if(so->vtx[i].y<box.p1.y)
			box.p1.y=so->vtx[i].y;

		if(so->vtx[i].z<box.p1.z)
			box.p1.z=so->vtx[i].z;

		if(so->vtx[i].x>box.p2.x)
			box.p2.x=so->vtx[i].x;

		if(so->vtx[i].y>box.p2.y)
			box.p2.y=so->vtx[i].y;

		if(so->vtx[i].z>box.p2.z)
			box.p2.z=so->vtx[i].z;
	}

	so->bv_center=(box.p1+box.p2)/2.0f;
	so->bv_radius=!(box.p2-so->bv_center);

/*	so->bbox[0].x=box.p1.x;
	so->bbox[0].y=box.p1.y;
	so->bbox[0].z=box.p1.z;

	so->bbox[1].x=box.p2.x;
	so->bbox[1].y=box.p1.y;
	so->bbox[1].z=box.p1.z;

	so->bbox[2].x=box.p2.x;
	so->bbox[2].y=box.p1.y;
	so->bbox[2].z=box.p2.z;

	so->bbox[3].x=box.p1.x;
	so->bbox[3].y=box.p1.y;
	so->bbox[3].z=box.p2.z;

	so->bbox[4].x=box.p1.x;
	so->bbox[4].y=box.p2.y;
	so->bbox[4].z=box.p1.z;

	so->bbox[5].x=box.p2.x;
	so->bbox[5].y=box.p2.y;
	so->bbox[5].z=box.p1.z;

	so->bbox[6].x=box.p2.x;
	so->bbox[6].y=box.p2.y;
	so->bbox[6].z=box.p2.z;

	so->bbox[7].x=box.p1.x;
	so->bbox[7].y=box.p2.y;
	so->bbox[7].z=box.p2.z;*/
}


void GL_OBJECT::_rendersub(GL_SUBOBJECT *so)
{
	GL_MATERIAL *_mat=(GL_MATERIAL*)_gldefmaterial;
	bool texture_enabled=false;
	UINT cface=GL_FRONT_AND_BACK,i;	
	float mtx[16],d;
	GL_VECTOR3D p;


	glPushMatrix();
	if(so->pos.x!=0.0f||so->pos.y!=0.0f||so->pos.z!=0.0f)
		glTranslatef(so->pos.x,so->pos.y,so->pos.z);
	if(so->rot.x!=0.0f)
		glRotatef(so->rot.x,1.0f,0.0f,0.0f);
	if(so->rot.z!=0.0f)
		glRotatef(so->rot.z,0.0f,0.0f,1.0f);
	if(so->rot.y!=0.0f)
		glRotatef(so->rot.y,0.0f,1.0f,0.0f);


	if(flags&GLO_CULLOBJ)
	{
		glGetFloatv(GL_MODELVIEW_MATRIX,mtx);
		p=so->bv_center*mtx;

		d=-((-p.z)-glCam.zs);
		if(d>so->bv_radius){glPopMatrix();return;}
		d=-(p.z+glCam.ze);
		if(d>so->bv_radius){glPopMatrix();return;}
		
		d=glCam._nx_x*p.x+glCam._nx_z*p.z;
		if(d>so->bv_radius){glPopMatrix();return;}
		d=(-glCam._nx_x)*p.x+glCam._nx_z*p.z;
		if(d>so->bv_radius){glPopMatrix();return;}
		
		d=glCam._ny_y*p.y+glCam._ny_z*p.z;
		if(d>so->bv_radius){glPopMatrix();return;}
		d=(-glCam._ny_y)*p.y+glCam._ny_z*p.z;
		if(d>so->bv_radius){glPopMatrix();return;}
	}

	if(so->mat)
		_mat=so->mat;

	if(glIsLightOn())
	{
		if(glIsCFaceOn())
			cface=GL_FRONT;
		glMaterialfv(cface,GL_AMBIENT,(float*)&_mat->amb);
		glMaterialfv(cface,GL_DIFFUSE,(float*)&_mat->dif);
		glMaterialfv(cface,GL_SPECULAR,(float*)&_mat->spec);
		glMaterialfv(cface,GL_EMISSION,(float*)&_mat->self);
		glMaterialfv(cface,GL_SHININESS,(float*)&_mat->exp);
	}
	else
		if(so->col==NULL)
			glColor4fv((float*)&_mat->dif);

	if(_mat->text)
		glBindTexture(GL_TEXTURE_2D,_mat->text);
	else
		if(glIsTextureOn())
		{
			glTextureOff();
			texture_enabled=true;
		}
				
	if(so->map)
	{
		if(so->col&&!glIsLightOn())
		{
			glBegin(GL_TRIANGLES);
			for(i=0;i<so->n_face;i++)
			{	
				glColor4fv((float*)so->face[i].col[0]);
				glTexCoord2fv((float*)so->face[i].map[0]);		
				glVertex3fv((float*)so->face[i].vtx[0]);
				glColor4fv((float*)so->face[i].col[1]);
				glTexCoord2fv((float*)so->face[i].map[1]);		
				glVertex3fv((float*)so->face[i].vtx[1]);
				glColor4fv((float*)so->face[i].col[2]);
				glTexCoord2fv((float*)so->face[i].map[2]);		
				glVertex3fv((float*)so->face[i].vtx[2]);	
			}
			glEnd();
		}
		else
		{
			glBegin(GL_TRIANGLES);
			for(i=0;i<so->n_face;i++)
			{	
				glNormal3fv((float*)so->face[i].nv[0]);
				glTexCoord2fv((float*)so->face[i].map[0]);
				glVertex3fv((float*)so->face[i].vtx[0]);
				glNormal3fv((float*)so->face[i].nv[1]);
				glTexCoord2fv((float*)so->face[i].map[1]);
				glVertex3fv((float*)so->face[i].vtx[1]);
				glNormal3fv((float*)so->face[i].nv[2]);
				glTexCoord2fv((float*)so->face[i].map[2]);
				glVertex3fv((float*)so->face[i].vtx[2]);		
			}
			glEnd();
		}
	}
	else
	{
		if(so->col&&!glIsLightOn())
		{
			glBegin(GL_TRIANGLES);
			for(i=0;i<so->n_face;i++)
			{		
				glColor4fv((float*)so->face[i].col[0]);	
				glVertex3fv((float*)so->face[i].vtx[0]);
				glColor4fv((float*)so->face[i].col[1]);
				glVertex3fv((float*)so->face[i].vtx[1]);
				glColor4fv((float*)so->face[i].col[2]);
				glVertex3fv((float*)so->face[i].vtx[2]);	
			}
			glEnd();
		}
		else
		{
			glBegin(GL_TRIANGLES);
			for(i=0;i<so->n_face;i++)
			{			
				glNormal3fv((float*)so->face[i].nv[0]);	
				glVertex3fv((float*)so->face[i].vtx[0]);
				glNormal3fv((float*)so->face[i].nv[1]);
				glVertex3fv((float*)so->face[i].vtx[1]);
				glNormal3fv((float*)so->face[i].nv[2]);
				glVertex3fv((float*)so->face[i].vtx[2]);
			}
			glEnd();
		}
	}

	if(texture_enabled)
		glTextureOn();
	glPopMatrix();
}

struct _glmaterial {
	char name[32];
	char text[32];
	GL_RGBACOLOR amb;
	GL_RGBACOLOR dif;
	GL_RGBACOLOR spec;
	GL_RGBACOLOR self;
	float exp;
};

int GL_OBJECT::load(const char *filename)
{
	_glmaterial _mat;
	FILE *fp;
	ULONG id;
	UINT i;	

	unload();

	if(glObjectFile)
		return load(filename,*glObjectFile);

	fp=fopen(filename,"rb");
	if(fp==NULL)
		return -1;

	fread(&id,sizeof(long),1,fp);
	fread(name,sizeof(char)*32,1,fp);
	fread(&n_mat,sizeof(UINT),1,fp);
	fread(&n_sobj,sizeof(UINT),1,fp);
	if(id!=*((ULONG*)"GLO#")||n_sobj==0)
	{
		fclose(fp);
		return 1;
	}
	
	if(n_mat)
	{
		mat=new GL_MATERIAL[n_mat];
		for(i=0;i<n_mat;i++)
		{			
			fread(&_mat,sizeof(_glmaterial),1,fp);
			memcpy(mat[i].name,_mat.name,sizeof(char)*32);
			mat[i].text=0;
			if(_mat.text[0])
			{
				if(glTextureFile)
					mat[i].text=glTextureFile->obj(_mat.text);
				else
					glLoadTexture(&mat[i].text,_mat.text);
			}	
			memcpy(&mat[i].amb,&_mat.amb,sizeof(GL_RGBACOLOR));
			memcpy(&mat[i].dif,&_mat.dif,sizeof(GL_RGBACOLOR));
			memcpy(&mat[i].spec,&_mat.spec,sizeof(GL_RGBACOLOR));
			memcpy(&mat[i].self,&_mat.self,sizeof(GL_RGBACOLOR));
			mat[i].exp=_mat.exp;
		}
	}

	sobj=new GL_SUBOBJECT[n_sobj];

	flags|=GLO_CULLOBJ;
	for(i=0;i<n_sobj;i++)
	{
		_loadsub(fp,&sobj[i]);
		_calcboxsub(&sobj[i]);
	}
	
	fclose(fp);

	return 0;
}

int GL_OBJECT::load(const char *filename,GL_DATFILE &df)
{
	_glmaterial _mat;
	PACKFILE *pfp;
	ULONG id;
	UINT i;	

	unload();

	pfp=df.lock(filename);
	if(pfp==NULL)
		return -1;

	pfp->read(&id,sizeof(long));
	pfp->read(name,sizeof(char)*32);
	pfp->read(&n_mat,sizeof(UINT));
	pfp->read(&n_sobj,sizeof(UINT));
	if(id!=*((ULONG*)"GLO#")||n_sobj==0)
	{
		df.unlock();
		return 1;
	}
	
	if(n_mat)
	{
		mat=new GL_MATERIAL[n_mat];
		for(i=0;i<n_mat;i++)
		{			
			pfp->read(&_mat,sizeof(_glmaterial));
			memcpy(mat[i].name,_mat.name,sizeof(char)*32);
			mat[i].text=0;
			if(_mat.text[0])
			{
				if(glTextureFile)
					mat[i].text=glTextureFile->obj(_mat.text);
				else
					glLoadTexture(&mat[i].text,_mat.text);
			}
			memcpy(&mat[i].amb,&_mat.amb,sizeof(GL_RGBACOLOR));
			memcpy(&mat[i].dif,&_mat.dif,sizeof(GL_RGBACOLOR));
			memcpy(&mat[i].spec,&_mat.spec,sizeof(GL_RGBACOLOR));
			memcpy(&mat[i].self,&_mat.self,sizeof(GL_RGBACOLOR));
			mat[i].exp=_mat.exp;
		}
	}

	sobj=new GL_SUBOBJECT[n_sobj];

	flags|=GLO_CULLOBJ;
	for(i=0;i<n_sobj;i++)
	{
		_loadsub(pfp,&sobj[i]);
		_calcboxsub(&sobj[i]);
	}
	
	df.unlock();

	return 0;
}

void GL_OBJECT::unload()
{
	UINT i;

	delete [] mat;
	for(i=0;i<n_sobj;i++)
	{
		delete [] sobj[i].vtx;
		delete [] sobj[i].nv;
		delete [] sobj[i].col;
		delete [] sobj[i].map;
		delete [] sobj[i].face;
	}
	delete [] sobj;
	memset(this,0,sizeof(GL_OBJECT));
}

void GL_OBJECT::render(UINT s,UINT n)
{
	UINT i;

	if(s+n>n_sobj||n==0)
		n=n_sobj-s;
	
	glPushMatrix();
	if(pos.x!=0.0f||pos.y!=0.0f||pos.z!=0.0f)
		glTranslatef(pos.x,pos.y,pos.z);
	if(rot.x!=0.0f)
		glRotatef(rot.x,1.0f,0.0f,0.0f);
	if(rot.z!=0.0f)
		glRotatef(rot.z,0.0f,0.0f,1.0f);
	if(rot.y!=0.0f)
		glRotatef(rot.y,0.0f,1.0f,0.0f);

	if(scl.x!=0.0f||scl.y!=0.0f||scl.z!=0.0f)
		glScalef(scl.x,scl.y,scl.z);

	for(i=0;i<n;i++)
		_rendersub(&sobj[i+s]);

	glPopMatrix();
}
