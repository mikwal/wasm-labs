#ifndef GLMATH_H
#define GLMATH_H

#include <math.h>

const float PI=3.141592f;
const float RAD_CIRCLE=6.283184f;
const float DEG_CIRCLE=360.0f;
inline float radf(float deg) {return (deg*0.017453f);}
inline float degf(float rad) {return (rad*57.295779f);}
inline float randf() {return float(rand()%1000)*0.001f;}



struct GL_VECTOR2D {
	union {float x,u;};
	union {float y,v;};

	inline GL_VECTOR2D() {}
	inline GL_VECTOR2D(float _xy) {x=y=_xy;}
	inline GL_VECTOR2D(float _x,float _y) {x=_x;y=_y;}
	
	inline float &operator[](int i) {return (&x)[i];}
	inline float operator!() {return sqrtf(x*x+y*y);}
	
	inline GL_VECTOR2D &operator+=(const GL_VECTOR2D& v) {x+=v.x;y+=v.y;return *this;}
    inline GL_VECTOR2D &operator-=(const GL_VECTOR2D& v) {x-=v.x;y-=v.y;return *this;}
    inline GL_VECTOR2D &operator*=(float s) {x*=s;y*=s;return *this;}
    inline GL_VECTOR2D &operator/=(float s) {x/=s;y/=s;return *this;}

	inline void rotate(float r) {*this=GL_VECTOR2D(x*cosf(r)-y*sinf(r),y*cosf(r)+x*sinf(r));}
	inline float angle() {return atan2f(y,x);}
};

inline GL_VECTOR2D operator+(const GL_VECTOR2D &v) {return v;}
inline GL_VECTOR2D operator-(const GL_VECTOR2D &v) {return GL_VECTOR2D(-v.x,-v.y);}
inline GL_VECTOR2D operator+(const GL_VECTOR2D &v1,const GL_VECTOR2D &v2) {return GL_VECTOR2D(v1.x+v2.x,v1.y+v2.y);}
inline GL_VECTOR2D operator-(const GL_VECTOR2D &v1,const GL_VECTOR2D &v2) {return GL_VECTOR2D(v1.x-v2.x,v1.y-v2.y);}

inline GL_VECTOR2D operator*(const GL_VECTOR2D &v,float s) {return GL_VECTOR2D(v.x*s,v.y*s);}
inline GL_VECTOR2D operator*(float s,const GL_VECTOR2D &v) {return v*s;}
inline GL_VECTOR2D operator/(const GL_VECTOR2D &v,float s) {return GL_VECTOR2D(v.x/s,v.y/s);}
inline GL_VECTOR2D operator/(float s,const GL_VECTOR2D &v) {return v/s;}
inline float operator*(const GL_VECTOR2D &v1,const GL_VECTOR2D &v2) {return v1.x*v2.x+v1.y*v2.y;}



struct GL_VECTOR3D {
	union {float x,r,u;};
	union {float y,g,v;};
	union {float z,b,w;};
	
	inline GL_VECTOR3D() {}
	inline GL_VECTOR3D(float _xyz) {x=y=z=_xyz;}
	inline GL_VECTOR3D(float _x,float _y,float _z) {x=_x;y=_y;z=_z;}

	inline float &operator[](int i) {return (&x)[i];}
	inline float operator!() {return sqrtf(x*x+y*y+z*z);}

	inline GL_VECTOR3D &operator+=(const GL_VECTOR3D& v) {x+=v.x;y+=v.y;z+=v.z;return *this;}
    inline GL_VECTOR3D &operator-=(const GL_VECTOR3D& v) {x-=v.x;y-=v.y;z-=v.z;return *this;}
    inline GL_VECTOR3D &operator*=(float s) {x*=s;y*=s;z*=s;return *this;}
    inline GL_VECTOR3D &operator/=(float s) {x/=s;y/=s;z/=s;return *this;}

	inline GL_VECTOR3D &operator*=(const float *m) {return *this=GL_VECTOR3D(x*m[0]+y*m[4]+z*m[8]+m[12],x*m[1]+y*m[5]+z*m[9]+m[13],x*m[2]+y*m[6]+z*m[10]+m[14]);}

	inline void xrotate(float r) {*this=GL_VECTOR3D(x,y*cosf(r)-z*sinf(r),z*cosf(r)+y*sinf(r));}
	inline void yrotate(float r) {*this=GL_VECTOR3D(x*cosf(r)-z*sinf(r),y,z*cosf(r)+x*sinf(r));}
	inline void zrotate(float r) {*this=GL_VECTOR3D(x*cosf(r)-y*sinf(r),y*cosf(r)+x*sinf(r),z);}
	inline float xangle() {return atan2f(z,y);}
	inline float yangle() {return atan2f(z,x);}
	inline float zangle() {return atan2f(y,x);}
	
};

inline GL_VECTOR3D operator+(const GL_VECTOR3D &v) {return v;}
inline GL_VECTOR3D operator-(const GL_VECTOR3D &v) {return GL_VECTOR3D(-v.x,-v.y,-v.z);}
inline GL_VECTOR3D operator+(const GL_VECTOR3D &v1,const GL_VECTOR3D &v2) {return GL_VECTOR3D(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z);}
inline GL_VECTOR3D operator-(const GL_VECTOR3D &v1,const GL_VECTOR3D &v2) {return GL_VECTOR3D(v1.x-v2.x,v1.y-v2.y,v1.z-v2.z);}

inline GL_VECTOR3D operator*(const GL_VECTOR3D &v,float s) {return GL_VECTOR3D(v.x*s,v.y*s,v.z*s);}
inline GL_VECTOR3D operator*(float s,const GL_VECTOR3D &v) {return v*s;}
inline GL_VECTOR3D operator/(const GL_VECTOR3D &v,float s) {return GL_VECTOR3D(v.x/s,v.y/s,v.z/s);}
inline GL_VECTOR3D operator/(float s,const GL_VECTOR3D &v) {return v/s;}
inline float operator*(const GL_VECTOR3D &v1,const GL_VECTOR3D &v2) {return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;}

inline GL_VECTOR3D operator^(const GL_VECTOR3D &v1,const GL_VECTOR3D &v2) {return GL_VECTOR3D((v1.y*v2.z)-(v1.z*v2.y),(v1.z*v2.x)-(v1.x*v2.z),(v1.x*v2.y)-(v1.y*v2.x));}
inline GL_VECTOR3D operator*(const float *m,const GL_VECTOR3D &v) {return GL_VECTOR3D(v.x*m[0]+v.y*m[4]+v.z*m[8]+m[12],v.x*m[1]+v.y*m[5]+v.z*m[9]+m[13],v.x*m[2]+v.y*m[6]+v.z*m[10]+m[14]);}
inline GL_VECTOR3D operator*(const GL_VECTOR3D &v,const float *m) {return m*v;}



struct GL_VECTOR4D {
	union {float x,r;};
	union {float y,g;};
	union {float z,b;};
	union {float w,a;};

	inline GL_VECTOR4D() {}
	inline GL_VECTOR4D(float _xyzw) {x=y=z=w=_xyzw;}
	inline GL_VECTOR4D(float _x,float _y,float _z,float _w) {x=_x;y=_y;z=_z;w=_w;}

	inline float &operator[](int i) {return (&x)[i];}
	inline float operator!() {return sqrtf(x*x+y*y+z*z+w*w);}

	inline GL_VECTOR4D &operator+=(const GL_VECTOR4D& v) {x+=v.x;y+=v.y;z+=v.z;w+=v.w;return *this;}
    inline GL_VECTOR4D &operator-=(const GL_VECTOR4D& v) {x-=v.x;y-=v.y;z-=v.z;w-=v.w;return *this;}
    inline GL_VECTOR4D &operator*=(float s) {x*=s;y*=s;z*=s;w*=s;return *this;}
    inline GL_VECTOR4D &operator/=(float s) {x/=s;y/=s;z/=s;w*=s;return *this;}

	inline void xrotate(float r) {*this=GL_VECTOR4D(x,y*cosf(r)-z*sinf(r),z*cosf(r)+y*sinf(r),w);}
	inline void yrotate(float r) {*this=GL_VECTOR4D(x*cosf(r)-z*sinf(r),y,z*cosf(r)+x*sinf(r),w);}
	inline void zrotate(float r) {*this=GL_VECTOR4D(x*cosf(r)-y*sinf(r),y*cosf(r)+x*sinf(r),z,w);}
	inline float xangle() {return atan2f(z,y);}
	inline float yangle() {return atan2f(z,x);}
	inline float zangle() {return atan2f(y,x);}
};

inline GL_VECTOR4D operator+(const GL_VECTOR4D &v) {return v;}
inline GL_VECTOR4D operator-(const GL_VECTOR4D &v) {return GL_VECTOR4D(-v.x,-v.y,-v.z,-v.w);}
inline GL_VECTOR4D operator+(const GL_VECTOR4D &v1,const GL_VECTOR4D &v2) {return GL_VECTOR4D(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z,v1.w+v2.w);}
inline GL_VECTOR4D operator-(const GL_VECTOR4D &v1,const GL_VECTOR4D &v2) {return GL_VECTOR4D(v1.x-v2.x,v1.y-v2.y,v1.z-v2.z,v1.w-v2.w);}

inline GL_VECTOR4D operator*(const GL_VECTOR4D &v,float s) {return GL_VECTOR4D(v.x*s,v.y*s,v.z*s,v.w*s);}
inline GL_VECTOR4D operator*(float s,const GL_VECTOR4D &v) {return v*s;}
inline GL_VECTOR4D operator/(const GL_VECTOR4D &v,float s) {return GL_VECTOR4D(v.x/s,v.y/s,v.z/s,v.w/s);}
inline GL_VECTOR4D operator/(float s,const GL_VECTOR4D &v) {return v/s;}
inline float operator*(const GL_VECTOR4D &v1,const GL_VECTOR4D &v2) {return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z+v1.w*v2.w;}



struct GL_BOX3D {
	GL_VECTOR3D p1;
	GL_VECTOR3D p2;

	inline GL_BOX3D() {}
	inline GL_BOX3D(float x1,float x2,float y1,float y2,float z1,float z2) {p1=GL_VECTOR3D(x1,y1,z1);p1=GL_VECTOR3D(x2,y2,z2);}
};

inline GL_BOX3D operator|(const GL_VECTOR3D &v1,const GL_VECTOR3D &v2) 	{return GL_BOX3D((v1.x<v2.x)?v1.x:v2.x,(v1.x>v2.x)?v1.x:v2.x,(v1.y<v2.y)?v1.y:v2.y,(v1.y>v2.y)?v1.y:v2.y,(v1.z<v2.z)?v1.z:v2.z,(v1.z>v2.z)?v1.z:v2.z);}
inline bool operator<(const GL_VECTOR3D &v,const GL_BOX3D &b) {return ((v.x>b.p1.x&&v.y>b.p1.y&&v.z>b.p1.z)&&(v.x<b.p2.x&&v.y<b.p2.y&&v.z<b.p2.z))?true:false;}
inline bool operator>(const GL_VECTOR3D &v,const GL_BOX3D &b) {return ((v.x<b.p1.x||v.y<b.p1.y||v.z<b.p1.z)||(v.x>b.p2.x||v.y>b.p2.y||v.z>b.p2.z))?true:false;}
inline bool operator<(const GL_BOX3D &b,const GL_VECTOR3D &v) {return v>b;}
inline bool operator>(const GL_BOX3D &b,const GL_VECTOR3D &v) {return v<b;}



typedef GL_VECTOR2D GL_TEXCOORD;
typedef GL_VECTOR3D GL_TEXCOORD3D;
typedef GL_VECTOR3D GL_VERTEX;
typedef GL_VECTOR3D GL_RGBCOLOR;
typedef GL_VECTOR4D GL_RGBACOLOR;
typedef float GL_MATRIX[16];

#endif




