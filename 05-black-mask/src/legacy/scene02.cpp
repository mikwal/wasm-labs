#include "main.h"
#include "scene.h"

GL_OBJECT telebox;
GL_STARFIELD stars;

static UINT tunnel_texture;
static float telebox_vel=0.0f;
static float telebox_ax=0.0006f;
static float tunnel_alpha=0.0f;
static float tunnel_posx=0.0f;
static float tunnel_velx=0.005f;
static float tunnel_posy=0.0f;
static float tunnel_vely=0.01f;
static GL_VECTOR2D bend_s(25.0f,0.0f);

void s2load()
{
	stars.create(150);
	telebox.load("telephone.glo");
}

void s2free()
{
	stars.free();
	telebox.unload();
}

void s2init()
{
	glFogOff();
	glCFaceOn();
	glZBufOn();
	glTextureOn();
	glAlphaOff();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glBlendOn();
	glLightOn();
	glSetBgColor(0.0f,0.0f,0.0f,0.5f);

	glCam.reset();
	glCam.fov=20.0f;
	glCam.zs=5.0f;
	glCam.ze=500.0f;
	glCam.updview();

	glLight[0].on();
	glLight[0].reset();
	glLight[0].dif=GL_RGBACOLOR(1.0f,0.25f,0.25f,1.0f);
	glLight[0].amb=GL_RGBACOLOR(0.2f,0.2f,0.2f,1.0f);
	glLight[0].spec=GL_RGBACOLOR(0.5f,0.2f,0.2f,1.0f);
	glLight[0].updall();

	glLight[1].off();

	telebox.pos.z=10.0f;
	telebox.rot.y=90.0f;

	tunnel_texture=tf["tunnel3.bmp"];
	stars.init(tf["star.bmp"],0.25f,-50.0f,-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f,50.0f);
	stars.reset();
	stars.pps=SECVAL(75.0f);
	stars.setcol(0.25f,1.0f,0.25f,50.0f);

}


bool s2move()
{

	
	telebox.pos.z+=telebox_vel;
	telebox_vel+=telebox_ax;

	telebox.pos.x=(bend_s.x*sinf(telebox.pos.z/200.0f));
	telebox.pos.y=(bend_s.y*sinf(telebox.pos.z/200.0f));
	telebox.rot.x+=0.9f;
	telebox.rot.y+=1.8f;
	telebox.rot.z-=0.9f;

	if(telebox.pos.z>300.0f)
		tunnel_vely+=0.0005f;

	if(telebox.pos.z>400.0f)
	{
		if(stars.flags&GLP_REGENERATE)
			stars.flags&=~GLP_REGENERATE;
		
		tunnel_alpha-=0.0075f;

		if(tunnel_alpha<-0.25f)
			return true;
	}
	else
		if(tunnel_alpha<0.75f)
			tunnel_alpha+=0.0075f;

	tunnel_posx+=tunnel_velx;
	if(tunnel_posx>1.0f)
		tunnel_posx-=1.0f;

	tunnel_posy+=tunnel_vely;
	if(tunnel_posy>1.0f)
		tunnel_posy-=1.0f;

	bend_s.rotate(PI/100.0f);
	
	if(glCam.fov<80.0f)
		glCam.fov+=0.08f;

	stars.move();
	stars.grav=SECVAL(GL_VECTOR3D(bend_s.x,bend_s.y,0.0f));

	glCam.updview();
	glCam.updmatrix();
	glLight[0].updpos();


	return false;
}




void s2render()
{
	UINT i,ii;
	GL_VECTOR3D v[4];
	GL_VECTOR2D t[2];

	glClear();
	


	glLightOff();
	glZBufMask(false);
	stars.render();
	glZBufMask(true);

	glSetTexture(tunnel_texture);
	
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	glTranslatef(tunnel_posx,tunnel_posy,0.0f);
	
	glColor4f(1.0f,0.5f,0.5f,tunnel_alpha);

	glBegin(GL_TRIANGLES);
			
	for(i=0;i<64;i++)
		for(ii=0;ii<32;ii++)
		{
			v[0].x=10.0f*(cosf((RAD_CIRCLE/32)*ii));
			v[0].y=10.0f*(sinf((RAD_CIRCLE/32)*ii));
			v[0].z=i*20.0f;

			v[1].x=10.0f*(cosf((RAD_CIRCLE/32)*(ii+1)));
			v[1].y=10.0f*(sinf((RAD_CIRCLE/32)*(ii+1)));
			v[1].z=v[0].z;

			v[2].x=v[0].x;
			v[2].y=v[0].y;
			v[2].z=(i+1)*20.0f;

			v[3].x=v[1].x;
			v[3].y=v[1].y;
			v[3].z=v[2].z;


			v[0].x+=bend_s.x*sinf(float(i)/10.0f);
			v[0].y+=bend_s.y*sinf(float(i)/10.0f);
			
			v[1].x+=bend_s.x*sinf(float(i)/10.0f);
			v[1].y+=bend_s.y*sinf(float(i)/10.0f);

			v[2].x+=bend_s.x*sinf(float(i+1)/10.0f);
			v[2].y+=bend_s.y*sinf(float(i+1)/10.0f);

			v[3].x+=bend_s.x*sinf(float(i+1)/10.0f);
			v[3].y+=bend_s.y*sinf(float(i+1)/10.0f);
			

			t[0].u=(float(ii)/32.0f);
			t[0].v=(float(i)/48.0f);

			t[1].u=(float(ii+1)/32.0f);
			t[1].v=(float(i+1)/48.0f);


			glTexCoord2f(t[0].u,t[1].v);
			glVertex3f(v[2].x,v[2].y,v[2].z);
			
			glTexCoord2f(t[1].u,t[1].v);
			glVertex3f(v[3].x,v[3].y,v[3].z);	

			glTexCoord2f(t[0].u,t[0].v);
			glVertex3f(v[0].x,v[0].y,v[0].z);


			glTexCoord2f(t[0].u,t[0].v);
			glVertex3f(v[0].x,v[0].y,v[0].z);

			glTexCoord2f(t[1].u,t[1].v);
			glVertex3f(v[3].x,v[3].y,v[3].z);		
			
			glTexCoord2f(t[1].u,t[0].v);
			glVertex3f(v[1].x,v[1].y,v[1].z);
		}
	glEnd();

	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);

	glLightOn();
	glBlendOff();
	telebox.render(0,0);
	glBlendOn();




	glSwapBuf();
}