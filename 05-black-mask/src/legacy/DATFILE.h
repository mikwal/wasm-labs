/*****************************************/
/***    MDF Dat Archive fileformat     ***/
/*****************************************/

#ifndef DATFILE_H
#define DATFILE_H

#include "PACKFILE.h"


struct DATOBJ {
	char *name;
	unsigned long offset, size;
	unsigned char *data;
};


class DATFILE {
protected:
	FILE *_file;
	PACKFILE _packfile;
	DATOBJ *_obj;
	int _nobj,_cobj;

public:
	void (*loadproc)(DATOBJ *,PACKFILE *);
	void (*freeproc)(DATOBJ *);
	
	inline DATFILE() {memset(this,0,sizeof(DATFILE));}
	inline DATFILE(const char *filename,bool _load=false,void (*_loadproc)(DATOBJ *,PACKFILE *)=NULL,void (*_freeproc)(DATOBJ *)=NULL) {memset(this,0,sizeof(DATFILE));open(filename,_load,_loadproc,_freeproc);}
	inline ~DATFILE() {close();}

	int open(const char *,bool _load=false,void (*_loadproc)(DATOBJ *,PACKFILE *)=NULL,void (*_freeproc)(DATOBJ *)=NULL);
	void close();
	
	int index(const char *);
	int nobj() {return _nobj;}
	int cobj() {return _cobj;}
	DATOBJ &obj(int i) {return _obj[i];}
	DATOBJ &obj(const char *name) {return _obj[index(name)];}
	DATOBJ &operator[](int i) {return _obj[i];}
	DATOBJ &operator[](const char *name) {return _obj[index(name)];}
	
	PACKFILE *lock(const int );
	inline PACKFILE *lock(const char *name) {return lock(index(name));}
	int unlock();
	void *load(const int );
	inline void *load(const char *name) {return load(index(name));}
	void unload(const int );
	inline void unload(const char *name) {unload(index(name));}

	bool good() {return _file==NULL?false:true;}
};

void DefaultDFLoadProc(DATOBJ *o,PACKFILE *pf);
void DefaultDFFreeProc(DATOBJ *o);

#endif