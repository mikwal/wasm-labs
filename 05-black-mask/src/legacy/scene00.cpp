#include "main.h"
#include "scene.h"


GL_STARFIELD starfield;
GL_OBJECT earth;
GL_PLASMABALL comet;


#define EARTHTEXT_DELAY 75.0f
#define DEFVBORDER 16.6666f

#define SFDIST	2000.0f
#define SFVOL	4000.0f
#define SFSIZE	50.0f
#define SFNSTAR	1500
#define SFGLOW	10000.0f


static GL_FONT font;
static float blend_screen=0.0f;

static GL_VECTOR3D camtarget_src(6.0f,0.0f,0.0f);
static GL_VECTOR3D camtarget;
static float camtarget_path=0.0f;
static UINT state=0;
static float delay=0.0f;
static float vborder=50.0f;

static char earthtext_src[32]="Planet Earth\nYear 2034";
static char earthtext[32]="";

static GL_VECTOR3D comet_dir;


void setview(float _vborder=0)
{
	glCam.vp_y=UINT(glWHeight*(_vborder/100.f));
	glCam.vp_h=glWHeight-(glCam.vp_y*2);
	glCam.updview();
}

inline void CLEAR_VBORDER_AREA()
{
	glColor4f(0.0f,0.0f,0.0f,1.0f);

	glBegin(GL_TRIANGLE_STRIP);
	glVertex3f(100.0f,100.0f,0.0f);
	glVertex3f(0.0f,100.0f,0.0f);
	glVertex3f(100.0f,100.0f-vborder,0.0f);
	glVertex3f(0.0f,100.0f-vborder,0.0f);	
	glEnd();

	glBegin(GL_TRIANGLE_STRIP);
	glVertex3f(100.0f,vborder,0.0f);
	glVertex3f(0.0f,vborder,0.0f);
	glVertex3f(100.0f,0.0f,0.0f);
	glVertex3f(0.0f,0.0f,0.0f);	
	glEnd();
}


inline void DRAW_VBORDER_HLINES()
{
	UINT i;

	glBegin(GL_LINES);
	for(i=0;i<5;i++)
	{
		glColor4f(0.0f,0.0f,0.8f,1.0f-(i*0.2f));
		glVertex2f(0.0f,(float)i);
		glColor4f(0.5f,0.5f,1.0f,1.0f-(i*0.2f));
		glVertex2f(100.0f,(float)i);
		glColor4f(0.5f,0.5f,1.0f,1.0f-(i*0.2f));
		glVertex2f(0.0f,100.0f-(float)i);
		glColor4f(0.0f,0.0f,0.8f,1.0f-(i*0.2f));
		glVertex2f(100.0f,100.0f-(float)i);
	}
	glEnd();
}



void s0load()
{
	earth.load("earth.glo");
	starfield.create(SFNSTAR);
	comet.create(200);
}


void s0free()
{
	earth.unload();
	starfield.free();
	comet.free();
}


void s0init()
{
	glCFaceOn();
	glZBufOn();
	glTextureOn();
	glAlphaOn();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glLineWidth(ceilf((float)glWHeight/150.0f-0.00001f));
	glSetBgColor(0.0f,0.0f,0.2f,0.5f);

	font.init(tf["font2.tga"],5.0f,5.0f);
	font.setspace(0.55f,1.0f);
	font.setcolor(1.0f,1.0f,1.0f,1.5f);

	glLight[0].on();
	glLight[0].reset();

	glCam.zs=1.0f;
	glCam.ze=SFDIST*2+SFVOL;
	glCam.pos=GL_VECTOR3D(0.0f,0.0f,-20.0f);
	glCam.settarget(&camtarget);
	
	comet.setpos(-25.0f,12.5f,-75.0f);
	comet.setradius(0.5f);
	comet.setvel_dif(5.0);
	comet.setgrav(0.5f);
	comet.setcol(0.5f,0.2f,1.0f,1.0f);
	comet.setcol_dif(0.1f,0.1f,0.1f,0.3f);
	comet.setfade(0.0f,0.0f,0.0f,-4.0f);
	comet.setfade_dif(0.0f,0.0f,0.0f,0.25f);
	comet.init(tf["plasma.bmp"],GLP_REGENERATE,1.0f,400);
	comet_dir=comet.pos/!comet.pos;

	starfield.init(tf["star.bmp"],SFSIZE,0.0f,-SFVOL,SFVOL,-SFVOL,SFVOL,SFDIST,SFDIST+SFVOL,SFGLOW);
}



bool s0move()
{
	UINT i;

	switch(state)
	{
		case 0:
			if(vborder>DEFVBORDER)
			{
				vborder-=1.42f;
				if(vborder<=DEFVBORDER)
				{
					vborder=DEFVBORDER;
					state++;
				}
			}
			break;
		
		case 1:
			if(delay>EARTHTEXT_DELAY)
			{
				i=strlen(earthtext);
				delay=0.0f;

				if(earthtext_src[i])
				{
					earthtext[i]=earthtext_src[i];
					earthtext[i+1]=0;
					if(earthtext_src[i]==10)
						delay=-EARTHTEXT_DELAY;
				}
				else
				{
					state++;
					comet.reset();
				}
			}
			else
				delay+=glFrameTime;
			break;

		case 2:
			if(glCam.pos.z>-95.0f)
			{
				camtarget_path+=0.0025f;
				glCam.pos.z-=0.5f;
				font.color.a-=0.025f;
			}
			else
				state++;
			break;
		
		case 3:
			comet.pos-=comet_dir*0.5f;
			glCam.pos-=comet_dir*0.25f;
			camtarget_path+=0.0025f;

			if(comet.pos.z>-7.5f)
			{
				comet.explode(0.125f,2.5f,true);
				state++;
			}
			break;

		case 4:
			if(blend_screen>=0.5f)
				state++;
			else
				blend_screen+=0.025f;
			break;

		case 5:
			if(vborder<50.0f)
			{
				vborder+=1.42f;
				blend_screen+=0.025f;
			}
			else
				state++;
			break;

		default:
			return true;
	}

	camtarget=(comet.pos*camtarget_path)+(camtarget_src*(1.0f-camtarget_path));
	comet.move();
	glCam.updmatrix();
	glLight[0].pos=glCam.pos;
	glLight[0].updpos();
	
	return false;
}


void s0render()
{
	setview();
	glClear();

	glTextModeOn();
	glTextureOff();
	CLEAR_VBORDER_AREA();
	glTextureOn();
	glTextModeOff();
	
	if(vborder<50.0f)
	{
		setview(vborder);

		if(blend_screen<1.0f)
		{	
			glLightOn();	
			earth.render(0,0);
			glLightOff();

			glBlendOn();
			glZBufMask(false);

			starfield.render();
			comet.render();

			glTextModeOn();	
			
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
			if(font.color.a>0.0f)
				font.printf(1.0f,95.0f,earthtext);
			
			glTextureOff();
			if(blend_screen>0.0f)
			{
				glColor4f(1.0f,1.0f,1.0f,blend_screen);
				glBegin(GL_TRIANGLE_STRIP);
				glVertex3f(100.0f,100.0f,0.0f);
				glVertex3f(0.0f,100.0f,0.0f);
				glVertex3f(100.0f,0.0f,0.0f);
				glVertex3f(0.0f,0.0f,0.0f);	
				glEnd();
			}
			glBlendFunc(GL_SRC_ALPHA,GL_ONE);	
			DRAW_VBORDER_HLINES();
			glTextureOn();
			glTextModeOff();

			glBlendOff();
			glZBufMask(true);
		}
		else
		{
			glTextureOff();
			glTextModeOn();
			glColor4f(1.0f,1.0f,1.0f,1.0f);
			glBegin(GL_TRIANGLE_STRIP);
			glVertex3f(100.0f,100.0f,0.0f);
			glVertex3f(0.0f,100.0f,0.0f);
			glVertex3f(100.0f,0.0f,0.0f);
			glVertex3f(0.0f,0.0f,0.0f);	
			glEnd();
			glTextModeOff();
			glTextureOn();
		}
	}
	
	glSwapBuf();

}


