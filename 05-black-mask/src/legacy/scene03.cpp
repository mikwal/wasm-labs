#include "main.h"
#include "scene.h"

extern GL_OBJECT land;
extern GL_OBJECT telebox;
GL_OBJECT gubbe;

static float leg_dv=0.12f;
static float head_dv=-2.4f;

static GL_VECTOR3D camtarget;
static GL_VECTOR3D camrelpos;
static GL_FONT font;

static UINT state=0;
static float delay=0.0f;
static float blend_screen=2.0f;

#define OUTRO_TEXT_DELAY 75.0f
static char outro_text[256]="";
static char outro_text_src[256]=
"The legendary ninja master\n"
"Black Mask has come to save\n"
"the world.";


void gubbe_walk()
{	
	GL_VECTOR3D dir(-1.0f,0.0f,0.0f);
	static float leg_dv=0.12f;

	gubbe.rot.z-=gubbe[1].rot.z/20.0f;
	dir.xrotate(radf(gubbe.rot.x));
	dir.zrotate(radf(gubbe.rot.z));
	dir.yrotate(radf(gubbe.rot.y));
	dir.x=-dir.x;
	gubbe.pos+=(fabsf(leg_dv)*fabsf(cosf(radf(gubbe[2].rot.z))))*dir;
		
	gubbe[1].rot.z+=leg_dv*60.0f;
	if(gubbe[1].rot.z>(45.0f)||gubbe[1].rot.z<-(45.0f))
		leg_dv=-leg_dv;
	
	gubbe[2].rot.z=-gubbe[1].rot.z;
	gubbe[10].rot.z=gubbe[7].rot.z=gubbe[2].rot.z/2.0f;
	gubbe[11].rot.z=gubbe[8].rot.z=gubbe[1].rot.z/2.0f;
	gubbe.rot.z+=gubbe[1].rot.z/20.0f;
}



void s3load()
{
	gubbe.load("legogubbe.glo");
}

void s3free()
{
	gubbe.unload();
}


void s3init()
{
	glCFaceOn();
	glZBufOn();
	glBlendOff();
	glTextureOn();
	glAlphaOn();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glLightOn();
	glFogOn();
	glLightOn();
	glSetBgColor(0.1f,0.1f,0.2f,1.0f);
	glSetFog(0.33f,25.0f,250.0f);

	glCam.reset();
	glCam.zs=0.75f;
	glCam.ze=250.0f;
	glCam.settarget(&camtarget);
	glCam.updview();

	glLight[0].on();
	glLight[0].reset();
	glLight[0].dif=GL_RGBACOLOR(0.75f,0.75f,0.75f,1.0f);
	glLight[0].amb=GL_RGBACOLOR(0.25f,0.25f,0.25f,1.0f);
	glLight[0].spec=GL_RGBACOLOR(0.25f,0.25f,0.25f,1.0f);
	glLight[1].off();

	font.init(tf["font2.tga"],5.0f,5.0f);
	font.setspace(0.55f,1.0f);
	font.setsize(4.0f,4.6f);

	telebox.pos=GL_VECTOR3D(0.0f,2.25f,30.0f);
	telebox.rot=GL_VECTOR3D(0.0f,0.0f,0.0f);
	gubbe.pos=GL_VECTOR3D(0.0f,0.0f,30.f);
	gubbe.rot=GL_VECTOR3D(0.0f,-90.0f,0.0f);

	camrelpos=GL_VECTOR3D(-8.0f,3.0f,10.0f);
}


bool s3move()
{
	UINT i;

	GL_VECTOR3D dir(-1.0f,0.0f,0.0f);

	switch(state)
	{
	case 0:
		if(blend_screen>0.0f)
			blend_screen-=0.025f;
		else
			state++;
		break;

	case 1:
		if(telebox[2].rot.y>-90.0f)
			telebox[2].rot.y-=0.9f;
		else
			state++;
		break;

	case 2:
		if(gubbe.pos.z>36.0f)
			state++;
		break;

	case 3:
		gubbe[0].rot.y+=head_dv;
		if(gubbe[0].rot.y>60.0f||gubbe[0].rot.y<-60.0f)
			head_dv=-head_dv;
		gubbe[9].rot.y=gubbe[5].rot.y=gubbe[0].rot.y;

		camrelpos+=GL_VECTOR3D(0.106f,-0.004f,-0.04f);

		if(telebox[2].rot.y<0.0f)
			telebox[2].rot.y+=0.9f;
		else
			state++;
		break;

	case 4:
		if(delay>OUTRO_TEXT_DELAY)
		{
			i=strlen(outro_text);

			if(outro_text_src[i])
			{
				outro_text[i]=outro_text_src[i];
				outro_text[i+1]=0;
				if(outro_text_src[i]==10)
					delay=-OUTRO_TEXT_DELAY;
			}
		}
		else
			delay+=glFrameTime;

		if(gubbe.pos.z>75.0f)
			state++;
		break;

	case 5:
		if(blend_screen<2.0f)
			blend_screen+=0.025f;
		else
		{
			gubbe[2].rot=0.0f;
			gubbe[1].rot=0.0f;
			gubbe[8].rot.z=0.0f;
			gubbe[7].rot.z=0.0f;
			gubbe[10].rot.z=0.0f;
			gubbe[11].rot.z=0.0f;
			return true;
		}
		break;
	} 


	if(state>1&&state!=3)
		gubbe_walk();

	camtarget=gubbe.pos+GL_VECTOR3D(0.0f,2.0f,0.0f);
	glCam.pos=gubbe.pos+camrelpos;
	glCam.updmatrix();

	glLight[0].pos=glCam.pos;
	glLight[0].ints=1.5f+(randf()*0.5f);
	glLight[0].updall();	

	return false;
}

void s3render()
{
	glClear();

	if(blend_screen<1.0f)
	{
		land.render(0,0);
		telebox.render(0);
		glLightOff();
		telebox.render(1);
		glLightOn();
		telebox.render(2);
		gubbe.render(0,0);
	}

	glTextModeOn();
	glLightOff();
	glBlendOn();
	if(outro_text[0])
		font.printf(6.0f,92.0f,outro_text);
	
	if(blend_screen>0.0f)
	{
		glTextureOff();
		glColor4f(0.0f,0.0f,0.0f,blend_screen);
		glBegin(GL_TRIANGLE_STRIP);
		glVertex3f(100.0f,100.0f,0.0f);
		glVertex3f(0.0f,100.0f,0.0f);
		glVertex3f(100.0f,0.0f,0.0f);
		glVertex3f(0.0f,0.0f,0.0f);	
		glEnd();
		glTextureOn();
	}
	glTextModeOff();
	glBlendOff();
	glLightOn();
	glSwapBuf();
}