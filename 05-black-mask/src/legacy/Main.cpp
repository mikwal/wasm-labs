#include "main.h"
#include "scene.h"
#include <midasdll.h>
#include "resource.h"


const UINT n_scene=5;
UINT c_scene=0;

GL_DEMOSCENE demoscene[]={
	{s0load,s0free,s0init,s0move,s0render},
	{s1load,s1free,s1init,s1move,s1render},
	{s2load,s2free,s2init,s2move,s2render},
	{s3load,s3free,s3init,s3move,s3render},
	{s4load,s4free,s4init,s4move,s4render}
};


MIDASmodule mModule;
MIDASmodulePlayHandle mPlayHandle;

float fps;
UINT fskip;

GL_TEXTUREFILE tf;
GL_DATFILE df;

void glLoadAll()
{
	float delay;
	GL_FONT font;
	UINT i;
	char load_text[]="Loading Demo";

	glSetTextureFile(&tf);
	glSetObjectFile(&df);

	tf.open("gltexture.mdf");
	
	font.init(tf["font2.tga"],5.0f,5.0f);
	font.setspace(0.55f,1.0f);
	font.setcolor(0.15f,0.75f,0.15f,1.0f);
	font.setalign(GLF_CENTER,GLF_CENTER);
	
	glSetBgColor(0.0f,0.0f,0.0f,0.0f);
	glTextureOn();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glBlendOn();
	glTextModeOn();
	
	glClear();
	font.printf(50.0f,50.0f,load_text);
	glSwapBuf();
	
	df.open("globject.mdf"); 
		
	for(i=0;i<n_scene;i++)
		demoscene[i].load();


	MIDASstartup();
    MIDASinit();
	MIDASsetAmplification(200);
    MIDASstartBackgroundPlay(0);
	mModule=MIDASloadModule("H2O.XM");
	mPlayHandle=MIDASplayModule(mModule,true);

	while(font.color.a>0.0f)
	{	
		delay=glTime()+glFrameTime;
		glClear();
		font.printf(50.0f,50.0f,load_text);
		font.color.a-=SECVAL(0.5f);
		glSwapBuf();
		while(delay>glTime());
	}
	
	glTextureOff();
	glBlendOff();
	glTextModeOff();
	delay=glTime()+1250.0f;
	while(delay>glTime());
}


void glFreeAll()
{
	UINT i;

	for(i=0;i<n_scene;i++)
		demoscene[i].free();
	
	tf.close();
	df.close();

	MIDASstopModule(mPlayHandle);
    MIDASfreeModule(mModule);
    MIDASstopBackgroundPlay();
    MIDASclose();
}


// LRESULT CALLBACK ConfigDlgProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)
// {
// 	ULONG i;
// 	int w,h,bpp;

// 	switch(msg)
// 	{
// 	case WM_INITDIALOG:
// 		SendMessage(hwnd,WM_SETICON,ICON_BIG,(long)LoadIcon((HINSTANCE)GetWindowLong(hwnd,GWL_HINSTANCE),MAKEINTRESOURCE(DLG_ICON)));
// 		SendDlgItemMessage(hwnd,DLG_800X600,BM_SETCHECK,BST_CHECKED,0);
// 		SendDlgItemMessage(hwnd,DLG_32BPP,BM_SETCHECK,BST_CHECKED,1);
// 		break;
	
// 	case WM_COMMAND:
// 		if(LOWORD(wparam)==IDOK)
// 		{
// 			for(i=DLG_320X200;i<DLG_1280X1024+1;i++)
// 				if(SendDlgItemMessage(hwnd,i,BM_GETCHECK,0,0)==BST_CHECKED)
// 				{
// 					switch(i)
// 					{
// 						case DLG_320X200: w=320; h=200; break;
// 						case DLG_320X240: w=320; h=240; break;
// 						case DLG_512X384: w=512; h=384; break;
// 						case DLG_640X400: w=640; h=400; break;
// 						case DLG_640X480: w=640; h=480; break;
// 						case DLG_800X600: w=800; h=600; break;
// 						case DLG_1024X768: w=1024; h=768; break;
// 						case DLG_1280X1024: w=1280; h=1024; break;
// 					}
// 					break;
// 				}
// 			for(i=DLG_16BPP;i<DLG_32BPP+1;i++)
// 				if(SendDlgItemMessage(hwnd,i,BM_GETCHECK,0,0)==BST_CHECKED)
// 				{
// 					switch(i)
// 					{
// 					case DLG_16BPP: bpp=16; break;
// 					case DLG_24BPP: bpp=24; break;
// 					case DLG_32BPP: bpp=32; break;
// 					}
// 					break;
// 				}
			
// 			if(glCreateWnd("ProjectDH2K",w,h,bpp,0)==0)
// 				EndDialog(hwnd,0);
// 		}
// 		break;

// 	case WM_CLOSE:
// 		EndDialog(hwnd,-1);
// 		break;
// 	}
// 	return 0;
// }


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	float ftime,stime;
	UINT fcount;
	MSG	msg;

	// if(DialogBox(hInstance,MAKEINTRESOURCE(DLG_MAIN),0,(DLGPROC)ConfigDlgProc))
	// 	return 0;

	// ShowCursor(NULL);

	glInitEnv();
	glSetFps(30);

	glLoadAll();

	fps=0.0f;
	fskip=0;
	fcount=0;
	ftime=stime=glTime();

	demoscene[c_scene].init();

	do
    {
        if(PeekMessage(&msg,NULL,0,0,PM_NOREMOVE))
        {
            if(GetMessage(&msg,NULL,0,0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
        }
        else 
			if(glWFlags&GLW_ACTIVE)
			{
				if(demoscene[c_scene].move())
				{
					c_scene++;	
					if(c_scene<n_scene)
					{
						demoscene[c_scene].init();
						demoscene[c_scene].move();
					}
					else
					{
						PostQuitMessage(0);
						break;
					}
				}
					
				if(glTime()<ftime+glFrameTime)
				{
					while(glTime()<ftime+glFrameTime);
					ftime=glTime();
					demoscene[c_scene].render();
				}
				else
				{
					ftime+=glFrameTime;
					fskip++;
				}		
					
				fcount+=1;
				fps=(float((fcount - fskip)*1000.0f)/(glTime()-stime));	
		}
	}
	while(msg.message!=WM_QUIT&&glKey[VK_ESCAPE]==0x00);

	glFreeAll();
	glDestroyWnd();
	
	return msg.wParam;
}


