#include "main.h"
#include "scene.h"

#define N_DLGTEXT 4

char dlg_text[N_DLGTEXT][256]=
{
	"Your time has come evil one.",
	"EAT ... EAT ... EAT",
	"I will destroy you with my\nultimate ninja skillz",
	"Remember those who not fight\nmight be the strongest!"
};



GL_OBJECT arena;

extern GL_OBJECT gubbe;
extern GL_OBJECT meat;
extern GL_BONE mbone;

static GL_FONT font;
static GL_FONT bfont;

static GL_VECTOR3D camtarget;
static GL_VECTOR3D camtargetsrc;
static float camtargetpath=0.0f;
static GL_PATH cpath;
static UINT cp=0;

static float meat_vely=0.35f;

static UINT state=0;
static float delay=0.0f;

static UINT intro_texture[2];
static float intro_vel=2.0f;
static char intro_text[3][32]={
	"Black Mask",
	"V.S.",
	"Evil Meatball"
};


static GL_VECTOR2D intro_textpos[3]=
{
	GL_VECTOR2D(-50.0f,70.0f),
	GL_VECTOR2D(50.0f,-50.0f),
	GL_VECTOR2D(150.0f,30.0f)
};


GL_PLASMABALL light[4];
GL_VECTOR3D light_pos[4]={
	GL_VECTOR3D(9.25f,4.0f,9.25f),
	GL_VECTOR3D(9.25f,4.0f,-9.25f),
	GL_VECTOR3D(-9.25f,4.0f,-9.25f),
	GL_VECTOR3D(-9.25f,4.0f,9.25f)
};
GL_RGBACOLOR light_col[4]={
	GL_RGBACOLOR(0.35f,0.8f,0.8f,1.0f),
	GL_RGBACOLOR(0.5f,0.3f,0.9f,1.0f),
	GL_RGBACOLOR(0.8f,0.6f,0.3f,1.0f),
	GL_RGBACOLOR(0.8f,0.25f,0.5f,1.0f)
};

void gubbe_walk();


static GL_VECTOR2D dlg_pos=0.0f;
static char dlg_buf[256]="";
static char *dlg_src=NULL;
static UINT dlg_face;
static float dlg_delay=0.0f;
static float dlg_alpha=2.5f;


#define DLG_TEXTDELAY 150.0f

void init_dialog(char *src,UINT face,float x,float y)
{
	dlg_buf[0]=0;
	dlg_src=src;
	dlg_pos=GL_VECTOR2D(x,y);
	dlg_face=face;
	dlg_delay=0.0f;
	dlg_alpha=2.5f;
}

void reset_dialog()
{
	dlg_buf[0]=0;
	dlg_src=NULL;
	dlg_pos=0.0f;
	dlg_face=0;
	dlg_delay=0.0f;
	dlg_alpha=2.5f;
}

bool do_dialog()
{
	UINT i;

	if(dlg_src)
	{
		if(dlg_delay>DLG_TEXTDELAY)
		{
			i=strlen(dlg_buf);
			delay=0.0f;

			if(dlg_src[i])
			{
				dlg_buf[i]=dlg_src[i];
				dlg_buf[i+1]=0;
				if(dlg_src[i]==10)
					dlg_delay=-DLG_TEXTDELAY;
			}
			else 
			{
				dlg_alpha-=0.05f;
				if(dlg_alpha<0.0f)
				{
					reset_dialog();
					return true;
				}
			}

		}
		else
			dlg_delay+=glFrameTime;
	}
	return false;
}

void draw_dialog()
{	
	if(dlg_src)
	{
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glSetTexture(dlg_face);
		glColor4f(1.0f,1.0f,1.0f,dlg_alpha);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(1.0f,1.0f);
		glVertex3f(dlg_pos.x+9.0f,dlg_pos.y+6.0f,0.0f);
		glTexCoord2f(0.0f,1.0f);
		glVertex3f(dlg_pos.x,dlg_pos.y+6.0f,0.0f);
		glTexCoord2f(1.0f,0.0f);
		glVertex3f(dlg_pos.x+9.0f,dlg_pos.y-6.0f,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex3f(dlg_pos.x,dlg_pos.y-6.0f,0.0f);	
		glEnd();
		font.color.a=dlg_alpha;
		font.printf(dlg_pos.x+10.0f,dlg_pos.y+3.0f,dlg_buf);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	}
}


void print_introtext(float a)
{
	UINT i;
	float x,y;
 
	x=intro_textpos[0].x-25.0f;
	y=100.0f-intro_textpos[1].y;

	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glSetTexture(intro_texture[0]);
	glColor4f(1.0f,1.0f,1.0f,a);
	glBegin(GL_TRIANGLE_STRIP);
	glTexCoord2f(1.0f,1.0f);
	glVertex3f(x+4.5f,y+6.0f,0.0f);
	glTexCoord2f(0.0f,1.0f);
	glVertex3f(x-4.5f,y+6.0f,0.0f);
	glTexCoord2f(1.0f,0.0f);
	glVertex3f(x+4.5f,y-6.0f,0.0f);
	glTexCoord2f(0.0f,0.0f);
	glVertex3f(x-4.5f,y-6.0f,0.0f);	
	glEnd();

	x=intro_textpos[2].x+25.0f;
	y=100.0f-intro_textpos[1].y;

	glSetTexture(intro_texture[1]);
	glColor4f(1.0f,1.0f,1.0f,a);
	glBegin(GL_TRIANGLE_STRIP);
	glTexCoord2f(1.0f,1.0f);
	glVertex3f(x+4.5f,y+6.0f,0.0f);
	glTexCoord2f(0.0f,1.0f);
	glVertex3f(x-4.5f,y+6.0f,0.0f);
	glTexCoord2f(1.0f,0.0f);
	glVertex3f(x+4.5f,y-6.0f,0.0f);
	glTexCoord2f(0.0f,0.0f);
	glVertex3f(x-4.5f,y-6.0f,0.0f);	
	glEnd();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);

	for(i=0;i<8;i++)
	{
		bfont.setcolor(0.0f,0.25f,0.75f,randf()*a/3.0f);
		bfont.printf(intro_textpos[0].x+((randf()-randf())*5.0f),intro_textpos[0].y+((randf()-randf())*5.0f),intro_text[0]);
	}
	bfont.setcolor(0.2f,1.0f,0.5f,a);
	bfont.printf(intro_textpos[0].x,intro_textpos[0].y,intro_text[0]);

	for(i=0;i<8;i++)
	{
		bfont.setcolor(0.0f,0.75f,0.25f,randf()*a/3.0f);
		bfont.printf(intro_textpos[1].x+((randf()-randf())*5.0f),intro_textpos[1].y+((randf()-randf())*5.0f),intro_text[1]);
	}
	bfont.setcolor(0.2f,0.5f,1.0f,a);
	bfont.printf(intro_textpos[1].x,intro_textpos[1].y,intro_text[1]);

	for(i=0;i<8;i++)
	{
		bfont.setcolor(0.0f,0.25f,0.75f,randf()*a/3.0f);
		bfont.printf(intro_textpos[2].x+((randf()-randf())*5.0f),intro_textpos[2].y+((randf()-randf())*5.0f),intro_text[2]);
	}
	bfont.setcolor(0.2f,1.0f,0.5f,a);
	bfont.printf(intro_textpos[2].x,intro_textpos[2].y,intro_text[2]);
}


void make_faceted(GL_SUBOBJECT &so)
{
	UINT i;

	delete [] so.nv;
	so.nv=new GL_VECTOR3D[so.n_face];
	for(i=0;i<so.n_face;i++)
	{
		so.face[i].nv[0]=so.face[i].nv[1]=so.face[i].nv[2]=&so.nv[i];
		so.nv[i]=((*so.face[i].vtx[1])-(*so.face[i].vtx[0]))^((*so.face[i].vtx[2])-(*so.face[i].vtx[0]));
		so.nv[i]/=!so.nv[i];
	}
}


static float jaw_v=0.0f;
static float jaw_dv=PI/60.0f;

void meat_tugga()
{
	jaw_v+=jaw_dv;
	mbone.zrotate_last(0,jaw_dv);
	mbone.zrotate_first(1,-jaw_dv);

	if(jaw_v>0.0f||jaw_v<-PI/5.0f)
		jaw_dv=-jaw_dv;		
}


void s4load()
{
	UINT i;

	arena.load("arena.glo");
	arena[4].mat->amb.a=0.0f;
	arena[4].mat->dif.a=0.6f;
	arena[4].mat->spec.a=0.9f;
	make_faceted(arena[3]);
	cpath.load("arena.pth");
	
	for(i=0;i<4;i++)
		light[i].create(250);
}

void s4free()
{
	UINT i;

	arena.unload();
	cpath.unload();

	for(i=0;i<4;i++)
		light[i].free();
}

void s4init()
{
	UINT i;

	glCFaceOn();
	glZBufOn();
	glFogOff();
	glTextureOn();
	glAlphaOn();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glLightOn();
	glSetAmbLight(0.0f,0.0f,0.0f,0.0f);
	glSetBgColor(0.0f,0.0f,0.0f,0.5f);

	glCam.reset();
	glCam.zs=1.5f;
	glCam.ze=500.0f;
	glCam.updview();
	camtargetsrc=camtarget=GL_VECTOR3D(0.0f,1.5f,0.0f);
	glCam.settarget(&camtarget);

	
	meat.pos=GL_VECTOR3D(-8.0f,3.60f,0.0f);
	meat.rot=GL_VECTOR3D(0.0f,0.0f,0.0f);
	meat.scl=1.0f;

	gubbe.pos=GL_VECTOR3D(8.0f,0.0f,0.0f);
	gubbe.rot=GL_VECTOR3D(0.0f,180.0f,0.0f);
	gubbe.scl=1.0f;

	glLight[4].amb=0.0f;
	glLight[4].dif=0.0f;
	glLight[4].spec=0.0f;

	for(i=0;i<4;i++)
	{
		glLight[i].on();
		glLight[i].reset();
		glLight[i].amb=light_col[i]*0.1f;
		glLight[i].dif=light_col[i]*0.6f;
		glLight[i].spec=light_col[i]*0.9f;
		glLight[i].c_atn=0.9f;
		glLight[i].l_atn=0.1f;
		glLight[i].ints=0.0f;
		glLight[i].updall();


		light[i].col=light_col[i]*1.5f;
		light[i].setradius(0.75f);
		light[i].setvel(1.5);
		light[i].setgrav(0.075f);
		light[i].setfade(0.0f,0.0f,0.0f,-3.0f);
		light[i].setfade_dif(0.0f,0.0f,0.0f,0.00f);
		light[i].init(tf["plasma.bmp"],GLP_REGENERATE,0.5f,50.0f);

		glLight[4].amb+=glLight[i].amb;
		glLight[4].dif+=glLight[i].dif;
		glLight[4].spec+=glLight[i].spec;
		
	}

	glLight[4].amb/=4.0f;
	glLight[4].dif/=4.0f;
	glLight[4].spec/=4.0f;


	font.init(tf["font2.tga"],5.0f,5.0f);
	font.setspace(0.55f,1.0f);
	font.setsize(4.0f,4.6f);

	bfont.init(tf["font3.tga"],4.8f,6.4f);
	bfont.setspace(0.6f,1.0f);
	bfont.setsize(7.5f,10.0f);
	bfont.setalign(GLF_CENTER,GLF_CENTER);

	intro_texture[0]=tf["bm.tga"];
	intro_texture[1]=tf["mb.tga"];

}


bool s4move()
{
	UINT i,n;




	switch(state)
	{
		case 0:
			intro_textpos[0].x+=intro_vel;
			intro_textpos[1].y+=intro_vel;
			intro_textpos[2].x-=intro_vel;
			intro_vel-=intro_vel/75.0f;
			if(intro_textpos[1].y>50.0f)
				state++;
			break;

		case 1:
			if(intro_vel>0.0f)
				intro_vel-=0.025f;
			else
				state++;
			break;

		case 2: 
			if(delay>2500.0f)
			{
				for(i=0;i<4;i++)
					light[i].reset();
				delay=0.0f;
				state++;
			}
			else
				delay+=glFrameTime;
			break;

		case 3:
			if(intro_vel<1.0f)
				intro_vel+=0.025f;
			else
				state++;
			break;	

		case 5:
			if(cpath.path[cp].c_pt>=(cpath.path[cp].n_pt-1.0f))
			{
				camtargetsrc=(gubbe[0].pos+gubbe.pos);
				camtargetpath=0.0f;
				cp++;
				state++;
				init_dialog(dlg_text[0],intro_texture[0],10.0f,25.0f);
			}
				
			cpath.travel(cp,0.5f);
			if(gubbe.pos.x<3.2f&&gubbe[1].rot.z>-3.0f&&gubbe[1].rot.z<3.0f)
			{
				if(camtargetpath<1.0f)
					camtargetpath+=0.025f;
			}
			else
				gubbe_walk();

			camtarget=((1.0f-camtargetpath)*camtargetsrc)+(camtargetpath*(gubbe[0].pos+gubbe.pos));
			break;

		case 6:
			if(do_dialog())
			{
				delay=0.0f;
				state++;
			}
			break;

		case 7:
			if(cpath.path[cp].c_pt>=(cpath.path[cp].n_pt-1.0f))
			{
				state++;
				init_dialog(dlg_text[1],intro_texture[1],10.0f,75.0f);
			}
				
			cpath.travel(cp,0.5f);
			if(camtargetpath<1.0f)
				camtargetpath+=0.005f;
			camtarget=((1.0f-camtargetpath)*camtargetsrc)+(camtargetpath*(meat[0].pos+meat.pos));
			break;

		case 8:
			meat_tugga();
			if(do_dialog())
			{
				mbone.zrotate_last(0,-jaw_v);
				mbone.zrotate_first(1,jaw_v);
				state++;
				init_dialog(dlg_text[2],intro_texture[0],10.0f,75.0f);
			}
			break;

		case 9:
			if(do_dialog())
			{
				gubbe[1].rot.z=80.0f;
				gubbe[2].rot.z=-gubbe[1].rot.z;
				state++;
			}
			break;

		case 10:
			gubbe.pos+=GL_VECTOR3D(-1.0f,0.33f,0.0f)/5.0f;
			if(gubbe.pos.x<-4.0f)
				state++;
			break;
		
		case 11:
			if(gubbe[1].rot.z>0.0f)
				gubbe[1].rot.z-=8.0f;
			else
				gubbe[1].rot.z=0;
			gubbe[2].rot.z=-gubbe[1].rot.z;
			if(gubbe.pos.y>0.0f)
				gubbe.pos.y-=0.082f;
			else
				gubbe.pos.y=0.0f;
			meat.pos.y+=meat_vely;
			meat_vely-=0.0092f;
			meat.pos.x-=0.35f;
			if(meat.pos.y<-100.0f)
			{
				state++;
				init_dialog(dlg_text[3],intro_texture[0],10.0f,75.0f);
				dlg_alpha=5.0f;
			}

		case 12:
			if(do_dialog())
				return true;

	}




	for(i=0,n=0;i<4;i++)
		if(light[i].n_alive()<light[i].n_p)
			light_pos[i].yrotate(PI/100.0f);
		else
			n++;
	if(n==4&&state==4)
		state++;



	glLight[4].ints=0;
	for(i=0;i<4;i++)
	{
		glLight[i].pos=light_pos[i];
		light[i].pos=light_pos[i];
		if(light[i].isalive())
		{
			glLight[i].ints=((float)light[i].n_alive()/(float)light[i].n_p);
			glLight[i].ints=(2.0f*glLight[i].ints)+((glLight[i].ints*randf())*0.5f);
		}
		glLight[i].updall();
		light[i].move();

		glLight[4].ints+=glLight[i].ints;
	}
	glLight[4].ints/=4.0f;
	glLight[4].pos=glCam.pos;
	glLight[4].updall();

	glCam.pos=cpath.getpos(cp);
	glCam.updmatrix();


	return false;
}



void s4render()
{
	UINT i;
	GL_VECTOR3D v[4];
	GL_VECTOR2D t[2];


	glClear();

	
	for(i=0;i<4;i++)
		glLight[i].off();

	glLight[4].on();
	gubbe.render(0,0);
	meat.render(0,0);
	glLight[4].off();

	for(i=0;i<4;i++)
		glLight[i].on();

	
	arena.render(0);
	arena.render(1);
	arena.render(2);
	arena.render(3);
	arena.render(5);
	glBlendOn();
	glZBufMask(false);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	arena.render(4);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glLightOff();


	for(i=0;i<4;i++)
		light[i].render();
	glTextModeOn();
	if(intro_vel<1.0f)
		print_introtext(1.0f-intro_vel);

	glZBufMask(true);

	draw_dialog();
	
	glTextModeOff();
	glLightOn();
	glBlendOff();
	glSwapBuf();
}