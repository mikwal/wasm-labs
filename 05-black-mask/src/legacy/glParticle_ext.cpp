#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <stdio.h>
#include "glEnv.h"
#include "glParticle.h"


void GL_STARFIELD::init(UINT _text,float _size,float v,float x1,float x2,float y1,float y2,float z1,float z2,float str)
{
	UINT i;

	text=_text;
	flags=GLP_REGENERATE|GLP_BOUNDINGBOX|GLP_LINEARALPHA;
	size=_size/2;
	pps=0;
	p_dead=0;
	p_used=n_p;

	setbbox(x1,x2,y1,y2,z1,z2);
	setvel(0.0f,0.0f,v);
	setcol(1.0f,1.0f,1.0f,str);

	setpos((x1+x2)/2.0f,(y1+y2)/2.0f,(z1+z2)/2.0f);
	setpos_dif((x2-x1)/2,(y2-y1)/2,(z2-z1)/2);

	for(i=0;i<n_p;i++)
	{
		_initp(i);
		p[i].alive=true;
	}
	
	if(v>0.0f)
	{
		setpos((x1+x2)/2.0f,(y1+y2)/2.0f,z1);
		setpos_dif((x2-x1)/2,(y2-y1)/2,0.0f);
	}

	if(v<0.0f)
	{
		setpos((x1+x2)/2.0f,(y1+y2)/2.0f,z2);
		setpos_dif((x2-x1)/2,(y2-y1)/2,0.0f);
	}
}
