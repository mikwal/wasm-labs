/*****************************************/
/***   LZSS compression file format    ***/
/*****************************************/

#include "PACKFILE.h"


void PACKFILE::_init_tree(void)
{
	int i;

	for(i=PF_N+1;i<=PF_N+256;i++)
		_rson[i]=PF_N;
	for(i=0;i<PF_N;i++)
		_dad[i]=PF_N;
}

void PACKFILE::_insert_node(int r)
{
	int i,p,cmp;
	unsigned char *key;

	cmp=1;
	key=&_text_buf[r];
	p=PF_N+1+key[0];
	_rson[r]=_lson[r]=PF_N;
	_match_length=0;
	for(;;) 
	{
		if(cmp>=0)
		{
			if(_rson[p]!= PF_N)
				p=_rson[p];
			else 
			{
				_rson[p]=r;
				_dad[r]=p;
				return;
			}
		} 
		else
		{
			if(_lson[p]!=PF_N)
				p=_lson[p];
			else 
			{
				_lson[p]=r;
				_dad[r]=p;
				return;
			}
		}
		for(i=1;i<PF_F;i++)
			if((cmp=key[i]-_text_buf[p+i])!= 0)
				break;
		if(i>_match_length)
		{
			_match_position=p;
			if((_match_length=i)>=PF_F)
				break;
		}
	}
	_dad[_r]=_dad[p];
	_lson[_r]=_lson[p];
	_rson[_r]=_rson[p];
	_dad[_lson[p]]=_r;
	_dad[_rson[p]]=_r;
	if(_rson[_dad[p]]==p)
		_rson[_dad[p]]=r;
	else
		_lson[_dad[p]]=r;
	_dad[p]=PF_N;
}

void PACKFILE::_delete_node(int p)
{
	int  q;
	
	if(_dad[p]==PF_N) 
		return;
	if(_rson[p]==PF_N)
		q=_lson[p];
	else 
		if(_lson[p]==PF_N)
			q=_rson[p];
		else 
		{
			q=_lson[p];
			if(_rson[q]!=PF_N) 
			{
				do{q=_rson[q];}while(_rson[q]!=PF_N);
				_rson[_dad[q]]=_lson[q];
				_dad[_lson[q]]=_dad[q];
				_lson[q]=_lson[p];
				_dad[_lson[p]]=q;
			}
			_rson[q]=_rson[p];
			_dad[_rson[p]]=q;
		}
	_dad[q]=_dad[p];
	if(_rson[_dad[p]]==p)
		_rson[_dad[p]]=q;
	else 
		_lson[_dad[p]]=q;
	_dad[p]=PF_N;
}

int PACKFILE::open(const char *filename,char mode)
{
	close();
	
	_textsize=0;
	_codesize=0;
	_mode=0; _pos=0;
	_match_position=0; _match_length=0; _len=0;
	_i=0; _j=0; _c=0; _k=0; _r=0, _s=0;
	_flags=0;
	_last_match_length=0; _code_buf_ptr=0;
	_mask=0;
	_eof=false; _vfile=false;

	switch(_mode=mode)
	{
	case 'r':
		if((_file=fopen(filename,"rb"))==NULL)
			return -1;
		for (_i=0;_i<PF_N-PF_F;_i++) 
			_text_buf[_i] = ' ';
		_r=PF_N-PF_F;
		_flags=0;
		return 0;

	case 'w':
		if((_file=fopen(filename,"wb"))==NULL)
			return -1;

		_init_tree();
		_code_buf[0]=0;
		_code_buf_ptr=_mask=1;
		_s=0;
		_r=PF_N-PF_F;
		for(_i=_s;_i<_r;_i++)
			_text_buf[_i]=' ';
		return 0;

	default:
		_mode=0;
		return 1;
	}
}

int PACKFILE::open(FILE *parent,char mode)
{
	close();

	_textsize=0;
	_codesize=0;
	_mode=0; _pos=0;
	_match_position=0; _match_length=0; _len=0;
	_i=0; _j=0; _c=0; _k=0; _r=0, _s=0;
	_flags=0;
	_last_match_length=0; _code_buf_ptr=0;
	_mask=0;
	_eof=false; _vfile=false;

	switch(_mode=mode)
	{
	case 'r':
		if((_file=parent)==NULL)
			return 1;
		for (_i=0;_i<PF_N-PF_F;_i++) 
			_text_buf[_i] = ' ';
		_r=PF_N-PF_F;
		_flags=0;
		_vfile=true;
		return 0;

	case 'w':
		if((_file=parent)==NULL)
			return 1;

		_init_tree();
		_code_buf[0]=0;
		_code_buf_ptr=_mask=1;
		_s=0;
		_r=PF_N-PF_F;
		for(_i=_s;_i<_r;_i++)
			_text_buf[_i]=' ';
		_vfile=true;
		return 0;

	default:
		_mode=0;
		return 1;
	}
}

void PACKFILE::close()
{
	unsigned long tsize,csize;

	if(good())
	{
		_eof=true;
		if(_mode=='w')
			put(0);
		if(!_vfile&&_file)
			fclose(_file);
		tsize=_textsize;
		csize=_codesize;
		memset(this,0,sizeof(PACKFILE));
		_textsize=tsize;
		_codesize=csize;
	}
}

int PACKFILE::get()
{
	if(_pos==1)
		goto pos1;
	if(_pos==2)
		goto pos2;

	for (;;) 
	{
		if(((_flags>>=1)&256)==0)
		{
			if((_c=getc(_file))== EOF)
			{
				_eof=true;
				return EOF;
			}
			_flags=_c|0xff00;		
		}							
		if(_flags&1)
		{
			if((_c=getc(_file))==EOF)
			{
				_eof=true;
				return EOF;
			}
			_pos=1;		
			return _c;
pos1:
			_text_buf[_r++]=_c;
			_r&=(PF_N-1);
		} 
		else
		{
			if((_i=getc(_file))==EOF)
			{
				_eof=true;
				return EOF;
			}				
			if((_j=getc(_file))==EOF)
			{
				_eof=true;
				return EOF;
			}			
			_i|=((_j&0xf0)<<4);
			_j=(_j&0x0f)+PF_THRESHOLD;
			for(_k=0;_k<=_j;_k++)
			{
				_c=_text_buf[(_i+_k)&(PF_N-1)];
				_pos=2;
				return _c;
pos2:
				_text_buf[_r++]=_c;
				_r&=(PF_N-1);
			}
		}
	}
}

void PACKFILE::put(int c)
{
	if(_pos==1)
		goto pos1;
	if(_pos==2)
		goto pos2;
	
	for(_len=0;_len<PF_F&&!_eof;_len++)
	{
		_c=c;
		_pos=1;
		return;
pos1:
		_text_buf[_r+_len]=_c;
	}

	if((_textsize=_len)==0)
		return;
	for(_i=1;_i<=PF_F;_i++)
		_insert_node(_r-_i);
	_insert_node(_r);

	do {
		if(_match_length>_len)
			_match_length=_len;
		if(_match_length<=PF_THRESHOLD) 
		{
			_match_length=1;
			_code_buf[0]|=_mask;
			_code_buf[_code_buf_ptr++]=_text_buf[_r];
		} 
		else
		{
			_code_buf[_code_buf_ptr++]=(unsigned char) _match_position;
			_code_buf[_code_buf_ptr++]=(unsigned char)(((_match_position>>4)&0xf0)|(_match_length-(PF_THRESHOLD+1)));  
		}
		if((_mask<<=1)==0) 
		{
			for(_i=0;_i<_code_buf_ptr;_i++)
				putc(_code_buf[_i],_file);
			_codesize+=_code_buf_ptr;
			_code_buf[0]=0;
			_code_buf_ptr=_mask=1;
		}
		_last_match_length=_match_length;
		for(_i=0;_i<_last_match_length&&!_eof;_i++)
		{
			_c=c;
			_pos=2;
			return;
pos2:
			_delete_node(_s);
			_text_buf[_s]=_c;
			if(_s<PF_F-1)
				_text_buf[_s+PF_N]=_c;
			_s=(_s+1)&(PF_N-1);
			_r=(_r+1)&(PF_N-1);
			_insert_node(_r);
		}
		_textsize+=_i;
		while(_i++<_last_match_length)
		{
			_delete_node(_s);
			_s=(_s+1)&(PF_N - 1);
			_r=(_r+1)&(PF_N - 1);
			if(--_len)
				_insert_node(_r);
		}
	}while(_len>0);
	if(_code_buf_ptr>1)
	{
		for(_i=0;_i<_code_buf_ptr;_i++)
			putc(_code_buf[_i],_file);
		_codesize+=_code_buf_ptr;
	}
}

size_t PACKFILE::read(void *data,size_t size)
{
	size_t p=0;

	if(size<=0||_eof)
		return p;

	if(_pos==1)
		goto pos1;
	if(_pos==2)
		goto pos2;

	for (;;) 
	{
		if(((_flags>>=1)&256)==0)
		{
			if((_c=getc(_file))==EOF)
			{
				_eof=true;
				return p;
			}
			_flags=_c|0xff00;		
		}							
		if(_flags&1)
		{
			if((_c=getc(_file))==EOF)
			{
				_eof=true;
				return p;
			}
			((unsigned char *)data)[p++]=_c;
			if(p==size)
			{
				_pos=1;
				return p;
			}
pos1:
			_text_buf[_r++]=_c;
			_r&=(PF_N-1);
		} 
		else
		{
			if((_i=getc(_file))==EOF)
			{
				_eof=true;
				return p;
			}				
			if((_j=getc(_file))==EOF)
			{
				_eof=true;
				return p;
			}			
			_i|=((_j&0xf0)<<4);
			_j=(_j&0x0f)+PF_THRESHOLD;
			for(_k=0;_k<=_j;_k++)
			{
				_c=_text_buf[(_i+_k)&(PF_N-1)];
				((unsigned char *)data)[p++]=_c;
				if(p==size)
				{
					_pos=2;
					return p;
				}
pos2:
				_text_buf[_r++]=_c;
				_r&=(PF_N-1);
			}
		}
	}

}

size_t PACKFILE::write(const void *data,size_t size)
{
	size_t p=0;

	if(size<=0)
		return p;
	if(_pos==1)
		goto pos1;
	if(_pos==2)
		goto pos2;
	
	for(_len=0;_len<PF_F&&!_eof;_len++)
	{
		_c=((unsigned char *)data)[p++];
		if(p==size)
		{
			_pos=1;
			return p;
		}
pos1:
		_text_buf[_r+_len]=_c;
	}

	if((_textsize=_len)==0)
		return 0;
	for(_i=1;_i<=PF_F;_i++)
		_insert_node(_r-_i);
	_insert_node(_r);

	do {
		if(_match_length>_len)
			_match_length=_len;
		if(_match_length<=PF_THRESHOLD) 
		{
			_match_length=1;
			_code_buf[0]|=_mask;
			_code_buf[_code_buf_ptr++]=_text_buf[_r];
		} 
		else
		{
			_code_buf[_code_buf_ptr++]=(unsigned char) _match_position;
			_code_buf[_code_buf_ptr++]=(unsigned char)(((_match_position>>4)&0xf0)|(_match_length-(PF_THRESHOLD+1)));  
		}
		if((_mask<<=1)==0) 
		{
			for(_i=0;_i<_code_buf_ptr;_i++)
				putc(_code_buf[_i],_file);
			_codesize+=_code_buf_ptr;
			_code_buf[0]=0;
			_code_buf_ptr=_mask=1;
		}
		_last_match_length=_match_length;
		for(_i=0;_i<_last_match_length&&!_eof;_i++)
		{
			_c=((unsigned char *)data)[p++];
			if(p==size)
			{
				_pos=2;
				return p;
			}
pos2:
			_delete_node(_s);
			_text_buf[_s]=_c;
			if(_s<PF_F-1)
				_text_buf[_s+PF_N]=_c;
			_s=(_s+1)&(PF_N-1);
			_r=(_r+1)&(PF_N-1);
			_insert_node(_r);
		}
		_textsize+=_i;
		while(_i++<_last_match_length)
		{
			_delete_node(_s);
			_s=(_s+1)&(PF_N - 1);
			_r=(_r+1)&(PF_N - 1);
			if(--_len)
				_insert_node(_r);
		}
	}while(_len>0);
	if(_code_buf_ptr>1)
	{
		for(_i=0;_i<_code_buf_ptr;_i++)
			putc(_code_buf[_i],_file);
		_codesize+=_code_buf_ptr;
	}

}


