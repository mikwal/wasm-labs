#include <windows.h>
#include <stdio.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "glEnv.h"
#include "glTexture.h"


UINT glTextureFilter=GLT_MIPMAP;


UCHAR *_glloadBMP(FILE *fp,UINT *w,UINT *h,UINT *bpp)
{
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;
	PALETTEENTRY pal[256];
	UCHAR c,*data;
	int i,l;

	fread(&bmfh,sizeof(bmfh),1,fp);
	fread(&bmih,sizeof(bmih),1,fp);

	if(bmfh.bfType!=0x4D42||bmih.biCompression!=0)
	{
		fclose(fp);
		return NULL;
	}
	
	*w=bmih.biWidth;
	*h=bmih.biHeight;
	*bpp=bmih.biBitCount;
	l=bmih.biWidth*bmih.biHeight;
	data=new UCHAR[l*3];

	switch(*bpp)
	{
	case 8:
		fread(pal,sizeof(pal),1,fp);
		for(i=0;i<l;i++)
		{
			c=fgetc(fp);
			data[i*3+2]=pal[c].peRed;
			data[i*3+1]=pal[c].peGreen;
			data[i*3]=pal[c].peBlue;
		}
		break;

	case 24:
		for(i=0;i<l;i++)
		{
			data[i*3+2]=fgetc(fp);
			data[i*3+1]=fgetc(fp);
			data[i*3]=fgetc(fp);
		}
		break;

	default:
		delete [] data;
		return NULL;
	}
	*bpp=24;

	return data;
}

UCHAR *_glloadBMP(PACKFILE *pf,UINT *w,UINT *h,UINT *bpp)
{
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;
	PALETTEENTRY pal[256];
	UCHAR c,*data;
	int i,l;

	pf->read(&bmfh,sizeof(bmfh));
	pf->read(&bmih,sizeof(bmih));

	if(bmfh.bfType!=0x4D42||bmih.biCompression!=0)
		return NULL;
	
	*w=bmih.biWidth;
	*h=bmih.biHeight;
	*bpp=bmih.biBitCount;
	l=bmih.biWidth*bmih.biHeight;
	data=new UCHAR[l*3];

	switch(*bpp)
	{
	case 8:
		pf->read(pal,sizeof(pal));
		for(i=0;i<l;i++)
		{
			c=pf->get();
			data[i*3+2]=pal[c].peRed;
			data[i*3+1]=pal[c].peGreen;
			data[i*3]=pal[c].peBlue;
		}
		break;

	case 24:
		for(i=0;i<l;i++)
		{
			data[i*3+2]=pf->get();
			data[i*3+1]=pf->get();
			data[i*3]=pf->get();
		}
		break;

	default:
		delete [] data;
		return NULL;
	}
	*bpp=24;

	return data;
}

UCHAR *_glloadTGA(FILE *fp,UINT *w,UINT *h,UINT *bpp)
{
	UCHAR tgah[18],*data;
	int l,i;

	fread(tgah,18,1,fp);
	if(tgah[2]!=2)
		return NULL;
	*w=tgah[12]|(tgah[13]<<8);
	*h=tgah[14]|(tgah[15]<<8);
	*bpp=tgah[16];

	l=(*w)*(*h);
	data=new UCHAR[l*(*bpp/8)];

	switch(*bpp)
	{
	case 24:
		for(i=0;i<l;i++)
		{
			data[i*3+2]=fgetc(fp);
			data[i*3+1]=fgetc(fp);
			data[i*3]=fgetc(fp);
		}
		break;

	case 32:
		for(i=0;i<l;i++)
		{
			data[i*4+2]=fgetc(fp);
			data[i*4+1]=fgetc(fp);
			data[i*4]=fgetc(fp);
			data[i*4+3]=fgetc(fp);
		}
		break;

	default:
		delete [] data;
		return NULL;
	}

	return data;
}

UCHAR *_glloadTGA(PACKFILE *pf,UINT *w,UINT *h,UINT *bpp)
{
	UCHAR tgah[18],*data;
	int l,i;

	pf->read(tgah,18);
	if(tgah[2]!=2)
		return NULL;
	*w=tgah[12]|(tgah[13]<<8);
	*h=tgah[14]|(tgah[15]<<8);
	*bpp=tgah[16];

	l=(*w)*(*h);
	data=new UCHAR[l*(*bpp/8)];

	switch(*bpp)
	{
	case 24:
		for(i=0;i<l;i++)
		{
			data[i*3+2]=pf->get();
			data[i*3+1]=pf->get();
			data[i*3]=pf->get();
		}
		break;

	case 32:
		for(i=0;i<l;i++)
		{
			data[i*4+2]=pf->get();
			data[i*4+1]=pf->get();
			data[i*4]=pf->get();
			data[i*4+3]=pf->get();
		}
		break;

	default:
		delete [] data;
		return NULL;
	}

	return data;
}


void glCreateTexture(UINT *texture,const void *data,UINT width,UINT height,UINT bpp)
{
	UINT type;

	glGenTextures(1,texture);
	glBindTexture(GL_TEXTURE_2D,*texture);

	type=(bpp==32)?GL_RGBA:GL_RGB;
	
	switch(glTextureFilter)
	{
	case GLT_NEAREST:
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D,0,bpp/8,width,height,0,type,GL_UNSIGNED_BYTE,data);
		break;

	case GLT_LINEAR:
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,0,bpp/8,width,height,0,type,GL_UNSIGNED_BYTE,data);
		break;

	default:
	case GLT_MIPMAP:	
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
		glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0, type,GL_UNSIGNED_BYTE,data);
        glGenerateMipmap(GL_TEXTURE_2D);
		break;
	}
}

int glLoadTexture(UINT *texture,const char *filename)
{
	UCHAR *data=NULL;
	FILE *fp;
	UINT w,h,bpp;

	glFreeTexture(texture);

	if((fp=fopen(filename,"rb"))==NULL)
		return -1;

	if(strcmpi(".BMP",(char*)(filename+strlen(filename)-4))==0)
		data=_glloadBMP(fp,&w,&h,&bpp);
	else
		if(strcmpi(".TGA",(char*)(filename+strlen(filename)-4))==0)	
			data=_glloadTGA(fp,&w,&h,&bpp);

	if(data==NULL)
	{
		fclose(fp);
		return -1;
	}
	fclose(fp);

	glCreateTexture(texture,data,w,h,bpp);
	delete [] data;

	return 0;
}

void glFreeTexture(UINT *texture)
{
	glDeleteTextures(1,texture);
	*texture=0;
}


UINT GL_TEXTUREFILE::obj(const int i)
{
	if((i>=0&&i<_nobj)&&_obj[i].data)
	{
		return *((UINT*)_obj[i].data);
	}
	else
		return 0;
}

void glTFLoadProc(DATOBJ *o,PACKFILE *pf)
{
	UINT texture;
	UCHAR *data;
	UINT w,h,bpp;

	if(strcmpi(".BMP",(char*)(o->name+strlen(o->name)-4))==0)
		data=_glloadBMP(pf,&w,&h,&bpp);
	else
		if(strcmpi(".TGA",(char*)(o->name+strlen(o->name)-4))==0)	
			data=_glloadTGA(pf,&w,&h,&bpp);

	if(data==NULL)
		return;

	glCreateTexture(&texture,data,w,h,bpp);
	delete [] data;

	o->data=new UCHAR[4];
	memcpy(o->data,&texture,4);
}

void glTFFreeProc(DATOBJ *o)
{
	glDeleteTextures(1,(UINT*)o->data);
	delete [] o->data;
}

