#include <cstdio>
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "glEnv.h"
#include "glWindow.h"



void GL_CAMERA::reset()
{
	glCam.pos=GL_VECTOR3D(0.0f,0.0f,0.0f);
	glCam.head=PI/2.0f;
	glCam.pitch=0.0f;
	glCam.roll=0.0f;
	//glGetIntegerv(GL_VIEWPORT,(int*)&glCam.vp_x);
	glCam.vp_x = 0;
	glCam.vp_y = 0;
	glCam.vp_w = glWWidth;
	glCam.vp_h = glWHeight;
	glCam.fov=45.0f;
	glCam.zs=0.5f;
	glCam.ze=200.0f;
	glCam.target=NULL;
	glCam.updview();
}

void GL_CAMERA::updmatrix()
{
	glLoadIdentity();

	if(target)
	{
		dir=*target-pos;
		dir/=!dir;
		
		head=dir.yangle();
		dir.yrotate(-head);
		pitch=dir.zangle();
	}

	dir=GL_VECTOR3D(1.0f,0.0f,0.0f);
	dir.zrotate(pitch);
	dir.yrotate(head);

	up=GL_VECTOR3D(0.0f,1.0f,0.0f);
	up.xrotate(roll);
	up.zrotate(pitch);
	up.yrotate(head);

	gluLookAt(pos.x,pos.y,pos.z,pos.x+dir.x,pos.y+dir.y,pos.z+dir.z,up.x,up.y,up.z);
	glGetFloatv(GL_MODELVIEW_MATRIX,mtx);
}

void GL_CAMERA::updview()
{
	glViewport(vp_x,vp_y,vp_w,vp_h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	_dy=zs*tanf(radf(fov/2.0f));
	_dx=_dy*(vp_h?(float)vp_w/(float)vp_h:1.0f);
	_nx_x=zs/sqrtf(zs*zs+_dx*_dx);
	_nx_z=_dx/sqrtf(zs*zs+_dx*_dx);
	_ny_y=zs/sqrtf(zs*zs+_dy*_dy);
	_ny_z=_dy/sqrtf(zs*zs+_dy*_dy);

	glFrustum(-_dx,_dx,-_dy,_dy,zs,ze);
	glMatrixMode(GL_MODELVIEW);
}


void GL_LIGHTSRC::reset()
{
	pos=GL_VECTOR3D(0.0f,0.0f,0.0f);
	pos_4=1.0f;
	s_dir=GL_VECTOR3D(0.0f,0.0f,-1.0f);
	s_target=NULL;
	s_exp=0.0f;
	s_ang=180.0f;
	c_atn=1.0f;
	l_atn=0.0f;
	q_atn=0.0f;
	ints=1.0f;
	
	amb=GL_RGBACOLOR(0.2f,0.2f,0.2f,1.0f);
	dif=GL_RGBACOLOR(1.0f,1.0f,1.0f,1.0f);
	spec=GL_RGBACOLOR(0.2f,0.2f,0.2f,1.0f);
	ints_amb=1.0f;
	ints_dif=1.0f;
	ints_spec=1.0f;
	updall();
}

void GL_LIGHTSRC::updspot()
{
	if(s_target)
	{
		s_dir=*s_target-pos;
		s_dir=(s_dir/!s_dir)+pos;
	}

	glLightfv(ID,GL_SPOT_DIRECTION,(float*)&s_dir);
	glLightf(ID,GL_SPOT_EXPONENT,s_exp);
	glLightf(ID,GL_SPOT_CUTOFF,s_ang);
	glLightf(ID,GL_CONSTANT_ATTENUATION,c_atn);
	glLightf(ID,GL_LINEAR_ATTENUATION,l_atn);
	glLightf(ID,GL_QUADRATIC_ATTENUATION,q_atn);
}

void GL_LIGHTSRC::updcolor()
{
	GL_RGBACOLOR _amb,_dif,_spec;

	_amb=(ints*ints_amb)*amb;
	_dif=(ints*ints_dif)*dif;
	_spec=(ints*ints_spec)*spec;

	glLightfv(ID,GL_AMBIENT,(float*)&amb);
	glLightfv(ID,GL_DIFFUSE,(float*)&dif);
	glLightfv(ID,GL_SPECULAR,(float*)&spec);
}

void GL_LIGHTSRC::updall()
{	
	GL_RGBACOLOR _amb,_dif,_spec;

	if(s_target)
	{
		s_dir=*s_target-pos;
		s_dir=(s_dir/!s_dir);
	}

	_amb=(ints*ints_amb)*amb;
	_dif=(ints*ints_dif)*dif;
	_spec=(ints*ints_spec)*spec;

	glLightfv(ID,GL_POSITION,(float*)&pos);
	glLightfv(ID,GL_SPOT_DIRECTION,(float*)&s_dir);
	glLightf(ID,GL_SPOT_EXPONENT,s_exp);
	glLightf(ID,GL_SPOT_CUTOFF,s_ang);
	glLightf(ID,GL_CONSTANT_ATTENUATION,c_atn);
	glLightf(ID,GL_LINEAR_ATTENUATION,l_atn);
	glLightf(ID,GL_QUADRATIC_ATTENUATION,q_atn);
	glLightfv(ID,GL_AMBIENT,(float*)&_amb);
	glLightfv(ID,GL_DIFFUSE,(float*)&_dif);
	glLightfv(ID,GL_SPECULAR,(float*)&_spec);	
}


GL_CAMERA glCam;
GL_LIGHTSRC glLight[GL_MAXLIGHTSRC];


GL_FOGVECTOR glFog;
GL_RGBACOLOR glBgColor;
GL_RGBACOLOR glAmbLight;


void glSetFog(float d,float zs,float ze)
{
	glFog.d=d;
	glFog.zs=zs;
	glFog.ze=ze;
	glUpdFog();
}

void glUpdFog()
{
	glFogf(GL_FOG_DENSITY,glFog.d);
	glFogf(GL_FOG_START,glFog.zs);
	glFogf(GL_FOG_END,glFog.ze);
}

void glSetBgColor(float r,float g,float b,float a)
{
	glBgColor.r=r;
	glBgColor.g=g;
	glBgColor.b=b;
	glBgColor.a=a;
	glUpdBgColor();
}

void glUpdBgColor()
{
	glClearColor(glBgColor.r,glBgColor.g,glBgColor.b,glBgColor.a);
	glFogfv(GL_FOG_COLOR,(float*)&glBgColor);
}

void glSetAmbLight(float r,float g,float b,float a)
{
	glAmbLight.r=r;
	glAmbLight.g=g;
	glAmbLight.b=b;
	glAmbLight.a=a;
	glUpdAmbLight();
}

void glUpdAmbLight()
{
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,(float*)&glAmbLight);	
}


// struct {
// 	__int64 freq;
// 	float res;
// 	unsigned long mmt_start;       
// 	unsigned long mmt_elapsed;
// 	bool pt;    
// 	__int64 pt_start;
// 	__int64 pt_elapsed;
// } _gltimer;	

void _glinittimer()
{
    //  memset(&_gltimer,0,sizeof(_gltimer));   

    //  if(!QueryPerformanceFrequency((LARGE_INTEGER*)&_gltimer.freq))
    //  {
    //       _gltimer.pt=false;
    //       _gltimer.mmt_start=timeGetTime();
    //       _gltimer.res=1.0f/1000.0f;
    //       _gltimer.freq=1000;   
    //       _gltimer.mmt_elapsed=_gltimer.mmt_start;
    //  }
    //  else
    //  {
    //       QueryPerformanceCounter((LARGE_INTEGER*)&_gltimer.pt_start);
    //       _gltimer.pt=true;
    //       _gltimer.res=(float)(((double)1.0f)/((double)_gltimer.freq));
    //       _gltimer.pt_elapsed=_gltimer.pt_start;
    //  }
}

#include <time.h>
float glTime()
{
    //  __int64 time;        

    //  if (_gltimer.pt)
    //  {
    //       QueryPerformanceCounter((LARGE_INTEGER*)&time);
    //       return ((float)(time-_gltimer.pt_start)*_gltimer.res)*1000.0f;
    //  }
    //  else
    //       return ((float)(timeGetTime()-_gltimer.mmt_start)*_gltimer.res)*1000.0f;
    timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
	return t.tv_sec*1000.0+t.tv_nsec/1000000.0;
}

float glFrameTime=0.0f;


void glInitEnv()
{
	UINT i;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glCam.reset();

	glSetFog(25.0f,10.0f,60.0f);
	glSetBgColor(0.0f,0.0f,0.0f,0.5f);
	glSetAmbLight(0.2f,0.2f,0.2f,1.0f);
	
	glClearDepth(1.0f);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,1.0f);
	glFogi(GL_FOG_MODE,GL_LINEAR);	
	glHint(GL_FOG_HINT,GL_NICEST);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
	glDepthFunc(GL_LEQUAL);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glAlphaFunc(GL_GREATER,0);
	glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_SPHERE_MAP);
	glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_SPHERE_MAP);

	for(i=0;i<GL_MAXLIGHTSRC;i++)
	{
		glLight[i].ID=GL_LIGHT0+i;
		glLight[i].reset();
	}
		
	_glinittimer();
	glSetFps(30.0f);

}


void glSetRenderMode(UINT mode,float size)
{
	switch(mode)
	{
	case GLR_POINTS:
		glPolygonMode(GL_FRONT_AND_BACK,GL_POINT);
		glPointSize(size);
		break;

	case GLR_LINES:
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		glLineWidth(size);
		break;

	case GLR_FILLED:
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		break;
	}
}
