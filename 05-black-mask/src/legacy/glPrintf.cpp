#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <stdio.h>
#include <stdarg.h>
#include "glEnv.h"
#include "glPrintf.h"

static bool zbuf_enabled=false;

void glTextModeOn(float scale)
{
	if(glIsZBufOn())
	{
		zbuf_enabled=true;
		glZBufOff();
	}
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0f,scale,0.0f,scale,0.1f,-scale);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

}

void glTextModeOff()
{
	if(zbuf_enabled)
	{
		zbuf_enabled=false;
		glZBufOn();
	}
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}


void GL_FONT::_print(float x,float y,float z,char *text)
{
	int l,i;
	UCHAR c;
	float tx,ty;
	float w2,h2;
	float dx;


	l=strlen(text);

	if(halign==GLF_CENTER)
		x-=(float)l*width/2.0f*hspace;
	else 
		if(halign==GLF_LEFT)
			x-=(float)l*width*hspace;

	dx=hspace*width;
	x+=dx/2.0f;
	w2=(float)width/2.0f;
	h2=(float)height/2.0f;
	
	glBegin(GL_TRIANGLES);
	for(i=0;i<l;i++,x+=dx)
	{
		c=text[i];

		tx=(c%16)*0.0625f;
		ty=1.0f-(c/16)*0.0625f;

		glTexCoord2f(tx+0.0620f,ty-0.0005f);
		glVertex3f(x+w2,y+h2,z);
		glTexCoord2f(tx+0.0005f,ty-0.0005f);
		glVertex3f(x-w2,y+h2,z);
		glTexCoord2f(tx+0.0620f,ty-0.0620f);
		glVertex3f(x+w2,y-h2,z);

		glTexCoord2f(tx+0.0620f,ty-0.0620f);
		glVertex3f(x+w2,y-h2,z);
		glTexCoord2f(tx+0.0005f,ty-0.0005f);
		glVertex3f(x-w2,y+h2,z);
		glTexCoord2f(tx+0.0005f,ty-0.0620f);
		glVertex3f(x-w2,y-h2,z);	
	}
	glEnd();

}


void GL_FONT::init(UINT _face,float _width,float _height)
{
	face=_face;
	width=_width;
	height=_height;
	halign=0;
	valign=0;
	hspace=0.5f;
	vspace=1.0f;
	color.r=1.0f;
	color.g=1.0f;
	color.b=1.0f;
	color.a=1.0f;
}

void GL_FONT::printf(float x,float y,char *text,...)
{
	char buf[256],buf2[256];
	va_list vlist;
	int n_line;
	int l;
	int i,wi;
	float dy;


	va_start(vlist,text);
	vsprintf(buf,text,vlist);
	va_end(vlist);

	glBindTexture(GL_TEXTURE_2D,face);
	glColor4fv((float*)&color);
	
	l=strlen(buf);
	for(i=0,n_line=1;i<l;i++)
		if(buf[i]==10)
			n_line++;
	
	if(valign==GLF_CENTER)
		y+=(float)n_line*height/2.0f*vspace;
	else 
		if(valign==GLF_BOTTOM)
			y+=(float)n_line*height*vspace;
	
	dy=-height*vspace;
	y+=dy/2.0f;

	for(i=0,wi=0;i<=l;i++)
		if(buf[i]==10||buf[i]==0)
		{
			if(i>wi)
			{
				strncpy(buf2,(char*)(buf+wi),i-wi);
				buf2[i-wi]=0;
				_print(x,y,0,buf2);
			}
			y+=dy;
			wi=i+1;
			if(buf[i]==0)
				break;
		}	
}

void GL_FONT::printf(GL_VECTOR3D p,GL_VECTOR3D r,char *text,...)
{
	char buf[256],buf2[256];
	va_list vlist;
	int n_line;
	int l;
	int i,wi;
	float dy;
	GL_VECTOR3D v(0.0f);


	va_start(vlist,text);
	vsprintf(buf,text,vlist);
	va_end(vlist);

	glBindTexture(GL_TEXTURE_2D,face);
	glColor4fv((float*)&color);
	
	l=strlen(buf);
	for(i=0,n_line=1;i<l;i++)
		if(buf[i]==10)
			n_line++;
	
	if(valign==GLF_CENTER)
		v.y+=(float)n_line*height/2.0f*vspace;
	else 
		if(valign==GLF_BOTTOM)
			v.y+=(float)n_line*height*vspace;
	
	dy=-height*vspace;
	v.y+=dy/2.0f;

	glPushMatrix();
	if(p.x!=0.0f||p.y!=0.0f||p.z!=0.0f)
		glTranslatef(p.x,p.y,p.z);
	if(r.x!=0.0f)
		glRotatef(r.x,1.0f,0.0f,0.0f);
	if(r.y!=0.0f)
		glRotatef(r.y,0.0f,1.0f,0.0f);
	if(r.z!=0.0f)
		glRotatef(r.z,0.0f,0.0f,1.0f);

	for(i=0,wi=0;i<=l;i++)
		if(buf[i]==10||buf[i]==0)
		{
			if(i>wi)
			{
				strncpy(buf2,(char*)(buf+wi),i-wi);
				buf2[i-wi]=0;
				_print(v.x,v.y,v.z,buf2);
			}
			v.y+=dy;
			wi=i+1;
			if(buf[i]==0)
				break;
		}	

	glPopMatrix();
}
