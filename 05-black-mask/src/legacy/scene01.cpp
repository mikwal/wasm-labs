#include "main.h"
#include "scene.h"

GL_OBJECT land;
GL_OBJECT meat;
GL_BONE mbone;
GL_PLASMABALL burnout;


static float blend_screen=3.0f;

static GL_PATH cpath;
static GL_VECTOR3D camtarget_src(0.0f,10.0f,0.0f);
static GL_VECTOR3D camtarget;

static float jaw_v=0.0f;
static float jaw_dv=PI/40.0f;
static GL_VECTOR3D meat_vel;
static float meat_bounce=0.0f;

static GL_FONT font,flash_font;


#define N_INTRO_TEXT 5
static char intro_text[N_INTRO_TEXT][128]={
"Released at DreamHack 2000",
"-The Attack of the mutant meatball-\n\n"
"starring:  Black Mask",
"coded by:  GoaWay",
"soundtrack:  extreme - H2O",
"enjoy the show =)"
};
static UINT intro_text_index=0;
static float intro_text_alpha=0.0f;
static float intro_text_fade=0.025f;

static float delay=0.0f;
#define OUTRO_TEXT_DELAY 75.0f
static char outro_text[256]="";
static char outro_text_src[256]=
"A large evil mutant meatball\n"
"from mars has landed on earth\n"
"and now it wants to eat up the\n"
"whole planet.\n\n"
"There is only one man who has\n"
"the power to stop it...";




void print_flashtext(char *s,float a)
{
	UINT i;

	for(i=0;i<8;i++)
	{
		flash_font.setcolor(0.0f,1.0f,0.0f,randf()*a/3.0f);
		flash_font.printf(50.0f+(randf()-randf())*1.5f,20.0f+(randf()-randf())*1.5f,s);
	}
	flash_font.setcolor(1.0f,1.0f,0.5f,a);
	flash_font.printf(50.0f,20.0f,s);
}


void s1load()
{
	land.load("land.glo");
	cpath.load("land.pth");
	
	meat.load("meatball.glo");
	mbone.load("meatball.bone");
	mbone.attach(meat,0.25f,2.5f,GLB_XYPLANE);
	burnout.create(100);

}

void s1free()
{
	land.unload();
	cpath.unload();
	meat.unload();
	mbone.unload();
	burnout.free();
}

void s1init()
{
	glCFaceOn();
	glZBufOn();
	glTextureOn();
	glAlphaOn();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glFogOn();
	glLightOn();
	glSetBgColor(0.1f,0.1f,0.2f,1.0f);
	glSetFog(0.33f,25.0f,250.0f);

	glCam.reset();
	glCam.zs=0.75f;
	glCam.ze=250.0f;
	glCam.settarget(&camtarget);
	glCam.updview();

	glLight[0].on();
	glLight[0].reset();
	glLight[0].dif=GL_RGBACOLOR(0.75f,0.75f,0.75f,1.0f);
	glLight[0].amb=GL_RGBACOLOR(0.25f,0.25f,0.25f,1.0f);
	glLight[0].spec=GL_RGBACOLOR(0.25f,0.25f,0.25f,1.0f);

	glLight[1].on();
	glLight[1].reset();
	glLight[1].pos=GL_VECTOR3D(0.0f,2.0f,0.0f);
	glLight[1].dif=GL_RGBACOLOR(0.25f,0.0f,0.75f,1.0f);
	glLight[1].amb=GL_RGBACOLOR(0.0f,0.0f,0.0f,0.0f);
	glLight[1].spec=GL_RGBACOLOR(0.25f,0.0f,0.75f,1.0f);
	glLight[1].c_atn=0.9f;
	glLight[1].l_atn=0.1f;

	burnout.setpos(0.0f,-1.5f,0.0f);
	burnout.setradius(4.5f);
	burnout.setvel_dif(4.5f);
	burnout.setgrav(0.45f);
	burnout.setcol(0.5f,0.2f,1.0f,1.0f);
	burnout.setcol_dif(0.1f,0.1f,0.1f,0.25f);
	burnout.setfade(0.0f,0.0f,0.0f,-0.05f);
	burnout.setfade_dif(0.0f,0.0f,0.0f,0.0125f);
	burnout.init(tf["plasma.bmp"],GLP_REGENERATE,9.0f,100);
	burnout.reset();

	font.init(tf["font2.tga"],5.0f,5.0f);
	font.setspace(0.55f,1.0f);
	font.setsize(4.0f,4.6f);

	flash_font.init(tf["font3.tga"],4.8f,6.4f);
	flash_font.setspace(0.6f,1.0f);
	flash_font.setsize(4.6f,6.9f);
	flash_font.setalign(GLF_CENTER,GLF_CENTER);

	meat.rot.y=-90.0f;
	meat_vel=SECVAL((15.0f*GL_VECTOR3D(sinf(radf(5.0f)),0.0f,cosf(radf(5.0f)))));
}

bool s1move()
{
	UINT i;
	
	if(intro_text_fade!=0.0f)
	{
		intro_text_alpha+=intro_text_fade;
		if(intro_text_alpha<0.0f)
		{
			intro_text_fade=-intro_text_fade;
			intro_text_index++;
			if(intro_text_index>=N_INTRO_TEXT)
			{
				intro_text_index=N_INTRO_TEXT-1;
				intro_text_fade=0.0f;
			}

		}
		if(intro_text_alpha>2.5f)
			intro_text_fade=-intro_text_fade;
	}

	if(meat.pos.z>260.0f)
	{
		if(blend_screen>2.0f)
		{
			mbone.zrotate_last(0,-jaw_v);
			mbone.zrotate_first(1,jaw_v);
			return true;	
		}
		else
			blend_screen+=0.025f;		
	}
	else
	{
		if(blend_screen>0.0f)
		{
			blend_screen-=0.025f;
			if(blend_screen<0.0f)
				blend_screen=0.0f;
			else
				if(blend_screen>=1.0f)
					return false;
		}
		
		cpath.travel(0,0.5f*(1.0f-blend_screen));
	}

	if(burnout.flags&GLP_REGENERATE&&cpath.path[0].c_pt>=24.0f*24.0f)
		burnout.flags&=~GLP_REGENERATE;
	burnout.move();

	if(cpath.path[0].c_pt>=31.0f*24.0f)
	{
		cpath.travel(1,1.0f);
		if(camtarget_src.y>2.0f)
			camtarget_src.y-=0.5f;

		jaw_v+=jaw_dv;
		mbone.zrotate_last(0,jaw_dv);
		mbone.zrotate_first(1,-jaw_dv);

		if(jaw_v>0.0f||jaw_v<-PI/5.0f)
			jaw_dv=-jaw_dv;		

	}
	
	if(cpath.path[0].c_pt>=cpath.path[0].n_pt-1.0f)
	{
		meat_vel.y-=SECVAL(0.92f);
		meat.pos.y+=meat_vel.y;
		
		meat_bounce+=meat_vel.y/20.0f;
		if(meat_bounce>0.0f||meat_vel.y<0.0f)
		{
			meat.pos.x+=meat_vel.x;
			meat.pos.z+=meat_vel.z;
		}
		if(meat_bounce>0.4f)
			meat_bounce=0.4f;
		if(meat_bounce<-0.4f)
			meat_bounce=-0.4f;

		meat.scl.x=1.0f-meat_bounce;
		meat.scl.z=1.0f-meat_bounce;
		meat.scl.y=1.0f+meat_bounce;

		if(meat.pos.y<((450.0f-(meat.pos.z*2.0f))/100.0f)+meat_bounce)
				meat_vel.y=SECVAL((randf()*7.5f+17.5f));
		
		if(meat.pos.z>90.0f)
		{
			glCam.settarget(NULL);
			if(delay>OUTRO_TEXT_DELAY)
			{
				i=strlen(outro_text);

				if(outro_text_src[i])
				{
					outro_text[i]=outro_text_src[i];
					outro_text[i+1]=0;
					if(outro_text_src[i]==10)
						delay=-OUTRO_TEXT_DELAY;
				}
			}
			else delay+=glFrameTime;
		}
	}
	else
		meat.pos=cpath.getpos(1);

	camtarget=camtarget_src+meat.pos;
	glCam.pos=cpath.getpos(0);
	glCam.updmatrix();

	glLight[0].pos=glCam.pos;
	glLight[0].ints=1.5f+randf()*0.5f;
	glLight[0].updall();

	glLight[1].ints=2.0f*(burnout.n_alive()/30.0f)+(randf()*burnout.n_alive()/30.0f);
	glLight[1].updall();
	
	return false;
}



void s1render()
{

	glClear();
	if(blend_screen<1.0f)
	{
		land.render(0,0);
		if(cpath.path[0].c_pt>=31.0f*24.0f)
		{
			meat.render(0,0);
		}
		
		glLightOff();
		glBlendOn();
		glZBufMask(false);
		burnout.render();
		glZBufMask(true);
		glLightOn();
		glBlendOff();

	}
	
	glTextModeOn();
	glLightOff();
	glBlendOn();

	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	if(outro_text[0])
		font.printf(6.0f,92.0f,outro_text);
		
	glTextureOff();
	if(blend_screen>0.0f)
	{
		glColor4f(0.0f,0.0f,0.0f,blend_screen);
		glBegin(GL_TRIANGLE_STRIP);
		glVertex3f(100.0f,100.0f,0.0f);
		glVertex3f(0.0f,100.0f,0.0f);
		glVertex3f(100.0f,0.0f,0.0f);
		glVertex3f(0.0f,0.0f,0.0f);	
		glEnd();
	}
	glTextureOn();
	if(intro_text_alpha>0.0f)
		print_flashtext(intro_text[intro_text_index],(intro_text_alpha>1.0f)?1.0f:intro_text_alpha);
		
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glTextModeOff();
	glBlendOff();
	glLightOn();
	glSwapBuf();
}