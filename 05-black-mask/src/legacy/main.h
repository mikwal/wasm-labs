#ifndef MAIN_H
#define MAIN_H

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glaux.h>

#include "glEnv.h"
#include "glWindow.h"
#include "glObject.h"
#include "glTexture.h"
#include "glParticle.h"
#include "glPrintf.h"

extern float fps;
extern UINT fskip;

extern GL_TEXTUREFILE tf;
extern GL_DATFILE df;


#endif