//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by winmain.rc
//
#define DLG_LOGOBITMAP                  103
#define DLG_CONFIG                      104
#define DLG_MAIN                        104
#define DLG_ICON                        105
#define IDC_CURSOR1                     106
#define DLG_320X200                     1000
#define DLG_320X240                     1001
#define DLG_512X384                     1002
#define DLG_640X400                     1003
#define DLG_640X480                     1004
#define DLG_800X600                     1005
#define DLG_1024X768                    1006
#define DLG_1280X1024                   1007
#define DLG_16BPP                       1008
#define DLG_24BPP                       1009
#define DLG_32BPP                       1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
