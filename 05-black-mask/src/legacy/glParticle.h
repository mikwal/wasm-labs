#ifndef GLPARTICLE_H
#define GLPARTICLE_H


#define GLP_REGENERATE		0x0001
#define GLP_GRAVITY			0x0002
#define GLP_GRAVCENTER		0x0006
#define GLP_BOUNDINGBOX		0x0008
#define GLP_LINEARALPHA		0x0010
#define GLP_SPHEREINIT		0x0020

struct GL_PARTICLE {
	GL_VECTOR3D pos;
	GL_VECTOR3D vel;
	GL_RGBACOLOR col;
	GL_RGBACOLOR fade;
	
	bool alive;
};


class GL_PARTICLESYSTEM {
protected:
	float _pc;

	void _initp(UINT );

public:
	UINT n_p;
	GL_PARTICLE *p;	
	UINT text;
	float size;
	
	UINT p_used;
	UINT p_dead;
	float pps;

	UINT flags;

	GL_VECTOR3D grav;
	GL_BOX3D bbox;
	
	GL_VECTOR3D pos;
	GL_VECTOR3D vel;
	GL_RGBACOLOR col;
	GL_RGBACOLOR fade;
	
	GL_VECTOR3D pos_dif;
	GL_VECTOR3D vel_dif;
	GL_RGBACOLOR col_dif;
	GL_RGBACOLOR fade_dif;

	inline GL_PARTICLESYSTEM() {memset(this,0,sizeof(GL_PARTICLESYSTEM));}
	inline GL_PARTICLESYSTEM(UINT n) {memset(this,0,sizeof(GL_PARTICLESYSTEM));create(n);}
	inline ~GL_PARTICLESYSTEM() {free();}
	inline void create(UINT n) {free();n_p=n;p=new GL_PARTICLE[n];}
	inline void free() {delete [] p;memset(this,0,sizeof(GL_PARTICLESYSTEM));}
	
	inline void init(UINT _text,UINT _flags,float _size,float _pps) {text=_text;flags=_flags;size=_size/2;pps=_pps*(glFrameTime/1000.0f);p_used=p_dead=n_p;}
	inline void reset() {p_used=p_dead=0;_pc=0.0f;}
	inline bool isdead() {return (p_dead>=n_p)?true:false;}
	inline bool isalive() {return (p_dead>=n_p)?false:true;}
	inline UINT n_alive() {return p_used-p_dead;}

	
	void move();
	void render();

	

	inline void setpos(float x,float y,float z) {pos=GL_VECTOR3D(x,y,z);}
	inline void setpos_dif(float x,float y,float z) {pos_dif=GL_VECTOR3D(x,y,z);}
	inline void setvel(float x,float y,float z) {vel=SECVAL(GL_VECTOR3D(x,y,z));}
	inline void setvel_dif(float x,float y,float z) {vel_dif=SECVAL(GL_VECTOR3D(x,y,z));}
	inline void setcol(float r,float g,float b,float a) {col=GL_RGBACOLOR(r,g,b,a);}
	inline void setcol_dif(float r,float g,float b,float a) {col_dif=GL_RGBACOLOR(r,g,b,a);}
	inline void setfade(float r,float g,float b,float a) {fade=SECVAL(GL_RGBACOLOR(r,g,b,a));}
	inline void setfade_dif(float r,float g,float b,float a) {fade_dif=SECVAL(GL_RGBACOLOR(r,g,b,a));}
	
	inline void setgrav(float x,float y,float z) {grav=SECVAL(GL_VECTOR3D(x,y,z));}
	inline void setbbox(float x1,float x2,float y1,float y2,float z1,float z2) {bbox.p1=GL_VECTOR3D(x1,y1,z1);bbox.p2=GL_VECTOR3D(x2,y2,z2);}
	
	void explode(float ,float ,bool );
};


class GL_STARFIELD : public GL_PARTICLESYSTEM {
public:	
	inline GL_STARFIELD() {memset(this,0,sizeof(GL_PARTICLESYSTEM));}
	inline GL_STARFIELD(UINT n) {memset(this,0,sizeof(GL_PARTICLESYSTEM));create(n);}
	inline ~GL_STARFIELD() {free();}

	void init(UINT ,float ,float ,float ,float ,float ,float ,float ,float ,float );
};


class GL_PLASMABALL : public GL_PARTICLESYSTEM {
public:	
	inline GL_PLASMABALL() {memset(this,0,sizeof(GL_PARTICLESYSTEM));}
	inline GL_PLASMABALL(UINT n) {memset(this,0,sizeof(GL_PARTICLESYSTEM));create(n);}
	inline ~GL_PLASMABALL() {free();}

	inline void init(UINT _text,UINT _flags,float _size,float _pps) {text=_text;flags=_flags|GLP_SPHEREINIT|GLP_GRAVCENTER;size=_size/2;pps=_pps*(glFrameTime/1000.0f);p_dead=n_p;}
	inline void setgrav(float _grav) {grav.x=SECVAL(_grav);}
	inline void setradius(float _radius) {pos_dif.x=_radius;}
	inline void setvel(float _vel) {vel_dif.y=SECVAL(_vel);}
	inline void setvel_dif(float _vel_dif) {vel_dif.y=SECVAL(_vel_dif);}
};



#endif