import { WASIModule, WASIModuleImportsProxy, newCharacterDeviceWriter } from '/js/wasi';

declare global {
    interface WASIModuleTypedImports {
        app: ReturnType<typeof getAppImports>;
    }    
}

export async function init(programUrl: string) {
    
    window.onunhandledrejection = event => {
        console.error('Unhandled promise rejection!', event.reason);
    };

    log(`Loading and compiling '${programUrl}'...`, { color: 'yellow' });

    const module = new WASIModule();
    try {
        await module.load(programUrl, { imports: { app: getAppImports(module) }, plugins: ['/js/app'] });
    }
    catch (error) {
        log(`Failed to load and compile '${programUrl}': ${(error as Error).message}`, { color: 'red' });
        return;
    }

    log(`Done, running program...`, { color: 'yellow' });

    module.stdout = newCharacterDeviceWriter(s => log(s, { noNewLine: true }));
    module.stderr = newCharacterDeviceWriter(s => log(s, { noNewLine: true, color: 'red' }));
    module.argv = ["hello", "world", "!!!"];
    module.env = { 'PATH': '/usr/bin', 'USER': 'mikwal' };

    const code = await module.start();

    log(`Program exited with code ${code}.`, { color: 'yellow' });
}

export async function applyProxyImports(imports: WASIModuleImportsProxy) {
    const originalExit = imports.sys.exit;
    imports.sys.exit = (code: number) => {
        console.warn(`Exit called with code ${code}`);
        return originalExit(code);
    };
}

function repositionConsoleInput() {
    const consoleElement = document.getElementById('console')!;
    const consoleInputElement = document.getElementById('console-input')!;
    consoleInputElement.remove();
    consoleElement.appendChild(consoleInputElement);
    consoleInputElement.querySelector('input')!.focus();
}

function log(message: string, options?: { color?: 'yellow' | 'red'; noNewLine?: boolean }) {
    const consoleElement = document.getElementById('console')!;
    const messageElement = document.createElement('span')!;
    const { color, noNewLine } = options || {};
    
    messageElement.innerText = message + (noNewLine ? '' : '\n');
    color && (messageElement.className = color);

    consoleElement.appendChild(messageElement);
    repositionConsoleInput();
    consoleElement.scrollTo({ top: consoleElement.scrollHeight - consoleElement.clientHeight });
}

let t0 = 0, n = 0;

function getAppImports(module: WASIModule) {
    const canvasContextInstances: CanvasRenderingContext2D[] = [];
    return {
        'add_progress_bar': (rgb: number) => {
            const consoleElement = document.getElementById('console')!;
            const wrapperElement = document.createElement('span')!;
            const canvasElement = document.createElement('canvas')!;
        
            canvasElement.width = 10000;
            canvasElement.height = 100;
            canvasElement.style.width = '100%';
            canvasElement.style.height = '20px';
            canvasElement.style.backgroundColor = 'gray';
            canvasElement.style.marginTop = '4px';
            canvasElement.style.marginBottom = '2px';
        
            wrapperElement.appendChild(canvasElement);
            consoleElement.appendChild(wrapperElement);
            repositionConsoleInput();
            consoleElement.scrollTo({ top: consoleElement.scrollHeight - consoleElement.clientHeight });
            
            const canvasContext = canvasElement.getContext('2d')!;
            canvasContext.fillStyle = `#${new Number(rgb).toString(16).padStart(6, '0').substr(-6, 6)}`;

            const handle = canvasContextInstances.length;
            canvasContextInstances.push(canvasContext)
            return handle;
        },
    
        'update_progress_bar': (handle: number, progress: number) => {
            const canvasContext = canvasContextInstances[handle];
            if (canvasContext) {
                const x = Math.max(0, Math.min(progress, 100))*100;
                canvasContext.clearRect(x, 0, 10000 - x, 100);
                canvasContext.fillRect(0, 0, x, 100);
            }
            n++;
        },

        'perf': (x: number) => {
            if (x === 0) {
                t0 = performance.now();
                n = 0;
            }
            console.log(`[${x ? (performance.now() - t0).toFixed(3) : 0}] draw(${n})`);
        },
        'i64_add': (a: bigint, b: bigint) => {
            const c = a + b;
            console.log('log', 'info', `i64_add(${a}, ${b}) = ${c}`);
            return c;
        },
        'trigger_callback': (callback: number) => {
            const unwrappedCallback = module.unwrapCallback(callback);
            setTimeout(unwrappedCallback, 2000);
        }
    };
}

