#include <cstdio>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> 
#include <time.h>
#include <fcntl.h>

#include <browser.h>

typedef long long L64;

extern "C" {
    int add_progress_bar(int rgb) __attribute__((__import_module__("app"), __import_name__("add_progress_bar")));
    void update_progress_bar(int handle, double progress) __attribute__((__import_module__("app"), __import_name__("update_progress_bar")));
    int perf(int i) __attribute__((__import_module__("app"), __import_name__("perf")));
    void trigger_callback(void (*callback)()) __attribute__((__import_module__("app"), __import_name__("trigger_callback")));
    L64 i64_add(L64 a, L64 b) __attribute__((__import_module__("app"), __import_name__("i64_add")));
}

struct X {
    X() {
        printf("X();\n");
    }
    ~X() {
        printf("~X();\n");
    }
};

static X x;

void callback() {
    printf("In callback!\n");
}

int main(int argc, char** argv) {

    timespec t;
    clock_gettime(CLOCK_REALTIME, &t);

    fprintf(stderr, "CLOCK\t=\t%f\n", t.tv_sec*1000.0+t.tv_nsec/1000000.0);
    
    printf("$PATH\t=\t%s\n", getenv("PATH"));
    printf("$USER\t=\t%s\n", getenv("USER"));

    printf("FDFLAG(0) = 0x%08x\n", fcntl(0, F_GETFL));
    printf("FDFLAG(1) = 0x%08x\n", fcntl(1, F_GETFL));
    printf("FDFLAG(2) = 0x%08x\n", fcntl(2, F_GETFL));
    for (auto i = 0; i < argc; i++)
        printf("argv[%d]\t=\t%s\n", i, argv[i]);

    trigger_callback(callback);

    browser::wait_for_interrupt();

    long long l = 0xFFFFFFFFFFFFFFFF;
    printf("0x%016llx\n", l);
    l = i64_add(l, 2);
    printf("0x%016llx\n", l);

    auto filename = "/test/a.txt";

    browser::load_file_async(filename);
    browser::wait_for_async();
/*
    int dir = chdir("/$localStorage/");
    printf("chdir(\"$localStorage\") = 0x%08x\n", dir);
    auto *f1 = fopen("/$localStorage/newfile.txt", "w");
    if (f1) {
        fprintf(f1, "Hello World.");
        fclose(f1);
        printf("Wrote file \"newfile.txt\".");
    }*/

    __proxy_batch_mode(true);

    FILE *f = fopen(filename, "rb");
    if (f) {
        char buffer[1024];
        size_t nread = fread(buffer, 1, sizeof(buffer) - 1, f);
        buffer[nread] = '\0';
        printf("Content of file '%s' (%lu bytes): %s\n", filename, nread, buffer);
    }
    else {
        printf("File '%s' does not exist\n", filename);
    }
    browser::remove_file(filename);

    auto progress_bar_handle = add_progress_bar(0xFF0000);

    auto N = 1000;
    __proxy_batch_mode(true);
    auto tX = browser::wait_for_animation_frame();
    perf(0);
    for (int i = 0; i < N; i++) {
        update_progress_bar(progress_bar_handle, i/(double)N);
    }
    perf(1);
    __proxy_batch_mode(false);
    double t0 = browser::wait_for_animation_frame();
    perf(0);
    for (int i = 0; i < N; i++) {
        update_progress_bar(progress_bar_handle, i/(double)N);
    }
    perf(1);
    update_progress_bar(progress_bar_handle, 0);
    double time = 0;
    const auto max_run_time = 4000.0;
    printf("tX=%f, t0=%f\n", tX, t0);
    for (;;) {
        if (time > max_run_time) {
            update_progress_bar(progress_bar_handle, 100.0);
            exit(1);
        }
        update_progress_bar(progress_bar_handle, 100.0*time/max_run_time);
        time = browser::wait_for_animation_frame() - t0;
    }
}