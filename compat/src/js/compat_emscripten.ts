import { WASIMemory, WASIModuleExportsProxy, WASIModuleImportsProxy, getProxyReturnChannel } from '/js/wasi';

const JS_CODE_ARG_I = 'i'.charCodeAt(0);
const JS_CODE_ARG_J = 'j'.charCodeAt(0);
const JS_CODE_ARG_P = 'p'.charCodeAt(0);

export function applyProxyImports(imports: WASIModuleImportsProxy, memory: WASIMemory) { 
    const jsCode: Record<number, (...args: any[]) => any> = {};
    const jsCodeScope = {
        Module: {
            cwrap(ident: number) {
                const exports = getProxyReturnChannel(imports) as WASIModuleExportsProxy;
                const fn = exports[ident];
                if (!fn) {
                    throw new Error(`cwrap('${ident}'): function not exported`);
                }
                return fn;
            }
        },
        get HEAP8() { return memory.views.int8; },
        get HEAP16() { return memory.views.int16; },
        get HEAP32() { return memory.views.int32; },
        get HEAP64() { return memory.views.int64; },
        get HEAPU8() { return memory.views.uint8; },
        get HEAPU16() { return memory.views.uint16; },
        get HEAPU32() { return memory.views.uint32; },
        get HEAPU64() { return memory.views.uint64; },
        get HEAPF32() { return memory.views.float32; },
        get HEAPF64() { return memory.views.float64; }
    };
    const jsCodeArgs: (number | bigint)[] = [];

    imports.compat_emscripten = {
        log(s, n) {
            console.log(memory.readString(s, n));
        },
        js_code(code, sig, argbuf) {
            if (!(code in jsCode)) {
                jsCode[code] = createJSCodeFunction(code, sig);
            }
            return jsCode[code](...unpackJSCodeArgs(sig, argbuf));
        }
    };

    function createJSCodeFunction(code: number, sig: number) {
        const codeString = memory.readNullTerminatedString(code)!;
        const sigString = memory.readNullTerminatedString(sig)!;
        const args = sigString.split('').map((_, index) => `$${index}`);
        const body = (`let \{${Object.keys(jsCodeScope).join(', ')}\} = this;`) + codeString;
        return new Function(...args, body).bind(jsCodeScope);
    }

    function unpackJSCodeArgs(sig: number, argbuf: number) {
        jsCodeArgs.length = 0;
        let type;
        while (type = memory.views.uint8[sig++]) {
            let stride = type === JS_CODE_ARG_I || type === JS_CODE_ARG_P 
                ? Int32Array.BYTES_PER_ELEMENT 
                : Float64Array.BYTES_PER_ELEMENT;
            argbuf += (stride - (argbuf % stride)) % stride;
            let value;
            switch (type) {
                case JS_CODE_ARG_I:
                case JS_CODE_ARG_P:
                    value = memory.readInt32(argbuf);
                    break;
                case JS_CODE_ARG_J:
                    value = memory.readInt64(argbuf);
                    break;
                default:
                    value = memory.readFloat64(argbuf);
                    break;
                
            }
            jsCodeArgs.push(value);
            argbuf += stride;
        }
        return jsCodeArgs;
    }
}
