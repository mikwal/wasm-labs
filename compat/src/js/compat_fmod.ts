
import { Mutex, WASIMemory, WASIModule, WASIModuleExports, WASIModuleImports, WASIModuleImportsProxy } from '/js/wasi';

const SHIM_ID = Symbol('SHIM_ID');
const SHIM_CALLBACKS_MAP = Symbol('SHIM_CALLBACKS_MAP');
const SHIM_AUDIO_BUFFER_READY = Symbol('SHIM_AUDIO_BUFFER_READY');
const SHIM_AUDIO_BUFFER_DATA = Symbol('SHIM_AUDIO_BUFFER_DATA');
const SHIM_AUDIO_BUFFER_TEMP = Symbol('SHIM_AUDIO_BUFFER_TEMP');
const SHIM_AUDIO_CONTEXT_STATES = ['suspended', 'running', 'closed'];


export function applyImports(imports: WASIModuleImports, module: WASIModule) {
    const shimTargets = new ShimInstancesMap();
    
    imports.compat_fmod = {
        __AudioContext_new(sampleRate: number) { return shimTargets.add(new AudioContext({ sampleRate: n2u(sampleRate) })); },
        __AudioContext_destination(context: number) { return shimTargets.add(shimTargets.get(context, AudioContext).destination); },
        __AudioContext_currentTime(context: number) { return shimTargets.get(context, AudioContext).currentTime; },
        __AudioContext_sampleRate(context: number) { return shimTargets.get(context, AudioContext).sampleRate; },
        __AudioContext_state(context: number) { return SHIM_AUDIO_CONTEXT_STATES.indexOf(shimTargets.get(context, AudioContext).state); },
        __AudioContext_createScriptProcessor(context: number, bufferSize: number, numberOfInputChannels: number, numberOfOutputChannels: number) {
            return shimTargets.add(
                shimTargets.get(context, AudioContext).
                    createScriptProcessor(
                        n2u(bufferSize), 
                        n2u(numberOfInputChannels), 
                        n2u(numberOfOutputChannels)
                )
            );
        },
        __AudioContext_baseLatency(context: number) { return shimTargets.get(context, AudioContext).baseLatency; },
        __AudioContext_outputLatency(context: number) { return shimTargets.get(context, AudioContext).outputLatency; }, 
        __AudioContext_close(context: number) { return shimTargets.get(context, AudioContext).close(); },
        __AudioContext_resume(context: number) { return shimTargets.get(context, AudioContext).resume(); },
        __AudioContext_suspend(context: number) { return shimTargets.get(context, AudioContext).suspend(); },
        __ScriptProcessorNode_connect(scriptProcessorNode: number, audioNode: number, output: number, input: number) { 
            return shimTargets.get(scriptProcessorNode, ScriptProcessorNode).connect(
                shimTargets.get(audioNode, AudioNode),
                n2u(output),
                n2u(input)
            ); 
        },
        __ScriptProcessorNode_bufferSize(scriptProcessorNode: number) { return shimTargets.get(scriptProcessorNode, ScriptProcessorNode).bufferSize; },
        __ScriptProcessorNode_onaudioprocess(scriptProcessorNode: number, callback: number) { 
            shimTargets.get(scriptProcessorNode, ScriptProcessorNode).onaudioprocess = function (ev: AudioProcessingEvent) {
                const { memory } = module;
                const { outputBuffer, inputBuffer, playbackTime } = ev;
 
                const copyAudioBuffersAndTriggerShimCallback = () => {
                    let data = this[SHIM_AUDIO_BUFFER_DATA];
                    Mutex.syncBusyWait(memory.views.int32, data / Int32Array.BYTES_PER_ELEMENT, () => {
                        data += Int32Array.BYTES_PER_ELEMENT;
                        memory.writeUint16(data, (inputBuffer && inputBuffer.numberOfChannels) || 0);
                        data += Uint16Array.BYTES_PER_ELEMENT;
                        memory.writeUint16(data, (outputBuffer && outputBuffer.numberOfChannels) || 0);
                        data += Uint16Array.BYTES_PER_ELEMENT;
                        memory.writeFloat64(data, playbackTime);
                        data += Float64Array.BYTES_PER_ELEMENT;
                        if (inputBuffer && inputBuffer.numberOfChannels > 0) {
                            memory.writeUint32(data, inputBuffer.length);
                            data += Uint32Array.BYTES_PER_ELEMENT;
                            memory.writeUint32(data, inputBuffer.sampleRate);
                            data += Uint32Array.BYTES_PER_ELEMENT;    
                            for (let channel = 0; channel < inputBuffer.numberOfChannels; channel++) {
                                inputBuffer.copyFromChannel(this[SHIM_AUDIO_BUFFER_TEMP], channel);
                                memory.writeFloat32v(this[SHIM_AUDIO_BUFFER_TEMP].subarray(0, inputBuffer.length), data);
                                data += inputBuffer.length * Float32Array.BYTES_PER_ELEMENT;
                            }
                        }
                        if (outputBuffer && outputBuffer.numberOfChannels > 0) {
                            memory.writeUint32(data, outputBuffer.length);
                            data += Uint32Array.BYTES_PER_ELEMENT;
                            memory.writeUint32(data, outputBuffer.sampleRate);
                            data += Uint32Array.BYTES_PER_ELEMENT;  
                            if (this[SHIM_AUDIO_BUFFER_READY]) {
                                for (let channel = 0; channel < outputBuffer.numberOfChannels; channel++) {
                                    this[SHIM_AUDIO_BUFFER_TEMP].set(memory.readFloat32v(data, outputBuffer.length));
                                    outputBuffer.copyToChannel(this[SHIM_AUDIO_BUFFER_TEMP].subarray(0, outputBuffer.length), channel);
                                    data += outputBuffer.length * Float32Array.BYTES_PER_ELEMENT;
                                }
                            }
                        }
                        this[SHIM_AUDIO_BUFFER_READY] = false;
                        module.exports.__ScriptProcessorNode_onaudioprocess(callback, this[SHIM_AUDIO_BUFFER_DATA]).
                            then(() => { 
                                this[SHIM_AUDIO_BUFFER_READY] = true; 
                            });
                    });
                };

                if (!this[SHIM_AUDIO_BUFFER_DATA]) {
                    const dataSize = 
                        (1 * Int32Array.BYTES_PER_ELEMENT) + 
                        (2 * Int16Array.BYTES_PER_ELEMENT) + 
                        (1 * Float64Array.BYTES_PER_ELEMENT) + 
                        (inputBuffer && inputBuffer.numberOfChannels > 0 
                            ? (
                                (2 * Int32Array.BYTES_PER_ELEMENT) +
                                (inputBuffer.numberOfChannels * inputBuffer.length * Float32Array.BYTES_PER_ELEMENT)
                            )
                            : 0
                        ) +
                        (outputBuffer && outputBuffer.numberOfChannels > 0 
                            ? (
                                (2 * Int32Array.BYTES_PER_ELEMENT) +
                                (outputBuffer.numberOfChannels * outputBuffer.length * Float32Array.BYTES_PER_ELEMENT)
                            )
                            : 0
                        );
                    const audioBufferTempSize = Math.max(
                        (inputBuffer && inputBuffer.length) || 0, 
                        (outputBuffer && outputBuffer.length) || 0
                    );
                    this[SHIM_AUDIO_BUFFER_TEMP] = new Float32Array(audioBufferTempSize);
                    module.memory.malloc(dataSize).
                        then(data => { this[SHIM_AUDIO_BUFFER_DATA] = data; }).
                        then(() => copyAudioBuffersAndTriggerShimCallback());
                    return;
                }
                if (!this[SHIM_AUDIO_BUFFER_READY]) {
                    console.warn('__ScriptProcessorNode_onaudioprocess: buffer not ready');
                }
                copyAudioBuffersAndTriggerShimCallback();
            };
        },
    };

    function n2u(value: number) {
        return value < 0 ? undefined : value;
    }
}

export function applyProxyImports(imports: WASIModuleImportsProxy & { [SHIM_CALLBACKS_MAP]: ShimInstancesMap }, memory: WASIMemory) {
    const { compat_fmod } = imports;
    
    imports[SHIM_CALLBACKS_MAP] = new ShimInstancesMap();
    delete imports.compat_fmod;

    class Shim {
        static #instances: WeakRef<Shim>[] = [];
        constructor(ctorName: string, ...ctorArgs: any[]) {
            Shim.#instances.push(new WeakRef(this));
            this[SHIM_ID] = compat_fmod[ctorName](...ctorArgs);
        }
        readonly [SHIM_ID]: number;
    }

    class AudioContextShim extends Shim {
        #destination;
        constructor(options: { sampleRate?: number } = {}) {
            super('__AudioContext_new', u2n(options.sampleRate));
            this.#destination = new AudioDestinationNodeShim(this);
        }
        get destination() { return this.#destination; }
        get currentTime() { return compat_fmod.__AudioContext_currentTime(this[SHIM_ID]); }
        get sampleRate() { return compat_fmod.__AudioContext_sampleRate(this[SHIM_ID]); }
        get state() { return SHIM_AUDIO_CONTEXT_STATES[compat_fmod.__AudioContext_state(this[SHIM_ID])]; }
        createScriptProcessor(bufferSize: number, numberOfInputChannels: number, numberOfOutputChannels: number) {
            return new ScriptProcessorNodeShim(this, bufferSize, numberOfInputChannels, numberOfOutputChannels);
        }
        get baseLatency() { return compat_fmod.__AudioContext_baseLatency(this[SHIM_ID]); }
        get outputLatency() { return compat_fmod.__AudioContext_outputLatency(this[SHIM_ID]); }
        suspend() { compat_fmod.__AudioContext_suspend(this[SHIM_ID]); }
        resume() { compat_fmod.__AudioContext_resume(this[SHIM_ID]); }
        close() { compat_fmod.__AudioContext_close(this[SHIM_ID]); }
    }
   
    class AudioDestinationNodeShim extends Shim {
        constructor(context: AudioContextShim) { 
            super('__AudioContext_destination', context[SHIM_ID]); 
        }
    }

    class ScriptProcessorNodeShim extends Shim {
        #inputBuffer: AudioBuffer = null!;
        #outputBuffer: AudioBuffer = null!;
        constructor(context: AudioContextShim, bufferSize: number, numberOfInputChannels: number, numberOfOutputChannels: number) { 
            super('__AudioContext_createScriptProcessor', context[SHIM_ID], u2n(bufferSize), u2n(numberOfInputChannels), u2n(numberOfOutputChannels)); 
        }
        connect(destination: AudioDestinationNodeShim, output: number, input: number) { compat_fmod.__ScriptProcessorNode_connect(this[SHIM_ID], destination[SHIM_ID], u2n(output), u2n(input)); }
        get bufferSize() { return compat_fmod.__ScriptProcessorNode_bufferSize(this[SHIM_ID]); }
        set onaudioprocess(listener: (this: ScriptProcessorNodeShim, ev: AudioProcessingEvent) => any) {
            compat_fmod.__ScriptProcessorNode_onaudioprocess(
                this[SHIM_ID],
                imports[SHIM_CALLBACKS_MAP].add((data: number) => Mutex.sync(memory.views.int32, data / Int32Array.BYTES_PER_ELEMENT, () => {
                    data += Int32Array.BYTES_PER_ELEMENT;
                    const inputBufferChannelCount = memory.readUint16(data);
                    data += Int16Array.BYTES_PER_ELEMENT;
                    const outputBufferChannelCount = memory.readUint16(data);
                    data += Int16Array.BYTES_PER_ELEMENT;
                    const playbackTime = memory.readFloat64(data);
                    data += Float64Array.BYTES_PER_ELEMENT;
                    let inputBuffer: AudioBuffer = null!;
                    if (inputBufferChannelCount > 0) {
                        const length = memory.readUint32(data);
                        data += Uint32Array.BYTES_PER_ELEMENT;
                        const sampleRate = memory.readUint32(data);
                        data += Uint32Array.BYTES_PER_ELEMENT;
                        inputBuffer = (this.#inputBuffer || (this.#inputBuffer = new AudioBuffer({ length, sampleRate, numberOfChannels: inputBufferChannelCount })));
                        for (let channel = 0; channel < inputBuffer.numberOfChannels; channel++) {
                            inputBuffer.copyToChannel(memory.views.float32, channel, data / Float32Array.BYTES_PER_ELEMENT);
                            data += inputBuffer.length * Float32Array.BYTES_PER_ELEMENT;
                        }
                    }
                    let outputBuffer: AudioBuffer = null!;
                    if (outputBufferChannelCount > 0) {
                        const length = memory.readUint32(data);
                        data += Uint32Array.BYTES_PER_ELEMENT;
                        const sampleRate = memory.readUint32(data);
                        data += Uint32Array.BYTES_PER_ELEMENT;
                        outputBuffer = (this.#outputBuffer || (this.#outputBuffer = new AudioBuffer({ length, sampleRate, numberOfChannels: outputBufferChannelCount })));
                    }
                    listener.call(this, new AudioProcessingEvent('audioprocess', { outputBuffer, inputBuffer, playbackTime }));
                    if (outputBuffer) {
                        for (let channel = 0; channel < outputBuffer.numberOfChannels; channel++) {
                            outputBuffer.copyFromChannel(memory.views.float32, channel, data / Float32Array.BYTES_PER_ELEMENT);
                            data += outputBuffer.length * Float32Array.BYTES_PER_ELEMENT;
                        }
                    }
                }))
            );
        }
    }

    class AudioBuffer {
        #channels: Float32Array[];

        constructor(options: { length: number, numberOfChannels: number, sampleRate: number }) {
            const { length, numberOfChannels, sampleRate } = options;
            this.length = length;
            this.numberOfChannels = numberOfChannels;
            this.sampleRate = sampleRate;
            this.duration = length / sampleRate;
            this.#channels = [];
            for (let i = 0; i < numberOfChannels; i++) { 
                this.#channels.push(new Float32Array(length)); 
            }
        }

        readonly length: number;
        readonly numberOfChannels: number;
        readonly sampleRate: number;
        readonly duration: number;

        getChannelData(channel: number) { return this.#channels[channel]; }
        copyFromChannel(destination: Float32Array, channel: number, bufferOffset: number) { destination.set(this.#channels[channel], bufferOffset); }
        copyToChannel(source: Float32Array, channel: number, bufferOffset: number) { this.#channels[channel].set(source.subarray(bufferOffset, bufferOffset + this.#channels[channel].length)); }
    }

    class AudioProcessingEvent extends Event {
        constructor(type: string, eventInitDict: EventInit & { outputBuffer: AudioBuffer, inputBuffer: AudioBuffer, playbackTime: number }) {
            super(type, eventInitDict);
            this.outputBuffer = eventInitDict.outputBuffer;
            this.inputBuffer = eventInitDict.inputBuffer;
            this.playbackTime = eventInitDict.playbackTime;
        }

        readonly outputBuffer: AudioBuffer;
        readonly inputBuffer: AudioBuffer; 
        readonly playbackTime: number
    }

    self.window ||= {} as unknown as typeof window;
    self.window.AudioContext = AudioContextShim as unknown as typeof AudioContext;

    function u2n(value: number | undefined): number {
        return value === undefined ? -1 : value;
    }
}

export async function applyProxyExports(imports: WASIModuleImportsProxy & { [SHIM_CALLBACKS_MAP]: ShimInstancesMap }, exports: WASIModuleExports) {
    exports.__ScriptProcessorNode_onaudioprocess = (callback, data) => {
        imports[SHIM_CALLBACKS_MAP].get(callback, Function)(data);
    };
}

// TODO: handle garbage collection
class ShimInstancesMap<T = any> {
    #instances: T[] = [];
    add(instance: T) {
        const id = this.#instances.length;
        this.#instances.push(instance);
        return id;
    }
    /** 
     * @template T
     * @param {number} id 
     * @param {T} Type
     * @returns {T['prototype']} 
     */
    get(id: number, Type: { new(...args: any[]): T }): T {
        const instance = this.#instances[id];
        if (!(instance instanceof Type)) {
            throw new Error(`No ${Type.name} target with id ${id}`);
        }
        return instance;
    }
}
