
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <ctime>

extern "C" {

double __emscripten_compat_js_code(const char *code, const char *sig, void *argbuf) __attribute__((__import_module__("compat_emscripten"), __import_name__("js_code")));

void __emscripten_compat_log(const char *s, int n) __attribute__((__import_module__("compat_emscripten"), __import_name__("log")));

double emscripten_get_now() {
    timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
	return (t.tv_sec * 1000.0) + (t.tv_nsec / 1000000.0); 
}

void emscripten_log(int flags, const char* format, ...) {
    static char buf[10];
	va_list ap;
	va_start(ap, format);
    int n = vsnprintf(buf, sizeof(buf), format, ap);
    if (n > sizeof(buf) - 1) {
        char* tmpbuf = (char *)malloc(n + 1);
        vsnprintf(tmpbuf, n, format, ap);
        __emscripten_compat_log(tmpbuf, n);
        free(tmpbuf);
    }
    else {
        __emscripten_compat_log(buf, n);
    }
	va_end(ap);
}

int emscripten_asm_const_int(const char *code, const char *arg_sigs, ...) {
	va_list ap;
	va_start(ap, arg_sigs);
    int rval = (int)__emscripten_compat_js_code(code, arg_sigs, ap);
	va_end(ap);
    return rval;
}

int siprintf(char *s, const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	int rval = vsprintf(s, fmt, ap);
	va_end(ap);
	return rval;
}

int __small_sprintf(char *s, const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	int rval = vsprintf(s, fmt, ap);
	va_end(ap);
	return rval;
}

}
