# ES_ASM calls made by FMOD

```js
    //-- FMOD_System_GetDriverInfo --//
    { infocontext = new (window.AudioContext || window.webkitAudioContext)(); if (!infocontext) { return 0; } inforate = infocontext.sampleRate; infocontext.close(); delete infocontext; return inforate; }
    
    //-- FMOD_System_Init --//
    { context = new (window.AudioContext || window.webkitAudioContext)(); if (!context) { return 0; } FMOD_JS_MixFunction = Module.cwrap('FMOD_JS_MixFunction', 'void', ['number']); return context.sampleRate; }

    //-- FMOD_System_Init --//
    { _as_script_node = context.createScriptProcessor($1, 0, $0); }

    //-- FMOD_System_Init --//
    { _as_script_node.connect(context.destination); _as_script_node.onaudioprocess = function(audioProcessingEvent) { _as_output_buffer = audioProcessingEvent.outputBuffer; FMOD_JS_MixFunction(_as_output_buffer.getChannelData(0).length); } }

    //-- FMOD_JS_MixFunction --//
    { var data = HEAPF32.subarray(($0 / 4), ($0 / 4) + ($2 * $3)); for (var channel = 0; channel < $3; channel++) { var outputData = _as_output_buffer.getChannelData(channel); for (var sample = 0; sample < $2; sample++) { outputData[sample+$1] = data[(sample*$3)+channel]; } } }

    //-- FMOD_System_MixerSuspend --//
    { context.suspend(); }

    //-- FMOD_System_MixerResume --//
    { context.resume(); }
```