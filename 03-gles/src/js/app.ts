import { WASIModule } from '/js/wasi';

export async function init(programUrl: string) {

    const canvas = document.getElementById('screen') as HTMLCanvasElement;

    console.log(`Loading and compiling '${programUrl}'...`);

    const module = new WASIModule();
    try {
        await module.load(programUrl, { 
            plugins: [
                ['/js/GLES2', { canvas }]
            ]
        });
    }
    catch (error) {
        console.error(`Failed to load and compile '${programUrl}':`, error);
        return;
    }

    console.log(`Done, running program...`);

    module.env = {
        SCREEN_WIDTH: canvas.width.toString(),
        SCREEN_HEIGHT: canvas.height.toString()
    };

    const code = await module.start();

    console.log(`Program exited with code ${code}.`);
}
