#include <cstdio>
#include <cstdlib>
#include <stdlib.h>
#include <stdio.h>
#include <browser.h>
#include <GLES2/gl2.h>

struct Context {
    GLsizei width;
    GLsizei height;
    GLuint programObject;
    GLuint vertexBufferObject;
};

GLuint loadShader(GLenum type, const char *shaderSrc) {
    
    GLuint shader;
    GLint compiled;

    shader = glCreateShader(type);
    if (!shader)
        return 0;

    glShaderSource(shader, 1, &shaderSrc, NULL);

    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled) {
        GLint infoLen = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

        if (infoLen > 1) {
            char* infoLog = (char *)malloc(sizeof(char) * infoLen);
            glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
            fprintf(stderr, "Error compiling shader:\n%s\n", infoLog);
            free(infoLog);
        }

        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

GLboolean init(Context *context) {
    GLchar vShaderStr[] =
        "attribute vec4 vPosition;    \n"
        "attribute vec4 vColor;       \n"
        "varying vec4 out_color;      \n"
        "void main()                  \n"
        "{                            \n"
        "   out_color = vColor;       \n"
        "   gl_Position = vPosition;  \n"
        "}                            \n";

    GLchar fShaderStr[] =
        "precision mediump float;                     \n"
        "varying vec4 out_color;                      \n"
        "void main()                                  \n"
        "{                                            \n"
        "  gl_FragColor = out_color;                  \n"
        "}                                            \n";

    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint programObject;
    GLint linked;

    vertexShader = loadShader(GL_VERTEX_SHADER, vShaderStr);
    fragmentShader = loadShader(GL_FRAGMENT_SHADER, fShaderStr);
    programObject = glCreateProgram();

    if (!programObject)
        return GL_FALSE;

    glAttachShader(programObject, vertexShader);
    glAttachShader(programObject, fragmentShader);

    glBindAttribLocation(programObject, 0, "vPosition");

    glLinkProgram(programObject);

    glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
    if (!linked) {
        GLint infoLen = 0;
        glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

        if (infoLen > 1) {
            char* infoLog = (char *)malloc(sizeof(char) * infoLen);
            glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
            fprintf(stderr, "Error linking program:\n%s\n", infoLog);
            free(infoLog);
        }

        glDeleteProgram(programObject);
        return GL_FALSE;
    }

    context->programObject = programObject;
    glGenBuffers(1, &context->vertexBufferObject);
    
    return GL_TRUE;
}

void draw(Context *context) {
    GLfloat vVertices[] = { 
        0.0f, 0.5f, 0.0f, 1.0f,
        1, 0, 0, 1,

        -0.5f, -0.5f, 0.0f, 1.0f,
        0, 1, 0, 1,
        
        0.5f, -0.5f, 0.0f, 1.0f,
        0, 0, 1, 1
    };

    glViewport(0, 0, context->width, context->height);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(context->programObject);

    glBindBuffer(GL_ARRAY_BUFFER, context->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vVertices), vVertices, GL_DYNAMIC_DRAW);
    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 32, (void *)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 32, (void *)16);
    glEnableVertexAttribArray(1);

    glDrawArrays(GL_TRIANGLES, 0, 3);
}

int main() {
    Context context;

    context.width = atoi(getenv("SCREEN_WIDTH"));
    context.height = atoi(getenv("SCREEN_HEIGHT"));

    if (!init(&context)) {
        return -1;
    }

    for (;;) {
        __proxy_batch_mode(true);
        
        browser::wait_for_animation_frame();
        draw(&context);

        __proxy_batch_mode(false);
    }
}
