#include "system.h"
#include "memory.h"


void* allocate(unsigned int n) {
    printf("Allocating %i bytes...", n);
    void *b = new char[n];
    *((unsigned int *)b) = n;
    return b;
}

void deallocate(void* b) {
    auto n = *((unsigned int *)b);
    printf("Releasing %i bytes...", n);
    delete[] (char*)b;
}

inline int min(int a, int b) {
    return a > b ? b : a; 
}

int main() {
    
    void *blocks[1000], *last_block;
    auto block_size = 2048;
    auto num_blocks = 0;

    block_size = 1024;
    do {
        block_size = min(block_size*2, __memory_available);

        printf("Before allocate %d byte blocks", block_size);
        debug_print_memory_stats();

        for (num_blocks = 0; num_blocks < sizeof(blocks)/sizeof(void*) - 1 && __memory_available >= block_size; num_blocks++) {
            blocks[num_blocks] = allocate(block_size);
        }
        if (__memory_available) {
            blocks[num_blocks++] = allocate(__memory_available);
        }

        printf("After allocate %d byte blocks", block_size);
        debug_print_memory_stats();

        for (auto i = 0; i < num_blocks; i++) {
            deallocate(blocks[i]);
        }

        printf("");
    }
    while (block_size < __memory_available);

    return 0;
}
