#include "system.h"

extern "C" void __wasm_call_ctors();

unsigned long __memory_heap;
unsigned long __memory_size;
unsigned long __memory_available;

const unsigned long __memory_page_size = 64 * 1024;

inline void __memory_expand(unsigned long size) {
    if (size > __memory_size) {
        debug("Expanding memory");
        auto delta = ((size - __memory_size) + (__memory_page_size - 1)) / __memory_page_size;
        auto result = __builtin_wasm_memory_grow(0, delta);
        if (result == ((unsigned long)-1)) {
            abort("ERR_OUT_OF_MEMORY");
        }
        __memory_size = (result + delta) * __memory_page_size;
    }
}

inline unsigned long __memory_align(unsigned long size) {
    return size == 0 ? 8 : (size + 8 - 1) & (-8);
}

enum class memory_block_status : unsigned int {
    free = 0x55555555,
    used = 0xAAAAAAAA
};

struct memory_block {
    memory_block* next_block;
    memory_block_status status;  
    unsigned long size;
};

static memory_block* __memory_first_block;

const unsigned long __memory_block_meta_size = __memory_align(sizeof(memory_block));


extern "C" void __init(unsigned long heap) {
    __wasm_call_ctors();
    __memory_heap = heap;
    __memory_size = __builtin_wasm_memory_grow(0, 0) * __memory_page_size;
    __memory_expand(__memory_heap + __memory_block_meta_size);
    __memory_first_block = (memory_block*)__memory_heap;
    __memory_first_block->status = memory_block_status::free;
    __memory_first_block->size = __memory_size - __memory_heap - __memory_block_meta_size;
    __memory_available = __memory_first_block->size;
    return;
}

void* malloc(unsigned long size) {
    size =  __memory_align(size);
    auto block = __memory_first_block;
    auto last_block = (memory_block*)nullptr;
    while (block) {
        if (block->status == memory_block_status::free) {
            while (block->next_block && block->next_block->status == memory_block_status::free && size > block->size) {
                debug("Merging blocks");
                block->size += __memory_block_meta_size + block->next_block->size;
                block->next_block = block->next_block->next_block;
                __memory_available += __memory_block_meta_size;
            }
            if (size <= block->size) {
                block->status = memory_block_status::used;
                break;
            }
        }
        last_block = block;
        block = block->next_block;
    }
    if (!block) {
        block = (memory_block*)__memory_size;
        __memory_expand(__memory_size + __memory_block_meta_size + size);
        block->status = memory_block_status::used;
        block->size = __memory_size - (unsigned long)block - __memory_block_meta_size;
        last_block->next_block = block;
    }

    auto left_over_space = block->size - size;
    if (left_over_space > __memory_block_meta_size) {
        debug("Splitting block");
        auto block_after_next_block = block->next_block;
        block->next_block = (memory_block*)((unsigned long)block + __memory_block_meta_size + size);
        block->next_block->next_block = block_after_next_block;
        block->next_block->status = memory_block_status::free; 
        block->next_block->size = left_over_space - __memory_block_meta_size; 
        block->size = size;
        __memory_available -= __memory_block_meta_size;
    }

    __memory_available -= block->size;

    return (void *)((unsigned long)block + __memory_block_meta_size);
}

void free(void* addr) {
    auto block = (memory_block*)((unsigned long)addr - __memory_block_meta_size);
    block->status = memory_block_status::free;
    __memory_available += block->size;
}

void* operator new(unsigned long size) {
    return malloc(size);
}

void* operator new[](unsigned long size) {
    return ::operator new(size);
}

void operator delete(void* addr) {
    free(addr);
}

void operator delete[](void* addr) {
    ::operator delete(addr);
}

void debug_print_memory_stats() {
    auto i = 1;
    auto block = __memory_first_block;
    debug("Memory: size=%d bytes, available=%d bytes", __memory_size, __memory_available);
    while (block) {
        debug(
            "Block %d: address=%p, size=%d bytes, status=%x", 
            i, 
            (unsigned long)block + __memory_block_meta_size, 
            block->size, 
            block->status == memory_block_status::free
                ? "free"
                : "used")
        block = block->next_block;
        i++;
    }
}