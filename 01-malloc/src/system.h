#pragma once 

extern "C" void __abort(const char* message, const char *function, const char* file, unsigned int line, ...);
extern "C" void __debug(const char* message, const char *function, const char* file, unsigned int line, ...);

extern "C" void printf(const char* format, ...);

#define abort(message, ...) __abort(message, __FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__);
#define debug(message, ...) __debug(message, __FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__);
