import { WASIMemory, WASIModule, newCharacterDeviceWriter } from '/js/wasi';

export async function init(programUrl: string) {
    window.onunhandledrejection = event => {
        console.error('Unhandled promise rejection!', event.reason);
    };
    const playButton = document.getElementById('play-button') as HTMLButtonElement;
    playButton.onclick = async () => {
        playButton.disabled = true;
        try {
            await run(programUrl);
        }
        finally {
            playButton.disabled = false;
        }
    };
}

export async function run(programUrl: string) {

    log(`Loading and compiling '${programUrl}'...`, { color: 'yellow' });

    const module = new WASIModule();
    try {
        await module.load(
            programUrl, 
            { 
                imports: { app: getAppImports(module.memory, { spectrumCanvas: document.getElementById('spectrum') as HTMLCanvasElement }) },
                plugins: [
                    '/js/compat_emscripten',
                    '/js/compat_fmod'
                ] 
            }
        );
    }
    catch (error) {
        log(`Failed to load and compile '${programUrl}': ${(error as Error).message}`, { color: 'red' });
        return;
    }

    log(`Done, running program...`, { color: 'yellow' });

    module.stdout = newCharacterDeviceWriter(s => log(s, { noNewLine: true }));
    module.stderr = newCharacterDeviceWriter(s => log(s, { noNewLine: true, color: 'red' }));

    const code = await module.start();

    log(`Program exited with code ${code}.`, { color: 'yellow' });
}

function log(message: string, options: { color?: 'yellow' | 'red'; noNewLine?: boolean }) {
    const consoleElement = document.getElementById('console')!;
    const messageElement = document.createElement('span')!;
    const { color, noNewLine } = options || {};
    
    messageElement.innerText = message + (noNewLine ? '' : '\n');
    color && (messageElement.className = color);

    consoleElement.appendChild(messageElement);
    consoleElement.scrollTo({ top: consoleElement.scrollHeight - consoleElement.clientHeight });
}

function getAppImports(memory: WASIModule['memory'], options: { spectrumCanvas: HTMLCanvasElement }) {
    const { spectrumCanvas } = options;
    return {
        draw_spectrum(length: number, left: number, right: number) {
            const { width, height } = spectrumCanvas;
            const xEnd = Math.min(length / 2, width);
            const canvasContext = spectrumCanvas.getContext('2d')!;
            
            canvasContext.globalCompositeOperation = 'source-over';
            canvasContext.fillStyle = 'rgba(0, 0, 0, 0.02)';
            canvasContext.fillRect(0, 0, width, height);

            canvasContext.fillStyle = 'rgba(0, 255, 128, 0.2)';
            for (let x = 0; x < xEnd; x++) {
                let a = lin2db(memory.readFloat32(left + (x * Float32Array.BYTES_PER_ELEMENT))); 
                let y0 = (0.5 - (a / 80) * 0.5) * height;
                let y1 = (0.5 + (a / 80) * 0.5) * height;
                canvasContext.fillRect(x, y0, 1, y1 - y0);
            }
            canvasContext.fillStyle = 'rgba(255, 128, 0, 0.2)';
            for (let x = 0; x < xEnd; x++) {
                let a = lin2db(memory.readFloat32(right + (x * Float32Array.BYTES_PER_ELEMENT))); 
                let y0 = (0.5 - (a / 80) * 0.5) * height;
                let y1 = (0.5 + (a / 80) * 0.5) * height;
                canvasContext.fillRect(x, y0, 1, y1 - y0);
            }
        }
    };
}

function lin2db(value: number) {
    return 80 + clamp(20 * Math.log10(value), -80, 0);
}

function clamp(value: number, lowerLimit: number, upperLimit: number) {
    return Math.max(lowerLimit, Math.min(value, upperLimit));
}
