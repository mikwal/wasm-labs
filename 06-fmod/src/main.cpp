#include "fmod_dsp_effects.h"
#include <cassert>
#include <cstdio>
#include <unistd.h> 
#include <browser.h>
#include <fmod.h>
#include <fmod_common.h>

FMOD_SYSTEM *fmodSystem = NULL;
int fmodSamplerRate = 0;
FMOD_SOUND *fmodSound = NULL;
FMOD_CHANNEL *fmodChannel = NULL;
FMOD_DSP *fmodDSP = NULL;

#define ASSERT_FMOD_OK(function, ...) assert(function(__VA_ARGS__) == FMOD_OK)

void draw_spectrum(int length, const float *left, const float *right) __attribute__((__import_module__("app"), __import_name__("draw_spectrum")));



FMOD_RESULT channelCallback(FMOD_CHANNELCONTROL *channelcontrol, FMOD_CHANNELCONTROL_TYPE controltype, FMOD_CHANNELCONTROL_CALLBACK_TYPE callbacktype, void *commanddata1, void *commanddata2) {
    
    FMOD_SYNCPOINT *syncPoint;
    char syncPointName[100];
    ASSERT_FMOD_OK(FMOD_Sound_GetSyncPoint, fmodSound, (int)commanddata1, &syncPoint);
    ASSERT_FMOD_OK(FMOD_Sound_GetSyncPointInfo, fmodSound, syncPoint, syncPointName, sizeof(syncPointName), NULL, FMOD_TIMEUNIT_MS);
    printf("CALLBACK : Sync Point = '%s'\n", syncPointName);

    return FMOD_OK;
}

int main(int argc, char** argv) {
    browser::load_file_async("/music.wav");
    browser::wait_for_async();

    ASSERT_FMOD_OK(FMOD_System_Create, &fmodSystem);

    ASSERT_FMOD_OK(FMOD_System_SetDSPBufferSize, fmodSystem, 2048, 2);
    ASSERT_FMOD_OK(FMOD_System_GetDriverInfo, fmodSystem, 0, NULL, NULL, NULL, &fmodSamplerRate, NULL, NULL);
    ASSERT_FMOD_OK(FMOD_System_SetSoftwareFormat, fmodSystem, fmodSamplerRate, FMOD_SPEAKERMODE_DEFAULT, 0);

    ASSERT_FMOD_OK(FMOD_System_Init, fmodSystem, 1024, FMOD_INIT_NORMAL, NULL);
    ASSERT_FMOD_OK(FMOD_System_MixerSuspend, fmodSystem);    
    
    ASSERT_FMOD_OK(FMOD_System_CreateSound, fmodSystem, "/music.wav", FMOD_LOOP_OFF, NULL, &fmodSound);
    ASSERT_FMOD_OK(FMOD_System_PlaySound, fmodSystem, fmodSound, NULL, true, &fmodChannel);

    ASSERT_FMOD_OK(FMOD_System_CreateDSPByType, fmodSystem, FMOD_DSP_TYPE_FFT, &fmodDSP);
    ASSERT_FMOD_OK(FMOD_DSP_SetParameterInt, fmodDSP, FMOD_DSP_FFT_WINDOWSIZE, 1024);

    ASSERT_FMOD_OK(FMOD_Channel_SetCallback, fmodChannel, channelCallback);
    ASSERT_FMOD_OK(FMOD_Channel_AddDSP, fmodChannel, FMOD_CHANNELCONTROL_DSP_HEAD, fmodDSP);
    ASSERT_FMOD_OK(FMOD_Channel_SetPaused, fmodChannel, 0);
 
    ASSERT_FMOD_OK(FMOD_System_MixerResume, fmodSystem);

    for (;;) {
        ASSERT_FMOD_OK(FMOD_System_Update, fmodSystem);
        FMOD_DSP_PARAMETER_FFT *spectrumData;
        unsigned int spectrumLength;
        ASSERT_FMOD_OK(FMOD_DSP_GetParameterData, fmodDSP, FMOD_DSP_FFT_SPECTRUMDATA, (void **)&spectrumData, &spectrumLength, NULL, 0);
        __proxy_batch_mode(true);
        browser::wait_for_animation_frame();
        if (spectrumData->length) {
            draw_spectrum(spectrumData->length, spectrumData->spectrum[0],  spectrumData->spectrum[1]);
        }
        __proxy_batch_mode(false);
    }
    return 0;
}

