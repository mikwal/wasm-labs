# Web Assembly labs
A bunch samples running C/C++ code in the browser and implementing a browser based [WASI](https://github.com/WebAssembly/wasi-sdk) implementation.


## 01-malloc (http://localhost:5001)
_Status:_ __DONE__  
  
Standalone implementation of a `malloc` and `free`.

## 02-wasi (http://localhost:5002)
_Status:_ __WIP__  
  
Testing out the WASI standard library functions provided by `libbrowser`.

## 03-gles (http://localhost:5003)
_Status:_ __TODO__  
  
Using OpenGL ES 2 interface provided by `libbrowser` to draw some stuff.

## 04-regal (http://localhost:5004)
_Status:_ __DONE__  
  
Using [Regal](https://github.com/mikaelwaltersson/regal) legacy OpenGL fixed function pipeline to draw some stuff.

## 05-black-mask (http://localhost:5005)
_Status:_ __DONE__  
  
Compiling my old 3D demo from [DreamHack 2000](https://www.pouet.net/prod.php?which=6589) to WebAssembly.  
Using [Regal](https://github.com/mikaelwaltersson/regal) for implementing legacy OpenGL fixed function pipeline.  
Most of the original code is left untouched by providing stubs for Windows headers and functions. 
  
Available at: https://black-mask.mikw.al  

## 06-black-mask (http://localhost:5006)
_Status:_ __DONE__  
  
Using the [FMOD](https://www.fmod.com) library to play a wav file and draw the FFT spectrum data.  
  
## 07-yngvenator (http://localhost:5007)
_Status:_ __WIP__  
  
Compiling Bongo Productions old 3D demo from [DreamHack 2001](https://www.pouet.net/prod.php?which=4592) to WebAssembly.  
Using [Regal](https://github.com/mikaelwaltersson/regal) for implementing legacy OpenGL fixed function pipeline.  
Most of the original code is left untouched by providing stubs for Windows headers and functions. 
  

## libbrowser
_Status:_ __WIP__  
  
Library implementing [WASI](https://github.com/WebAssembly/wasi-sdk) (partially) and GLES2 for the browser.
The WebAssembly runs in a seperate worker thread so we can run the C/C++ synchronously without blocking the event loop.  
Function calls are proxied across `SharedArrayBuffer`-backed message buses with support for batching to enable executing multiple drawing function as part of the same bunch of microtasks in a `requestAnimationFrame` callback.

