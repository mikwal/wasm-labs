#include <wasi/api.h>
#include <browser/mounted_dir.h>

__wasi_errno_t __mounted_dir_attach(
    const char *path,
    size_t path_len,
    __browser_inode_t ino, 
    __wasi_filesize_t size, 
    uint8_t* header, 
    uint32_t header_size
) {
    // TODO
    return __WASI_ERRNO_NOSYS;
}

__wasi_errno_t __mounted_dir_file_info(
    const char *path,
    size_t path_len,
    __browser_inode_t *ino,
    __wasi_filesize_t *start,
    __wasi_filesize_t *end
) {
    return __WASI_ERRNO_NOENT;
}

__wasi_errno_t __mounted_dir_detach(
    const char *path,
    size_t path_len
) {
        // TODO
    return __WASI_ERRNO_NOSYS;
}
