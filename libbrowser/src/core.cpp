#include <cstddef>
#include <cstring>
#include <features.h>
#include <stdlib.h>
#include <wasi/api.h>
#include <browser/core.h>
#include <browser/mounted_dir.h>


typedef uint32_t __browser_inode_t;

typedef struct open_file_t {
    __wasi_fd_t dirfd;
    const char *path;
    size_t path_len;
    __browser_inode_t ino;
    __wasi_filesize_t start;
    __wasi_filesize_t end;
    __wasi_filesize_t offset;
    __wasi_fdstat_t stat;
} open_file_t;

static_assert(sizeof(char) == sizeof(uint8_t), "char should be 8 bit");


static open_file_t stdin = {
    .dirfd = -1,
    .path = "/dev/stdin", 
    .path_len = 10, 
    .ino = __BROWSER_STDIN_INODE,
    .start = 0, 
    .end = 0, 
    .offset = 0, 
    .stat = {
        .fs_filetype = __WASI_FILETYPE_CHARACTER_DEVICE,
        .fs_flags = __WASI_FDFLAGS_DSYNC | __WASI_FDFLAGS_NONBLOCK,
        .fs_rights_base = __WASI_RIGHTS_FD_READ,
        .fs_rights_inheriting = __WASI_RIGHTS_FD_READ
    } 
};

static open_file_t stdout = {
    .dirfd = -1,
    .path = "/dev/stdout", 
    .path_len = 11, 
    .ino = __BROWSER_STDOUT_INODE,
    .start = 0, 
    .end = 0, 
    .offset = 0, 
    .stat = {
        .fs_filetype = __WASI_FILETYPE_CHARACTER_DEVICE,
        .fs_flags = __WASI_FDFLAGS_DSYNC,
        .fs_rights_base = __WASI_RIGHTS_FD_WRITE,
        .fs_rights_inheriting = __WASI_RIGHTS_FD_WRITE
    } 
};

static open_file_t stderr = {
    .dirfd = -1,
    .path = "/dev/stderr", 
    .path_len = 11, 
    .ino = __BROWSER_STDERR_INODE,
    .start = 0, 
    .end = 0, 
    .offset = 0, 
    .stat = {
        .fs_filetype = __WASI_FILETYPE_CHARACTER_DEVICE,
        .fs_flags = __WASI_FDFLAGS_DSYNC,
        .fs_rights_base = __WASI_RIGHTS_FD_WRITE,
        .fs_rights_inheriting = __WASI_RIGHTS_FD_WRITE
    } 
};

static open_file_t rootdir = {
    .dirfd = -1,
    .path = "/", 
    .path_len = 1, 
    .ino = __BROWSER_ROOTDIR_INODE,
    .start = 0, 
    .end = 0, 
    .offset = 0, 
    .stat = {
        .fs_filetype = __WASI_FILETYPE_DIRECTORY,
        .fs_flags = 0,
        .fs_rights_base = __WASI_RIGHTS_PATH_OPEN | __WASI_RIGHTS_FD_READ | __WASI_RIGHTS_FD_READDIR,
        .fs_rights_inheriting = __WASI_RIGHTS_PATH_OPEN | __WASI_RIGHTS_FD_READ | __WASI_RIGHTS_FD_READDIR
    },
};

static open_file_t localstorage = {
    .dirfd = -1,
    .path = "/$localStorage", 
    .path_len = 14, 
    .ino = __BROWSER_LOCALSTORAGE_INODE,
    .start = 0, 
    .end = 0, 
    .offset = 0, 
    .stat = {
        .fs_filetype = __WASI_FILETYPE_DIRECTORY,
        .fs_flags = 0,
        .fs_rights_base = __WASI_RIGHTS_PATH_OPEN | __WASI_RIGHTS_FD_READ | __WASI_RIGHTS_FD_READDIR,
        .fs_rights_inheriting = __WASI_RIGHTS_PATH_OPEN | __WASI_RIGHTS_FD_READ | __WASI_RIGHTS_FD_READDIR
    },
};

static open_file_t sessionstorage = {
    .dirfd = -1,
    .path = "/$sessionStorage", 
    .path_len = 16, 
    .ino = __BROWSER_SESSIONSTORAGE_INODE,
    .start = 0, 
    .end = 0, 
    .offset = 0, 
    .stat = {
        .fs_filetype = __WASI_FILETYPE_DIRECTORY,
        .fs_flags = 0,
        .fs_rights_base = __WASI_RIGHTS_PATH_OPEN | __WASI_RIGHTS_FD_READ | __WASI_RIGHTS_FD_READDIR,
        .fs_rights_inheriting = __WASI_RIGHTS_PATH_OPEN | __WASI_RIGHTS_FD_READ | __WASI_RIGHTS_FD_READDIR
    },
};

static open_file_t* open_files[__BROWSER_MAX_FD] = {
    &stdin,
    &stdout,
    &stderr,
    &rootdir,
    &localstorage,
    &sessionstorage
};

static __wasi_fd_t next_free_fd = __BROWSER_FIRST_FREE_FD;

#define WASI_FUNC(return_type, name, ...) extern "C" return_type __imported_wasi_snapshot_preview1_##name(__VA_ARGS__)

WASI_FUNC(
    __wasi_errno_t, args_get,
    uint8_t **argv,
    uint8_t *argv_buf
) {
    return __browser_get_startup_strings(__BROWSER_STARTUP_STRINGS_ARGS, argv, argv_buf);
}

WASI_FUNC(
    __wasi_errno_t, args_sizes_get,
    size_t *argc,
    size_t *argv_buf_size
) {
    return __browser_get_startup_strings_sizes(__BROWSER_STARTUP_STRINGS_ARGS, argc, argv_buf_size);
}

WASI_FUNC(
    __wasi_errno_t, clock_res_get,
    __wasi_clockid_t clock_id,
    __wasi_timestamp_t *resolution
) {
    *resolution = 1000000; // 1ms as we can't know the resolution of the browser clock
    return __WASI_ERRNO_SUCCESS;
}

WASI_FUNC(
    int32_t, clock_time_get, 
    __wasi_clockid_t clock_id,
    __wasi_timestamp_t precision,
    __wasi_timestamp_t *time
) { 
    double time_in_ms;
    auto errno = __browser_clock(clock_id, &time_in_ms);
    *time = (__wasi_timestamp_t)(time_in_ms*1000000);
    return errno;
}

WASI_FUNC(
    int32_t, environ_get, 
    uint8_t **environ,
    uint8_t *environ_buf
) { 
    return __browser_get_startup_strings(__BROWSER_STARTUP_STRINGS_ENVIRON, environ, environ_buf);
}

WASI_FUNC(
    int32_t, environ_sizes_get, 
    size_t *environ_count,
    size_t *environ_buf_size
) { 
    return __browser_get_startup_strings_sizes(__BROWSER_STARTUP_STRINGS_ENVIRON, environ_count, environ_buf_size);
}

WASI_FUNC(
    int32_t, fd_prestat_get, 
    __wasi_fd_t fd, 
    __wasi_prestat_t *buf
) { 
    if (fd < __BROWSER_MAX_FD && open_files[fd]) {
        auto file = open_files[fd];
        if (file->stat.fs_filetype == __WASI_FILETYPE_DIRECTORY) {
            buf->tag = __WASI_PREOPENTYPE_DIR;
            buf->u.dir.pr_name_len = file->path_len;
            return __WASI_ERRNO_SUCCESS;
        }
    }
    return __WASI_ERRNO_BADF;
}

WASI_FUNC(
    int32_t, fd_prestat_dir_name, 
    __wasi_fd_t fd, 
    uint8_t *path, 
    size_t path_len
) { 
    if (fd < __BROWSER_MAX_FD && open_files[fd]) {
        auto file = open_files[fd];
        if (file->stat.fs_filetype == __WASI_FILETYPE_DIRECTORY) {
            if (path_len > file->path_len) {
                memset(path + file->path_len, 0, (path_len - file->path_len)*sizeof(uint8_t));
                path_len = file->path_len;
            }
            memcpy(path, file->path, path_len*sizeof(uint8_t));
            return __WASI_ERRNO_SUCCESS;
        }
    }
    return __WASI_ERRNO_BADF;
}

WASI_FUNC(
    int32_t, fd_close, 
    __wasi_fd_t fd
) {
    if (fd < __BROWSER_FIRST_FREE_FD) {
        return __WASI_ERRNO_NOTCAPABLE;
    }
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    auto dirfd = file->dirfd;
    auto ino = file->ino;
    free(file);
    open_files[fd] = NULL;
    if (fd < next_free_fd) {
        next_free_fd = fd;
    }
    return __browser_close_file(dirfd, ino);
}

WASI_FUNC(
    int32_t, fd_datasync, 
    __wasi_fd_t fd
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    return __browser_flush_file(file->dirfd, file->ino);
}

WASI_FUNC(
    int32_t, fd_pread, 
    __wasi_fd_t fd,
    const __wasi_iovec_t *iovs,
    size_t iovs_len,
    __wasi_filesize_t offset,
    size_t *nread
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    switch (file->stat.fs_filetype) {
        case __WASI_FILETYPE_CHARACTER_DEVICE:
            if (offset != 0) {
                return __WASI_ERRNO_SPIPE;
            }
        case __WASI_FILETYPE_REGULAR_FILE:
            if (offset > file->end) {
                return __WASI_ERRNO_INVAL;
            }
            return __browser_read_file(file->dirfd, file->ino, file->stat.fs_filetype, iovs, iovs_len, &offset, nread);
        default:
             return __WASI_ERRNO_NOSYS;
    }
}

WASI_FUNC(
    int32_t, fd_pwrite, 
    __wasi_fd_t fd,
    const __wasi_ciovec_t *iovs,
    size_t iovs_len,
    __wasi_filesize_t offset,
    size_t *nwritten
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    switch (file->stat.fs_filetype) {
        case __WASI_FILETYPE_CHARACTER_DEVICE:
            if (offset != 0) {
                return __WASI_ERRNO_SPIPE;
            }
        case __WASI_FILETYPE_REGULAR_FILE:
            if (offset > file->end) {
                return __WASI_ERRNO_INVAL;
            }
            return __browser_write_file(file->dirfd, file->ino, file->stat.fs_filetype, iovs, iovs_len, &offset, nwritten);
        default:
             return __WASI_ERRNO_NOSYS;
    }
}

WASI_FUNC(
    int32_t, fd_read, 
    __wasi_fd_t fd,
    const __wasi_iovec_t *iovs,
    size_t iovs_len,
    size_t *nread
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    auto offset = &file->offset;
    switch (file->stat.fs_filetype) {
        case __WASI_FILETYPE_CHARACTER_DEVICE:
            __wasi_filesize_t _;
            offset = &_;
        case __WASI_FILETYPE_REGULAR_FILE:
            return __browser_read_file(file->dirfd, file->ino, file->stat.fs_filetype, iovs, iovs_len, offset, nread);
        default:
            return __WASI_ERRNO_NOSYS;
    }
}

WASI_FUNC(
    int32_t, fd_renumber, 
    __wasi_fd_t from,
    __wasi_fd_t to
 ) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, fd_seek, 
    __wasi_fd_t fd,
    __wasi_filedelta_t offset,
    __wasi_whence_t whence,
    __wasi_filesize_t *newoffset
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    if (file->stat.fs_filetype != __WASI_FILETYPE_REGULAR_FILE) {
        return __WASI_ERRNO_SPIPE;
    }
    auto errno = __WASI_ERRNO_INVAL;
    switch (whence) {
        case __WASI_WHENCE_CUR:
            if (offset < 0) {
                offset = -offset;
                if (offset <= file->offset - file->start) {
                    file->offset -= offset;
                    errno = __WASI_ERRNO_SUCCESS;
                }
            }
            else if (offset <= file->end - file->offset) {
                file->offset += offset;
                errno = __WASI_ERRNO_SUCCESS;
            }
            break;
        case __WASI_WHENCE_END:
            if (offset <= 0) {
                offset = -offset;
                if (offset <= file->end - file->start) {
                    file->offset = file->end - offset; 
                    errno = __WASI_ERRNO_SUCCESS;
                }
            }
            break;
        case __WASI_WHENCE_SET:
            if (offset >= 0 && file->start + offset <= file->end) {
                file->offset = file->start + offset;
                errno = __WASI_ERRNO_SUCCESS;
            }
            break;
    }
    if (errno == __WASI_ERRNO_SUCCESS) {
        *newoffset = file->offset - file->start;
    }
    return errno;
}

WASI_FUNC(
    int32_t, fd_tell, 
    __wasi_fd_t fd, 
    __wasi_filesize_t *newoffset
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    if (file->stat.fs_filetype != __WASI_FILETYPE_REGULAR_FILE) {
        return __WASI_ERRNO_NOSYS;
    }
    *newoffset = file->offset - file->start;
    return __WASI_ERRNO_SUCCESS;
}

WASI_FUNC(
    int32_t, fd_fdstat_get, 
    __wasi_fd_t fd, 
    __wasi_fdstat_t *buf
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    *buf = file->stat;
    return __WASI_ERRNO_SUCCESS;
}

WASI_FUNC(
    int32_t, fd_fdstat_set_flags, 
    __wasi_fd_t fd,
    __wasi_fdflags_t flags
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, fd_fdstat_set_rights, 
    __wasi_fd_t fd,
    __wasi_rights_t fs_rights_base,
    __wasi_rights_t fs_rights_inheriting
) { 
    return __WASI_ERRNO_NOSYS;
}


WASI_FUNC(
    int32_t, fd_sync, 
    __wasi_fd_t fd
) {
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    return __browser_flush_file(file->dirfd, file->ino);
}

WASI_FUNC(
    int32_t, fd_write, 
    __wasi_fd_t fd,
    const __wasi_ciovec_t *iovs,
    size_t iovs_len,
    size_t *nwritten
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]){
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    auto offset = &file->offset;
    switch (file->stat.fs_filetype) {
        case __WASI_FILETYPE_CHARACTER_DEVICE:
            __wasi_filesize_t _;
            offset = &_;
        case __WASI_FILETYPE_REGULAR_FILE:
            return __browser_write_file(file->dirfd, file->ino, file->stat.fs_filetype, iovs, iovs_len, offset, nwritten);
        default:
            return __WASI_ERRNO_NOSYS;
    }
}

WASI_FUNC(
    int32_t, fd_advise, 
    __wasi_fd_t fd, 
    __wasi_filesize_t offset,
    __wasi_filesize_t len,
    __wasi_advice_t advice
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    return __WASI_ERRNO_SUCCESS;
}

WASI_FUNC(
    __wasi_errno_t, fd_allocate,
    __wasi_fd_t fd,
    __wasi_filesize_t offset,
    __wasi_filesize_t len
) {
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_create_directory, 
    __wasi_fd_t fd,
    const char *path,
    size_t path_len
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_link, 
    __wasi_fd_t old_fd,
    __wasi_lookupflags_t old_flags,
    const char *old_path,
    size_t old_path_len,
    __wasi_fd_t new_fd,
    const char *new_path,
    size_t new_path_len
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_open, 
    __wasi_fd_t dirfd,
    __wasi_lookupflags_t dirflags,
    const char *path,
    size_t path_len,
    __wasi_oflags_t oflags,
    __wasi_rights_t fs_rights_base,
    __wasi_rights_t fs_rights_inheriting,
    __wasi_fdflags_t fs_flags,
    __wasi_fd_t *fd
) { 
    if (oflags & __WASI_OFLAGS_DIRECTORY) {
        if (oflags & (__WASI_OFLAGS_CREAT | __WASI_OFLAGS_EXCL | __WASI_OFLAGS_TRUNC)) {
            return __WASI_ERRNO_ISDIR;
        }
        return __WASI_ERRNO_NOSYS;
    }
    if (next_free_fd >= __BROWSER_MAX_FD) {
        return __WASI_ERRNO_NFILE;
    }

    auto file = (open_file_t*)malloc(sizeof(open_file_t) + (path_len + 1)*sizeof(char));
    if (!file) {
        return __WASI_ERRNO_NOMEM;
    }

    char *path_copy = (char*)(file + 1);
    memcpy(path_copy, path, path_len*sizeof(char));
    path_copy[path_len] = '\0';
    
    file->dirfd = dirfd;
    file->path = path_copy;
    file->path_len = path_len;
    file->start = 0;
    file->offset = 0;
    file->stat.fs_filetype = __WASI_FILETYPE_REGULAR_FILE;
    file->stat.fs_flags = fs_flags;
    file->stat.fs_rights_base = fs_rights_base;
    file->stat.fs_rights_inheriting = fs_rights_inheriting;
    
    auto errno = __WASI_ERRNO_NOENT;
    if (dirfd == __BROWSER_ROOTDIR_FD) {
        errno = __mounted_dir_file_info(path, path_len, &file->ino, &file->start, &file->end);
    }
    if (errno == __WASI_ERRNO_NOENT) {
        errno = __browser_open_file(dirfd, path, path_len, oflags, &file->ino, &file->end);
    }
    if (errno == __WASI_ERRNO_SUCCESS) {
        *fd = next_free_fd;
        open_files[next_free_fd] = file;
        do {
            next_free_fd++;
        }
        while (next_free_fd < __BROWSER_MAX_FD && open_files[next_free_fd]);
    }
    else {
        free(file);
    } 

    return errno;
}

WASI_FUNC(
    int32_t, fd_readdir, 
    __wasi_fd_t fd,
    void *buf,
    size_t buf_len,
    __wasi_dircookie_t cookie,
    size_t *bufused
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_readlink, 
    __wasi_fd_t fd,
    const char *path,
    size_t path_len,
    char *buf,
    size_t buf_len,
    size_t *bufused
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_rename, 
    __wasi_fd_t old_fd,
    const char *old_path,
    size_t old_path_len,
    __wasi_fd_t new_fd,
    const char *new_path,
    size_t new_path_len
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, fd_filestat_get, 
    __wasi_fd_t fd, 
    __wasi_filestat_t *buf
) { 
    if (fd >= __BROWSER_MAX_FD || !open_files[fd]) {
        return __WASI_ERRNO_BADF;
    }
    auto file = open_files[fd];
    buf->dev = 0;
    buf->ino = file->ino;
    buf->filetype = file->stat.fs_filetype;
    buf->nlink = 1;
    buf->size = file->end - file->start;
    buf->atim = 0;
    buf->mtim = 0;
    buf->ctim = 0;
    return __WASI_ERRNO_SUCCESS;
}

WASI_FUNC(
    int32_t, fd_filestat_set_times, 
    __wasi_fd_t fd,
    __wasi_timestamp_t st_atim,
    __wasi_timestamp_t st_mtim,
    __wasi_fstflags_t fstflags
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, fd_filestat_set_size, 
    __wasi_fd_t fd,
    __wasi_filesize_t st_size
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_filestat_get, 
    __wasi_fd_t fd,
    __wasi_lookupflags_t flags,
    const char *path,
    size_t path_len,
    __wasi_filestat_t *buf
) { 
    switch (fd) {
        case __BROWSER_ROOTDIR_FD:
        case __BROWSER_LOCALSTORAGE_FD:
        case __BROWSER_SESSIONSTORAGE_FD:
            break;
        default:
            return __WASI_ERRNO_BADF;
    }
    if (path_len == 1 && *path == '.') {
        buf->dev = 0;
        buf->ino = open_files[fd]->ino;
        buf->filetype = open_files[fd]->stat.fs_filetype;
        buf->nlink = 1;
        buf->size = 0;
        buf->atim = 0;
        buf->mtim = 0;
        buf->ctim = 0;
        return __WASI_ERRNO_SUCCESS;
    }
    __browser_inode_t ino;
    __wasi_filesize_t start, end;
    auto errno = __WASI_ERRNO_NOENT;
    if (fd == __BROWSER_ROOTDIR_FD) {
        errno = __mounted_dir_file_info(path, path_len, &ino, &start, &end);
    }
    if (errno == __WASI_ERRNO_NOENT) {
        start = 0;
        errno = __browser_file_info(fd, path, path_len, &ino, &end);
    }
    if (errno == __WASI_ERRNO_SUCCESS) {
        buf->dev = 0;
        buf->ino = ino;
        buf->filetype = __WASI_FILETYPE_REGULAR_FILE;
        buf->nlink = 1;
        buf->size = end - start;
        buf->atim = 0;
        buf->mtim = 0;
        buf->ctim = 0;
    }
    return errno;
}

WASI_FUNC(
    int32_t, path_filestat_set_times, 
    __wasi_fd_t fd,
    __wasi_lookupflags_t flags,
    const char *path,
    size_t path_len,
    __wasi_timestamp_t st_atim,
    __wasi_timestamp_t st_mtim,
    __wasi_fstflags_t fstflags
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_symlink, 
    const char *old_path,
    size_t old_path_len,
    __wasi_fd_t fd,
    const char *new_path,
    size_t new_path_len
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, path_unlink_file, 
    __wasi_fd_t fd,
    const char *path,
    size_t path_len
) { 
    return __browser_unlink_file(fd, path, path_len);
}

WASI_FUNC(
    int32_t, path_remove_directory, 
    __wasi_fd_t fd,
    const char *path,
    size_t path_len
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, poll_oneoff, 
    const __wasi_subscription_t *in,
    __wasi_event_t *out,
    size_t nsubscriptions,
    size_t *nevents
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    _Noreturn void, proc_exit, 
    __wasi_exitcode_t rval
) {
    __browser_exit(rval);
}

WASI_FUNC(
    int32_t, random_get, 
    void *buf,
    size_t buf_len
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, sock_recv, 
    __wasi_fd_t sock,
    const __wasi_iovec_t *ri_data,
    size_t ri_data_len,
    __wasi_riflags_t ri_flags,
    size_t *ro_datalen,
    __wasi_roflags_t *ro_flags
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, sock_send, 
    __wasi_fd_t sock,
    const __wasi_ciovec_t *si_data,
    size_t si_data_len,
    __wasi_siflags_t si_flags,
    size_t *so_datalen
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, sock_shutdown, 
    __wasi_fd_t sock, 
    __wasi_sdflags_t how
) { 
    return __WASI_ERRNO_NOSYS;
}

WASI_FUNC(
    int32_t, sched_yield
) {
    return __WASI_ERRNO_NOSYS;
}
