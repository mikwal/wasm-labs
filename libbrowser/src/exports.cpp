#include <cstdlib>

extern "C" {

void *_malloc(size_t __size) __attribute__((__export_name__("_malloc"))) { return malloc(__size); }

void _free(void *__ptr) __attribute__((__export_name__("_free"))) { return free(__ptr); }

}
