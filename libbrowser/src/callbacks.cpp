#include <browser/callbacks.h>

#define CALLBACK_STORE_MAX_SLOTS    (64)

namespace browser {

    namespace __callback_store {

        static callback_t* slots = (callback_t*)::malloc(CALLBACK_STORE_MAX_SLOTS*sizeof(callback_t));
        static uint64_t used;

        void* malloc() {
            int i = ((__builtin_ffsll(~used)) - 1);
            if (i >= 0) {
                used |= (1ULL << i);
                return slots + i;
            } 
            return ::malloc(sizeof(callback_t));    
        }

        void free(void* ptr) {
            if (ptr >= slots && ptr < (slots + CALLBACK_STORE_MAX_SLOTS)) {
                int i = ((callback_t*)ptr) - slots;
                used &= ~(1ULL << i);
                return;
            }
            ::free(ptr);
        }
    }


}

