#include <errno.h>
#include <browser.h>
#include <browser/core.h>
#include <browser/callbacks.h>
#include <browser/mounted_dir.h>

namespace browser {

    void load_file_async(const string& path, load_file_callback_t&& callback) {
        __browser_load_rootdir_file_async(
            path.c_str(), 
            path.length(), 
            [](auto result, auto content_length, auto downloaded_bytes, auto user_state) {
                if (user_state) {
                    auto status = load_file_status::partial;
                    switch (result) {
                        case __WASI_ERRNO_SUCCESS: 
                            status = load_file_status::finished;
                        case __BROWSER_LOAD_CALLBACK_RESULT_IN_PROGRESS: 
                            errno = __WASI_ERRNO_SUCCESS;
                            break;
                        default: 
                            errno = result;
                            status = load_file_status::failed;
                            break;
                    }
                    callbacks<load_file_callback_t>::invoke(status, content_length, downloaded_bytes, user_state);
                    callbacks<load_file_callback_t>::release(user_state);
                }
            }, 
            callback
                ? callbacks<load_file_callback_t>::store(std::move(callback))
                : nullptr);
    }

    bool remove_file(const string& path) {
        errno = __browser_remove_rootdir_file(path.c_str(), path.length());
        return errno == __WASI_ERRNO_SUCCESS;
    }

    bool mount_archive(const string& path) {
        __browser_inode_t ino;
        __wasi_iovec_t iovs;
        __wasi_filesize_t size;
        __wasi_filesize_t offset;
        size_t nread;
        struct { uint32_t offset; uint32_t size; } header_info;
        uint8_t* header = NULL;
        __wasi_fd_t dirfd = __BROWSER_ROOTDIR_FD;

        errno = __browser_open_file(dirfd, path.c_str(), path.length(), 0, &ino, &size);
        if (errno == __WASI_ERRNO_SUCCESS) {
            offset = size - sizeof(header_info);
            iovs.buf = (uint8_t*)&header_info;
            iovs.buf_len = sizeof(header_info);
            errno = __browser_read_file(dirfd, ino, __WASI_FILETYPE_REGULAR_FILE, &iovs, 1, &offset, &nread);
        }
        if (errno == __WASI_ERRNO_SUCCESS && nread != iovs.buf_len) {
            errno = __WASI_ERRNO_NOTDIR;
        }
        if (errno == __WASI_ERRNO_SUCCESS) {
            header = (uint8_t*)malloc(header_info.size);
            offset = header_info.offset;
            iovs.buf = header;
            iovs.buf_len = header_info.size;
            errno = __browser_read_file(dirfd, ino, __WASI_FILETYPE_REGULAR_FILE, &iovs, 1, &offset, &nread);
        }
        if (errno == __WASI_ERRNO_SUCCESS && nread != iovs.buf_len) {
            errno = __WASI_ERRNO_NOTDIR;
        }
        if (errno == __WASI_ERRNO_SUCCESS) {
            errno = __mounted_dir_attach(path.c_str(), path.size(), ino, size, header, header_info.size);
        }
        if (errno != __WASI_ERRNO_SUCCESS) {
            free(header);
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Wunused-result"
            __browser_close_file(dirfd, ino);
            #pragma clang diagnostic pop
        }
        return errno == __WASI_ERRNO_SUCCESS;
    }

    bool unmount_archive(const string& path) {
        errno = __mounted_dir_detach(path.c_str(), path.size());
        return errno == __WASI_ERRNO_SUCCESS;
    }

    void wait_for_async() {
        __browser_wait_for_async();
    }

    double wait_for_animation_frame() {
        return __browser_wait_for_animation_frame();
    }

    void wait_for_interrupt() {
        __browser_wait(__BROWSER_WAIT_FOR_INTERRUPT, 0);
    }

    void wait_for_interrupt(double timeout) {
        __browser_wait(__BROWSER_WAIT_FOR_INTERRUPT | __BROWSER_WAIT_FOR_TIMEOUT, timeout); 
    }

    void wait(double timeout) {
        __browser_wait(__BROWSER_WAIT_FOR_TIMEOUT, timeout);
    }
}