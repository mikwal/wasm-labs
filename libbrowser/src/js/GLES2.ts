import { WASIMemory, WASIModule, WASIModuleImports, WASIModuleImportsProxy, DEBUG, WASIModuleExportsProxy } from '/js/wasi';

declare global {
    interface WASIModuleTypedImports {
        gl: ReturnType<typeof getGLES2>;
    }    
}

const DEFAULT_BATCH_MODE_DATA_BUFFER_SIZE = 10 * 1024 * 1024;

/**
 * From include/GLES2/gl2.h, needed for the proxy to locally handle some function calls.
 */
const GL_NO_ERROR = 0;
const GL_ALPHA = 0x1906;
const GL_RGB = 0x1907;
const GL_RGBA = 0x1908;
const GL_LUMINANCE = 0x1909;
const GL_LUMINANCE_ALPHA = 0x190A;
const GL_INVALID_ENUM = 0x0500;
const GL_INVALID_VALUE = 0x0501;
const GL_UNPACK_ALIGNMENT = 0x0CF5;
const GL_PACK_ALIGNMENT = 0x0D05;
const GL_UNSIGNED_BYTE = 0x1401;
const GL_UNSIGNED_SHORT_4_4_4_4 = 0x8033;
const GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034;
const GL_UNSIGNED_SHORT_5_6_5 = 0x8363;
const GL_INFO_LOG_LENGTH = 0x8B84;


export function applyImports(this: { options: { canvas: HTMLCanvasElement } }, imports: WASIModuleImports, module: WASIModule) {
    const { canvas } = this.options;
    if (!(canvas instanceof HTMLCanvasElement)) {
        throw new Error('GLES2: invalid or no canvas supplied in options');
    }

    imports.gl = getGLES2(module.memory, canvas);
}

function getGLES2(memory: WASIModule['memory'], canvas: HTMLCanvasElement) {

    const gl = canvas.getContext('webgl2', { alpha: false });
    if (gl === null) {
        throw new Error('GLES2: Unable to get webgl2 context from canvas');
    }

    const glInstances = createInstanceTables();
    const glStrings = 
        Object.fromEntries(([
            'RENDERER', 
            'SHADING_LANGUAGE_VERSION', 
            'VENDOR', 
            'VERSION'
            ] as const).map(id => [
                gl[id], 
                { id, text: '', value: 0, initialized: false }
            ]));

    return {
        blendFunc(sfactor: number, dfactor: number) {
            gl.blendFunc(sfactor, dfactor);
        },
        clear(mask: number) {
            gl.clear(mask);
        },
        clearColor(red: number, green: number, blue: number, alpha: number) {
            gl.clearColor(red, green, blue, alpha);
        },
        clearStencil(s: number) {
            gl.clearStencil(s);
        },
        colorMask(red: number, green: number, blue: number, alpha: number) {
            gl.colorMask(Boolean(red), Boolean(green), Boolean(blue), Boolean(alpha));
        },
        cullFace(mode: number) {
            gl.cullFace(mode);
        },
        depthFunc(func: number) {
            gl.depthFunc(func);
        },
        depthMask(flag: number) {
            gl.depthMask(Boolean(flag));
        },
        disable(cap: number) {
            gl.disable(cap);
        },
        enable(cap: number) {
            gl.enable(cap);
        },
        finish() {
            gl.finish();
        },
        flush() {
            gl.flush();
        },
        frontFace(mode: number) {
            gl.frontFace(mode);
        },
        getBooleanv(pname: number, data: number) {
            writeParameterToMemory(gl.getParameter(pname), data, Uint8Array);
        },
        getError() {
            return gl.getError();
        },
        getFloatv(pname: number, data: number) {
            writeParameterToMemory(gl.getParameter(pname), data, Float32Array);
        },
        getIntegerv(pname: number, data: number) {
            writeParameterToMemory(gl.getParameter(pname), data, Int32Array);
        },
        async getString(name: number) {
            const s = glStrings[name];
            if (!s) {
                return 0;
            }
            if (!s.initialized) {
                const text = gl.getParameter(name);
                if (typeof text === 'string') {
                    const textPtr = await memory.malloc(memory.getEncodedStringLength(text) + 1);
                    memory.writeString(textPtr, text);
                    s.text = text;
                    s.value = textPtr;
                }
                s.initialized = true;
            }
            return s.value;  
        },
        getTexParameterfv(target: number, pname: number, params: number) {
            writeParameterToMemory(gl.getTexParameter(pname, target), params, Float32Array);
        },
        getTexParameteriv(target: number, pname: number, params: number) {
            writeParameterToMemory(gl.getTexParameter(target, pname), params, Int32Array);
        },
        hint(target: number, mode: number) {
            gl.hint(target, mode);
        },
        isEnabled(cap: number): number {
            return gl.isEnabled(cap) ? 1 : 0;
        },
        lineWidth(width: number) {
            gl.lineWidth(width);
        },
        pixelStorei(pname: number, param: number) {
            gl.pixelStorei(pname, param);
        },
        readPixels(x: number, y: number, width: number, height: number, format: number, type: number, pixels: number) {
            gl.readPixels(x, y, width, height, format, type, memory.views.uint8, pixels);
        },
        scissor(x: number, y: number, width: number, height: number) {
            gl.scissor(x, y, width, height);
        },
        stencilFunc(func: number, ref: number, mask: number) {
            gl.stencilFunc(func, ref, mask);
        },
        stencilMask(mask: number) {
            gl.stencilMask(mask);
        },
        stencilOp(fail: number, zfail: number, zpass: number) {
            gl.stencilOp(fail, zfail, zpass);
        },
        texImage2D(target: number, level: number, internalformat: number, width: number, height: number, border: number, format: number, type: number, pixels: number) {
            if (pixels === 0) {
                gl.texImage2D(target, level, internalformat, width, height, border, format, type, null);
            }
            else {
                gl.texImage2D(target, level, internalformat, width, height, border, format, type, memory.views.uint8, pixels);
            }
        },
        texParameterf(target: number, pname: number, param: number) {
            gl.texParameterf(target, pname, param);
        },
        texParameterfv(target: number, pname: number, params: number) {
            gl.texParameteri(target, pname, memory.views.float32[params]);
        },
        texParameteri(target: number, pname: number, param: number) {
            gl.texParameteri(target, pname, param);
        },
        texParameteriv(target: number, pname: number, params: number) {
            gl.texParameteri(target, pname, memory.views.int32[params]);
        },
        viewport(x: number, y: number, width: number, height: number) {
            gl.viewport(x, y, width, height);
        },
        bindTexture(target: number, texture: number) {
            gl.bindTexture(target, glInstances.textures.get(texture));
        },
        copyTexImage2D(target: number, level: number, internalformat: number, x: number, y: number, width: number, height: number, border: number) {
            gl.copyTexImage2D(target, level, internalformat, x, y, width, height, border);
        },
        copyTexSubImage2D(target: number, level: number, xoffset: number, yoffset: number, x: number, y: number, width: number, height: number) {
            gl.copyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
        },
        deleteTextures(n: number, textures: number) {
            for (let i = 0; i < n; i++) {
                gl.deleteTexture(glInstances.textures.remove(memory.readUint32(textures + i * Uint32Array.BYTES_PER_ELEMENT)));
            }
        },
        drawArrays(mode: number, first: number, count: number) {
            gl.drawArrays(mode, first, count);
        },
        drawElements(mode: number, count: number, type: number, indices: number) {
            gl.drawElements(mode, count, type, indices);
        },
        genTextures(n: number, textures: number) {
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    textures + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.textures.add(gl.createTexture()!)
                );
            }
        },
        isTexture(texture: number): number {
            return gl.isTexture(glInstances.textures.get(texture)) ? 1 : 0;
        },
        polygonOffset(factor: number, units: number) {
            gl.polygonOffset(factor, units);
        },
        texSubImage2D(target: number, level: number, xoffset: number, yoffset: number, width: number, height: number, format: number, type: number, pixels: number) {
            if (pixels === 0) {
                gl.texSubImage2D(target, level, xoffset, yoffset, width, height, format, type, null);
            }
            else {
                gl.texSubImage2D(target, level, xoffset, yoffset, width, height, format, type, memory.views.uint8, pixels);
            }
        },
        activeTexture(texture: number) {
            gl.activeTexture(texture);
        },
        compressedTexImage2D(target: number, level: number, internalformat: number, width: number, height: number, border: number, imageSize: number, data: number) {
            gl.compressedTexImage2D(target, level, internalformat, width, height, border, memory.views.uint8.subarray(data, imageSize));
        },
        compressedTexSubImage2D(target: number, level: number, xoffset: number, yoffset: number, width: number, height: number, format: number, imageSize: number, data: number) {
            gl.compressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, memory.views.uint8.subarray(data, imageSize));
        },
        sampleCoverage(value: number, invert: number) {
            gl.sampleCoverage(value, Boolean(invert));
        },
        blendColor(red: number, green: number, blue: number, alpha: number) {
            gl.blendColor(red, green, blue, alpha);
        },
        blendEquation(mode: number) {
            gl.blendEquation(mode);
        },
        blendFuncSeparate(sfactorRGB: number, dfactorRGB: number, sfactorAlpha: number, dfactorAlpha: number) {
            gl.blendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
        },
        bindBuffer(target: number, buffer: number) {
            gl.bindBuffer(target, glInstances.buffers.get(buffer));
        },
        bufferData(target: number, size: number, data: number, usage: number) {
            if (data === 0) {
                gl.bufferData(target, size, usage);
            }
            else {
                gl.bufferData(target, memory.views.uint8, usage, data, size);
            }
        },
        bufferSubData(target: number, offset: number, size: number, data: number) {
            gl.bufferSubData(target, offset, memory.views.uint8, data, size);
        },
        deleteBuffers(n: number, buffers: number) {
            for (let i = 0; i < n; i++) {
                gl.deleteBuffer(glInstances.buffers.remove(memory.readUint32(buffers + i * Uint32Array.BYTES_PER_ELEMENT)));
            }
        },
        genBuffers(n: number, buffers: number) {
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    buffers + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.buffers.add(gl.createBuffer()!)
                );
            }
        },
        getBufferParameteriv(target: number, pname: number, params: number) {
            writeParameterToMemory(gl.getBufferParameter(target, pname), params, Int32Array);
        },
        isBuffer(buffer: number): number {
            return gl.isBuffer(glInstances.buffers.get(buffer)) ? 1 : 0;
        },
        attachShader(program: number, shader: number) {
            gl.attachShader(glInstances.programs.get(program)!, glInstances.shaders.get(shader)!);
        },
        bindAttribLocation(program: number, index: number, name: number) {
            const l = memory.readNullTerminatedString(name)!;
            gl.bindAttribLocation(glInstances.programs.get(program)!, index, l);
        },
        blendEquationSeparate(modeRGB: number, modeAlpha: number) {
            gl.blendEquationSeparate(modeRGB, modeAlpha);
        },
        compileShader(shader: number) {
            gl.compileShader(glInstances.shaders.get(shader)!);
        },
        createProgram() {
            return glInstances.programs.add(gl.createProgram()!);
        },
        createShader(type: number) {
            return glInstances.shaders.add(gl.createShader(type)!);
        },
        deleteProgram(program: number) {
            gl.deleteProgram(glInstances.programs.remove(program));
        },
        deleteShader(shader: number) {
            gl.deleteShader(glInstances.shaders.remove(shader));
        },
        detachShader(program: number, shader: number) {
            gl.detachShader(glInstances.programs.get(program)!, glInstances.shaders.get(shader)!);
        },
        disableVertexAttribArray(index: number) {
            gl.disableVertexAttribArray(index);
        },
        enableVertexAttribArray(index: number) {
            gl.enableVertexAttribArray(index);
        },
        getActiveAttrib(program: number, index: number, bufSize: number, length: number, size: number, type: number, name: number) {
            const attrib = gl.getActiveAttrib(glInstances.programs.get(program)!, index);
            if (attrib) {
                memory.writeUint32(type, attrib.type);
                memory.writeUint32(size, attrib.size);
                writeBufSizeLimitedString(attrib.name, bufSize, length, name);
            }
        },
        getActiveUniform(program: number, index: number, bufSize: number, length: number, size: number, type: number, name: number) {
            const uniform = gl.getActiveUniform(glInstances.programs.get(program)!, index);
            if (uniform) {
                memory.writeUint32(type, uniform.type);
                memory.writeUint32(size, uniform.size);
                writeBufSizeLimitedString(uniform.name, bufSize, length, name);
            }
        },
        getAttachedShaders(program: number, maxCount: number, count: number, shaders: number) {
            const attachedShaders = gl.getAttachedShaders(glInstances.programs.get(program)!);
            if (attachedShaders) {
                memory.writeInt32(count, attachedShaders.length);
                for (let i = 0; i < maxCount && i < attachedShaders.length; i++) {
                    memory.writeUint32(shaders + i * Uint32Array.BYTES_PER_ELEMENT, glInstances.shaders.idOf(attachedShaders[i]))
                }
            }
        },
        getAttribLocation(program: number, name: number) {
            return gl.getAttribLocation(glInstances.programs.get(program)!, memory.readNullTerminatedString(name)!);
        },
        getProgramInfoLog(program: number, bufSize: number, length: number, infoLog: number) {
            const programInfoLog = gl.getProgramInfoLog(glInstances.programs.get(program)!);
            if (programInfoLog) {
                writeBufSizeLimitedString(programInfoLog, bufSize, length, infoLog);
            }
        },
        getProgramiv(program: number, pname: number, params: number) {
            const value = pname === GL_INFO_LOG_LENGTH
                ? memory.getEncodedStringLength(gl.getProgramInfoLog(glInstances.programs.get(program)!) || '')
                : gl.getProgramParameter(glInstances.programs.get(program)!, pname);
            writeParameterToMemory(value, params, Int32Array);
        },
        getShaderInfoLog(shader: number, bufSize: number, length: number, infoLog: number) {
            const shaderInfoLog = gl.getShaderInfoLog(glInstances.shaders.get(shader)!);
            if (shaderInfoLog) {
                writeBufSizeLimitedString(shaderInfoLog, bufSize, length, infoLog);
            }
        },
        getShaderSource(shader: number, bufSize: number, length: number, source: number) {
            const shaderSource = gl.getShaderSource(glInstances.shaders.get(shader)!);
            if (shaderSource) {
                writeBufSizeLimitedString(shaderSource, bufSize, length, source);
            }
        },
        getShaderiv(shader: number, pname: number, params: number) {
            const value = pname === GL_INFO_LOG_LENGTH
                ? memory.getEncodedStringLength(gl.getShaderInfoLog(glInstances.shaders.get(shader)!) || '')
                : gl.getShaderParameter(glInstances.shaders.get(shader)!, pname);
            writeParameterToMemory(value, params, Int32Array);
        },
        getUniformLocation(program: number, name: number) {
            const nameString = memory.readNullTerminatedString(name)!;
            const location = gl.getUniformLocation(glInstances.programs.get(program)!, nameString)!;
            DEBUG(this, () => `getUniformLocation(${program}, '${nameString}') = ${location}`);
            return glInstances.locations.add(program, location);
        },
        getUniformfv(program: number, location: number, params: number) {
            writeParameterToMemory(gl.getUniform(glInstances.programs.get(program)!, glInstances.locations.get(location)!), params, Float32Array);
        },
        getUniformiv(program: number, location: number, params: number) {
            writeParameterToMemory(gl.getUniform(glInstances.programs.get(program)!, glInstances.locations.get(location)!), params, Int32Array);
        },
        getVertexAttribPointerv(index: number, pname: number, pointer: number) {
            memory.writeUint32(pointer, gl.getVertexAttribOffset(index, pname));
        },
        getVertexAttribfv(index: number, pname: number, params: number) {
            writeParameterToMemory(gl.getVertexAttrib(index, pname), params, Float32Array);
        },
        getVertexAttribiv(index: number, pname: number, params: number) {
            writeParameterToMemory(gl.getVertexAttrib(index, pname), params, Int32Array);
        },
        isProgram(program: number): number {
            return gl.isProgram(glInstances.programs.get(program)) ? 1 : 0;
        },
        isShader(shader: number): number {
            return gl.isShader(glInstances.shaders.get(shader)) ? 1 : 0;
        },
        linkProgram(program: number) {
            gl.linkProgram(glInstances.programs.get(program)!);
        },
        shaderSource(shader: number, count: number, string: number, length: number) {
            let source = '';
            for (let i = 0; i < count; i++) {
                source += (
                    length !== 0 
                        ? memory.readString(
                            memory.readUint32(string + i * Uint32Array.BYTES_PER_ELEMENT), 
                            memory.readUint32(length + i * Uint32Array.BYTES_PER_ELEMENT)
                        )
                        : memory.readNullTerminatedString(
                            memory.readUint32(string + i * Uint32Array.BYTES_PER_ELEMENT)
                        )
                );
            }
            DEBUG(this, () => `shaderSource(${shader},\n"\n${source}\n")`);
            gl.shaderSource(glInstances.shaders.get(shader)!, source);
        },
        stencilFuncSeparate(face: number, func: number, ref: number, mask: number) {
            gl.stencilFuncSeparate(face, func, ref, mask);
        },
        stencilMaskSeparate(face: number, mask: number) {
            gl.stencilMaskSeparate(face, mask);
        },
        stencilOpSeparate(face: number, sfail: number, dpfail: number, dppass: number) {
            gl.stencilOpSeparate(face, sfail, dpfail, dppass);
        },
        uniform1f(location: number, v0: number) {
            gl.uniform1f(glInstances.locations.get(location), v0);
        },
        uniform1fv(location: number, count: number, value: number) {
            gl.uniform4fv(glInstances.locations.get(location), new Float32Array(memory.buffer, value, count * 1));
        },
        uniform1i(location: number, v0: number) {
            gl.uniform1i(glInstances.locations.get(location), v0);
        },
        uniform1iv(location: number, count: number, value: number) {
            gl.uniform3iv(glInstances.locations.get(location),  new Int32Array(memory.buffer, value, count * 1));
        },
        uniform2f(location: number, v0: number, v1: number) {
            gl.uniform2f(glInstances.locations.get(location), v0, v1);
        },
        uniform2fv(location: number, count: number, value: number) {
            gl.uniform4fv(glInstances.locations.get(location), new Float32Array(memory.buffer, value, count * 2));
        },
        uniform2i(location: number, v0: number, v1: number) {
            gl.uniform2i(glInstances.locations.get(location), v0, v1);
        },
        uniform2iv(location: number, count: number, value: number) {
            gl.uniform3iv(glInstances.locations.get(location),  new Int32Array(memory.buffer, value, count * 2));
        },
        uniform3f(location: number, v0: number, v1: number, v2: number) {
            gl.uniform3f(glInstances.locations.get(location), v0, v1, v2);
        },
        uniform3fv(location: number, count: number, value: number) {
            gl.uniform4fv(glInstances.locations.get(location), new Float32Array(memory.buffer, value, count * 3));
        },
        uniform3i(location: number, v0: number, v1: number, v2: number) {
            gl.uniform3i(glInstances.locations.get(location), v0, v1, v2);
        },
        uniform3iv(location: number, count: number, value: number) {
            gl.uniform3iv(glInstances.locations.get(location),  new Int32Array(memory.buffer, value, count * 4));
        },
        uniform4f(location: number, v0: number, v1: number, v2: number, v3: number) {
            gl.uniform4f(glInstances.locations.get(location), v0, v1, v2, v3);
        },
        uniform4fv(location: number, count: number, value: number) {
            gl.uniform4fv(glInstances.locations.get(location), new Float32Array(memory.buffer, value, count * 4));
        },
        uniform4i(location: number, v0: number, v1: number, v2: number, v3: number) {
            gl.uniform4i(glInstances.locations.get(location), v0, v1, v2, v3);
        },
        uniform4iv(location: number, count: number, value: number) {
            gl.uniform3iv(glInstances.locations.get(location),  new Int32Array(memory.buffer, value, count * 4));
        },
        uniformMatrix2fv(location: number, count: number, transpose: number, value: number) {
            gl.uniformMatrix2fv(glInstances.locations.get(location), Boolean(transpose), new Float32Array(memory.buffer, value, count * 2 * 2));
        },
        uniformMatrix3fv(location: number, count: number, transpose: number, value: number) {
            gl.uniformMatrix3fv(glInstances.locations.get(location), Boolean(transpose), new Float32Array(memory.buffer, value, count * 3 * 3));
        },
        uniformMatrix4fv(location: number, count: number, transpose: number, value: number) {
            gl.uniformMatrix4fv(glInstances.locations.get(location), Boolean(transpose), new Float32Array(memory.buffer, value, count * 4 * 4));
        },
        useProgram(program: number) {
            gl.useProgram(glInstances.programs.get(program));
            glInstances.locations.useProgram(program);
        },
        validateProgram(program: number) {
            gl.validateProgram(glInstances.programs.get(program)!);
        },
        vertexAttrib1f(index: number, x: number) {
            gl.vertexAttrib1f(index, x);
        },
        vertexAttrib1fv(index: number, v: number) {
            gl.vertexAttrib1fv(index, new Float32Array(memory.buffer, v, 1));
        },
        vertexAttrib2f(index: number, x: number, y: number) {
            gl.vertexAttrib2f(index, x, y);
        },
        vertexAttrib2fv(index: number, v: number) {
            gl.vertexAttrib2fv(index, new Float32Array(memory.buffer, v, 2));
        },
        vertexAttrib3f(index: number, x: number, y: number, z: number) {
            gl.vertexAttrib3f(index, x, y, z);
        },
        vertexAttrib3fv(index: number, v: number) {
            gl.vertexAttrib3fv(index, new Float32Array(memory.buffer, v, 3));
        },
        vertexAttrib4f(index: number, x: number, y: number, z: number, w: number) {
            gl.vertexAttrib4f(index, x, y, z, w);
        },
        vertexAttrib4fv(index: number, v: number) {
            gl.vertexAttrib4fv(index, new Float32Array(memory.buffer, v, 4));
        },
        vertexAttribPointer(index: number, size: number, type: number, normalized: number, stride: number, pointer: number) {
            gl.vertexAttribPointer(index, size, type, Boolean(normalized), stride, pointer);
        },
        clearDepthf(d: number) {
            gl.clearDepth(d);
        },
        depthRangef(n: number, f: number) {
            gl.depthRange(n, f);
        },
        getShaderPrecisionFormat(shadertype: number, precisiontype: number, range: number, precision: number) {
            const format = gl.getShaderPrecisionFormat(shadertype, precisiontype);
            if (format) {
                memory.writeInt32v(range, [format.rangeMin, format.rangeMax]);
                memory.writeInt32(precision, format.precision);
            }
        },
        releaseShaderCompiler() {},
        shaderBinary(count: number, shaders: number, binaryformat: number, binary: number, length: number) {
            throw new Error('GLES2: Method glShaderBinary() is not implemented');
        },
        bindFramebuffer(target: number, framebuffer: number) {
            gl.bindFramebuffer(target, glInstances.framebuffers.get(framebuffer));
        },
        bindRenderbuffer(target: number, renderbuffer: number) {
            gl.bindRenderbuffer(target, glInstances.renderbuffers.get(renderbuffer));
        },
        checkFramebufferStatus(target: number) {
            return gl.checkFramebufferStatus(target);
        },
        deleteFramebuffers(n: number, framebuffers: number) {
            for (let i = 0; i < n; i++) {
                gl.deleteFramebuffer(glInstances.framebuffers.remove(memory.readUint32(framebuffers + i * Uint32Array.BYTES_PER_ELEMENT)));
            }
        },
        deleteRenderbuffers(n: number, renderbuffers: number) {
            for (let i = 0; i < n; i++) {
                gl.deleteRenderbuffer(glInstances.renderbuffers.remove(memory.readUint32(renderbuffers + i * Uint32Array.BYTES_PER_ELEMENT)));
            }
        },
        framebufferRenderbuffer(target: number, attachment: number, renderbuffertarget: number, renderbuffer: number) {
            gl.framebufferRenderbuffer(target, attachment, renderbuffertarget, glInstances.renderbuffers.get(renderbuffer));
        },
        framebufferTexture2D(target: number, attachment: number, textarget: number, texture: number, level: number) {
            gl.framebufferTexture2D(target, attachment, textarget, glInstances.textures.get(texture), level);
        },
        genFramebuffers(n: number, framebuffers: number) {
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    framebuffers + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.framebuffers.add(gl.createFramebuffer()!)
                );
            }
        },
        genRenderbuffers(n: number, renderbuffers: number) {
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    renderbuffers + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.renderbuffers.add(gl.createRenderbuffer()!)
                );
            }
        },
        generateMipmap(target: number) {
            gl.generateMipmap(target);
        },
        getFramebufferAttachmentParameteriv(target: number, attachment: number, pname: number, params: number) {
            memory.writeInt32(params, gl.getFramebufferAttachmentParameter(target, attachment, pname));
        },
        getRenderbufferParameteriv(target: number, pname: number, params: number) {
            memory.writeInt32(params, gl.getRenderbufferParameter(target, pname));
        },
        isFramebuffer(framebuffer: number): number {
            return gl.isFramebuffer(glInstances.framebuffers.get(framebuffer)) ? 1 : 0;
        },
        isRenderbuffer(renderbuffer: number): number {
            return gl.isRenderbuffer(glInstances.renderbuffers.get(renderbuffer)) ? 1 : 0;
        },
        renderbufferStorage(target: number, internalformat: number, width: number, height: number) {
            gl.renderbufferStorage(target, internalformat, width, height);
        }
    };

    function writeBufSizeLimitedString(value: string, bufSize: number, length: number, offset: number) {
        let buf = memory.getEncodedString(value);
        if (length !== 0) {
            memory.writeInt32(length, buf.byteLength);
        }
        if (buf.byteLength + 1 > bufSize) {
            buf = buf.subarray(0, Math.max(0, bufSize - 1));
        }
        memory.writeBytes(offset, buf);
        memory.views.uint8[buf.byteLength] = 0;
    }

    function writeParameterToMemory(value: unknown, offset: number, ArrayType: { new (buffer: ArrayBufferLike, byteOffset?: number, length?: number): Uint8Array | Int32Array | Float32Array }) {
        const result = new ArrayType(memory.buffer, offset);
        switch (typeof value) {
            case 'bigint':
            case 'number':
            case 'boolean':
                result[0] = Number(value);
                break;
            case 'object':
                if (value === null) {
                    result[0] = 0;
                }
                else if (isArrayLike(value)) {
                    for (let i = 0; i < value.length; i++) {
                        switch (typeof value[i]) {
                            case 'bigint':
                            case 'number':
                            case 'boolean':
                                result[i] = Number(value[i] || 0);
                                break;
                            default:
                                result[i] = 0;
                        }
                    }
                }
                break;
        }
    }

    function isArrayLike(value: { length?: unknown }): value is ArrayLike<unknown> {
        return typeof value.length === 'number';
    }
}

export async function applyProxyImports(this: { options: { batchModeDataBufferSize?: number } }, imports: WASIModuleImportsProxy, memory: WASIMemory) {
    const { batchModeDataBufferSize = DEFAULT_BATCH_MODE_DATA_BUFFER_SIZE } = this.options;
    const { gl } = imports;
    const glInstances = createInstanceTables();
    
    const BATCH_MODE_OFF = () => imports.__proxy.batch_mode(false);
    const BATCH_MODE_ON = () => imports.__proxy.batch_mode(true);

    const TARGET_FUNCTION_WITH_PRELUDE = 
        (prelude: () => void) => <T extends (this: typeof gl, ...args: number[]) => void | number>(targetFunction: T) => {
            const boundFunction = targetFunction.bind(gl);
            return (...args: Parameters<T>) => {
                prelude();
                return boundFunction(...args) as ReturnType<T>;
            };
        };

    const BATCHED = TARGET_FUNCTION_WITH_PRELUDE(BATCH_MODE_ON);
    const PASSTHROUGH = TARGET_FUNCTION_WITH_PRELUDE(BATCH_MODE_OFF);

    const batchModeDataBuffer = {
        start: 0,
        size: batchModeDataBufferSize,
        nextFree: 0
    };

    const pixelStorei = { 
        [GL_UNPACK_ALIGNMENT]: 4, 
        [GL_PACK_ALIGNMENT]: 4 
    };

    let lastRecordedLocalError = GL_NO_ERROR;

    imports.__proxy.batch_mode.addEventListener('changed', ({ enabled }) => {
        if (enabled) {
            if (!batchModeDataBuffer.start) {
                batchModeDataBuffer.start = memory.malloc(batchModeDataBuffer.size);
                batchModeDataBuffer.nextFree = batchModeDataBuffer.start;
            }
        } 
        else {
            batchModeDataBuffer.nextFree = batchModeDataBuffer.start;
        }
    });  

    imports.gl = {
        blendFunc: BATCHED(gl.blendFunc),
        clear: BATCHED(gl.clear),
        clearColor: BATCHED(gl.clearColor),
        clearStencil: BATCHED(gl.clearStencil),
        colorMask: BATCHED(gl.colorMask),
        cullFace: BATCHED(gl.cullFace),
        depthFunc: BATCHED(gl.depthFunc),
        depthMask: BATCHED(gl.depthMask),
        disable: BATCHED(gl.disable),
        enable: BATCHED(gl.enable),
        finish: PASSTHROUGH(gl.finish),
        flush: PASSTHROUGH(gl.flush),
        frontFace: BATCHED(gl.frontFace),
        getBooleanv: PASSTHROUGH(gl.getBooleanv),
        getError() {
            let localError = lastRecordedLocalError;
            lastRecordedLocalError = GL_NO_ERROR;
            BATCH_MODE_OFF();
            return localError || gl.getError();
        },
        getFloatv: PASSTHROUGH(gl.getFloatv),
        getIntegerv: PASSTHROUGH(gl.getIntegerv),
        getString: PASSTHROUGH(gl.getString),
        getTexParameterfv: PASSTHROUGH(gl.getTexParameterfv),
        getTexParameteriv: PASSTHROUGH(gl.getTexParameteriv),
        hint: BATCHED(gl.hint),
        isEnabled: PASSTHROUGH(gl.isEnabled),
        lineWidth: BATCHED(gl.lineWidth),
        pixelStorei(pname, param) {
            let localError = GL_NO_ERROR;
            if (!(pname in pixelStorei)) {
                localError = GL_INVALID_ENUM;
            }
            else if (!(param === 1 || param === 2 || param === 4 || param === 8)) {
                localError = GL_INVALID_VALUE;
            }
            if (localError !== GL_NO_ERROR) {
                console.warn(`GL2ES: glPixelStorei(pname=${pname}, param=${param}) is invalid (error=${localError}).`);
                lastRecordedLocalError ||= localError;
                return;
            }
            BATCH_MODE_ON();
            pixelStorei[pname as keyof typeof pixelStorei] = param;
            gl.pixelStorei(pname, param);
        },
        readPixels: PASSTHROUGH(gl.readPixels),
        scissor: BATCHED(gl.scissor),
        stencilFunc: BATCHED(gl.stencilFunc),
        stencilMask: BATCHED(gl.stencilMask),
        stencilOp: BATCHED(gl.stencilOp),
        texImage2D(target, level, internalformat, width, height, border, format, type, pixels) {
            const pixelDataSize = getPixelDataSize(width, height, format, type, pixelStorei[GL_UNPACK_ALIGNMENT]);
            if (pixelDataSize < 0) {
                const localError = GL_INVALID_ENUM;
                console.warn(`GL2ES: glTexImage2D(width=${width}, height=${height}, format=${format}, type=${type}) is invalid (error=${localError}).`);
                lastRecordedLocalError ||= localError;
                return;
            }
            BATCH_MODE_ON();
            pixels = copyDataForBatchMode(pixels, pixelDataSize);
            gl.texImage2D(target, level, internalformat, width, height, border, format, type, pixels);
        },
        texParameterf: BATCHED(gl.texParameterf),
        texParameterfv: PASSTHROUGH(gl.texParameterf),
        texParameteri: BATCHED(gl.texParameteri),
        texParameteriv: PASSTHROUGH(gl.texParameterf),
        viewport: BATCHED(gl.viewport),
        bindTexture: BATCHED(gl.bindTexture),
        copyTexImage2D: BATCHED(gl.copyTexImage2D),
        copyTexSubImage2D: BATCHED(gl.copyTexSubImage2D),
        deleteTextures(n, textures) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                glInstances.textures.remove(memory.readUint32(textures + i * Uint32Array.BYTES_PER_ELEMENT));
            }
            textures = copyDataForBatchMode(textures, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.deleteTextures(n, textures);
        },
        drawArrays: BATCHED(gl.drawArrays),
        drawElements: BATCHED(gl.drawElements),
        genTextures(n, textures) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    textures + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.textures.add({ })
                );
            }
            textures = copyDataForBatchMode(textures, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.genTextures(n, textures);
        },
        isTexture: PASSTHROUGH(gl.isTexture),
        polygonOffset: BATCHED(gl.polygonOffset),
        texSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels) {
            const pixelDataSize = getPixelDataSize(width, height, format, type, pixelStorei[GL_UNPACK_ALIGNMENT]);
            if (pixelDataSize < 0) {
                const localError = GL_INVALID_ENUM;
                console.warn(`GL2ES: glTexSubImage2D(width=${width}, height=${height}, format=${format}, type=${type}) is invalid (error=${localError}).`);
                lastRecordedLocalError ||= localError;
                return;
            }
            BATCH_MODE_ON();
            pixels = copyDataForBatchMode(pixels, pixelDataSize);
            gl.texSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
        },
        activeTexture: BATCHED(gl.activeTexture),
        compressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data) {
            BATCH_MODE_ON();
            data = copyDataForBatchMode(data, imageSize);
            gl.compressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data);
        },
        compressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data) {
            BATCH_MODE_ON();
            data = copyDataForBatchMode(data, imageSize);
            gl.compressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data);
        },
        sampleCoverage: BATCHED(gl.sampleCoverage),
        blendColor: BATCHED(gl.blendColor),
        blendEquation: BATCHED(gl.blendEquation),
        blendFuncSeparate: BATCHED(gl.blendFuncSeparate),
        bindBuffer: BATCHED(gl.bindBuffer),
        bufferData(target, size, data, usage) {
            BATCH_MODE_ON();
            data = copyDataForBatchMode(data, size);
            gl.bufferData(target, size, data, usage);
        },
        bufferSubData(target, offset, size, data) {
            BATCH_MODE_ON();
            data = copyDataForBatchMode(data, size);
            gl.bufferSubData(target, offset, size, data);
        },
        deleteBuffers(n, buffers) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                glInstances.buffers.remove(memory.readUint32(buffers + i * Uint32Array.BYTES_PER_ELEMENT));
            }
            buffers = copyDataForBatchMode(buffers, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.deleteBuffers(n, buffers);
        },
        genBuffers(n, buffers) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    buffers + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.buffers.add({ })
                );
            }
            buffers = copyDataForBatchMode(buffers, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.genBuffers(n, buffers);
        },
        getBufferParameteriv: PASSTHROUGH(gl.getBufferParameteriv),
        isBuffer: PASSTHROUGH(gl.isBuffer),
        attachShader(program, shader) {
            BATCH_MODE_ON();
            const programInstance = glInstances.programs.get(program);
            const shaderInstance = glInstances.shaders.get(shader);
            if (programInstance && shaderInstance) {
                programInstance.shaders ||= [];
                programInstance.shaders.push(shaderInstance);
            }
            gl.attachShader(program, shader);
        },
        bindAttribLocation(program, index, name) {
            BATCH_MODE_ON();
            name = copyNullTerminatedStringForBatchMode(name);
            return gl.bindAttribLocation(program, index, name);
        },
        blendEquationSeparate: BATCHED(gl.compileShader),
        compileShader: BATCHED(gl.compileShader),
        createProgram() {
            BATCH_MODE_ON();
            gl.createProgram();
            return glInstances.programs.add({ });
        },
        createShader(type) {
            BATCH_MODE_ON();
            gl.createShader(type);
            return glInstances.shaders.add({ type });
        },
        deleteProgram: BATCHED(gl.deleteProgram),
        deleteShader: BATCHED(gl.deleteShader),
        detachShader: BATCHED(gl.detachShader),
        disableVertexAttribArray: BATCHED(gl.disableVertexAttribArray),
        enableVertexAttribArray: BATCHED(gl.enableVertexAttribArray),
        getActiveAttrib: PASSTHROUGH(gl.getActiveAttrib),
        getActiveUniform: PASSTHROUGH(gl.getActiveUniform),
        getAttachedShaders: PASSTHROUGH(gl.getAttachedShaders),
        getAttribLocation: PASSTHROUGH(gl.getAttribLocation),
        getProgramInfoLog: PASSTHROUGH(gl.getProgramInfoLog),
        getProgramiv: PASSTHROUGH(gl.getProgramiv),
        getShaderInfoLog: PASSTHROUGH(gl.getShaderInfoLog),
        getShaderSource: PASSTHROUGH(gl.getShaderSource),
        getShaderiv: PASSTHROUGH(gl.getShaderiv),
        getUniformLocation(program, name) {
            const programInstance = glInstances.programs.get(program);
            const uniformName = memory.readNullTerminatedString(name)!.replace(/\[.*\]/, '');
            let location = -1;
            if (programInstance && 
                programInstance.shaders && 
                programInstance.shaders.some(shader => shader.uniforms && shader.uniforms.includes(uniformName))
            ) {
                location = glInstances.locations.add(program, location);
                BATCH_MODE_ON();
                name = copyNullTerminatedStringForBatchMode(name);
                gl.getUniformLocation(program, name);
            }
            DEBUG(this, () => `getUniformLocation(${program}, '${uniformName}') = ${location}`);
            return location;
        },
        getUniformfv: PASSTHROUGH(gl.getUniformfv),
        getUniformiv: PASSTHROUGH(gl.getUniformiv),
        getVertexAttribPointerv: PASSTHROUGH(gl.getVertexAttribPointerv),
        getVertexAttribfv: PASSTHROUGH(gl.getVertexAttribfv),
        getVertexAttribiv: PASSTHROUGH(gl.getVertexAttribiv),
        isProgram: PASSTHROUGH(gl.isProgram),
        isShader: PASSTHROUGH(gl.isShader),
        linkProgram: BATCHED(gl.linkProgram),
        shaderSource(shader, count, string, length) {
            BATCH_MODE_ON();
            parseShaderUniformNames(shader, count, string, length);
            string = copyDataForBatchMode(string, count * Uint32Array.BYTES_PER_ELEMENT);
            length = copyDataForBatchMode(length, count * Uint32Array.BYTES_PER_ELEMENT);
            for (let i = 0; i < count; i++) {
                memory.writeInt32(
                    string + i * Uint32Array.BYTES_PER_ELEMENT,
                    length !== 0
                        ? copyDataForBatchMode(
                            memory.readUint32(string + i * Uint32Array.BYTES_PER_ELEMENT), 
                            memory.readUint32(length + i * Uint32Array.BYTES_PER_ELEMENT)
                        )
                        : copyNullTerminatedStringForBatchMode(
                            memory.readUint32(string + i * Uint32Array.BYTES_PER_ELEMENT)
                        )
                    );
            }
            gl.shaderSource(shader, count, string, length);
        },
        stencilFuncSeparate: BATCHED(gl.stencilFuncSeparate),
        stencilMaskSeparate: BATCHED(gl.stencilMaskSeparate),
        stencilOpSeparate: BATCHED(gl.stencilOpSeparate),
        uniform1f: BATCHED(gl.uniform1f),
        uniform1fv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 1 * Float32Array.BYTES_PER_ELEMENT);
            gl.uniform1fv(location, count, value);
        },
        uniform1i: BATCHED(gl.uniform1i),
        uniform1iv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 1 * Int32Array.BYTES_PER_ELEMENT);
            gl.uniform1iv(location, count, value);
        },
        uniform2f: BATCHED(gl.uniform2f),
        uniform2fv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 2 * Float32Array.BYTES_PER_ELEMENT);
            gl.uniform2fv(location, count, value);
        },
        uniform2i: BATCHED(gl.uniform2i),
        uniform2iv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 2 * Int32Array.BYTES_PER_ELEMENT);
            gl.uniform2iv(location, count, value);
        },
        uniform3f: BATCHED(gl.uniform3f),
        uniform3fv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 3 * Float32Array.BYTES_PER_ELEMENT);
            gl.uniform3fv(location, count, value);
        },
        uniform3i: BATCHED(gl.uniform3i),
        uniform3iv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 3 * Int32Array.BYTES_PER_ELEMENT);
            gl.uniform3iv(location, count, value);
        },
        uniform4f: BATCHED(gl.uniform4f),
        uniform4fv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 4 * Float32Array.BYTES_PER_ELEMENT);
            gl.uniform4fv(location, count, value);
        },
        uniform4i: BATCHED(gl.uniform4i),
        uniform4iv(location, count, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 4 * Int32Array.BYTES_PER_ELEMENT);
            gl.uniform4iv(location, count, value);
        },
        uniformMatrix2fv(location, count, transpose, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 2 * 2 * Float32Array.BYTES_PER_ELEMENT);
            gl.uniformMatrix2fv(location, count, transpose, value);
        },
        uniformMatrix3fv(location, count, transpose, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 3 * 3 * Float32Array.BYTES_PER_ELEMENT);
            gl.uniformMatrix3fv(location, count, transpose, value);
        },
        uniformMatrix4fv(location, count, transpose, value) {
            BATCH_MODE_ON();
            value = copyDataForBatchMode(value, count * 4 * 4 * Float32Array.BYTES_PER_ELEMENT);
            gl.uniformMatrix4fv(location, count, transpose, value);
        },
        useProgram(program) {
            BATCH_MODE_ON();
            gl.useProgram(program);
            glInstances.locations.useProgram(program);
        },
        validateProgram: BATCHED(gl.validateProgram),
        vertexAttrib1f: BATCHED(gl.vertexAttrib1f),
        vertexAttrib1fv(index, v) {
            BATCH_MODE_ON();
            v = copyDataForBatchMode(v, 1 * Float32Array.BYTES_PER_ELEMENT);
            gl.vertexAttrib1fv(index, v);
        },
        vertexAttrib2f: BATCHED(gl.vertexAttrib2f),
        vertexAttrib2fv(index, v) {
            BATCH_MODE_ON();
            v = copyDataForBatchMode(v, 2 * Float32Array.BYTES_PER_ELEMENT);
            gl.vertexAttrib2fv(index, v);
        },
        vertexAttrib3f: BATCHED(gl.vertexAttrib3f),
        vertexAttrib3fv(index, v) {
            BATCH_MODE_ON();
            v = copyDataForBatchMode(v, 3 * Float32Array.BYTES_PER_ELEMENT);
            gl.vertexAttrib3fv(index, v);
        },
        vertexAttrib4f: BATCHED(gl.vertexAttrib4f),
        vertexAttrib4fv(index, v) {
            BATCH_MODE_ON();
            v = copyDataForBatchMode(v, 4 * Float32Array.BYTES_PER_ELEMENT);
            gl.vertexAttrib4fv(index, v);
        },
        vertexAttribPointer: BATCHED(gl.vertexAttribPointer),
        clearDepthf: BATCHED(gl.clearDepthf),
        depthRangef: BATCHED(gl.depthRangef),
        getShaderPrecisionFormat: PASSTHROUGH(gl.getShaderPrecisionFormat),
        releaseShaderCompiler: BATCHED(gl.releaseShaderCompiler),
        shaderBinary: PASSTHROUGH(gl.shaderBinary),
        bindFramebuffer: BATCHED(gl.bindFramebuffer),
        bindRenderbuffer: BATCHED(gl.bindRenderbuffer),
        checkFramebufferStatus: PASSTHROUGH(gl.checkFramebufferStatus),
        deleteFramebuffers(n, framebuffers) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                glInstances.framebuffers.remove(memory.readUint32(framebuffers + i * Uint32Array.BYTES_PER_ELEMENT));
            }
            framebuffers = copyDataForBatchMode(framebuffers, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.deleteFramebuffers(n, framebuffers);
        },
        deleteRenderbuffers(n, renderbuffers) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                glInstances.renderbuffers.remove(memory.readUint32(renderbuffers + i * Uint32Array.BYTES_PER_ELEMENT));
            }
            renderbuffers = copyDataForBatchMode(renderbuffers, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.deleteRenderbuffers(n, renderbuffers);
        },
        framebufferRenderbuffer: BATCHED(gl.framebufferRenderbuffer),
        framebufferTexture2D: BATCHED(gl.framebufferTexture2D),
        genFramebuffers(n, framebuffers) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    framebuffers + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.framebuffers.add({ })
                );
            }
            framebuffers = copyDataForBatchMode(framebuffers, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.genFramebuffers(n, framebuffers);
        },
        genRenderbuffers(n, renderbuffers) {
            BATCH_MODE_ON();
            for (let i = 0; i < n; i++) {
                memory.writeUint32(
                    renderbuffers + i * Uint32Array.BYTES_PER_ELEMENT, 
                    glInstances.renderbuffers.add({ })
                );
            }
            renderbuffers = copyDataForBatchMode(renderbuffers, n * Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
            gl.genRenderbuffers(n, renderbuffers);
        },
        generateMipmap: BATCHED(gl.generateMipmap),
        getFramebufferAttachmentParameteriv: PASSTHROUGH(gl.getFramebufferAttachmentParameteriv),
        getRenderbufferParameteriv: PASSTHROUGH(gl.getRenderbufferParameteriv),
        isFramebuffer: PASSTHROUGH(gl.isFramebuffer),
        isRenderbuffer: PASSTHROUGH(gl.isRenderbuffer),
        renderbufferStorage: BATCHED(gl.renderbufferStorage)
    };
    return;

    function getByteAlignmentPadding(size: number, alignment: number) {
        return (alignment - (size % alignment)) % alignment;
    }

    function copyDataForBatchMode(data: number, size: number, alignment = 4) {
        if (data === 0) {
            return data;
        }
        const padding = getByteAlignmentPadding(size, alignment);
        if (batchModeDataBuffer.nextFree + size + padding > batchModeDataBuffer.start + batchModeDataBuffer.size) {
            BATCH_MODE_OFF();
            return data;
        }
        memory.views.uint8.set(
            memory.views.uint8.subarray(data, data + size), 
            batchModeDataBuffer.nextFree
        );
        data = batchModeDataBuffer.nextFree;
        batchModeDataBuffer.nextFree += size + padding;
        return data;
    }

    function copyNullTerminatedStringForBatchMode(data: number) {
        let size = 0;
        for (; memory.views.uint8[data + size] !== 0; size++);
        return copyDataForBatchMode(data, size + 1);
    }

    function getPixelSize(format: number, type: number) {
        switch (type) {
            case GL_UNSIGNED_BYTE:
                switch (format) {
                    case GL_ALPHA:
                    case GL_LUMINANCE:
                        return 1;
                    case GL_LUMINANCE_ALPHA:
                        return 2;
                    case GL_RGB:
                        return 3;
                    case GL_RGBA:
                        return 4;
                    default:
                        return 0;
                }
            case GL_UNSIGNED_SHORT_5_6_5:
            case GL_UNSIGNED_SHORT_4_4_4_4:
            case GL_UNSIGNED_SHORT_5_5_5_1:
                return 2;
            default:
                return 0;
        }   
    }

    function getPixelDataSize(width: number, height: number, format: number, type: number, alignment = 4) {
        const pixelSize = getPixelSize(format, type);
        const rowPadding = getByteAlignmentPadding(width * pixelSize, alignment);
        return pixelSize > 0 ? ((width * pixelSize) + rowPadding) * height : -1;
    }

    function parseShaderUniformNames(shader: number, count: number, string: number, length: number) {
        let source = '';
        for (let i = 0; i < count; i++) {
            source += (
                length !== 0 
                    ? memory.readString(
                        memory.readUint32(string + i * Uint32Array.BYTES_PER_ELEMENT), 
                        memory.readUint32(length + i * Uint32Array.BYTES_PER_ELEMENT)
                    )
                    : memory.readNullTerminatedString(
                        memory.readUint32(string + i * Uint32Array.BYTES_PER_ELEMENT)
                    )
            );
        }
        DEBUG({}, () => `shaderSource(${shader},\n"\n${source}\n")`);
        const shaderInstance = glInstances.shaders.get(shader);
        if (shaderInstance) {
            shaderInstance.uniforms = getShaderUniformNames(source);
        }
    }

    function getShaderUniformNames(source: string) {
        let defines: string[] = [];
        let uniformNames: string[] = [];
        let uniformMatch: RegExpExecArray | null;
        source = source.replace(/\/\*(.|\n)*\*\//g, '');
        source = source.replace(/\/\/.*$/gm, '');
        source = source.replace(/(?<=^|\n)\s*#define\s+([a-zA-Z_][a-zA-Z0-9_]+)(\([^\)]*\))?\s(.|\\\n)*([^\\]\n)/g, s => (defines.push(s), ''));
        source = defines.
            map(d => d.trim().replace(/(\s|\\\n)+/g, ' ')).
            map(d => /^#define ([a-zA-Z0-9_]+)(?:\([^\)]*\))?\s*(.*)$/.exec(d) as string[]).
            map(([, name, replace]) => [new RegExp(`(?<![a-zA-Z0-9_])${name}(\\(.*\\))? ?(?![a-zA-Z0-9_])`, 'g'), replace] as const).
            reduce((value, [search, replace]) => value.replace(search, replace), source);
        const uniformRegExp = /(?:^|;)\s*uniform\s+(?:[a-zA-Z0-9_]+)\s+([a-zA-Z0-9_]+)\s*(\[[^\]]*\])?\s*(?=;)/g;
        while (uniformMatch = uniformRegExp.exec(source)) {
            uniformNames.push(uniformMatch[1]);
        }
        return uniformNames;
    }
}

interface WebGLProgram {
    shaders?: WebGLShader[];
}

interface WebGLShader {
    type?: number;
    uniforms?: string[];
}

function createInstanceTables() {
    return {
        buffers: new InstanceTable<WebGLBuffer>('buffer'),
        framebuffers: new InstanceTable<WebGLFramebuffer>('framebuffer'),
        locations: new ProgramUniformLocationTable(),
        programs: new InstanceTable<WebGLProgram>('program'),
        renderbuffers: new InstanceTable<WebGLRenderbuffer>('renderbuffer'),
        shaders: new InstanceTable<WebGLShader>('shader'),
        textures: new InstanceTable<WebGLTexture>('texture')
    };
}

class InstanceTable<T> {
    #name = '';
    #instances: (T | null)[] = [];
    #firstFreeSlotHint = 0;

    constructor(name: string) {
        this.#name = name;
    }

    get(id: number) {
        if (id === 0) {
            return null;
        }
        const instance = this.#instances[id - 1];
        if (!instance) {
            console.warn(`GLES2: ${this.#name} ${id} is invalid.`);
            return null;
        }
        return instance;
    }

    idOf(instance: T) {
        return this.#instances.indexOf(instance) + 1;
    }

    remove(id: number) {
        let slot = id - 1;
        if (slot >= 0 && slot < this.#instances.length && this.#instances[slot]) {
            const instance = this.#instances[slot];
            this.#instances[slot] = null;
            if (slot < this.#firstFreeSlotHint) {
                this.#firstFreeSlotHint = slot;
            }
            return instance;
        }
        return null;
    }

    add(instance: T) {
        if (!instance) {
            throw new Error(`GLES2.InstanceTable[${this.#name}]: Can not add NULL instance, won't work in batched mode`);
        }
        for (let slot = this.#firstFreeSlotHint; slot < this.#instances.length; slot++) {
            if (!this.#instances[slot]) {
                this.#instances[slot] = instance;
                this.#firstFreeSlotHint = slot + 1;
                return slot + 1;
            }
        }
        this.#instances.push(instance);
        this.#firstFreeSlotHint = this.#instances.length;
        return this.#instances.length;
    }
}

class ProgramUniformLocationTable {
    #tables = new Map<number, Map<number, WebGLUniformLocation>>();
    #currentTable: Map<number, WebGLUniformLocation> | undefined;

    add(program: number, instance: WebGLUniformLocation) {
        if (program < 1 || !instance) {
            throw new Error(`GLES2.ProgramUniformLocationTable: Can not add NULL instance, won't work in batched mode`);
        }
        let table = this.#tables.get(program);
        if (!table) {
            this.#tables.set(program, table = new Map());
        }
        let location = table.size;
        table.set(location, instance);
        return location;
    }

    get(location: number) {
        return (this.#currentTable && this.#currentTable.get(location)) || null;
    }

    useProgram(program: number) {
        this.#currentTable = this.#tables.get(program);
    }
}
