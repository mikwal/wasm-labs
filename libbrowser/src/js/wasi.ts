
declare global {
    var __ENABLE_WASI_DEBUG_LOG: boolean | undefined;
    var __ENABLE_WASI_WORKER_KEEP_ALIVE: boolean | undefined;

    interface WASIModuleTypedImports {
        sys: ReturnType<typeof getSystem>['imports'];
    }

    interface WASIModuleTypedExports {
        _malloc(size: number): Promise<number>;
        _free(ptr: number): Promise<void>;
        _start(): Promise<void>;
    }
}

type MakeSync<T> = 
    T extends Promise<infer TWrapped> 
        ? TWrapped 
        : T extends  (...args: any[]) => any
        ? (...args: Parameters<T>) => MakeSync<ReturnType<T>>
        : T extends Record<string, any>
        ? { [P in keyof T]: MakeSync<T[P]> }
        : T;

    
export type WASIModuleImports = 
    Record<string, Record<string, (...args: any[]) => any>> &
    WASIModuleTypedImports;

export type WASIModuleImportsProxy = 
    Proxy &
    WASIModuleProxy & 
    Record<string, Record<string, (...args: any[]) => any>> & 
    MakeSync<WASIModuleTypedImports>;

export type WASIModuleExports =
    Record<string, (...args: any[]) => any> & 
    MakeSync<WASIModuleTypedExports> & {
        readonly memory: WebAssembly.Memory;
        readonly __indirect_function_table: { 
            get<T extends (...args:  any[]) => any>(id: number): T;
        }
    };
    
export type WASIModuleExportsProxy =
    Proxy &
    Record<string, (...args: any[]) => any> & 
    WASIModuleTypedExports & {
        readonly memory: { buffer: ArrayBuffer };
        readonly __indirect_function_table: { 
            get<T extends (...args:  any[]) => Promise<any>>(id: number): T;
        }
    };

export interface WASIModuleOptions {
    imports?: Partial<WASIModuleImports>;
    plugins?: (string | [string, Object])[];
    proxyBatchSize?: number;
}

export interface WASIPlugin<TOption extends Object = Object> {
    applyImports?(this: { options: TOption; }, imports: Partial<WASIModuleImports>, module: WASIModule): void | Promise<void>;
    applyProxyImports?(this: { options: TOption; }, imports: Partial<WASIModuleImportsProxy>, memory: WASIMemory): void | Promise<void>;
    applyProxyExports?(this: { options: TOption; }, imports: Partial<WASIModuleImportsProxy>, exports: WASIModuleExports, memory: WASIMemory): void | Promise<void>;
    getProxyPluginOptions?(this: { options: TOption; }): Object;
}

export const WASI_MODULE_DEFAULT_PROXY_BATCH_SIZE = 10000;

export class WASIMemory<TExports extends WASIModuleExports | WASIModuleExportsProxy = WASIModuleExports> {
    #memory: { buffer: ArrayBuffer } = null!;
    #malloc: TExports['_malloc'] = null!;
    #free: TExports['_free'] = null!;
    #textDecoder = new TextDecoder();
    #textEncoder = new TextEncoder();
    #views: {
        uint8: Uint8Array,
        int8: Int8Array,
        uint16: Uint16Array,
        int16: Int16Array,
        uint32: Uint32Array,
        int32: Int32Array,
        int64: BigInt64Array,
        uint64: BigUint64Array,
        float32: Float32Array,
        float64: Float64Array
    } = null!;

    init(exports: TExports) {
        if (this.#memory) {
            throw new Error('WASIMemory: init() already called');
        }
        if (!exports.memory || !(exports.memory.buffer instanceof SharedArrayBuffer)) {
            throw new Error('WASIMemory: Binary does not export a memory of type SharedArrayBuffer');
        }
        if (typeof exports._malloc !== 'function') {
            throw new Error('WASIMemory: Binary does not export _malloc(size) method');
        }
        if (typeof exports._free !== 'function') {
            throw new Error('WASIMemory: Binary does not export a _free(ptr) method');
        }
        this.#memory = exports.memory;
        this.#malloc = exports._malloc;
        this.#free = exports._free;
    }


    get views() {
        if (!this.#memory) {
            throw new Error('WASIMemory: init() has not been called');
        }
        if (!this.#views || this.#views.uint8.buffer !== this.#memory.buffer) {
            this.#views = {
                uint8: new Uint8Array(this.#memory.buffer),
                int8: new Int8Array(this.#memory.buffer),
                uint16: new Uint16Array(this.#memory.buffer),
                int16: new Int16Array(this.#memory.buffer),
                uint32: new Uint32Array(this.#memory.buffer),
                int32: new Int32Array(this.#memory.buffer),
                uint64: new BigUint64Array(this.#memory.buffer),
                int64: new BigInt64Array(this.#memory.buffer),
                float32: new Float32Array(this.#memory.buffer),
                float64: new Float64Array(this.#memory.buffer)
            };
        }
        return this.#views;
    }

    get buffer() {
        if (!this.#memory) {
            throw new Error('WASIMemory: init() has not been called');
        }
        return this.#memory.buffer;
    }

    malloc(size: number) {
        if (!this.#malloc) {
            throw new Error('WASIMemory: init() has not been called');
        }
        return this.#malloc(size) as ReturnType<TExports['_malloc']>;
    }

    free(ptr: number) {
        if (!this.#free) {
            throw new Error('WASIMemory: init() has not been called');
        }
        return this.#free(ptr) as ReturnType<TExports['_free']>;
    }


    readBytes(byteOffset: number, length: number) {
        return this.views.uint8.slice(byteOffset, byteOffset + length);
    }

    writeBytes(byteOffset: number, values: ArrayLike<number>) {
        this.views.uint8.set(values, byteOffset);
    }


    readUint16(byteOffset: number) {
        return this.views.uint16[byteOffset / Uint16Array.BYTES_PER_ELEMENT];
    }

    readUint16v(byteOffset: number, length: number) {
        const offset = byteOffset / Uint16Array.BYTES_PER_ELEMENT;
        return this.views.uint16.slice(offset, offset + length);
    }

    writeUint16(byteOffset: number, value: number) {
        this.views.uint16[byteOffset / Uint16Array.BYTES_PER_ELEMENT] = value;
    }

    writeUint16v(byteOffset: number, values: ArrayLike<number>) {
        this.views.uint16.set(values, byteOffset / Uint16Array.BYTES_PER_ELEMENT);
    }


    readInt16(byteOffset: number) {
        return this.views.int16[byteOffset / Int16Array.BYTES_PER_ELEMENT];
    }

    readInt16v(byteOffset: number, length: number) {
        const offset = byteOffset / Int16Array.BYTES_PER_ELEMENT;
        return this.views.int16.slice(offset, offset + length);
    }

    writeInt16(byteOffset: number, value: number) {
        this.views.int16[byteOffset / Int16Array.BYTES_PER_ELEMENT] = value;
    }

    writeInt16v(byteOffset: number, values: ArrayLike<number>) {
        this.views.int16.set(values, byteOffset / Int16Array.BYTES_PER_ELEMENT);
    }

    readUint32(byteOffset: number) {
        return this.views.uint32[byteOffset / Uint32Array.BYTES_PER_ELEMENT];
    }

    readUint32v(byteOffset: number, length: number) {
        const offset = byteOffset / Uint32Array.BYTES_PER_ELEMENT;
        return this.views.uint32.slice(offset, offset + length);
    }

    writeUint32(byteOffset: number, value: number) {
        this.views.uint32[byteOffset / Uint32Array.BYTES_PER_ELEMENT] = value;
    }

    writeUint32v(byteOffset: number, values: ArrayLike<number>) {
        this.views.uint32.set(values, byteOffset / Uint32Array.BYTES_PER_ELEMENT);
    }


    readInt32(byteOffset: number) {
        return this.views.int32[byteOffset / Int32Array.BYTES_PER_ELEMENT];
    }

    readInt32v(byteOffset: number, length: number) {
        const offset = byteOffset / Int32Array.BYTES_PER_ELEMENT;
        return this.views.int32.slice(offset, offset + length);
    }

    writeInt32(byteOffset: number, value: number) {
        this.views.int32[byteOffset / Int32Array.BYTES_PER_ELEMENT] = value;
    }

    writeInt32v(byteOffset: number, values: ArrayLike<number>) {
        this.views.int32.set(values, byteOffset / Int32Array.BYTES_PER_ELEMENT);
    }


    readUint64(byteOffset: number) {
        return this.views.uint64[byteOffset / BigUint64Array.BYTES_PER_ELEMENT];
    }

    readUint64v(byteOffset: number, length: number) {
        const offset = byteOffset / BigUint64Array.BYTES_PER_ELEMENT;
        return this.views.uint64.slice(offset, offset + length);
    }

    writeUint64(byteOffset: number, value: bigint) {
        this.views.uint64[byteOffset / BigUint64Array.BYTES_PER_ELEMENT] = value;
    }

    writeUint64v(byteOffset: number, values: ArrayLike<bigint>) {
        this.views.uint64.set(values, byteOffset / BigUint64Array.BYTES_PER_ELEMENT);
    }


    readInt64(byteOffset: number) {
        return this.views.int64[byteOffset / BigInt64Array.BYTES_PER_ELEMENT];
    }

    readInt64v(byteOffset: number, length: number) {
        const offset = byteOffset / BigInt64Array.BYTES_PER_ELEMENT;
        return this.views.int64.slice(offset, offset + length);
    }

    writeInt64(byteOffset: number, value: bigint) {
        this.views.int64[byteOffset / BigInt64Array.BYTES_PER_ELEMENT] = value;
    }

    writeInt64v(byteOffset: number, values: ArrayLike<bigint>) {
        this.views.int64.set(values, byteOffset / BigInt64Array.BYTES_PER_ELEMENT);
    }


    readFloat32(byteOffset: number) {
        return this.views.float32[byteOffset / Float32Array.BYTES_PER_ELEMENT];
    }

    readFloat32v(byteOffset: number, length: number) {
        const offset = byteOffset / Float32Array.BYTES_PER_ELEMENT;
        return this.views.float32.slice(offset, offset + length);
    }

    writeFloat32(byteOffset: number, value: number) {
        this.views.float32[byteOffset / Float32Array.BYTES_PER_ELEMENT] = value;
    }

    writeFloat32v(byteOffset: number, values: ArrayLike<number>) {
        this.views.float32.set(values, byteOffset / Float32Array.BYTES_PER_ELEMENT);
    }

    readFloat64(byteOffset: number) {
        return this.views.float64[byteOffset / Float64Array.BYTES_PER_ELEMENT];
    }

    readFloat64v(byteOffset: number, length: number) {
        const offset = byteOffset / Float64Array.BYTES_PER_ELEMENT;
        return this.views.float64.slice(offset, offset + length);
    }

    writeFloat64(byteOffset: number, value: number) {
        this.views.float64[byteOffset / Float64Array.BYTES_PER_ELEMENT] = value;
    }

    writeFloat64v(byteOffset: number, values: ArrayLike<number>) {
        this.views.float64.set(values, byteOffset / Float64Array.BYTES_PER_ELEMENT);
    }


    readString(byteOffset: number, length: number) {
        if (byteOffset === 0) {
            return null;
        }
        return this.#textDecoder.decode(this.views.uint8.slice(byteOffset, byteOffset + length));
    }

    readNullTerminatedString(byteOffset: number) {
        if (byteOffset === 0) {
            return null;
        }
        const uint8view = this.views.uint8;
        let length = 0;
        for (; byteOffset + length < this.#memory.buffer.byteLength && uint8view[byteOffset + length] !== 0; length++);
        return this.#textDecoder.decode(this.views.uint8.slice(byteOffset, byteOffset + length));
    }

    writeString(byteOffset: number, value: string) {
        const encodedString = this.getEncodedString(value);
        this.writeBytes(byteOffset, encodedString);
        this.views.uint8[byteOffset + encodedString.byteLength] = 0;
    }

    getEncodedString(value: string) {
        return this.#textEncoder.encode(value);
    }

    getEncodedStringLength(value: string) {
        return this.getEncodedString(value).length;
    }
}

export class WASIModule {

    #memory = new WASIMemory<WASIModuleExportsProxy>();
    #system = getSystem(this.#memory);
    #exports: WASIModuleExportsProxy = null!;

    async load(path: string, options: WASIModuleOptions = {}) {
        let {
            imports = {} as WASIModuleImports,
            plugins = [],
            proxyBatchSize = WASI_MODULE_DEFAULT_PROXY_BATCH_SIZE
        } = options;

        if (imports.sys) {
            throw new Error('WASIModule: overriding imports.sys is not allowed');
        }
        
        imports = { ...imports };
        imports.sys = this.#system.imports;

        const pluginModules = await loadPluginModules(plugins);
        const proxyPlugins = pluginModules.
            filter(plugin => plugin.applyProxyExports || plugin.applyProxyImports).
            map(plugin => [plugin.path, plugin.getProxyPluginOptions!()]);

        for (const plugin of pluginModules) {
            if (plugin.applyImports) {
                await plugin.applyImports(imports, this);
            }
        }

        const importsProxy = Proxy.createServer(imports, { name: 'imports', mode: 'async', batchSize: proxyBatchSize });

        const wasmBinary = await httpGET(path);
        const wasmWorker = new Worker(import.meta.url || '/js/wasi', { type: 'module' });

        await new Promise<void>((resolve, reject) => {
            const terminate = () => {
                try {
                    if (!__ENABLE_WASI_WORKER_KEEP_ALIVE) {
                        wasmWorker.terminate();
                    }
                }
                catch (e) {
                    console.error('Failed to terminate worker:', e);
                }
            };
            const terminateAndReject = (e: Error) => {
                terminate();
                reject && reject(e);
                reject = null!;
            };
            wasmWorker.onmessage = ({ data }) => {
                switch (data[0]) {
                    case 'success':
                        const exportsProxy = Proxy.deserialize<WASIModuleExportsProxy>(data[1], { name: 'exports', mode: 'async' });
                        this.#memory.init(exportsProxy);
                        this.#exports = exportsProxy;
                        Proxy.setReturnChannel(importsProxy, exportsProxy);
                        Proxy.setReturnChannel(exportsProxy, importsProxy);
                        wasmWorker.postMessage([]); // To start listening synchronously
                        resolve();
                        break;
                    case 'log':
                        console[data[1] as ('log' | 'warn' | 'error')](data[2]);
                        break;
                    case 'finished':
                        terminate();
                        break;
                    case 'error':
                        terminateAndReject(new Error(data[1]));
                        break;
                    default:
                        terminateAndReject(new Error(`Invalid response from worker: ${data[0]}`));
                        break;
                }
            };
            wasmWorker.postMessage(['compileWasmModule', [wasmBinary, Proxy.serialize(importsProxy), { plugins: proxyPlugins, __ENABLE_WASI_DEBUG_LOG }]], [wasmBinary]);
        });
    }

    start() {
        return this.#system.start(this.#exports);
    }

    unwrapCallback<T extends (...args: any[]) => Promise<any>>(callback: number) {
        return this.#system.unwrapCallback<T>(callback);
    }

    get memory() {
        return this.#memory;
    }

    get exports() {
        return this.#exports;
    }

    set stdout(value: (bytes: Uint8Array) => number) {
        this.#system.standardIO[1].write = value;
    }

    set stderr(value: (bytes: Uint8Array) => number) {
        this.#system.standardIO[2].write = value;
    }

    set argv(value: string[]) {
        this.#system.setArguments(value);
    }

    set env(value: Record<string, string>) {
        this.#system.setEnvironment(value);
    }

    addFile(path: string, buffer: ArrayBuffer) {
        return this.#system.addFile(path, buffer);
    }

    getFileContent(path: string) {
        return this.#system.getFileContent(path);
    }

    removeFile(path: string) {
        return this.#system.removeFile(path);
    }
}

export const DEBUG_LOGGER = Symbol('DEBUG_LOGGER');


if (typeof localStorage !== 'undefined') {
    const getBoolean = (key: string) => /1|on|true|enabled/i.test((localStorage.getItem(key) || '').trim());
    Object.assign(self, {
        __ENABLE_WASI_DEBUG_LOG: getBoolean('.wasi.debug'),
        __ENABLE_WASI_WORKER_KEEP_ALIVE: getBoolean('.wasi.worker.keepAlive')
    });
}


export function DEBUG(target: any, messageBuilder: () => string) {
    if (__ENABLE_WASI_DEBUG_LOG) {
        const message = messageBuilder();
        if (target && target[DEBUG_LOGGER]) {
            target[DEBUG_LOGGER](message);
        }
        else {
            console.log(message);
        }
    }
}


const PROXY_MESSAGE_CONTROL_MUTEX = 0;
const PROXY_MESSAGE_CONTROL_HAS_MESSAGE = 1;
const PROXY_MESSAGE_CONTROL_INVOCATION_ID = 2;

const PROXY_MESSAGE_NUM_HEADER_ELEMENTS = 4;
const PROXY_MESSAGE_NUM_VALUE_ELEMENTS = 10;

const PROXY_MESSAGE_BYTE_OFFSET_FOR_HEADER = 4 * Int32Array.BYTES_PER_ELEMENT;
const PROXY_MESSAGE_BYTE_OFFSET_FOR_VALUES =
    PROXY_MESSAGE_BYTE_OFFSET_FOR_HEADER +
    PROXY_MESSAGE_NUM_HEADER_ELEMENTS * Int32Array.BYTES_PER_ELEMENT;

const PROXY_MESSAGE_BUFFER_LENGTH =
    PROXY_MESSAGE_NUM_HEADER_ELEMENTS * Int32Array.BYTES_PER_ELEMENT +
    PROXY_MESSAGE_NUM_VALUE_ELEMENTS * Float64Array.BYTES_PER_ELEMENT;

const PROXY_MESSAGE_HEADER_TYPE = 0;
const PROXY_MESSAGE_HEADER_FUNCTION_ID = 1;
const PROXY_MESSAGE_HEADER_VALUE_COUNT = 2;
const PROXY_MESSAGE_HEADER_VALUE_FLAGS = 3;

const PROXY_MESSAGE_TYPE_BATCH = 1;
const PROXY_MESSAGE_TYPE_RETURN = 2;
const PROXY_MESSAGE_TYPE_FUNCTION = 3;
const PROXY_MESSAGE_TYPE_CALLBACK_0 = 4;

const PROXY_BATCHED_MESSAGE_MUTEX = PROXY_MESSAGE_BUFFER_LENGTH / Int32Array.BYTES_PER_ELEMENT;
const PROXY_BATCHED_MESSAGE_COUNT = PROXY_MESSAGE_BUFFER_LENGTH / Int32Array.BYTES_PER_ELEMENT + 1;
const PROXY_BATCHED_FIRST_MESSAGE_HEADER = PROXY_MESSAGE_BUFFER_LENGTH / Int32Array.BYTES_PER_ELEMENT + 2;
const PROXY_BATCHED_FIRST_MESSAGE_VALUES = PROXY_MESSAGE_BUFFER_LENGTH / Float64Array.BYTES_PER_ELEMENT + 1;

const PROXY_TARGET = Symbol('PROXY_TARGET');
const PROXY_TARGET_FUNCTION_FULL_NAME = Symbol('PROXY_TARGET_FUNCTION_FULL_NAME');
const PROXY_WORKER_EXIT = Symbol('PROXY_WORKER_EXIT');

function getProxyBufferSizeForBatchSize(batchSize: number) {
    return PROXY_MESSAGE_BYTE_OFFSET_FOR_HEADER + (
        batchSize > 1
            ? PROXY_MESSAGE_BUFFER_LENGTH * (1 + batchSize) + 2 * Int32Array.BYTES_PER_ELEMENT /* MUTEX and MESSAGE_COUNT */
            : PROXY_MESSAGE_BUFFER_LENGTH
    );
}

export interface WASIModuleProxy {
    __proxy: { batch_mode: ProxyBatchModeControl }
}

export interface ProxyBatchModeControl {
    (enabled: boolean): void;
    readonly enabled: boolean;
    addEventListener(
        type: 'changed',
        callback: (evt: ProxyBatchModeChangedEvent) => void,
        options?: AddEventListenerOptions
    ): void;
}

export interface ProxyBatchModeChangedEvent extends Event {
    readonly type: 'changed';
    readonly enabled: boolean;
}

type SerializedProxy = [SharedArrayBuffer, Record<string, SerializedProxyValue>];
type SerializedProxyValue =
    { type: 'object'; properties: Record<string, SerializedProxyValue> } |
    { type: 'function'; id: number } |
    { type: 'table', id: number } |
    { type: 'memory', buffer: SharedArrayBuffer };

type ProxyMessage = readonly [number, number, number, (number | bigint)[]];

interface ProxyInvocationContext {
    id: number;
    mode: 'sync' | 'async';
    functionName: string;
    returnMessage?: ProxyMessage;
    [DEBUG_LOGGER](message: string): void;
}

interface ProxyTargetInfo {
    [PROXY_TARGET]: Proxy;
    [PROXY_TARGET_FUNCTION_FULL_NAME]: string;
}

class Proxy {
    #name: string;
    #mode: 'sync' | 'async';
    #buffer: SharedArrayBuffer;
    #messageControl: Int32Array;
    #messageHeader: Int32Array;
    #messageValues: Float64Array;
    #messageValuesInt64: BigInt64Array;
    #returnChannel: Proxy = null!;
    #functions = new Map();
    #nextFunctionId = 1;
    #tables = new Map();
    #nextTableId = 1;
    #batchMode = Proxy.#setupBatchModeControl(this);
    #invocations: ProxyInvocationContext[] = [];

    [PROXY_WORKER_EXIT]?: { rval: number; };

    constructor(name: string, mode: 'sync' | 'async' = 'sync', buffer: SharedArrayBuffer) {
        this.#name = name;
        this.#mode = mode;
        this.#buffer = buffer;
        this.#messageControl = new Int32Array(buffer, 0);
        this.#messageHeader = new Int32Array(buffer, PROXY_MESSAGE_BYTE_OFFSET_FOR_HEADER);
        this.#messageValues = new Float64Array(buffer, PROXY_MESSAGE_BYTE_OFFSET_FOR_VALUES);
        this.#messageValuesInt64 = new BigInt64Array(buffer, PROXY_MESSAGE_BYTE_OFFSET_FOR_VALUES);
    }

    static createServer<TTarget extends Object>(target: TTarget, options: { name: string; mode?: 'sync' | 'async'; batchSize?: number }) {
        const buffer = new SharedArrayBuffer(getProxyBufferSizeForBatchSize(options.batchSize || 0));
        const proxy = new Proxy(options.name, options.mode, buffer);
        Object.assign(proxy, bindFunctions(target));
        return proxy as (Proxy & TTarget);
    }

    static serialize(target: Proxy): SerializedProxy {
        return [target.#buffer, serializeObject(target, target.#name) || {}];

        function serializeObject(obj: Object, parentName: string): Record<string, SerializedProxyValue> | undefined {
            if ((obj as ProxyTargetInfo)[PROXY_TARGET] !== target) {
                if ((obj as ProxyTargetInfo)[PROXY_TARGET]) {
                    throw new Error(`Proxy.serialize(): target has already been used for another proxy instance`);
                }
                (obj as ProxyTargetInfo)[PROXY_TARGET] = target;
            }
            const entries =
                Object.entries(obj || {}).
                    filter(([key]) => typeof key === 'string').
                    map(([key, value]) => [key, serializeValue(value, parentName + '.' + key, obj)]).
                    filter(([, value]) => value !== undefined);
            return entries.length > 0 ? Object.fromEntries(entries) : undefined;
        }

        function serializeValue(value: unknown, name: string, obj: Object): SerializedProxyValue | undefined {
            if (typeof value === 'function') {
                let id = target.#functions.get(value);
                if (id === undefined) {
                    id = target.#nextFunctionId++;
                    value = value.bind(obj);
                    (value as ProxyTargetInfo)[PROXY_TARGET_FUNCTION_FULL_NAME] = name;
                    target.#functions.set(value, id);
                    target.#functions.set(id, value);
                }
                return { type: 'function', id };
            }
            else if (value instanceof WebAssembly.Table) {
                let id = target.#tables.get(value);
                if (id === undefined) {
                    id = target.#nextTableId++;
                    target.#tables.set(value, id);
                    target.#tables.set(id, value);
                    (value as unknown as ProxyTargetInfo)[PROXY_TARGET_FUNCTION_FULL_NAME] = name;
                }
                return { type: 'table', id };
            }
            else if (value instanceof WebAssembly.Memory) {
                const { buffer } = value;
                if (!(buffer instanceof SharedArrayBuffer)) {
                    throw new Error('Proxy.serialize(): memory object must be of type SharedArrayBuffer');
                }
                return { type: 'memory', buffer: buffer };
            }
            else if (typeof value === 'object' && value !== null) {
                const properties = serializeObject(value, name);
                return properties && { type: 'object', properties };
            }
        }
    }

    static deserialize<TTarget extends Object>(data: SerializedProxy, options: { name: string; mode?: 'sync' | 'async' }) {
        const proxy = new Proxy(options.name, options.mode, data[0]);
        Object.assign(
            proxy,
            deserializeObject(data[1], proxy.#name),
            { __proxy: { batch_mode: proxy.#batchMode } }
        );
        return proxy as (Proxy & TTarget);

        function deserializeObject(obj: Record<string, SerializedProxyValue>, parentName: string): Object | undefined {
            const entries =
                Object.entries(obj || {}).
                    map(([key, value]) => [key, deserializeValue(value, parentName + '.' + key)]).
                    filter(([, value]) => value !== undefined);
            return entries.length > 0 ? Object.fromEntries(entries) : undefined;
        }

        function deserializeValue(value: SerializedProxyValue, name: string) {
            switch (value.type) {
                case 'function':
                    return (...args: (number | bigint)[]) => proxy.#invoke(name, PROXY_MESSAGE_TYPE_FUNCTION, value.id, args);
                case 'table':
                    return { get: (index: number) => (...args: (number | bigint)[]) => proxy.#invoke(`${name}[${value.id}]`, value.id + PROXY_MESSAGE_TYPE_CALLBACK_0 - 1, index, args) };
                case 'memory':
                    return { buffer: value.buffer };
                case 'object':
                    return deserializeObject(value.properties, name);
            }
        }
    }

    static setReturnChannel(target: Proxy, returnChannel: Proxy) {
        target.#returnChannel = returnChannel;
    }

    static listen(target: Proxy) {
        if (target.#mode !== 'sync') {
            throw new Error(`Proxy.listen(): can not be called for mode '${target.#mode}'`);
        }
        try {
            for (; ;) {
                target.#processMessage(target.#receiveMessage());
            }
        }
        catch (e) {
            const workerExit = target.#returnChannel[PROXY_WORKER_EXIT];
            if (workerExit && workerExit === e) {
                DEBUG(target, () => `WebAssembly module exited with code ${workerExit.rval}.`);
                return;
            }
            if (e instanceof Error && /RuntimeError:\s*unreachable\n\s*at\s*abort\s*\(/.test(e.stack!)) {
                DEBUG(target, () => `WebAssembly module aborted.`);
                postMessage(['log', 'warn', `WebAssembly module aborted.\n${e.stack!.split('\n').slice(2).join('\n')}`]);
                try { (target.#returnChannel as unknown as WASIModuleImportsProxy).sys.exit(-1); } catch { }
                return;
            }
            console.error(`[Proxy '${target.#name}'] uncaught error:`, e);
            postMessage(['log', 'error', `Uncaught error in WebAssembly module worker:\n${(e as Error).stack}`]);
        }
    }

    static getProxy(target: Object) {
        const proxy = (target as ProxyTargetInfo)[PROXY_TARGET];
        if (!proxy) {
            throw new Error(`Proxy.getProxy(): target has no associated proxy`);
        }
        return proxy;
    }

    static getReturnChannel(target: Proxy) {
        return target.#returnChannel;
    }


    static async waitForReturnChannelMessage(target: Proxy, timeout?: number) {
        if (target.#mode !== 'async') {
            throw new Error(`Proxy.waitForReturnChannelMessage(): can not be called for mode '${target.#mode}'`);
        }
        await Atomics.waitAsync(target.#returnChannel.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE, 0, timeout).value;
    }

    #getTargetFunction(type: number, functionId: number) {
        if (type === PROXY_MESSAGE_TYPE_FUNCTION) {
            const targetFunction = this.#functions.get(functionId);
            const functionName = targetFunction[PROXY_TARGET_FUNCTION_FULL_NAME];
            if (!targetFunction) {
                console.warn(`[Proxy '${this.#name}'] #processMessage: Invalid function #${functionId}.`);
                return [undefined, undefined];
            }
            return [targetFunction, functionName];
        }
        if (type >= PROXY_MESSAGE_TYPE_CALLBACK_0) {
            const tableId = type - PROXY_MESSAGE_TYPE_CALLBACK_0 + 1;
            const table = this.#tables.get(tableId);
            if (!table) {
                console.warn(`[Proxy '${this.#name}'] Invalid table #${tableId}.`);
                return [undefined, undefined];
            }
            const targetFunction = table.get(functionId);
            const functionName = `${table[PROXY_TARGET_FUNCTION_FULL_NAME]}[${functionId}]`;
            if (!targetFunction) {
                console.warn(`[Proxy '${this.#name}'] Invalid callback #${functionName}.`);
                return [undefined, undefined];
            }
            return [targetFunction, functionName];
        }
        return [undefined, undefined];
    }

    #processMessage(message: ProxyMessage, context: Proxy | ProxyInvocationContext = this) {
        const [invocationId, type, functionId, values] = message;

        if (type === PROXY_MESSAGE_TYPE_RETURN) {
            return this.#returnChannel.#endInvocationContext(message, context);
        }

        if (type === PROXY_MESSAGE_TYPE_BATCH) {
            return this.#processBatchedMessages(invocationId, context);
        }

        const [targetFunction, functionName] = this.#getTargetFunction(type, functionId);
        DEBUG(context, () => `Executing ${functionName} started. (args=[${values.join(', ')}])`);
        switch (this.#mode) {
            case 'sync': {
                const returnValue = targetFunction(...values) || 0;
                DEBUG(context, () => `Executing ${functionName} finished. (result=${returnValue})`);
                this.#returnChannel.#sendMessage(invocationId, PROXY_MESSAGE_TYPE_RETURN, functionId, [returnValue], context);
                return;
            }
            case 'async': {
                return Promise.resolve(targetFunction(...values)).
                    then(returnValue => returnValue || 0).
                    then(async returnValue => {
                        DEBUG(context, () => `Executing ${functionName} finished. (result=${returnValue})`);
                        await this.#returnChannel.#sendMessageAsync(invocationId, PROXY_MESSAGE_TYPE_RETURN, functionId, [returnValue], context);
                    });
            }
        }
    }

    #processBatchedMessages(invocationId: number, context: Proxy | ProxyInvocationContext = this) {
        return Mutex[this.#mode](this.#messageHeader, PROXY_BATCHED_MESSAGE_MUTEX, () => {
            DEBUG(context, () => `Executing batch started.`);

            let asyncResult = this.#mode === 'async' ? Promise.resolve() : undefined;
            let messageCount = Atomics.exchange(this.#messageHeader, PROXY_BATCHED_MESSAGE_COUNT, 0);

            for (let i = 0; i < messageCount; i++) {
                const headerOffset = PROXY_BATCHED_FIRST_MESSAGE_HEADER + i * (PROXY_MESSAGE_BUFFER_LENGTH / Int32Array.BYTES_PER_ELEMENT);
                const valuesOffset = PROXY_BATCHED_FIRST_MESSAGE_VALUES + i * (PROXY_MESSAGE_BUFFER_LENGTH / Float64Array.BYTES_PER_ELEMENT);

                const type = this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_TYPE];
                const functionId = this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_FUNCTION_ID];
                const valueCount = this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_VALUE_COUNT];
                const valueFlags = this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_VALUE_FLAGS];
                const values = this.#readMessageValues(valueCount, valueFlags, valuesOffset);

                this.#messageHeader.fill(0, headerOffset, headerOffset + PROXY_MESSAGE_NUM_HEADER_ELEMENTS);
                this.#messageValues.fill(0, valuesOffset, valuesOffset + PROXY_MESSAGE_NUM_VALUE_ELEMENTS);

                if (type < PROXY_MESSAGE_TYPE_FUNCTION) {
                    console.warn(`[Proxy '${this.#name}'] Invalid message type in batch: type=${type}.`);
                    continue;
                }

                const [targetFunction, functionName] = this.#getTargetFunction(type, functionId);
                if (!targetFunction) {
                    console.warn(`[Proxy '${this.#name}'] Invalid function in batch: type=${type}, functionId=${functionId}.`);
                    continue;
                }

                switch (this.#mode) {
                    case 'sync':
                        DEBUG(context, () => `Executing ${functionName} in batch started. (args=[${values.join(', ')}])`);
                        targetFunction(...values);
                        DEBUG(context, () => `Executing ${functionName} in batch finished.`);
                        continue;
                    case 'async':
                        asyncResult = asyncResult!.
                            then(() => DEBUG(context, () => `Executing ${functionName} in batch started. (args=[${values.join(', ')}])`)).
                            then(() => targetFunction(...values)).
                            then(() => DEBUG(context, () => `Executing ${functionName} in batch finished.`));
                        continue;
                }
            }

            if (asyncResult) {
                return asyncResult.
                    then(() => DEBUG(context, () => `Executing batch finished.`)).
                    then(() => this.#returnChannel.#sendMessageAsync(invocationId, PROXY_MESSAGE_TYPE_RETURN, 0, [messageCount], context)) as unknown as void;
            }
            else {
                DEBUG(context, () => `Executing batch finished.`);
                this.#returnChannel.#sendMessage(invocationId, PROXY_MESSAGE_TYPE_RETURN, 0, [messageCount], context);
            }
        });
    }

    #canEnableBatchMode() {
        return (
            this.#mode === 'sync' &&
            this.#buffer.byteLength > (PROXY_MESSAGE_BYTE_OFFSET_FOR_HEADER + PROXY_MESSAGE_BUFFER_LENGTH)
        );
    }

    #canBatchFunctionCall(functionName: string) {
        return (
            !functionName.startsWith('imports.sys.') ||
            functionName === 'imports.sys.wait_for_animation_frame'
        );
    }

    #invoke(functionName: string, type: number, functionId: number, values: (number | bigint)[]) {
        if (this.#batchMode.enabled) {
            if (this.#batchMessage(functionName, type, functionId, values)) {
                return;
            }
            this.#batchMode(false);
        }
        const context = this.#beginInvocationContext(functionName);
        DEBUG(context, () => `Proxying ${functionName} started. (args=[${values.join(', ')}])`);
        switch (this.#mode) {
            case 'sync':
                this.#sendMessage(context.id, type, functionId, values, context);
                if (this[PROXY_WORKER_EXIT]) {
                    return;
                }
                for (; ;) {
                    const message = this.#returnChannel.#receiveMessage(context);
                    const result = this.#returnChannel.#processMessage(message, context) as (void | { returnValue: number | bigint });
                    if (result) {
                        DEBUG(context, () => `Proxying ${functionName} finished. (result=${result.returnValue})`);
                        return result.returnValue;
                    }
                }
            case 'async':
                return this.#sendMessageAsync(context.id, type, functionId, values, context).
                    then(async () => {
                        if (this[PROXY_WORKER_EXIT]) {
                            return;
                        }
                        for (; ;) {
                            const message = await this.#returnChannel.#receiveMessageAsync(context);
                            const result = await this.#returnChannel.#processMessage(message, context);
                            if (result) {
                                DEBUG(context, () => `Proxying ${functionName} finished. (result=${result.returnValue})`);
                                return result.returnValue;
                            }
                        }
                    });
        }
    }

    #batchMessage(functionName: string, type: number, functionId: number, values: (number | bigint)[]) {
        return this.#canBatchFunctionCall(functionName) && Mutex.sync(this.#messageHeader, PROXY_BATCHED_MESSAGE_MUTEX, () => {
            let i = this.#messageHeader[PROXY_BATCHED_MESSAGE_COUNT];

            if (getProxyBufferSizeForBatchSize(i + 1) > this.#buffer.byteLength) {
                console.warn(`[Proxy '${this.#name}'] #batch: buffer exhausted.`);
                return false;
            }

            DEBUG(this, () => `Adding proxied invocation ${functionName} to batch. (args=[${values.join(', ')}])`);

            const headerOffset = PROXY_BATCHED_FIRST_MESSAGE_HEADER + i * (PROXY_MESSAGE_BUFFER_LENGTH / Int32Array.BYTES_PER_ELEMENT);
            const valuesOffset = PROXY_BATCHED_FIRST_MESSAGE_VALUES + i * (PROXY_MESSAGE_BUFFER_LENGTH / Float64Array.BYTES_PER_ELEMENT);

            const valueFlags = this.#writeMessageValues(values, valuesOffset);

            this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_TYPE] = type;
            this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_FUNCTION_ID] = functionId;
            this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_VALUE_COUNT] = values.length;
            this.#messageHeader[headerOffset + PROXY_MESSAGE_HEADER_VALUE_FLAGS] = valueFlags;

            Atomics.add(this.#messageHeader, PROXY_BATCHED_MESSAGE_COUNT, 1);
            return true;
        });
    }

    #flushBatch() {
        const context = this.#beginInvocationContext('#flushBatch');
        DEBUG(context, () => `Flushing batch started.`);
        this.#sendMessage(context.id, PROXY_MESSAGE_TYPE_BATCH, 0, [], context);
        for (; ;) {
            const message = this.#returnChannel.#receiveMessage(context);
            const result = this.#returnChannel.#processMessage(message, context) as (void | { returnValue: number | bigint });
            if (result) {
                DEBUG(context, () => `Flushing batch finished. (${result.returnValue} calls)`);
                return;
            }
        }
    }

    #sendMessage(invocationId: number, type: number, functionId: number, values: (number | bigint)[], context: Proxy | ProxyInvocationContext = this) {
        do {
            Atomics.wait(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE, 1);
        }
        while (!this.#tryWriteMessage(invocationId, type, functionId, values));
        DEBUG(context, () => `>> sendMessage(type=${type}, functionId=${functionId}, values=[${values.join()}])`);
    }

    async #sendMessageAsync(invocationId: number, type: number, functionId: number, values: (number | bigint)[], context: Proxy | ProxyInvocationContext = this) {
        do {
            await Atomics.waitAsync(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE, 1).value;
        }
        while (!(await this.#tryWriteMessage(invocationId, type, functionId, values)));
        DEBUG(context, () => `>> sendMessageAsync(type=${type}, functionId=${functionId}, values=[${values.join()}])`);
    }

    #receiveMessage(context: Proxy | ProxyInvocationContext = this) {
        for (; ;) {
            Atomics.wait(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE, 0);
            const message = (context !== this && (context as ProxyInvocationContext).returnMessage) || this.#tryReadMessage() as (ProxyMessage | null);
            if (!message) {
                continue;
            }
            DEBUG(context, () => `<< receiveMessage() => [iid=${message[0]}, type=${message[1]}, functionId=${message[2]}, values=[${message[3].join()}]]`);
            return message;
        }
    }

    async #receiveMessageAsync(context: Proxy | ProxyInvocationContext = this) {
        for (; ;) {
            await Atomics.waitAsync(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE, 0).value;
            const message = (context !== this && (context as ProxyInvocationContext).returnMessage) || await this.#tryReadMessage();
            if (!message) {
                continue;
            }
            DEBUG(context, () => `<< receiveMessageAsync() => [iid=${message[0]}, type=${message[1]}, functionId=${message[2]}, values=[${message[3].join()}]]`);
            return message;
        }
    }

    #tryWriteMessage(invocationId: number, type: number, functionId: number, values: (number | bigint)[]) {
        return Mutex[this.#mode](this.#messageControl, PROXY_MESSAGE_CONTROL_MUTEX, () => {
            if (Atomics.load(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE) === 1) {
                return false;
            }

            const valueFlags = this.#writeMessageValues(values);

            this.#messageHeader[PROXY_MESSAGE_HEADER_TYPE] = type;
            this.#messageHeader[PROXY_MESSAGE_HEADER_FUNCTION_ID] = functionId;
            this.#messageHeader[PROXY_MESSAGE_HEADER_VALUE_COUNT] = values.length;
            this.#messageHeader[PROXY_MESSAGE_HEADER_VALUE_FLAGS] = valueFlags;

            this.#messageControl[PROXY_MESSAGE_CONTROL_HAS_MESSAGE] = 1;
            this.#messageControl[PROXY_MESSAGE_CONTROL_INVOCATION_ID] = invocationId;

            Atomics.notify(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE);
            return true;
        });
    }

    #writeMessageValues(values: (number | bigint)[], valuesOffset = 0) {
        let valueFlags = 0;
        for (let i = 0; i < values.length; i++) {
            const value = values[i];
            if (typeof value === 'bigint') {
                this.#messageValuesInt64[valuesOffset + i] = value;
                valueFlags |= 1 << i;
            }
            else {
                this.#messageValues[valuesOffset + i] = value;
            }
        }
        this.#messageValues.fill(0, valuesOffset + values.length, valuesOffset + PROXY_MESSAGE_NUM_VALUE_ELEMENTS);
        return valueFlags;
    }

    #tryReadMessage(): Promise<ProxyMessage | null> | ProxyMessage | null {
        return Mutex[this.#mode](this.#messageControl, PROXY_MESSAGE_CONTROL_MUTEX, () => {
            if (Atomics.load(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE) === 0) {
                return null;
            }

            const invocationId = this.#messageControl[PROXY_MESSAGE_CONTROL_INVOCATION_ID];
            const type = this.#messageHeader[PROXY_MESSAGE_HEADER_TYPE];
            const functionId = this.#messageHeader[PROXY_MESSAGE_HEADER_FUNCTION_ID];
            const valueCount = this.#messageHeader[PROXY_MESSAGE_HEADER_VALUE_COUNT];
            const valueFlags = this.#messageHeader[PROXY_MESSAGE_HEADER_VALUE_FLAGS]
            const values = this.#readMessageValues(valueCount, valueFlags);

            this.#messageControl[PROXY_MESSAGE_CONTROL_HAS_MESSAGE] = 0;
            this.#messageControl[PROXY_MESSAGE_CONTROL_INVOCATION_ID] = 0;
            this.#messageHeader.fill(0, 0, PROXY_MESSAGE_NUM_HEADER_ELEMENTS);
            this.#messageValues.fill(0, 0, PROXY_MESSAGE_NUM_VALUE_ELEMENTS);

            Atomics.notify(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE);

            return [invocationId, type, functionId, values] as const;
        });
    }

    #readMessageValues(valueCount: number, valueFlags: number, valuesOffset = 0) {
        const values: (number | bigint)[] = [];
        values.length = Math.min(valueCount, PROXY_MESSAGE_NUM_VALUE_ELEMENTS);
        for (let i = 0; i < values.length; i++) {
            if (valueFlags & 1) {
                values[i] = this.#messageValuesInt64[valuesOffset + i];
            }
            else {
                values[i] = this.#messageValues[valuesOffset + i];
            }
            valueFlags >>= 1;
        }
        return values;
    }

    static #setupBatchModeControl(proxy: Proxy): ProxyBatchModeControl {
        let isEnabled = false;
        const eventTarget = new EventTarget();
        return Object.defineProperties(
            function setProxyBatchMode(enabled) {
                enabled = Boolean(enabled);
                if (isEnabled === enabled) {
                    return;
                }
                if (enabled && !proxy.#canEnableBatchMode()) {
                    throw new Error('Proxy: No buffer space allocated for batching or trying to enable batch mode on async proxy');
                }
                isEnabled = enabled;
                if (!enabled) {
                    proxy.#flushBatch();
                }
                eventTarget.dispatchEvent(
                    Object.assign(
                        new Event('changed'),
                        { get enabled() { return enabled; } }
                    )
                );
            } as ProxyBatchModeControl,
            {
                addEventListener: {
                    value: eventTarget.addEventListener.bind(eventTarget),
                    enumerable: false,
                    configurable: false,
                    writable: false
                },
                removeEventListener: {
                    value: eventTarget.removeEventListener.bind(eventTarget),
                    enumerable: false,
                    configurable: false,
                    writable: false
                },
                enabled: {
                    get: () => isEnabled,
                    enumerable: false,
                    configurable: false
                },
            }
        );
    }

    #beginInvocationContext(functionName: string): ProxyInvocationContext {
        let id = this.#invocations.findIndex(invocation => invocation === null);
        if (id < 0) {
            id = this.#invocations.length;
            this.#invocations.push(null!);
        }
        return this.#invocations[id] = {
            id,
            mode: this.#mode,
            functionName,
            [DEBUG_LOGGER](message) {
                console.log(`[mode=${this.mode}, iid=${this.id}] ${message}`);
            }
        };
    }

    #endInvocationContext(message: ProxyMessage, context: Proxy | ProxyInvocationContext) {
        const [invocationId, , , values] = message;

        if (context !== this && invocationId === (context as ProxyInvocationContext).id) {
            this.#invocations[invocationId] = null!;
            return { returnValue: values[0] };
        }

        this.#invocations[invocationId].returnMessage = message;

        Atomics.notify(this.#messageControl, PROXY_MESSAGE_CONTROL_HAS_MESSAGE);
    }

    [DEBUG_LOGGER](message: string) {
        console.log(`[mode=${this.#mode}] ${message}`);
    }
}

export function getProxyReturnChannel(target: Proxy) {
    return getRealProxyContructor(target).getReturnChannel(target);
}

function getRealProxyContructor(target: Proxy): typeof Proxy {
    return Object.getPrototypeOf(target).constructor;
}

function workerMain(worker: Worker) {
    worker.onmessage = async ({ data }) => {
        switch (data[0]) {
            case 'compileWasmModule':
                // @ts-ignore
                compileWasmModule(worker, ...data[1]);
                break;
            case 'waitAsyncPolyfill':
                // @ts-ignore
                waitAsyncPolyfill(worker, ...data[1])
                break;
        }
    }
}

// @ts-ignore
if (typeof WorkerGlobalScope !== 'undefined' && self instanceof WorkerGlobalScope) {
    self.onunhandledrejection = event => {
        console.error('Unhandled promise rejection!', event.reason);
    };
    // @ts-ignore
    workerMain(self);
}

async function compileWasmModule(worker: Worker, wasmBinary: ArrayBuffer, imports: SerializedProxy, options: { plugins?: [string, Object][]; __ENABLE_WASI_DEBUG_LOG?: boolean; } = {}) {
    self.__ENABLE_WASI_DEBUG_LOG ||= Boolean(options.__ENABLE_WASI_DEBUG_LOG);
    try {
        const importsProxy = Proxy.deserialize<WASIModuleImportsProxy>(imports, { name: 'imports' });

        const memory = new WASIMemory();
        const pluginModules = await loadPluginModules(options.plugins || []);

        setupWorkerLocalFunctionOverrides(importsProxy, memory);

        for (const plugin of pluginModules) {
            if (plugin.applyProxyImports) {
                await plugin.applyProxyImports(importsProxy, memory);
            }
        }

        const wasmModule = await WebAssembly.compile(wasmBinary);
        const wasmInstance = await WebAssembly.instantiate(wasmModule, importsProxy);

        let exports = { ...wasmInstance.exports } as WASIModuleExports;

        memory.init(exports);

        for (const plugin of pluginModules) {
            if (plugin.applyProxyExports) {
                await plugin.applyProxyExports(importsProxy, exports, memory);
            }
        }

        const exportsProxy = Proxy.createServer(exports, { name: 'exports' });

        Proxy.setReturnChannel(importsProxy, exportsProxy);
        Proxy.setReturnChannel(exportsProxy, importsProxy);

        worker.postMessage(['success', Proxy.serialize(exportsProxy)]);
        worker.onmessage = () => {
            Proxy.listen(exportsProxy);
            worker.postMessage(['finished']);
        };
    }
    catch (e) {
        worker.postMessage(['error', (e as Error).message]);
        console.error('Failed to compile WASM module: ', e);
    }
}

function loadPluginModules(plugins: (string | [string, Object])[]): Promise<({ path: string; options: Object; } & WASIPlugin)[]> {
    return Promise.all(plugins.map(async path => {
        let options = {};
        if (Array.isArray(path)) {
            options = path[1] || options;
            path = path[0];
        }

        const {
            applyImports,
            applyProxyImports,
            applyProxyExports,
            getProxyPluginOptions = defaultGetProxyPluginOptions
        } = (await import(path)) as WASIPlugin;

        const plugin = {
            path,
            options,
            applyImports,
            applyProxyImports,
            applyProxyExports,
            getProxyPluginOptions
        };

        for (const fn of ['applyImports', 'applyProxyImports', 'applyProxyExports', 'getProxyPluginOptions'] as const) {
            if (!(typeof plugin[fn] === 'function' || plugin[fn] === undefined)) {
                throw new Error(`Expected exported member '${fn}' in '${path}' to be a function`);
            }
        }
        if (!plugin.applyImports && !plugin.applyProxyImports && !plugin.applyProxyExports) {
            throw new Error(`Expected '${path}' to at least export one of applyImports, applyProxyImports or applyProxyExports`);
        }

        return plugin;
    }));
}

function defaultGetProxyPluginOptions(this: { options: Object }) {
    return serialize(this.options) as Object;

    function serialize(obj: unknown): bigint | boolean | number | string | Object | null | undefined {
        switch (typeof obj) {
            case 'bigint':
            case 'boolean':
            case 'number':
            case 'string':
                return obj;
            case 'object':
                if (obj === null) {
                    return obj;
                }
                if (Array.isArray(obj)) {
                    return obj.map(serialize);
                }
                else {
                    return Object.fromEntries(
                        Object.entries(obj).
                            map(([key, value]) => [key, serialize(value)]).
                            filter(([, value]) => value !== undefined)
                    );
                }
        }
    }
}

function setupWorkerLocalFunctionOverrides(proxy: Proxy & WASIModuleImportsProxy, memory: WASIMemory) {
    const { exit } = proxy.sys;
    
    proxy.sys.exit = (rval) => {
        proxy[PROXY_WORKER_EXIT] = { rval };
        proxy.__proxy.batch_mode(false);
        exit(rval);
        throw proxy[PROXY_WORKER_EXIT];
    };

    // No need for a round trip to the main thread for this function
    proxy.sys.clock = (clock_id: number, time: number) => {
        switch (clock_id) {
            case __WASI_CLOCK_REALTIME:
                memory.writeFloat64(time, Date.now());
                return __WASI_ERRNO_SUCCESS;
            case __WASI_CLOCK_MONOTONIC:
                memory.writeFloat64(time, performance.now());
                return __WASI_ERRNO_SUCCESS;
            default:
                return __WASI_ERRNO_INVAL;
        }
    };
}

function waitAsyncPolyfill(worker: Worker, typedArray: any, index: number, value: any, timeout: number) {
    let result = Atomics.wait(typedArray, index, value, timeout);
    if (result !== 'timed-out') {
        result = 'ok'; // We know that it was 'ok' prior to the async invocation
    }
    worker.postMessage(result);
}

// We need to polyfill Atomics.waitAsync for Firefox
if (typeof Window !== 'undefined' && self instanceof Window && !Atomics.waitAsync) {
    const cachedWorkers: Worker[] = [];
    Atomics.waitAsync = function waitAsync(typedArray: any, index: number, value: any, timeout: number) {
        if (timeout === 0) {
            return { async: false, value: 'timeout' }
        }
        if (Atomics.load(typedArray, index) !== value) {
            return { async: false, value: 'not-equal' }
        }
        const worker = cachedWorkers.pop() || new Worker(import.meta.url || '/js/wasi', { type: 'module' });
        return {
            async: true,
            value: new Promise(resolve => {
                worker.onmessage = ({ data }) => {
                    worker.onmessage = null;
                    cachedWorkers.push(worker);
                    resolve(data);
                };
                worker.postMessage(['waitAsyncPolyfill', [typedArray, index, value, timeout]]);
            })
        };
    } as typeof Atomics.waitAsync;
}

export function newCharacterDeviceWriter(writer: (chars: string) => void): (chars: Uint8Array) => number {
    const decoder = new TextDecoder();
    return bytes => {
        writer(decoder.decode(bytes, { stream: true }));
        return bytes.length;
    };
}

export function newLineBufferedCharacterDeviceWriter(writer: (line: string) => void): (chars: Uint8Array) => number {
    let buffer = '';
    return newCharacterDeviceWriter(chars => {
        buffer += chars;
        if (buffer.indexOf('\n') >= 0) {
            const lines = buffer.split('\n');
            buffer = lines.splice(-1, 1)[0];
            for (let line of lines) {
                writer(line);
            }
        }
    });
}

const __BROWSER_ROOTDIR_FD = 3;
const __BROWSER_LOCALSTORAGE_FD = 4;
const __BROWSER_SESSIONSTORAGE_FD = 5;

const __BROWSER_LOAD_CALLBACK_RESULT_IN_PROGRESS = 0xFFFF;

const __WASI_ERRNO_SUCCESS = 0;
const __WASI_ERRNO_ACCES = 2;
const __WASI_ERRNO_AGAIN = 6;
const __WASI_ERRNO_CANCELED = 11;
const __WASI_ERRNO_INVAL = 28;
const __WASI_ERRNO_IO = 29;
const __WASI_ERRNO_NOENT = 44;
const __WASI_ERRNO_PERM = 63;
const __WASI_ERRNO_ROFS = 69;
const __WASI_ERRNO_NOTCAPABLE = 76;

const __WASI_FILETYPE_CHARACTER_DEVICE = 2;
const __WASI_FILETYPE_REGULAR_FILE = 4;

const __WASI_OFLAGS_CREAT = 1 << 0;
const __WASI_OFLAGS_EXCL = 1 << 2;
const __WASI_OFLAGS_TRUNC = 1 << 3;

const __WASI_CLOCK_REALTIME = 0;
const __WASI_CLOCK_MONOTONIC = 1;

const __BROWSER_STARTUP_STRINGS_ARGS = 0;
const __BROWSER_STARTUP_STRINGS_ENVIRON = 1;

const __BROWSER_WAIT_FOR_TIMEOUT = 0x01;
const __BROWSER_WAIT_FOR_INTERRUPT = 0x02;

function getSystem(memory: WASIModule['memory']) {

    const startupStrings: {
        kind?: number;
        count?: number;
        pointers?: Uint32Array;
        buffer?: Uint8Array;
        sources: Record<number, string[]>;
    } = {
        sources: {}
    };

    const standardIO: {
        write?: (chars: Uint8Array) => number;
        buffer?: ArrayBuffer;
    }[] = [
            {},
            { write: newLineBufferedCharacterDeviceWriter(s => console.info(s)) },
            { write: newLineBufferedCharacterDeviceWriter(s => console.warn(s)) }
        ];

    const fileSystems = {
        [__BROWSER_ROOTDIR_FD]: new RootFileSystem(),
        [__BROWSER_LOCALSTORAGE_FD]: new BrowserStorageFileSystem(localStorage),
        [__BROWSER_SESSIONSTORAGE_FD]: new BrowserStorageFileSystem(sessionStorage),
    };

    let hasStarted = false;
    let indirectFunctionTable: WASIModuleExportsProxy['__indirect_function_table'] = null!;

    const asyncMethodCalls = new AsyncMethodCalls();
    const exit = { rval: 0 };

    const imports = {
        get_startup_strings(kind: number, strings: number, buf: number) {
            if (!(startupStrings && startupStrings.kind === kind)) {
                loadStartupStrings(kind);
            }
            memory.writeUint32v(strings, startupStrings.pointers!.map(p => buf + p));
            memory.writeBytes(buf, startupStrings.buffer!);
            clearStartupStrings();
            return __WASI_ERRNO_SUCCESS;
        },
        get_startup_strings_sizes(kind: number, strings_count: number, buf_size: number) {
            loadStartupStrings(kind);
            memory.writeUint32(strings_count, startupStrings.count!);
            memory.writeUint32(buf_size, startupStrings.buffer!.byteLength);
            return __WASI_ERRNO_SUCCESS;
        },
        clock(clock_id: number, time: number) {
            switch (clock_id) {
                case __WASI_CLOCK_REALTIME:
                    memory.writeFloat64(time, Date.now());
                    return __WASI_ERRNO_SUCCESS;
                case __WASI_CLOCK_MONOTONIC:
                    memory.writeFloat64(time, performance.now());
                    return __WASI_ERRNO_SUCCESS;
                default:
                    return __WASI_ERRNO_INVAL;
            }
        },
        file_info(dirfd: number, path: number, path_len: number, ino: number, size: number) {
            const pathString = memory.readString(path, path_len);
            if (pathString === null) {
                throw new Error('Invalid value NULL for argument path in sys.file_info');
            }
            const fileSystem = fileSystems[dirfd as keyof typeof fileSystems];
            const file = fileSystem && fileSystem.get(pathString);
            if (!file) {
                return __WASI_ERRNO_NOENT;
            }
            memory.writeUint32(ino, file.id);
            memory.writeUint32v(size, [file.buffer.byteLength % 0x100000000, file.buffer.byteLength / 0x100000000]);
            return __WASI_ERRNO_SUCCESS;
        },
        open_file(dirfd: number, path: number, path_len: number, oflags: number, ino: number, size: number) {
            const pathString = memory.readString(path, path_len);
            if (pathString === null) {
                throw new Error('Invalid value NULL for argument path in sys.open_file');
            }
            const fileSystem = fileSystems[dirfd as keyof typeof fileSystems];
            let file = fileSystem && fileSystem.get(pathString);
            if (oflags & __WASI_OFLAGS_CREAT) {
                if (!fileSystem.writable) {
                    return __WASI_ERRNO_ROFS;
                }
                else if (!file) {
                    file = fileSystem.create(pathString)
                }
                else if (oflags & __WASI_OFLAGS_EXCL) {
                    return __WASI_ERRNO_CANCELED;
                }
            }
            else if (!file) {
                return __WASI_ERRNO_NOENT;
            }
            else if (oflags & __WASI_OFLAGS_TRUNC) {
                if (!fileSystem.writable) {
                    return __WASI_ERRNO_ROFS;
                }
                fileSystem.truncate(file);
            }
            // TODO: write / read access
            file.referenceCount++;
            memory.writeUint32(ino, file.id);
            memory.writeUint32v(size, [file.buffer.byteLength % 0x100000000, file.buffer.byteLength / 0x100000000]);
            return __WASI_ERRNO_SUCCESS;
        },
        read_file(dirfd: number, ino: number, filetype: number, iovs: number, iovs_len: number, offset: number, nread: number) {
            let readBuffer = null;
            switch (filetype) {
                case __WASI_FILETYPE_CHARACTER_DEVICE: {
                    const device = standardIO[ino];
                    readBuffer = device && device.buffer;
                    if (readBuffer && readBuffer.byteLength === 0) {
                        return __WASI_ERRNO_AGAIN;
                    }
                    break;
                }
                case __WASI_FILETYPE_REGULAR_FILE: {
                    const fileSystem = fileSystems[dirfd as keyof typeof fileSystems];
                    const file = fileSystem && fileSystem.getById(ino);
                    readBuffer = file && file.buffer;
                    break;
                }
            }
            if (!readBuffer) {
                memory.writeUint32(nread, 0);
                return __WASI_ERRNO_NOTCAPABLE;
            }
            const [offset_lo, offset_hi] = memory.readUint32v(offset, 2);
            let byteOffset = offset_hi * 0x100000000 + offset_lo;
            let bytesRead = 0;
            for (let i = 0; byteOffset < readBuffer.byteLength && i < iovs_len; i++) {
                const buf = memory.readUint32v(iovs + (i * 2) * Uint32Array.BYTES_PER_ELEMENT, 1)[0];
                const buf_len = memory.readUint32v(iovs + (i * 2 + 1) * Uint32Array.BYTES_PER_ELEMENT, 1)[0];
                const byteLength = Math.min(buf_len, readBuffer.byteLength - byteOffset);
                memory.writeBytes(buf, new Uint8Array(readBuffer, byteOffset, byteLength));
                byteOffset += byteLength;
                bytesRead += byteLength;
            }
            memory.writeUint32v(offset, [byteOffset % 0x100000000, byteOffset / 0x100000000]);
            memory.writeUint32(nread, bytesRead);
            return __WASI_ERRNO_SUCCESS;
        },
        write_file(dirfd: number, ino: number, filetype: number, iovs: number, iovs_len: number, offset: number, nwritten: number) {
            let write: ((chars: Uint8Array, offset: number) => number) | undefined;
            switch (filetype) {
                case __WASI_FILETYPE_CHARACTER_DEVICE: {
                    write = standardIO[ino] && standardIO[ino].write;
                    break;
                }
                case __WASI_FILETYPE_REGULAR_FILE: {
                    const fileSystem = fileSystems[dirfd as keyof typeof fileSystems];
                    const file = fileSystem && fileSystem.getById(ino);
                    write = (file && fileSystem.writable && fileSystem.write.bind(fileSystem, file)) || undefined;
                    break;
                }
            }
            if (!write) {
                memory.writeUint32(nwritten, 0);
                return __WASI_ERRNO_NOTCAPABLE;
            }
            const bytes = [];
            for (let i = 0; i < iovs_len; i++) {
                const buf = memory.readUint32(iovs + (i * 2) * Uint32Array.BYTES_PER_ELEMENT);
                const buf_len = memory.readUint32(iovs + (i * 2 + 1) * Uint32Array.BYTES_PER_ELEMENT);
                bytes.push(...memory.readBytes(buf, buf_len));
            }
            const bytesWritten = write(new Uint8Array(bytes), offset);
            memory.writeUint32(nwritten, bytesWritten);
            return __WASI_ERRNO_SUCCESS;
        },
        flush_file(dirfd: number, ino: number) {
            const fileSystem = fileSystems[dirfd as keyof typeof fileSystems];
            const file = fileSystem && fileSystem.getById(ino);
            if (!file) {
                return __WASI_ERRNO_NOENT;
            }
            if (fileSystem.writable) {
                fileSystem.flush(file);
            }
            return __WASI_ERRNO_SUCCESS;
        },
        close_file(dirfd: number, ino: number) {
            const fileSystem = fileSystems[dirfd as keyof typeof fileSystems];
            const file = fileSystem && fileSystem.getById(ino);
            if (!file) {
                return __WASI_ERRNO_NOENT;
            }
            if (file.referenceCount > 0) {
                if (fileSystem.writable) {
                    fileSystem.flush(file);
                }
                file.referenceCount--;
            }
            return __WASI_ERRNO_SUCCESS;
        },
        unlink_file(dirfd: number, path: number, path_len: number) {
            const pathString = memory.readString(path, path_len);
            if (pathString === null) {
                throw new Error('Invalid value NULL for argument path in sys.unlink_file');
            }
            const fileSystem = fileSystems[dirfd as keyof typeof fileSystems];
            const file = fileSystem && fileSystem.get(pathString);
            if (!file) {
                return __WASI_ERRNO_NOENT;
            }
            if (!fileSystem.writable) {
                return __WASI_ERRNO_ROFS;
            }
            if (file.referenceCount > 0) {
                return __WASI_ERRNO_ACCES
            }
            fileSystem.unlink(file);
            return __WASI_ERRNO_SUCCESS;
        },
        load_rootdir_file_async(path: number, path_len: number, callback: number, user_state: number) {
            const pathString = memory.readString(path, path_len);
            if (pathString === null) {
                throw new Error('Invalid value NULL for argument path in sys.load_rootdir_file_async');
            }
            asyncMethodCalls.add(
                (async () => {
                    const unwrappedCallback = AsyncMethodCalls.synchronize(unwrapCallback(callback));
                    const onprogress = (total: number, loaded: number) => unwrappedCallback(__BROWSER_LOAD_CALLBACK_RESULT_IN_PROGRESS, total, loaded, user_state);
                    try {
                        const response = await httpGET(pathString, onprogress);
                        const loaded = response.byteLength, total = response.byteLength;
                        const errno = addFile(pathString, response) ? __WASI_ERRNO_SUCCESS : __WASI_ERRNO_ACCES;
                        await unwrappedCallback(errno, total, loaded, user_state);
                    }
                    catch (error) {
                        const errno = (error instanceof HttpError && error.errno) || HttpError.errno.default;
                        await unwrappedCallback(errno, 0, 0, user_state);
                    }
                })()
            );
        },
        remove_rootdir_file(path: number, path_len: number) {
            const pathString = memory.readString(path, path_len);
            if (pathString === null) {
                throw new Error('Invalid value NULL for argument path in sys.remove_rootdir_file');
            }
            return removeFile(pathString) ? __WASI_ERRNO_SUCCESS : __WASI_ERRNO_ACCES;
        },
        async wait_for_async() {
            do {
                await asyncMethodCalls.done();
            }
            while (asyncMethodCalls.hasPending());
        },
        wait_for_animation_frame() {
            return new Promise<number>(resolve => requestAnimationFrame(time => resolve(time)));
        },
        wait(flags: number, timeout: number) {
            return new Promise<void>(resolve => {
                if (flags & __BROWSER_WAIT_FOR_INTERRUPT) {
                    timeout = (flags & __BROWSER_WAIT_FOR_TIMEOUT) ? timeout : undefined!;
                    Proxy.waitForReturnChannelMessage(Proxy.getProxy(this), timeout).then(() => setTimeout(() => resolve(), 0));
                }
                else if (flags & __BROWSER_WAIT_FOR_TIMEOUT) {
                    setTimeout(() => resolve(), timeout);
                }
                else {
                    throw new Error(`Invalid value for flags in sys.wait(): ${flags}`);
                }
            });
        },
        exit(rval: number): never {
            exit.rval = rval;
            throw exit;
        }
    };

    return {
        imports,
        standardIO,
        setArguments,
        setEnvironment,
        addFile,
        getFileContent,
        removeFile,
        start,
        unwrapCallback
    };

    function setArguments(values: string[]) {
        startupStrings.sources[__BROWSER_STARTUP_STRINGS_ARGS] = ((Array.isArray(values) && values) || []).map(s => String(s));
    }

    function setEnvironment(values: Record<string, string>) {
        startupStrings.sources[__BROWSER_STARTUP_STRINGS_ENVIRON] = Object.entries(values || {}).map(([key, value]) => `${key}=${value}`);
    }

    function addFile(path: string, buffer: ArrayBuffer) {
        return fileSystems[__BROWSER_ROOTDIR_FD].add(path, buffer);
    }

    function getFileContent(path: string): ArrayBuffer | undefined {
        const file = fileSystems[__BROWSER_ROOTDIR_FD].get(path);
        return file && file.buffer;
    }

    function removeFile(path: string) {
        return fileSystems[__BROWSER_ROOTDIR_FD].remove(path);
    }

    async function start(exports: WASIModuleExportsProxy) {
        if (hasStarted) {
            throw new Error('WASIModule: Invalid operation, start() can only be invoked once');
        }
        if (typeof exports._start !== 'function') {
            throw new Error('WASIModule: Binary does not export an _start() method');
        }
        if (!exports.__indirect_function_table || typeof exports.__indirect_function_table.get !== 'function') {
            throw new Error('WASIModule: Binary does not export an __indirect_function_table for callbacks');
        }
        hasStarted = true;
        indirectFunctionTable = exports.__indirect_function_table;
        try {
            await exports._start();
        }
        catch (error) {
            if (error !== exit) {
                throw error;
            }
        }
        return exit.rval;
    }

    function unwrapCallback<T extends (...args: any[]) => Promise<any>>(callback: number) {
        return indirectFunctionTable.get<T>(callback);
    }

    function loadStartupStrings(kind: number) {
        const textEncoder = new TextEncoder();
        const strings = startupStrings.sources[kind] || [];
        const encodedStrings = strings.map(s => [...textEncoder.encode(s), 0]);
        const pointers = encodedStrings.map(s => s.length).reduce<[number[], number]>(([result, offset], l) => (result.push(offset), [result, offset + l]), [[], 0])[0];
        const buffer = ([] as number[]).concat(...encodedStrings);
        startupStrings.kind = kind;
        startupStrings.count = strings.length;
        startupStrings.pointers = new Uint32Array(pointers);
        startupStrings.buffer = new Uint8Array(buffer);
    }

    function clearStartupStrings() {
        delete startupStrings.kind;
        delete startupStrings.count;
        delete startupStrings.pointers;
        delete startupStrings.buffer;
    }
}


function httpGET(path: string, onprogress?: (total: number, loaded: number) => void) {
    return new Promise<ArrayBuffer>((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.responseType = 'arraybuffer';
        xhr.onload = () => {
            const { status, statusText, response } = xhr;
            if (status >= 200 && status <= 299) {
                resolve(response);
            }
            else {
                reject(new HttpError('GET', path, status, statusText));
            }
        };
        if (onprogress) {
            xhr.onprogress = e => onprogress(e.total, e.loaded);
        }
        xhr.onerror = () => reject(new Error(`GET '${path}' failed`));
        xhr.ontimeout = () => reject(new Error(`GET '${path}' timed out`));
        xhr.onabort = () => reject(new Error(`GET '${path}' was aborted`));
        xhr.open('GET', path);
        xhr.send();
    });
}

interface FileSystemEntry {
    path: string;
    id: number;
    buffer: ArrayBuffer;
    referenceCount: number;
}

abstract class FileSystem {
    #filesById: Record<number, FileSystemEntry> = {};
    #filesByPath: Record<string, FileSystemEntry> = {};
    #nextId = 1;

    add(path: string, buffer: ArrayBuffer): FileSystemEntry {
        if (typeof path !== 'string') {
            throw new Error(`FileSystem: argument 'path' is not of type string`);
        }
        while (path.startsWith('/')) {
            path = path.substring(1);
        }
        this.remove(path);
        if (!(buffer instanceof ArrayBuffer)) {
            throw new Error(`FileSystem: argument 'buffer' is not of type ArrayBuffer`);
        }
        const id = this.#nextId++;
        const file = { path, id, buffer, referenceCount: 0 };
        this.#filesByPath[path] = file;
        this.#filesById[id] = file;
        return file;
    }

    get(path: string): FileSystemEntry | undefined {
        return this.#filesByPath[path];
    }

    getById(id: number): FileSystemEntry | undefined {
        return this.#filesById[id];
    }

    remove(path: string): boolean {
        const file = this.#filesByPath[path];
        if (file) {
            delete this.#filesByPath[path];
            delete this.#filesById[file.id];
            return true;
        }
        else {
            return false;
        }
    }
}

interface ReadonlyFileSystem extends FileSystem {
    readonly writable: false;
}

interface WritableFileSystem extends FileSystem {
    create(path: string): FileSystemEntry;
    truncate(file: FileSystemEntry): void;
    flush(file: FileSystemEntry): void;
    unlink(file: FileSystemEntry): void;
    write(file: FileSystemEntry, chars: Uint8Array, offset: number): number;
    readonly writable: true;
}

class RootFileSystem extends FileSystem implements ReadonlyFileSystem {
    get writable() {
        return false as const;
    }
}

class BrowserStorageFileSystem extends FileSystem implements WritableFileSystem {
    #storage: Storage;
    #encoder = new TextEncoder();
    #decoder = new TextDecoder();

    constructor(storage: Storage) {
        super();
        this.#storage = storage;
    }

    get writable() {
        return true as const;
    }

    create(path: string): FileSystemEntry {
        // TODO
        throw new Error('Not implemented');
    }

    truncate(file: FileSystemEntry): void {
        // TODO
        throw new Error('Not implemented');
    }

    flush(file: FileSystemEntry): void {
        // TODO
        throw new Error('Not implemented');
    }

    unlink(file: FileSystemEntry): void {
        // TODO
        throw new Error('Not implemented');
    }

    write(file: FileSystemEntry, chars: Uint8Array, offset: number): number {
        // TODO
        throw new Error('Not implemented');
    }
}

class HttpError extends Error {

    errno = 0;

    constructor(method: string, path: string, status: number, statusText: string) {
        super(`${method} ${path} ${status} (${statusText})`);
        this.errno = HttpError.errno[status] || HttpError.errno.default;
    }

    static errno: Record<string, number> = {
        default: __WASI_ERRNO_IO,
        '401': __WASI_ERRNO_PERM,
        '403': __WASI_ERRNO_PERM,
        '404': __WASI_ERRNO_NOENT
    };
}

class AsyncMethodCalls {

    #pending: Record<string, Promise<unknown>> = {};

    add(value: Promise<unknown>) {
        const id = uuid();
        const done = () => delete this.#pending[id];
        this.#pending[id] = Promise.resolve(value).finally(done);
    }

    done() {
        return Promise.all(Object.values(this.#pending));
    }

    hasPending() {
        return Object.keys(this.#pending).length > 0;
    }

    static synchronize<T extends (...args: any[]) => any>(callback: T) {
        let previous = Promise.resolve();
        return (...args: Parameters<T>) => {
            previous = previous.then(() => callback(...args));
            return previous as Promise<ReturnType<T>>;
        }
    }
}

export const Mutex = Object.freeze({
    async async<T>(typedArray: Int32Array, index: number, callback: () => Promise<T> | T): Promise<T> {
        while (Atomics.compareExchange(typedArray, index, 0, 1) !== 0) {
            await Atomics.waitAsync(typedArray, index, 1).value;
        }
        try {
            return await callback();
        }
        finally {
            Atomics.store(typedArray, index, 0);
            Atomics.notify(typedArray, index);
        }
    },
    sync<T>(typedArray: Int32Array, index: number, callback: () => T): T {
        while (Atomics.compareExchange(typedArray, index, 0, 1) !== 0) {
            Atomics.wait(typedArray, index, 1);
        }
        try {
            return callback();
        }
        finally {
            Atomics.store(typedArray, index, 0);
            Atomics.notify(typedArray, index);
        }
    },
    syncBusyWait<T>(typedArray: Int32Array, index: number, callback: () => T): T {
        while (Atomics.compareExchange(typedArray, index, 0, 1) !== 0);
        try {
            return callback();
        }
        finally {
            Atomics.store(typedArray, index, 0);
            Atomics.notify(typedArray, index);
        }
    }
});

function bindFunctions(target: Object): Object {
    return Object.fromEntries(
        Object.entries(target).
            map(([key, value]) => [key, typeof value === 'function' ? value.bind(target) : value])
    );
}

function uuid() {
    return Array.
        from(crypto.getRandomValues(new Uint8Array(16))).
        map(i => ((i & 0xF0) >> 4).toString(16) + (i & 0x0F).toString(16)).
        join('');
}
