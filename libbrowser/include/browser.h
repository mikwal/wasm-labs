#ifndef __libbrowser_browser_h
#define __libbrowser_browser_h

#include <string>
#include <functional>

namespace browser {

    using std::string;
    using std::function;

    enum class load_file_status : uint8_t {
        partial = 0,
        finished = 1,
        failed = 2
    };

    typedef function<void (
        load_file_status status, 
        size_t content_length, 
        size_t downloaded_bytes)> load_file_callback_t;

    void load_file_async(const string& path, load_file_callback_t&& callback = nullptr);

    bool remove_file(const string& path);

    bool mount_archive(const string& path);

    bool unmount_archive(const string& path);

    void wait_for_async();

    double wait_for_animation_frame();

    void wait_for_interrupt();
    void wait_for_interrupt(double timeout);

    void wait(double timeout);
}

extern "C" void __proxy_batch_mode(bool enable) __attribute__((__import_module__("__proxy"), __import_name__("batch_mode")));

#endif