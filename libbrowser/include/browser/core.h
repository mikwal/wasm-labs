#ifndef __libbrowser_browser_core_h
#define __libbrowser_browser_core_h

#include <wasi/api.h>

extern "C" {

typedef int8_t __browser_startup_strings_kind_t; 
typedef uint32_t __browser_inode_t;

typedef void (*__browser_load_file_callback_t)(
    __wasi_errno_t result,
    size_t content_length,
    size_t downloaded_bytes,
    void *user_state
);

#define __BROWSER_STARTUP_STRINGS_ARGS      (0)
#define __BROWSER_STARTUP_STRINGS_ENVIRON   (1)

#define __BROWSER_STDIN_INODE           (0)
#define __BROWSER_STDOUT_INODE          (1)
#define __BROWSER_STDERR_INODE          (2)
#define __BROWSER_ROOTDIR_INODE         (3)
#define __BROWSER_LOCALSTORAGE_INODE    (4)
#define __BROWSER_SESSIONSTORAGE_INODE  (5)

#define __BROWSER_ROOTDIR_FD        __BROWSER_ROOTDIR_INODE
#define __BROWSER_LOCALSTORAGE_FD   __BROWSER_LOCALSTORAGE_INODE
#define __BROWSER_SESSIONSTORAGE_FD __BROWSER_SESSIONSTORAGE_INODE
#define __BROWSER_FIRST_FREE_FD     (6)
#define __BROWSER_MAX_FD            (1024)

#define __BROWSER_WAIT_FOR_TIMEOUT           (0x01)
#define __BROWSER_WAIT_FOR_INTERRUPT    (0x02)

#define __BROWSER_LOAD_CALLBACK_RESULT_IN_PROGRESS  (0xFFFF)

#define __BROWSER_SYSCALL_NAME(name) __attribute__((__import_module__("sys"), __import_name__(#name)))

__wasi_errno_t __browser_get_startup_strings(
    __browser_startup_strings_kind_t kind,
    uint8_t **strings,
    uint8_t *buf
) __BROWSER_SYSCALL_NAME(get_startup_strings) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_get_startup_strings_sizes(
    __browser_startup_strings_kind_t kind,
    size_t *strings_count,
    size_t *buf_size
) __BROWSER_SYSCALL_NAME(get_startup_strings_sizes) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_clock(
    __wasi_clockid_t clock_id,
    double* time
) __BROWSER_SYSCALL_NAME(clock) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_file_info(
    __wasi_fd_t dirfd,
    const char *path,
    size_t path_len,
    __browser_inode_t *ino,
    __wasi_filesize_t *size
) __BROWSER_SYSCALL_NAME(file_info) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_open_file(
    __wasi_fd_t dirfd,
    const char *path,
    size_t path_len,
    __wasi_oflags_t oflags,
    __browser_inode_t *ino,
    __wasi_filesize_t *size
) __BROWSER_SYSCALL_NAME(open_file) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_read_file(
    __wasi_fd_t dirfd,
    __browser_inode_t ino,
    __wasi_filetype_t filetype,
    const __wasi_iovec_t *iovs,
    size_t iovs_len,
    __wasi_filesize_t* offset,
    size_t *nread
) __BROWSER_SYSCALL_NAME(read_file) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_write_file(
    __wasi_fd_t dirfd,
    __browser_inode_t ino,
    __wasi_filetype_t filetype,
    const __wasi_ciovec_t *iovs,
    size_t iovs_len,
    __wasi_filesize_t* offset,
    size_t *nwritten
) __BROWSER_SYSCALL_NAME(write_file) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_flush_file(
    __wasi_fd_t dirfd,
    __browser_inode_t ino
) __BROWSER_SYSCALL_NAME(flush_file) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_close_file(
    __wasi_fd_t dirfd,
    __browser_inode_t ino
) __BROWSER_SYSCALL_NAME(close_file) __attribute__((__warn_unused_result__));

__wasi_errno_t __browser_unlink_file(
    __wasi_fd_t dirfd,
    const char *path,
    size_t path_len
) __BROWSER_SYSCALL_NAME(unlink_file) __attribute__((__warn_unused_result__));

void __browser_load_rootdir_file_async(
    const char *path,
    size_t path_len,
    __browser_load_file_callback_t callback,
    void *user_state
) __BROWSER_SYSCALL_NAME(load_rootdir_file_async);

__wasi_errno_t __browser_remove_rootdir_file(
    const char *path,
    size_t path_len
) __BROWSER_SYSCALL_NAME(remove_rootdir_file) __attribute__((__warn_unused_result__));

void __browser_wait_for_async() __BROWSER_SYSCALL_NAME(wait_for_async);

double __browser_wait_for_animation_frame() __BROWSER_SYSCALL_NAME(wait_for_animation_frame);

void __browser_wait(
    int flags,
    double timeout
) __BROWSER_SYSCALL_NAME(wait);

void __browser_exit(
    __wasi_exitcode_t rval
) __BROWSER_SYSCALL_NAME(exit) __attribute__((__noreturn__));

#undef __BROWSER_SYSCALL_NAME

}

#endif