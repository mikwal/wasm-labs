#ifndef __libbrowser_browser_callbacks_h
#define __libbrowser_browser_callbacks_h


#include <functional>

namespace browser {

    using std::function;
    
    namespace __callback_store {
        typedef function<void()> callback_t;
        void* malloc();
        void free(void* ptr);
    }
    
    template<class T>
    class callbacks;

    template<class R, class ...Args>
    class callbacks<function<R(Args...)>> {
    public:
        typedef function<R(Args...)> callback_t;

        static_assert(
            sizeof(callback_t) == sizeof(__callback_store::callback_t), 
            "Expects all std::function<T> to have the same size");
    
        static void* store(callback_t&& callback) {
            return new (__callback_store::malloc()) callback_t(std::move(callback));
        }

        static R invoke(Args... args, void *user_state) {
            return ((callback_t*)user_state)->operator()(args...);
        }

        static void release(void* user_state) {
            if (user_state) {
                ((callback_t*)user_state)->~callback_t();
                __callback_store::free(user_state);
            }
        }
    };
}


#endif