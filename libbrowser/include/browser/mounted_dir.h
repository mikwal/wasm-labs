#ifndef __libbrowser_browser_mounted_dir_h
#define __libbrowser_browser_mounted_dir_h

#include <wasi/api.h>
#include <browser/core.h>

__wasi_errno_t __mounted_dir_attach(
    const char *path,
    size_t path_len,
    __browser_inode_t ino, 
    __wasi_filesize_t size,
    uint8_t* header,
    uint32_t header_size
) __attribute__((__warn_unused_result__));

__wasi_errno_t __mounted_dir_file_info(
    const char *path,
    size_t path_len,
    __browser_inode_t *ino,
    __wasi_filesize_t *start,
    __wasi_filesize_t *end
);

__wasi_errno_t __mounted_dir_detach(
    const char *path,
    size_t path_len
) __attribute__((__warn_unused_result__));

#endif