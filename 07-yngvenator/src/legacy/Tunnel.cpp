#include "glBase.h"
#include "Tunnel.h"


Tunnel::Tunnel(const char* texture_name,int _length_seg,int _radius_seg,float _time_scale)
{
	material.flags=0;
	material.ambient=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);
	material.diffuse=RGBACOLOR(0.8f,0.8f,0.8f,1.0f);
	material.specular=RGBACOLOR(0.2f,0.2f,0.2f,1.0f);
	material.emission=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);
	material.shininess=10.0f;
	material.mapping=0;
	material.texture_rgb=0;
	material.texture_luminance=0;	

	glTextureListGet(texture_name,&material.texture_rgb,GL_RGB);
	glTextureListGet(texture_name,&material.texture_luminance,GL_LUMINANCE);

	
	length_seg=_length_seg;
	radius_seg=_radius_seg;

	length_seg_inc=1;
	radius_seg_inc=1;

	time_scale=_time_scale;

	xbend_inc=(2*PI/_time_scale);
	zbend_inc=(2*PI/_time_scale);
	map_inc=2.0f/_time_scale;

	xbend_strength=0.0f;
	zbend_strength=0.0f;
	map_offset=0.0f;
}

void Tunnel::update(float time)
{
	xbend_strength=time*xbend_inc;
	zbend_strength=time*zbend_inc;
	map_offset=time*map_inc;
}



void Tunnel::render(int pass,int renderflags)
{
	int i,j;
	VECTOR v[4];
	TEXCOORD t[2];
	
	glMaterial(&material);
	glLoadMatrixf((float*)&glCam->wmatrix);
	
	
	for(i=0;i<length_seg;i+=length_seg_inc)
		for(j=0;j<radius_seg;j+=radius_seg_inc)
		{
			v[0].x=cosf((2.0f*RAD_CIRCLE/float(radius_seg))*j);
			v[0].y=sinf((2.0f*RAD_CIRCLE/float(radius_seg))*j);
			v[0].z=i;

			v[1].x=cosf((2.0f*RAD_CIRCLE/float(radius_seg))*(j+1));
			v[1].y=sinf((2.0f*RAD_CIRCLE/float(radius_seg))*(j+1));
			v[1].z=i;

			v[2].x=v[0].x;
			v[2].y=v[0].y;
			v[2].z=i+1;

			v[3].x=v[1].x;
			v[3].y=v[1].y;
			v[3].z=v[2].z;


			v[0].x+=sinf(zbend_strength)*sinf(float(i)/10.0f);
			v[0].y+=cosf(xbend_strength)*sinf(float(i)/10.0f);
			
			v[1].x+=sinf(zbend_strength)*sinf(float(i)/10.0f);
			v[1].y+=cosf(xbend_strength)*sinf(float(i)/10.0f);

			v[2].x+=sinf(zbend_strength)*sinf(float(i+1)/10.0f);
			v[2].y+=cosf(xbend_strength)*sinf(float(i+1)/10.0f);

			v[3].x+=sinf(zbend_strength)*sinf(float(i+1)/10.0f);
			v[3].y+=cosf(xbend_strength)*sinf(float(i+1)/10.0f);
			

			t[0].s=(2.0f*float(j)/radius_seg);
			t[0].t=(float(i)/length_seg)+map_offset;

			t[1].s=(2.0f*float(j+1)/radius_seg);
			t[1].t=(float(i+1)/length_seg)+map_offset;

			glBegin(GL_TRIANGLE_STRIP);
			
			glTexCoord2f(t[0].s,t[0].t);
			glVertex3f(v[3].x,v[3].y,v[3].z);	
			
			glTexCoord2f(t[1].s,t[0].t);
			glVertex3f(v[2].x,v[2].y,v[2].z);
			
			glTexCoord2f(t[0].s,t[1].t);
			glVertex3f(v[1].x,v[1].y,v[1].z);

			glTexCoord2f(t[1].s,t[1].t);
			glVertex3f(v[0].x,v[0].y,v[0].z);
			
			glEnd();

		}
}