#include "particle.h"


/* PGROUP !!!!!!!!!!! */

PGroup::~PGroup() {
	delete [] list;
}

bool PGroup::Add(	VERTEX *_pos,			// positio
					VERTEX *_posB,			// position B
					RGBACOLOR *_color,		// color
					RGBACOLOR *_colorB,		// color B
					VECTOR *_size,			// size
					VECTOR *_velocity		// velocity
					) {
	if(pCount>=pMax) {
		// 2 much partiklar
		return false;
	} else {
		list[pCount].pos = *_pos;
		list[pCount].posB = *_posB;
		list[pCount].color = *_color;
		list[pCount].colorB = *_colorB;
		list[pCount].size = *_size;
		if(*_velocity == 0) {
			list[pCount].velocity = _ranVel();
		} else {
			list[pCount].velocity = *_velocity;
		}
		list[pCount].velocityB = _ranVel();
		list[pCount].dV = 0;
		list[pCount].dLife = _ranLife();
		list[pCount].dRandVel = _randVel(6.28319f);
		pCount++;
		return true;
	}
}


// add with group vars + random velocity
bool PGroup::Add() {
	if(pCount>=pMax) {
		// 2 much partiklar
		return false;
	} else {
		list[pCount].pos = pos;
		list[pCount].posB = posB;
		list[pCount].color = color;
		list[pCount].colorB = colorB;
		list[pCount].size = size;
		list[pCount].velocity = _ranVel();
		list[pCount].velocityB = _ranVel();
		list[pCount].dV = 0;
		list[pCount].dLife = _ranLife();
		list[pCount].dRandVel = _randVel(6.28319f);
		pCount++;
		return true;
	}

}




void PGroup::Init(int _pMax) {
	pMax = _pMax;
	pos = 0;
	posB = 0;
	color = 0;
	colorB = 0;
	gravity = 0;
	n = 0;
	size = 1;
	//fixa minne f�r pMax partiklar
	list = new Particle [pMax];
}

void PGroup::Init(VERTEX *_pos,			// positio
		 VERTEX *_posB,			// position B
		 RGBACOLOR *_color,		// color
		 RGBACOLOR *_colorB,	// color B
		 VECTOR *_gravity,		// gravity
		 VECTOR *_n,			// normal
		 VECTOR *_size,			// storleksfanskap
		 int _pMax				// max parts
		 ) {
	pos = *_pos;
	posB = *_posB;
	color = *_color;
	colorB = *_colorB;
	gravity = *_gravity;
	n = *_n;
	size = *_size;
	pMax = _pMax;
	//fixa minne f�r pMax partiklar
	list = new Particle [pMax];


	mode = PG_STARFIELD;
	pCount = 0;
}

void PGroup::Render() {
	VECTOR _pos;
	bool const_size;
	if(pCount<1)
		return;
	if(size==0) 
		const_size = true;
	pPreRend(texture);
	for(int i=0; i<pCount; i++) {
		if(list[i].color.a>0) {		// grym optimering
			_pos = (list[i].pos) * glCam->wmatrix;
			glColor(list[i].color);
			glBegin(GL_TRIANGLE_STRIP);
				// rottweiler
				glTexCoord2f(1,1); glVertex3f(_pos.x + size.x, _pos.y + size.y, _pos.z); // Top Right
				glTexCoord2f(0,1); glVertex3f(_pos.x - size.x, _pos.y + size.y, _pos.z); // Top Left
				glTexCoord2f(1,0); glVertex3f(_pos.x + size.x, _pos.y - size.y, _pos.z); // Bottom Right
				glTexCoord2f(0,0); glVertex3f(_pos.x - size.x, _pos.y - size.y, _pos.z); // Bottom Left
			glEnd();
		}
	}
	pPostRend();
}

bool PGroup::Remove(int _n) {
	if(pCount - _n < 0) {
		_n = pCount;
	}
	pCount-=_n;
	return true;
}

void PGroup::Live(int _m) {
	static float a=0;
	a+=0.1f; // hur snabbt partiklarna �ker i sina randomvektorer
	for(int i=0;i<pCount;i++) {
		VECTOR newpos= list[i].pos;
		
		// velocity
		newpos += list[i].velocity * 0.5f * float(sinf(a + list[i].dRandVel));

		//list[i].pos += gravity;

		// vitality

		list[i].dV = (list[i].pos - g) / (g - list[i].pos).length()/gMod;


		list[i].pos = newpos;

		//list[i].color.a-=list[i].dLife;
	}
};