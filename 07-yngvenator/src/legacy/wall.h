#ifndef __Wall__
#define __Wall__

#include "glBase.h"
#include "math3d.h"

/*
	balls.h
	---
	h�ller koll p� v�ggar/o�ndliga plan.
*/


class Wall {
	public:
		
		void Init(VECTOR *_origin, VECTOR *_p1, VECTOR *_p2, float _height, float _lenght);
		void Update();
		void Draw();
		
		PLANE plane;
		float height;
		float lenght;
		
		VECTOR pP;
	private:
		VECTOR n;
		VECTOR p;
		
		
		VECTOR pN;
		VECTOR pO;
		VECTOR p1;
		VECTOR p2;

		VECTOR a1;
		VECTOR a2;
		VECTOR a3;
		
		
		
};

#endif
