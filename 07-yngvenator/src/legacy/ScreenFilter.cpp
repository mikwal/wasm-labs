#include "glBase.h"
#include "ScreenFilter.h"





void ScreenFilter::init(int type,int format,int res)
{
	color=RGBACOLOR(1.0f,1.0f,1.0f,1.0f);
	scale=1.0f;
	speed=1.0f;
	min=0.0f;
	max=1.0f;

	
	switch(_type=type)
	{
	default:
		return;

	case SF_NOISE:		
		switch(_format=format)
		{
		default:
			return;

		case GL_LUMINANCE:
		case GL_ALPHA:
			_w=res;
			_h=res;
			_buf=new unsigned char[res*res];
			memset(_buf,0,res*res);

			_counter=0;
			break;		
		}
		break;
		

	case SF_VLINES:
		_w=res;
		_h=1;
		goto SF_LINES;

	case SF_HLINES:
		_w=1;
		_h=res;
		
	SF_LINES:
		switch(_format=format)
		{
		default:
			return;

		case GL_LUMINANCE:
		case GL_ALPHA:
			_buf=new unsigned char[res*4];
			for(int i=0;i<res;i++)
			{
				_buf[res*1+i]=rand()%0xFF;
				_buf[res*2+i]=rand()%0x0F;
				_buf[res*3+i]=rand()%2;
			}
			break;		
		}
		break;
	}
	
	glCreateTexture(&_texture,_w,_h,_format);
}


void ScreenFilter::free() 
{
	_type=0;
	glFreeTexture(&_texture);
	delete [] _buf;
	_buf=NULL;
}


void ScreenFilter::update()
{
	unsigned char *tmp;
	int tmpsize;
	int	b,s;


	tmp=_buf;
	tmpsize=_w*_h;

	b=min*255.0f;
	s=max*255.0f;
								
	s=LIMIT(s,1,255);
	b=LIMIT(b,1,255);
	b=MIN(b,s);
	s+=1-b;

	switch(_type)
	{
	case SF_NOISE:
		if(_counter<=0.0f)
		{
			while(tmpsize--)
				*(tmp++)=b+rand()%s;

			if(speed>0)
				_counter=1.0f/speed;
			else
				_counter=1.0f;
		}
		else
			_counter-=1.0f;
		break;

	case SF_VLINES:
	case SF_HLINES:
		int res=tmpsize;
		while(tmpsize--)
		{
			unsigned char *tmp2;
			int c,v,d;
			
			tmp2=tmp;
			c=*(tmp2+=res);
			v=*(tmp2+=res);
			d=*(tmp2+=res);

			tmp2=tmp;
			
			if(d)
			{
				c+=v;

				if(c>0xFF)
				{
					d=0;
					c=0xFF;

					*(tmp2+=res)=c;
					tmp2+=res;
					*(tmp2+=res)=d;
				}
				else
					*(tmp2+=res)=c;
			}
			else
			{
				c-=v;

				if(c<0x01)
				{
					c=0x01;
					v=float(int(rand()%0x0F)*speed)+1.0f;
					v=LIMIT(v,1.0f,16.0f);
					d=1;

					*(tmp2+=res)=c;
					*(tmp2+=res)=v;
					*(tmp2+=res)=d;
				}
				else
					*(tmp2+=res)=c;
			}

			*(tmp++)=b-1+float((s*c)/255.0f);
		}
		break;
	}
}


void ScreenFilter::render()
{


	float tx,ty;
	float x,y;
	
	if(_buf)
	{
		glBindTexture(&_texture);
		glBltDataToTexture(0,0,_buf,_format,_w,_h);
		glColor(color);
		
		if(glView->x||glView->y||glView->w||glView->h)
		{
			x=glView->w;
			y=glView->h;
		}
		else
		{
			x=glScreenWidth;
			y=glScreenHeight;
		}

		if(scale==0.0f)
			tx=ty=1.0f;
		else
		{
			tx=(x/float(_w))/scale;
			ty=(y/float(_h))/scale;
		}

		glOrthoViewOn(x,y,1.0f);

		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(tx,ty);	
		glVertex2f(x,y);
		glTexCoord2f(0.0f,ty);
		glVertex2f(0.0f,y);
		glTexCoord2f(tx,0.0f);
		glVertex2f(x,0.0f);
		glTexCoord2f(0.0f,0.0f);
		glVertex2f(0.0f,0.0f);	
		glEnd();	

		glOrthoViewOff();
	}
}