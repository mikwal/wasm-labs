#ifndef __particle_ye__
#define __particle_ye__

#include "particle.h"

#define PS_PLANE 0
#define PS_CIRCLE 1
#define PS_SQUARE 2
#define PS_HELIX 3

#define NUM_MAPS 2

class PS_YE {
public:
	PS_YE();
	~PS_YE();
	void Init(TEXTURE *_texture);
	void Live();
	void Render();

	inline void SetPos(VECTOR *_p) {pos = *_p;}
	inline void SetNormal(VECTOR *_v) {n = *_v;}
	inline void SetSize(VECTOR *_size) {size = *_size;}
	inline void SetResolution(int w,int h) {cMap->w = w; cMap->h = h;}
	inline void SetNewMap(int i) {nMap = &map[i];}
	void SetMoveType(int _type);
	PGroup pg;
private:
	TEXTURE *texture;
	//FX
	bool _fade();	//fadar mellan 2 maps
	int move_type;

	//Boring
	bool _loadMaps();
	long pCount;

	GLuint plist;

	VECTOR n;
	VECTOR size;
	VECTOR pos;
	struct PARTICLEMAP {
		int w,h,bpp;
		VECTOR *pPosPlane, *pPosCircle, *pPosSquare, *pPosHelix;
		RGBACOLOR *pColor;
	} map[NUM_MAPS];
	PARTICLEMAP *cMap, *nMap;
};

#endif