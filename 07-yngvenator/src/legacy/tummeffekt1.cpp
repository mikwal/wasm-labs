#include "tummeffekt1.h"

void TEffect::Init(glSceneObject * _obj)
{
	obj = _obj;
	splinetime = 0.0f; 
	splinetimedir = SECVAL(PI/2);
}

void TEffect::Update()
{

if (active)
	{
	splinetime+=splinetimedir;
	obj->update((50+sin(splinetime - (PI/2))*50.0f));
	//obj->update(sinf(splinetime)*100.0f);
	}

}

void TEffect::SetTranslate(VECTOR _pos)
{
	MATRIX m=obj->matrix;
	obj->matrix=m*Mtranslate(_pos.x, _pos.y, _pos.z);
	
}


void TEffect::Draw()
{
	MATRIX m=obj->matrix;

	
	
	glApplyAllLights();
	
	glZBufOff();
	glBlendOn();
	
		obj->render(1);
	//glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		obj->matrix=m*Mrotate(PI/2, 1.0f ,0.0f ,0.0f);
		obj->render(1);
	//glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
		obj->matrix=m*Mrotate(PI, 1.0f, 0.0f, 0.0f);
		obj->render(1);
	//glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
		obj->matrix=m*Mrotate(3*PI/2, 1.0f, 0.0f, 0.0f);
		obj->render(1);
	//obj->matrix=m;	
	
	// Andra h�llet
	
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	obj->matrix=m*Mrotate(PI, 0.0f, 1.0f, 0.0f);
	obj->render(1);
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	obj->matrix=m*Mrotate(PI, 0.0f, 1.0f, 0.0f)*Mrotate(PI/2, 1.0f, 0.0f, 0.0f);
		obj->render(1);
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	obj->matrix=m*Mrotate(PI, 0.0f, 1.0f, 0.0f)*Mrotate(PI, 1.0f, 0.0f, 0.0f);
		obj->render(1);
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	obj->matrix=m*Mrotate(PI, 0.0f, 1.0f, 0.0f) *Mrotate(3*PI/2, 1.0f, 0.0f, 0.0f);
		obj->render(1);
	
	// Sista h�llet
	
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	obj->matrix=m*Mrotate(PI, 0.0f, 1.0f, 0.0f) *Mrotate(PI/2, 0.0f, 0.0f, 1.0f);
	obj->render(1);
	//glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	obj->matrix=m*Mrotate(PI/2, 0.0f, 0.0f, 1.0f);
	obj->render(1);
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);
	obj->matrix=m*Mrotate(PI, 0.0f, 1.0f, 0.0f) *Mrotate(PI/2, 0.0f, 0.0f, -1.0f);
	obj->render(1);
	//glRotatef(90.0f, 0.0f, 0.0f, -1.0f);
	obj->matrix=m*Mrotate(PI/2, 0.0f, 0.0f, -1.0f);
		obj->render(1);
	obj->matrix = m;

}