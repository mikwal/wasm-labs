#ifndef GLTEXTURE_H
#define GLTEXTURE_H


int LoadTGA(IOStream *s,unsigned char **data,int *w,int *h,int *bpp);
int LoadBMP(IOStream *s,unsigned char **data,int *w,int *h,int *bpp);



typedef unsigned int TEXTURE;




extern int glLoadTextureFilter;


int glLoadTexture(const char *filename,TEXTURE *textures,int *formats,int ncopy);
inline int glLoadTexture(const char *filename,TEXTURE *texture,int format=GL_RGB) {return glLoadTexture(filename,texture,&format,1);}

int glCreateTexture(TEXTURE *texture,int w,int h,int type);
int glCreateTexture(TEXTURE *texture,int w,int h,int type,const void *data);
int glCreateMipmaps(TEXTURE *texture,int w,int h,int type,const void *data);

inline void glBindTexture(TEXTURE *texture) {glBindTexture(GL_TEXTURE_2D,*texture);}
inline void glFreeTexture(TEXTURE *texture) {glDeleteTextures(1,texture);*texture=0;}

void glSetTextureFilter(int filter);
void glSetTextureWrap(int wrap);

inline void glBltBufferToTexture(int tx,int ty,int bx,int by,int bw,int bh){glCopyTexSubImage2D(GL_TEXTURE_2D,0,tx,ty,bx,by,bw,bh);}
inline void glBltDataToTexture(int tx,int ty,const void *data,int data_type,int data_w,int data_h) {glTexSubImage2D(GL_TEXTURE_2D,0,tx,ty,data_w,data_h,data_type,GL_UNSIGNED_BYTE,data);}

inline void glBltTexture(float src_x,float src_y,float src_w,float src_h,float dest_x,float dest_y,float dest_w,float dest_h) {glBegin(GL_TRIANGLE_STRIP);	glTexCoord2f(src_x+src_w,src_y+src_h);	glVertex2i(dest_x+dest_w,dest_y+dest_h); glTexCoord2f(src_x,src_y+src_h); glVertex2i(dest_x,dest_y+dest_h); glTexCoord2f(src_x+src_w,src_y); glVertex2i(dest_x+dest_w,dest_y); glTexCoord2f(src_x,src_y); glVertex2i(dest_x,dest_y); glEnd();}

int glTextureListLoad(const char *dirname,int *formats,int ncopy);
inline int glTextureListLoad(const char *dirname,int format) {return glTextureListLoad(dirname,&format,1);}
inline int glTextureListLoad(const char *dirname) {int formats[2]={GL_RGB,GL_LUMINANCE}; return glTextureListLoad(dirname,formats,2);}
void glTextureListFreeAll();
int glTextureListGet(const char *name,TEXTURE *texture,int format=GL_RGB);
inline void glBindTexture(const char *name,int format=GL_RGB) {TEXTURE t=0;glTextureListGet(name,&t,format);glBindTexture(&t);}




#endif

