#include <stdlib.h>
#include <string.h>
#include <mlib.h>


struct _memfile
{
	unsigned char *data;
	int length;
	int pos;
};


unsigned int _memfile_open(const char *name)
{
	IOStream *s;

	if(s=fopen(name,F_READ))
	{
		_memfile *handle=NULL;
		long size;

		if(size=s->size())
		{
			handle=new _memfile;
			handle->data=new unsigned char[size];
			handle->length=size;
			handle->pos=0;

			s->read(handle->data,size);
		}

		delete s;
		
		return (unsigned int)handle;
	}
	else
		return 0;
}


void _memfile_close(unsigned int handle)
{
	if(handle)
	{
		delete [] ((_memfile*)handle)->data;
		delete ((_memfile*)handle);
	}
}


int _memfile_read(void *buf,int size,unsigned int handle)
{
	if(((_memfile*)handle)->pos+size>=((_memfile*)handle)->length)
		size=((_memfile*)handle)->length-((_memfile*)handle)->pos;

	memcpy(buf,((_memfile*)handle)->data+((_memfile*)handle)->pos, size);
	
	((_memfile*)handle)->pos+= size;
	
	return size;
}


int _memfile_seek(unsigned int handle,int pos,signed char mode)
{
	switch(mode)
	{
	case SEEK_SET:
		((_memfile*)handle)->pos=pos;
		break;

	case SEEK_CUR:
		((_memfile*)handle)->pos+=pos;
		break;

	case SEEK_END:
		((_memfile*)handle)->pos=((_memfile*)handle)->length+pos;
		break;
	}

	if(((_memfile*)handle)->pos>((_memfile*)handle)->length)
		((_memfile*)handle)->pos=((_memfile*)handle)->length;
	else
		if(((_memfile*)handle)->pos<0)
			((_memfile*)handle)->pos=0;

	return ((_memfile*)handle)->pos;
}


int _memfile_tell(unsigned int handle)
{
	return ((_memfile*)handle)->pos;
}









unsigned int _stream_open(const char *name)
{
	return (unsigned int)fopen(name,F_READ);
}


void _stream_close(unsigned int handle)
{
	delete (IOStream*)handle;
}


int _stream_read(void *buf,int size,unsigned int handle)
{
	return ((IOStream*)handle)->read(buf,size);
}


int _stream_seek(unsigned int handle,int pos,signed char mode)
{
	return ((IOStream*)handle)->seek(pos,mode);
}


int _stream_tell(unsigned int handle)
{
	return ((IOStream*)handle)->tell();
}
