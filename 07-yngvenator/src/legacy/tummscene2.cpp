#include "glBase.h"
#include "liquidsurface.h"
#include "psystems.h"
#include "fmodFileCallbacks.h"
#include "fmodSpectrum.h"


#define RSPEED 0.005f
#define MSPEED 0.005f

////////////////////////////////

GL_NEW_SCENE(tumm02_Scene);




////////////////////////////////


static glCamera cam[2];
static int cam_i=0;
static float time = 0.0f, timedir = SECVAL(2.0f);

static glFont font1;

//glRootObject * buh; 
static glSceneObject * c, * p1, * f1, * f2, * buh;

//static TEXTURE fsys, texApor, test2;
static TEXTURE fsys, texPart;

//LSurface lsurface("blomma.bmp", 20, 20, 1, 0.75f);
LSurface * lsurface;
FireFlies * flies;

extern FSOUND_STREAM *bgmusic;
extern glScene tumm01_Scene;

void tumm02_Scene_load()
{
	glTextureListGet("particle03.tga", &texPart);
	buh = glLoadScene("sce.dat|Blommor.sce");
	
	c = buh->getobject("camera01");
	f1 = buh->getobject("fluga01");
	f2 = buh->getobject("fluga02");
	p1 = buh->getobject("planpunkt");
	lsurface = new LSurface("water.bmp", 40, 40, 1, 0.95f, 0);
	flies = new FireFlies;
	//LSurface lsurface("blomma.bmp", 20, 20, 1, 0.75f);
	
}


void tumm02_Scene_init()
{
	
	glResetEnviroment();
	glAddAllLights(buh);
	
	glLightOn();
	cam[0].zfar=1000.0f;
	cam[1].zfar=1000.0f;

	font1.width=3.0f;
	font1.height=4.5f;
	font1.texture=fsys;
	font1.color = (0.0f, 0.0f, 0.0f, 1.0f);
	//glView->light = RGBACOLOR(0.02, 0.02, 0.02, 1);
	//flies->list[1].size = VECTOR(250.0f, 250.0f, 0.0f);
	//flies->list[2].size = VECTOR(250.0f, 250.0f, 0.0f);
	flies->texture = texPart;

	glView->fog_mode=GL_EXP2;
	glView->fog_dens=0.00020;
	
	lsurface->matrix = p1->matrix;
	glBlendOff();
	lsurface->material.emission=RGBACOLOR(0.2f, 0.2f, 0.2f, 1.0f);
	flies->Init(&VERTEX(0.0f, 0.0f, 0.0f), 0.0f, &VECTOR(40.0f, 40.0f, 0.0f),&RGBACOLOR(1.0f, 0.0f, 0.0f, 1.0f), 3);

		
}

void tumm02_Scene_free()
{
	
	glFreeTexture(&fsys);
}




int tumm02_Scene_update()
{

	float a=fmodUpdatePeakChecker();
		
	lsurface->map[30 * 40 + 10]=-a*18.0;
	lsurface->map[20 * 40 + 20]=-a*12.0;
	lsurface->map[10 * 40 + 10]=-a*18.0;


	
	time+=timedir;
	buh->update(time*7);

	//f1->update(time*20);
	
	
	c->update(time*7);

	
	flies->list[0].pos = f2->getmatrix().p;
	//flies->list[0].color = ((glLight*)f2)->diffuse;
	flies->list[0].color = RGBACOLOR(0.6f, 0.4f, 0.9f, 1.0f);

	flies->list[1].pos = f1->getmatrix().p;
	flies->list[1].color = RGBACOLOR(1.0f, 0.4f, 0.4f, 1.0f);
	//flies->list[1].color = ((glLight*)f1)->diffuse;
	
	
	lsurface->update(0);

	if(FSOUND_Stream_GetTime(bgmusic)>1000*249)
	{
		glScene::current=&tumm01_Scene;
		glScene::current->init();
	}
	
	return 0;
}


#define PLANE(p1,p2,p3,p4) glBegin(GL_TRIANGLE_STRIP),glVertex3fv((float*)&p1),glVertex3fv((float*)&p2),glVertex3fv((float*)&p3),glVertex3fv((float*)&p4),glEnd();


void tumm02_Scene_render()
{

	glBlendOn();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glLightOff();
	
	
	glView->color.a = 0.2f;
	glView->color.r = 0.4f;
	glView->color.g = 0.4f;
	glView->color.b = 0.6f;
	glView->clear();
	//glSetCamera(c);
	
	
	glZBufOn();
	glBlendOn();
	
	glSetCamera(c);
	glApplyAllLights();
	glLightOn();
	glBlendOff();
	lsurface->render(0);
	//c->update(time*2);
	//glSetCamera(c);
	buh->render(0);
    
	glLightOff();
	glFogOff();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glBlendOn();
	glZBufMask(0);
	flies->Render();
	glZBufMask(1);
	glFogOn();
	glLightOn();
	glZBufOn();
	glSwapBuffer();
}

