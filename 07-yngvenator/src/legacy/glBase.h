#ifndef GLBASE_H
#define GLBASE_H

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

extern float FPS;
extern int OPF;
extern int PPF;

#include <mlib.h>
#include <fmod.h>


#include "math3d.h"

#include "glWindow.h"
#include "glTexture.h"
#include "glEnviroment.h"
#include "glFont.h"
#include "glScene.h"
#include "glSceneObject.h"





#endif