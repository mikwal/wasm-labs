#include "glBase.h"
#include <process.h>
#include "resource.h"

float FPS;

static int bpp,flags;


LRESULT CALLBACK OptionDialogProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)
{
	ULONG i;
	

	switch(msg)
	{
	// case WM_INITDIALOG:
	// 	SendDlgItemMessage(hwnd,IDC_32BPP,BM_SETCHECK,BST_CHECKED,0);
	// 	SendDlgItemMessage(hwnd,IDC_WINDOWED,BM_SETCHECK,BST_UNCHECKED,0);
	// 	break;
	// case WM_COMMAND:
	// 	if(LOWORD(wparam)==IDOK)
	// 	{
	// 		if(SendDlgItemMessage(hwnd,IDC_32BPP,BM_GETCHECK,0,0)==BST_CHECKED)
	// 			bpp=32;
	// 		else
	// 			bpp=16;

			
	// 		if(SendDlgItemMessage(hwnd,IDC_WINDOWED,BM_GETCHECK,0,0)==BST_CHECKED)
	// 			flags=0;
	// 		else
	// 			flags=GLW_FULLSCREEN;

		
	// 		EndDialog(hwnd,0);
	// 	}
	// 	break;

	// case WM_CLOSE:
	// 	EndDialog(hwnd,-1);
	// 	break;
	}
	return 0;
}


LRESULT CALLBACK WinProc(HWND hWnd,UINT Msg,WPARAM wParam,LPARAM lParam)
{
	switch(Msg)
	{
	// case WM_KEYDOWN:
	// 	if(wParam==VK_ESCAPE)
	// 		PostQuitMessage(0);
	// 	break;
	}

	return DefWindowProc(hWnd,Msg,wParam,lParam);
}



int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	MSG	msg;
	float ftime, stime;
	int fcount, i;

	// if(DialogBox(hInstance,MAKEINTRESOURCE(ID_DIALOG),0,(DLGPROC)OptionDialogProc))
	// 	return 0;


	// if(glCreateWindow("OpenGL application",800,600,bpp,flags,WinProc))
	// 	return 0;

	// ShowCursor(0);

	glInitTimer();
	glSetFPS(30.0f);

	glScene::first->load();
	glScene::first->init();

	for(i=0;i<glScene::list_count;i++)
		if(glScene::list[i]!=glScene::first)
			glScene::list[i]->load();

	fcount=0;
	ftime=stime=glTime();

	do
    {
        if(PeekMessage(&msg,NULL,0,0,PM_NOREMOVE))
        {
            if(GetMessage(&msg,NULL,0,0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
        }
        else 
		{
			if(glScene::current->update())	// TEST UPDATE SCENE
				glDestroyWindow();

			if(ftime+glFrameTime>glTime())
			{
				while(glTime()<ftime+glFrameTime);
					
				ftime=glTime();	
				
				glScene::current->render(); // TEST RENDER SCENE 
				

				fcount+=1;
			}
			else
				ftime+=glFrameTime;
			
			if(glTime()>stime+1000.0f)
			{
				FPS=float(fcount*1000.0f)/(glTime()-stime);
				fcount=0;
				stime=glTime();
			}
		}
	}
	while(msg.message!=WM_QUIT);
	
	
	for(i=0;i<glScene::list_count;i++)
		if(glScene::list[i]!=glScene::first)
			glScene::list[i]->free();

	glScene::first->free();


	glTextureListFreeAll();
	glDestroyWindow();

	
	return 0;
}