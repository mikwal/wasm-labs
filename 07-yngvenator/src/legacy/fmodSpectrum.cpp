#include <fmod.h>
#include <fmod_legacy.h>

#define freq 44100.0f

float fmodGetSpectrumAmplitude(float start_freq,float end_freq)
{
	float *spectrum=FSOUND_DSP_GetSpectrum();
	int s=(start_freq*1024.0f)/freq;
	int e=(end_freq*1024.0f)/freq+1.0f;
	
	float A;
	int i;

	if(e>512)
		e=512;
	if(s<0)
		s=0;

	for(A=0.0f,i=s;i<e;i++)
		A+=spectrum[i];

	return A*100.0f/float(e-s);
}



float fmodMaxPeak=0.0f;

static float prevA=0.0f;
static float curA=0.0f;
static float nextA=0.0f;

static float A_start_freq=0.0f;
static float A_end_freq=0.0f;



void fmodSetPeakCheckerFreq(float start_freq,float end_freq)
{
	fmodMaxPeak=0.0f;
	A_start_freq=start_freq;
	A_end_freq=end_freq;
	prevA=0.0f;
	curA=0.0f;
	nextA=0.0f;
}


float fmodUpdatePeakChecker()
{
	float a=fmodGetSpectrumAmplitude(A_start_freq,A_end_freq);

	if(a>fmodMaxPeak)
		fmodMaxPeak=a;

	prevA=curA;
	curA=nextA;
	nextA=a;

	if(curA>prevA&&curA>nextA)
		return curA;
	else
		return 0;
}

