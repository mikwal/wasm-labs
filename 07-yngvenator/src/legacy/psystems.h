#ifndef __psystems_h__
#define __psystems_h__

#include "particle.h"
class FireFlies : public PGroup {
public:
	void Init(VERTEX *_pos,
					 float _domain,		// size of area where the flies are :)
					 VECTOR *_size,
					 RGBACOLOR *_color,
					 int n_flies);
	void Update();
	float time;
	float domain;	//radie
};


class Rain : public PGroup {
public:
	void Init(VERTEX *_pos,
				VECTOR *_size,
				VECTOR *_cloud,
				RGBACOLOR *_color,
				VECTOR *_grav,
				PLANE *_ground,
				int density);
	void Update();
	~Rain();
	VECTOR *p0;
	VECTOR cloud;	// b�rjar p� pos; omr�de d�r regn kan falla
	PLANE ground;
};

#endif