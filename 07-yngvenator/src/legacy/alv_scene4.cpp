#include "glBase.h"
#include "textlines.h"
#include "ps_ye.h"

#include <stdio.h>
// credz =P


//GL_NEW_SCENE(Scene04);




signed char s4_sync(FSOUND_STREAM *stream, void *buff, int len, int param);

#ifdef DEBUG
	extern glFont fsys;
#endif
static glCamera *cam;
static glSceneObject *scene4;
extern FSOUND_STREAM *bgmusic;

static PS_YE ye;

static TEXTURE wallpaper;

#define NUM_TL 35
static textline tl[NUM_TL];

// Load Textures, etc
void Scene04_load()
{
	scene4 = glLoadScene("sce.dat|alv_scene4.sce");
	for(int i=0;i<NUM_TL;i++) {
		glTextureListGet("fontPres.tga", &tl[i].texture0);
	}
	glTextureListGet("wallpaper.bmp", &wallpaper);
}


// Initialize objects and environment
void Scene04_init()
{
int q =0;
	glResetEnviroment();
	FSOUND_Stream_SetSynchCallback(bgmusic, s4_sync, 0);
	cam = (glCamera*)scene4->getobject("camera01");
	tl[0].Init("our c++ teacher YNGVE");
	tl[1].Init("www.planetzeus.net");
	tl[2].Init("#bongo");
	tl[3].Init("ola & potatisarna");
	tl[4].Init("RAZOR 1911");
	tl[5].Init("GLASS");
	tl[6].Init("Ulf Johnsen");
	tl[7].Init("de som losar I CS");
	tl[8].Init("SolidChaos");
	tl[9].Init("{cD}");
	tl[10].Init("#djuren.org");
	tl[11].Init("xem & gei-pozer");
	tl[12].Init("KAKIS-chan");
	tl[13].Init("lampan@paradise.net");
	tl[14].Init("porr-budd");
	tl[15].Init("Rocco Siffredi");
	
	tl[16].Init("all j00 Gamers Out There!");
	tl[17].Init("j00 ruul we love jo *kiss*");
	tl[18].Init("VOTE FOR US!");

	tl[19].Init("[GAJ]-klanen");
	tl[20].Init("noice");
	tl[21].Init("Security-Andreas");
	tl[20].Init("Joppe");
	tl[21].Init("Kyngen");
	tl[22].Init("& silvia");
	tl[23].Init("Varan-TEATERN");
	tl[24].Init("greeting #21");
	tl[25].Init("Milsbo Sysamfund");
	tl[26].Init("Modern");
	tl[27].Init("faDREN");
	tl[28].Init("Outbush Gangstahz");
	tl[29].Init("Jan Banan");
	tl[30].Init("Outbreak");
	tl[31].Init("NINTENDO");
	tl[32].Init("spuun :)");
	tl[33].Init("xtreamist");

	tl[34].Init("WE'LL BE BACK... ;)");
	tl[34].font.setsize(10,10);
	tl[34].pos.x = 60;
	tl[34].color0 = 1;
	for(q = 0;q<tl[34].len;q++) {
		tl[34].fade[0] = 0;
		tl[34].vel[q]=0;
	}

	for(int hora=0;hora<NUM_TL;hora++) {
		if(hora%2 == 0)
			tl[hora].pos.y = 80;
		else
			tl[hora].pos.y = 20;
	}

	TEXTURE tmp;
	glTextureListGet("particle01.bmp", &tmp);
	ye.Init(&tmp);
	ye.SetMoveType(PS_CIRCLE);
}

// Free textures, etc
void Scene04_free()
{

	FSOUND_Stream_Close(bgmusic);
	FSOUND_Close();
}


static int majk=0;

// Move cameras and effects!
int Scene04_update()
{
	float test;
	int cp;
	test = FSOUND_Stream_GetTime(bgmusic);
	if(test>1000*272) 		tl[0].active = 1;
	if(test>1000*272.9f) 		tl[1].active = 1;
	if(test>1000*273.6f) 		tl[2].active = 1;
	if(test>1000*274.3f) 		tl[3].active = 1;
	if(test>1000*275) 		tl[4].active = 1;
	if(test>1000*275.7f) 		tl[5].active = 1;
	if(test>1000*276.4f) 		tl[6].active = 1;

	if(test>1000*277) 		tl[7].active = 1;
	if(test>1000*277.4f) 		tl[8].active = 1;
	if(test>1000*277.8f) 		tl[9].active = 1;

	if(test>1000*278.5f) 		tl[10].active = 1;
	if(test>1000*273.2f) 		tl[11].active = 1;
	if(test>1000*279.9f) 		tl[12].active = 1;
	if(test>1000*280.5f) 		tl[13].active = 1;
	if(test>1000*281.3f) 		tl[14].active = 1;
	if(test>1000*282.0f) 		tl[15].active = 1;

	if(test>1000*282.6f) 		tl[16].active = 1;
	if(test>1000*283.0f) 		tl[17].active = 1;
	if(test>1000*283.36f) 	tl[18].active = 1;

	if(test>1000*284.4f) 		tl[19].active = 1;
	if(test>1000*284.8f) 		tl[20].active = 1;
	if(test>1000*285.5f) 		tl[21].active = 1;
	if(test>1000*286.15f) 	tl[22].active = 1;
	if(test>1000*286.85f) 	tl[23].active = 1;
	if(test>1000*287.5f) 		tl[24].active = 1;

	if(test>1000*288.45f) 	tl[25].active = 1;
	if(test>1000*289.0f) 		tl[26].active = 1;
	if(test>1000*289.65f) 	tl[27].active = 1;

	if(test>1000*290.35f) 	tl[28].active = 1;
	if(test>1000*291.0f) 		tl[29].active = 1;
	if(test>1000*291.7f) 		tl[30].active = 1;
	if(test>1000*292.4f) 		tl[31].active = 1;
	if(test>1000*293.1f) 		tl[32].active = 1;
	if(test>1000*293.84f) 	tl[33].active = 1;
	if(test>1000*294.15f)		tl[34].active = 1;

	for(majk=0;majk<35;majk++)
		if(!tl[majk].active)
			break;
	majk--;

	if(majk<0)
		majk=0;


//	for(int kuk=0;kuk<34;kuk++) {
//		tl[kuk].active = 1;
//	}

	for(int i=0;i<NUM_TL;i++) {
		if(tl[i].active) {
			tl[i].Update();
		}
	}

	ye.Live();
	return 0;
}


// Render Scene:: All rendering function calls goes here!
void Scene04_render()
{
	//glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE);
	glView->color.a = 0.4f;
	glView->clear();
	glView->light = 1.0f;
	
	glSetCamera(cam);
	glOrthoViewOn();
	glLightOff();
	glBlendOff();
	glAlphaOff();

	char tmp[60];

	if(majk>30)
		majk-=15;
	sprintf(tmp,"d%d.bmp",majk/2+1);

	glColor(1,1,1,1);
	glBindTexture(tmp);
	glBltTexture(0,0,1,1,0,0,100,100);

	glBlendOn();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	for(int i=0;i<NUM_TL;i++) {
		if(tl[i].active) {
			tl[i].Render();
		}
	}	
	glOrthoViewOff();

	glPushAttrib();
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		ye.Render();
	glPopAttrib();

	glSwapBuffer();
}

signed char s4_sync(FSOUND_STREAM *stream, void *buff, int len, int param)
{
	if(buff) {
		return true;
	} else {
		return true;
	}
}