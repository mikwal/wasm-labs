#ifndef GLFONT_H
#define GLFONT_H

#define GLF_RIGHT	0x00
#define GLF_CENTER	0x01
#define GLF_LEFT	0x02
#define GLF_TOP		GLF_RIGHT
#define GLF_BOTTOM	GLF_LEFT




class glFont {
private:
	void _print(float x,float y,float z,char *text);

public:
	TEXTURE texture;
	float width,height;
	float hspace,vspace;
	int halign,valign;
	RGBACOLOR color;

	inline glFont() {texture=0;width=4.5f;height=6.0f;halign=0;valign=0;hspace=0.5f;vspace=1.0f;color=RGBACOLOR(1.0f);}
	inline glFont(TEXTURE _texture,float _width,float _height) {texture=_texture;width=_width;height=_height;halign=0;valign=0;hspace=0.5f;vspace=1.0f;color=RGBACOLOR(1.0f);}
	inline glFont(const char *_texture_name,float _width,float _height) {texture=0; glTextureListGet(_texture_name,&texture,GL_RGB); width=_width;height=_height;halign=0;valign=0;hspace=0.5f;vspace=1.0f;color=RGBACOLOR(1.0f);}
	inline ~glFont() {}

	inline void setsize(float _width,float _height) {width=_width;height=_height;}	
	inline void setalign(int _halign,int _valign) {halign=_halign&0x0F;valign=_valign&0x0F;}
	inline void setspace(float _hspace,float _vspace) {hspace=_hspace;vspace=_vspace;}
	inline void setcolor(float r,float g,float b,float a) {color=RGBACOLOR(r,g,b,a);}
	inline void setcolor(const RGBACOLOR &c) {color=c;}		
	
	inline void bind() {glBindTexture(&texture);}

	void put(float x,float y,int c);
	void put(const VECTOR &p, RGBACOLOR *col,int c);
	
	void printf(float x,float y,char *text,...);
	void printf(const VECTOR &p,char *text,...);
};



#endif