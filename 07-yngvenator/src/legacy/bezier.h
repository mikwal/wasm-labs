#ifndef __BEZIER_H__
#define __BEZIER_H__

#include "glBase.h"

#define BEZIER_STILL 0
#define BEZIER_FLAGGA 1
#define BEZIER_WATER 2

class BEZIER {
public:
	//vars
	VERTEX p[4]; // kontrolldasar 
	VERTEX q[4]; // kontrolldasar 
	VECTOR pos, size;
	TEXTURE texture;
	int w,h;	// segment
	int pw,ph;
	float tx,ty; // texturkonstantdasar (beh�ver bara r�knas ut en g�ng m.h.a. w & h
	
	//functions
	BEZIER() {
		w=8;
		h=8;
		mode = BEZIER_STILL;
	}
	~BEZIER();
	void Generate();	// Genererar nya punkter i en rak dase
	void GenPatch(VECTOR *P, int dase); // l�gger p� bezierpatch
	void Render();
	void Live();
	void Init();
	inline void SetTexture(GLuint tex) {texture = tex;}
	int mode;
private:
	VERTEX _GenPoint(float t, VECTOR *P);	// r�knar ut punkten t
	VERTEX *list;
};


#endif