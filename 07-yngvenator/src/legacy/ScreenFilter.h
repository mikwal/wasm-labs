#ifndef SCREENFILTER_H
#define SCREENFILTER_H




#define SF_NOISE	0x0001
#define SF_VLINES	0x0002
#define SF_HLINES	0x0003


class ScreenFilter {
private:
	int _type;
	int _format;
	int _w;
	int _h;
	float _s;
	float _t;
	unsigned char *_buf;
	TEXTURE	_texture;
	
	
	float _counter;

public:
	RGBACOLOR color;
	float scale;
	float speed;
	float min;
	float max;


	inline ScreenFilter() {_type=0;_buf=NULL;}
	inline ~ScreenFilter() {glFreeTexture(&_texture); delete [] _buf;}
	
	void init(int type,int format,int res);
	void free();

	void update();
	void render();
	inline void bind() {glBindTexture(&_texture);}
};








#endif