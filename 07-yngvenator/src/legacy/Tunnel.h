#ifndef TUNNEL_H
#define TUNNEL_H


class Tunnel : public glSceneObject {
public:
	MATERIAL material;
	int length_seg;
	int radius_seg;
	float time_scale;

	int length_seg_inc;
	int radius_seg_inc;

	float xbend_inc;
	float zbend_inc;
	float map_inc;

	float xbend_strength;
	float zbend_strength;
	float map_offset;

	Tunnel() {}
	Tunnel(const char* texturename,int _length_seg,int _radius_seg,float _time_scale=100.0f);

	void update(float time);
	void render(int pass,int renderflags=0);
};







#endif
