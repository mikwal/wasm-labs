#include "glBase.h"



glScene::glScene(void (*loadproc)(),void (*freeproc)(),void (*initproc)(),int (*updateproc)(),void (*renderproc)())
{
	if(list_count<=GL_MAX_SCENES)
	{
		list[list_count++]=this;
		load=loadproc;
		free=freeproc;
		init=initproc;
		update=updateproc;
		render=renderproc;
	}
}

int glScene::list_count=0;
glScene *glScene::list[GL_MAX_SCENES]={NULL};
