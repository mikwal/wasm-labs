/*
	"introscenen" 
	---
	h�r rullar det f�rbi en massa massa kod nerifr�n upp
	gr�n flouscenrande text i interlacedl�ge
	h�ftiga kommentarer ska lysa upp i vitt och stanna n�nstans p� sk�rmen s� man hinner l�sa dom

  */

#include "glBase.h"
//#include "bezier.h"
#include "ScreenFilter.h"
#include <fmod.h>
#include "fmodFileCallbacks.h"
#include "psystems.h"

////////////////////////////////

GL_NEW_SCENE(Scene02);
GL_NEW_SCENE(data_scenen);
GL_NEW_SCENE(tunnel_scene);
GL_NEW_SCENE(test03);
GL_NEW_SCENE(Scene04);

GL_FIRST_SCENE(Scene02);

////////////////////////////////

signed char s2_sync(FSOUND_STREAM *stream, void *buff, int len, int param);


glFont fsys;


static glFont c64;
static glCamera *cam;
static glSceneObject *scene03, *scene03_2;
static float globalTime;

static ScreenFilter Filter1;

static char **rad;
static int rader = 0;
static int rows_on_screen = 150;
static float kod_y = 0;
static char *kodklumpen;
static int klump_len;
//static char buffer[512];
static IOStream *fp;

static bool ytrig;
static TEXTURE yngve_arm;

FSOUND_STREAM *bgmusic;




static struct Ptext {
	glFont pres;
	bool trig;
	float pres_timer;	// for pulse
	char *text;
	int x,y;
	float dx,dy;	// hur mycket vspace �ndras
	void reset(int i);
} pt[10];

inline void Ptext::reset(int i) {
	pt[i].pres.setsize(6,8);
	pt[i].pres.setspace(0.2f,1);
	pt[i].pres.color = RGBACOLOR(1.0f,1.0f,1.0f, 1.0f);
	pt[i].trig=0;
	pt[i].pres_timer=0;
	pt[i].x = 50;
	pt[i].y = 80;
	pt[i].dx = 0;
	pt[i].dy = 0;
	pt[i].pres.halign = GLF_CENTER;
}

static bool fadein[4];



// Load Textures, etc
void Scene02_load()
{
	TEXTURE splash;

	glColor3f(1,1,1);
	glTextureOn();
	glLoadTexture("gfx.dat|splash.bmp",&splash);
	glOrthoViewOn();
	glBltTexture(0,0,1,1,0,0,100,100);
	glFreeTexture(&splash);
	glOrthoViewOff();
	glSwapBuffer();

	// Ljuddoningar
	FSOUND_SetOutput(FSOUND_OUTPUT_WINMM);
	FSOUND_SetDriver(0);
	FSOUND_Init(44100,32,0);
	FSOUND_File_SetCallbacks(FSOUND_FILE_CALLBACKS_STREAM_FROM_FILE);
	bgmusic=FSOUND_Stream_OpenFile("music.wav",FSOUND_STEREO|FSOUND_LOOP_NORMAL,0);

	

	glTextureListLoad("gfx.dat|",GL_RGB);
	//glTextureListGet("paricoe01.bmp",&t);
	glTextureListGet("fixedsys.tga",&fsys.texture);
	glTextureListGet("c64.tga",&c64.texture);
//	glTextureListGet("particle1.bmp", &pg.texture);
	glTextureListGet("yngve-arm.tga", &yngve_arm);

	for(int i2=0;i2<10;i2++) {
		glTextureListGet("fontPres.tga", &pt[i2].pres.texture);
	}

	scene03 = glLoadScene("sce.dat|alv_scene03.sce");
	cam = (glCamera*)scene03->getobject("camera01");
	
	
	// L�s in shit
	fp = fopen("sce.dat|scrollkod.txt");
	klump_len = fp->size();
	kodklumpen = new char[klump_len+1];
	fp->read(kodklumpen, klump_len);
	kodklumpen[klump_len] = 0;
	fclose(fp);

	// Dela up SHIT i rader
	int i=0,j=0,k=0;
	
	// r�nka rader + allokera minne
	while(i<klump_len) {
		while(kodklumpen[i+j] != '\n') {
			j++;
		}
		i+=j+1;
		j=0;
		rader++;
	}
	rad = new char * [rader];
	i=j=k=0;

	// g� igenom skiten igen och allokera minne + kopiera str�ng
	while(i<klump_len) {
		// r�kna tecken och allokera minne
		while(kodklumpen[i+j] != '\n') {
			j++;
		}
		rad[k] = new char [j+1];
		j=0;

		// b�rja om igen och kopiera raden fr�n kodklumpen
		while(kodklumpen[i+j] != '\n') {
			rad[k][j] = kodklumpen[i+j];
			j++;
		}

		rad[k][j] = 0;
		k++;
		i+=j+1;
		j=0;
	}
}


// Initialize objects and environment
void Scene02_init()
{
	glResetEnviroment();
	glAddAllLights(scene03);
	glLightOn();
	c64.setsize(2.0f,2.0f);
	c64.setspace(1.0f,1.0f);

	int i;
	for(i=0;i<10;i++) {
		pt[i].reset(i);
	}

	// LAMAZ papaz poesi ^^
	pt[0].text = "thIs iS a FloATiNG tExt!";
	pt[1].text = "your mind is an orange";
	pt[2].text = "the universe is an ocean";
	pt[3].text = "love";
	pt[4].text = "h\na\nt\ne";
	pt[5].text = "do you know what we want?";
	pt[6].text = "!!! We Know !!!";
	pt[7].text = ".... but for now ....";
	pt[8].text = "Bongo Presents:";
	pt[9].text = "yngvenator";

	for(i=0;i<4;i++) {
		fadein[i]=0;
	}

	cam->color_bias = 0;
	cam->color_conversion = 0;
	cam->color_scale = 0;

	Filter1.init(SF_HLINES, GL_LUMINANCE, 32);
	Filter1.min = 0.0f;
	Filter1.max = 0.3f;
	Filter1.scale = 2.0f;

	//FSOUND_Stream_SetEndCallback(bgmusic, s1_sync, 0);
	FSOUND_Stream_SetSynchCallback(bgmusic, s2_sync, 0);

	FSOUND_Stream_Play(FSOUND_FREE,bgmusic);

//	pg.Init(500);
//	pg.pos = pg.posB = VECTOR(0,100,0);
}

// Free textures, etc
void Scene02_free()
{
	delete [] kodklumpen;
	for(int i=0;i<rader;i++) {
		delete [] rad[i];
	}
}


static float time = 0;

// Move cameras and effects!
int Scene02_update()
{
/*#ifdef DEBUG
	glScene::current = &data_scenen;
	glScene::current->init();
#endif*/



	time+=0.1f;
	scene03->update(time);
	if(time>100)time=0;
	Filter1.update();
	
//	ff.Update();
	
	kod_y+=0.1f;
	
	// fade in scene
	if(fadein[0]) {
		if(glCam->color_bias>0.5f)glCam->color_bias-=0.05f;
		if(glCam->color_scale.g<1)glCam->color_scale.g += 0.05f;
		if(glCam->color_scale.b<1)glCam->color_scale.b += 0.05f;
	}
	if(fadein[1]) {
		if(glCam->color_scale.r<1)glCam->color_scale.r += 0.05f;
	}

	// show text messages ^^
	for(int i=0;i<10;i++) {
		if(pt[i].trig) {
			//pt[i].pres.color = 1 - (sinf(pt[i].pres_timer*5)/1.5);
			if(pt[i].pres.color.a>0)pt[i].pres.color.a -= 0.005f;
			pt[i].pres_timer+=0.01f;
			pt[i].pres.hspace += pt[i].dx;
			pt[i].pres.vspace += pt[i].dy;
			//pt[i].pres.printf(30-pt[i].pres_timer*10,80, pt[i].text);

			// special case: yngvenator
			if(i==9) pt[i].dx *=1.01f;
		}
	}



	return 0;
}


// Render Scene:: All rendering function calls goes here!
void Scene02_render()
{
	//glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	//glPushAttrib();
		glBlendOn();
		glZBufOff();
		glBlendFunc(GL_ZERO, GL_ONE);
		glView->color.a = 1.0f;
		glView->clear();
		glBlendOff();
		glZBufOn();
		glBlendFunc(GL_SRC_ALPHA,GL_ONE);
		//glView->light = 1.5f;
	//glPopAttrib();
	glLightOff();
	
	glSetCamera(cam);
	//glApplyAllLights();

	glBlendFunc(GL_SRC_ALPHA, GL_ZERO);
	scene03->render(0);	
	
	
	glOrthoViewOn();
	glBlendOn();
	glBlendFunc(GL_DST_COLOR, GL_ONE);

	float dy=0;
	int i;
	for(i=kod_y; i<kod_y+rows_on_screen; i++) {
		if(i<rader) {
			c64.printf(3,120-dy+2*(kod_y-int(kod_y)),&rad[i][0]);
			dy+=2.0f;
		}
	}
	
	// glPushAttrib();
	glAlphaOn();
	glZBufOff();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	for(i=0;i<10;i++) {
		if(pt[i].trig && pt[i].pres.color.a>0) {
			pt[i].pres.printf(pt[i].x,pt[i].y, pt[i].text);
			pt[i].pres.color/=3;
			pt[i].pres.printf(pt[i].x,pt[i].y-3+sinf(pt[i].pres_timer), pt[i].text);
			pt[i].pres.color*=3;
		}
	}
	// glPopAttrib();
	glAlphaOff();
	glZBufOn();

#ifdef DEBUG
		fsys.printf(0,100,"%.2f",FPS);
		fsys.printf(15,100,"%d", PPF);
		fsys.printf(30,100,"%d", OPF);
#endif
	
		if(ytrig) {
			glBlendOn();
			glBlendFunc(GL_ONE,GL_ZERO);
			glAlphaOn();
			glBindTexture(&yngve_arm);
			glSetTextureWrap(GL_CLAMP);
			static float dYngve = 0;
			dYngve +=1.6f;
			glColor(1,1,1,1);
			glBegin(GL_TRIANGLE_STRIP);
				glTexCoord2f(1,1); glVertex3f(87.2f+12.8f, dYngve+25.6f-30,0); // Top Right
				glTexCoord2f(0,1); glVertex3f(87.2f, dYngve+25.6f-30,0); // Top Left
				glTexCoord2f(1,0); glVertex3f(87.2f+12.8f, dYngve-30,0); // Bottom Right
				glTexCoord2f(0,0); glVertex3f(87.2f, dYngve-30,0); // Bottom Left
			glEnd();
		}
	glOrthoViewOff();
	glBlendOn();
	glBlendFunc(GL_ONE_MINUS_SRC_COLOR, GL_ONE);
	Filter1.render();
	glSwapBuffer();
}



signed char s2_sync(FSOUND_STREAM *stream, void *buff, int len, int param)
{
	// end of stream callback doesnt have a 'buff' value, if it doesnt it could be a synch point.
	if (buff)
	{
		//
		if(!strcmp((const char *) buff, (const char *) "s1fadeBias")) {
			//fade in 1 (bias)
			fadein[0] = 1;
		}
		
		if(!strcmp((const char *) buff, (const char *) "s1fadeScale")) {
			//fade in 2 (scale)
			fadein[1] = 1;
		} 
		//pt[0].text = "thIs iS a FloATiNG tExt!";
		else if(!strcmp((const char *) buff, (const char *) "s1text1")) {
			pt[0].reset(0);
			pt[0].x = 25;
			pt[0].y = 20;
			pt[0].trig = 1;
			pt[0].dx = 0.004f;
			pt[0].pres.hspace = 0.1f;
		}
		//pt[1].text = "your mind is an orange";
		else if(!strcmp((const char *) buff, (const char *) "s1text2")) {
			pt[1].reset(1);
			pt[1].x = 36;
			pt[1].y = 23;
			pt[1].trig = 1;
			pt[1].dx = 0.004f;
			pt[1].pres.hspace = 0.04f;
		}
		//pt[2].text = "the universe is an ocean";
		else if(!strcmp((const char *) buff, (const char *) "s1text3")) {
			pt[2].reset(2);
			pt[2].pres.hspace = 0.17f;
			pt[2].pres.color.a = 2;
			pt[2].x = 50;
			pt[2].y = 30;
			pt[2].trig = 1;
			pt[2].dx = 0.004f;
		}
		//pt[3].text = "love";
		if(!strcmp((const char *) buff, (const char *) "s1text4")) {
			pt[3].reset(3);
			pt[3].pres.hspace = 0.7f;
			pt[3].pres.setsize(10,10);
			pt[3].pres.color = RGBACOLOR(1,0.3f,0.3f,1);
			pt[3].x = 24;
			pt[3].y = 20;
			pt[3].trig = 1;
			pt[3].dx = 0.01f;
		}
		//pt[4].text = "hate";
		else if(!strcmp((const char *) buff, (const char *) "s1text5")) {
			pt[4].reset(4);
			pt[4].pres.setspace(0,0.3f);
			pt[4].pres.setsize(10,10);
			pt[4].pres.color = RGBACOLOR(0.3f,0.3f,1,2);
			pt[4].x = 87;
			pt[4].y = 65;
			pt[4].trig = 1;
			pt[4].dy = 0.012f;
		}
		//pt[5].text = "do you know what we want?";
		else if(!strcmp((const char *) buff, (const char *) "s1text6")) {
			pt[5].reset(5);
			pt[5].x = 45;
			pt[5].y = 73;
			pt[5].trig = 1;
			pt[5].dx = 0.005f;
		}
		//pt[6].text = "?";
		else if(!strcmp((const char *) buff, (const char *) "s1text7")) {
			pt[6].reset(6);
			pt[6].x = 38;
			pt[6].y = 13;
			pt[6].trig = 1;
			pt[6].dx = 0.001f;
		}
		//pt[7].text = "we know";
		else if(!strcmp((const char *) buff, (const char *) "s1text8")) {
			pt[7].reset(7);
			pt[7].x = 60;
			pt[7].y = 20;
			pt[7].trig = 1;
			pt[7].dx = 0.001f;
		}

		//pt[8].text = "!!";
		else if(!strcmp((const char *) buff, (const char *) "s1text9")) {
			pt[8].reset(8);
			pt[8].x = 60;
			pt[8].y = 10;
			pt[8].trig = 1;
			pt[8].dx = 0.001f;
		}

		//pt[9].text = "yngvenator";
		else if(!strcmp((const char *) buff, (const char *) "s1text10")) {
			pt[9].reset(9);
			pt[9].x = 50;
			pt[9].y = 80;
			pt[9].pres.color = RGBACOLOR(1.0f,0.2f,0.2f,2.0f);
			pt[9].pres.setsize(8,12);
			pt[9].pres.setspace(5,1);
			pt[9].trig = 1;
			pt[9].dx = -0.01f;
			ytrig=1;
		}
		else if(!strcmp((const char *) buff, (const char *) "s2start")) {
			glScene::current = &data_scenen;
			glScene::current->init();
		}

	}
	else
	{
		// musiken �r slut
	}
	return true;
}
