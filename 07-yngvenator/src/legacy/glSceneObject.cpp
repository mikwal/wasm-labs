#include "glBase.h"
#include <cassert>




MATERIAL glDefaultMat;
MATERIAL *glMat=&glDefaultMat;

void glMaterial(MATERIAL *m)
{
	if(m)
	{
		int face;

		glMat=m;

		if(m->flags&GLM_TWOSIDED)
		{
			glDisable(GL_CULL_FACE);
			face=GL_FRONT_AND_BACK;
		}
		else
		{
			glEnable(GL_CULL_FACE);
			face=GL_FRONT;
		}

		if(m->flags&GLM_TRANSLUCENT)
		{
			glDepthMask(0);
			glEnable(GL_BLEND);
		}
		else
		{
			glDepthMask(1);
			glDisable(GL_BLEND);
		}


		glColor4fv((float*)&glCam->getcolor(m->diffuse));
		glMaterialfv(face,GL_AMBIENT,(float*)&glCam->getcolor(m->ambient));
		glMaterialfv(face,GL_DIFFUSE,(float*)&glCam->getcolor(m->diffuse));
		glMaterialfv(face,GL_SPECULAR,(float*)&glCam->getcolor(m->specular));
		glMaterialfv(face,GL_EMISSION,(float*)&glCam->getcolor(m->emission));
		glMaterialf(face,GL_SHININESS,m->shininess);
		
		if(glCam->color_conversion==GL_LUMINANCE)
			glBindTexture(&m->texture_luminance);
		else
			glBindTexture(&m->texture_rgb);
		
		if(m->mapping)
		{
			glEnable(GL_TEXTURE_GEN_S);
			glTexGeni(GL_TEXTURE_GEN_S,GL_TEXTURE_GEN_MODE,m->mapping);
			glEnable(GL_TEXTURE_GEN_T);
			glTexGeni(GL_TEXTURE_GEN_T,GL_TEXTURE_GEN_MODE,m->mapping);
		}
		else
		{
			glDisable(GL_TEXTURE_GEN_S);
			glDisable(GL_TEXTURE_GEN_T);
		}
	}
	else
		glMaterial(&glDefaultMat);
}



void glMesh(MESH *m)
{
	int i,mode;


	if(glMat->flags&GLM_WIREFRAME)
		mode=GL_LINE_STRIP;
	else
		mode=GL_TRIANGLES;

	if(glMat->flags&GLM_FACETED)
	{
		glBegin(mode);
		for(i=0;i<m->face_count;i++)
		{	
			PPF++;
			glNormal3fv((float*)&m->face[i].fnormal);
			glTexCoord2fv((float*)&m->face[i].texcoord[0]);
			glVertex3fv((float*)&m->vertex[m->face[i].vindex[0]]);
			glTexCoord2fv((float*)&m->face[i].texcoord[1]);
			glVertex3fv((float*)&m->vertex[m->face[i].vindex[1]]);
			glTexCoord2fv((float*)&m->face[i].texcoord[2]);
			glVertex3fv((float*)&m->vertex[m->face[i].vindex[2]]);
		}
		glEnd();
	}
	else
	{
		glBegin(mode);
		for(i=0;i<m->face_count;i++)
		{	
			PPF++;
			glTexCoord2fv((float*)&m->face[i].texcoord[0]);
			glNormal3fv((float*)&m->vnormal[m->face[i].vindex[0]]);
			glVertex3fv((float*)&m->vertex[m->face[i].vindex[0]]);
			glTexCoord2fv((float*)&m->face[i].texcoord[1]);
			glNormal3fv((float*)&m->vnormal[m->face[i].vindex[1]]);
			glVertex3fv((float*)&m->vertex[m->face[i].vindex[1]]);
			glTexCoord2fv((float*)&m->face[i].texcoord[2]);
			glNormal3fv((float*)&m->vnormal[m->face[i].vindex[2]]);
			glVertex3fv((float*)&m->vertex[m->face[i].vindex[2]]);
		}
		glEnd();
	}
}


// note: dos'nt calculate new face normal


void glSkin(SKIN *s,MESH *m)
{
	int i,j,k;



	if(s->face_count!=m->face_count||s->vertex_count!=m->vertex_count)
		return;

	for(i=0;i<s->bone_count;i++)
		s->bone[i].tempmatrix= Minverse(s->restmatrix) * s->bone[i].matrixowner->gettransform() * Minverse(s->bone[i].restmatrix) * s->restmatrix;

	for(i=0;i<s->face_count;i++)
	{	
		s->face[i].vindex[0]=m->face[i].vindex[0];
		s->face[i].texcoord[0]=m->face[i].texcoord[0];

		s->face[i].vindex[1]=m->face[i].vindex[1];
		s->face[i].texcoord[1]=m->face[i].texcoord[1];

		s->face[i].vindex[2]=m->face[i].vindex[2];
		s->face[i].texcoord[2]=m->face[i].texcoord[2];

		s->face[i].fnormal=s->face[i].fnormal;
	}

	for(i=0;i<s->vertex_count;i++)
	{	
		s->vertex[i]=0.0f;
		s->vnormal[i]=0.0f;


		for(j=0;j<s->weight_count[i];j++)
		{	
			k=i*s->weight_count_max+j;

			s->vertex[i]+=(m->vertex[i]*s->bone[s->bone_index[k]].tempmatrix)*s->weight_value[k];
			s->vnormal[i]+=(m->vnormal[i]*s->bone[s->bone_index[k]].tempmatrix.rotation())*s->weight_value[k];
		}
	}

}




void ScalarController::update(float time)
{
	int i;
	float u;


	// handle special cases

	if(time<=key[0].time)
		value.scalar=key[0].value;
	else
		if(time>=key[key_count-1].time)
			value.scalar=key[key_count-1].value;
		else
		{
			for(i=0; time>=key[i+1].time; i++);	// get key index
			
			u=(time-key[i].time)/(key[i+1].time-key[i].time);		// normalized time
			
			if(key[i].smooth)
				value.scalar= key[i].value*((1.0f-u)*(1.0f-u)*(1.0f-u)) + key[i+1].value*(u*u*u)+ key[i].tout*(3.0f*u*(1.0f-u)*(1.0f-u))+ key[i+1].tin*(3.0f*u*u*(1.0f-u));
			else
				value.scalar= key[i].value*(1.0f-u) + key[i+1].value*u;
		}
}


void VectorController::update(float time)
{
	int i;
	float u;


	// handle special cases

	if(time<=key[0].time)
		value.vector=key[0].value;
	else
		if(time>=key[key_count-1].time)
			value.vector=key[key_count-1].value;
		else
		{
			for(i=0; time>=key[i+1].time; i++);	// get key index
			
			u=(time-key[i].time)/(key[i+1].time-key[i].time);		// normalized time
		
			if(key[i].smooth)
				value.vector= key[i].value*((1.0f-u)*(1.0f-u)*(1.0f-u)) + key[i+1].value*(u*u*u)+ key[i].tout*(3.0f*u*(1.0f-u)*(1.0f-u))+ key[i+1].tin*(3.0f*u*u*(1.0f-u));
			else
				value.vector= key[i].value*(1.0f-u) + key[i+1].value*u;
		}
}

void RotationController::update(float time)
{
	
	int i;
	float u;


	// handle special cases


	if(time<=key[0].time)
		value.rotation=Mrotate(key[0].angle,-key[0].value);
	else
		if(time>=key[key_count-1].time)	
		{
			value.rotation=Midentity;
			
			for(i=0;i<key_count; i++)
				value.rotation=Mrotate(key[i].angle,-key[i].value)*value.rotation;
		}
		else
		{
			value.rotation=Midentity;

			for(i=0; time>=key[i].time; i++)	// get key index
				value.rotation=Mrotate(key[i].angle,-key[i].value)*value.rotation;	// and apply rotations
		
			u=(time-key[i-1].time)/(key[i].time-key[i-1].time);		// normalized time

			value.rotation=Mrotate(key[i].angle*u,-key[i].value)*value.rotation;

		}

	/***
	 
	   inverse axis and inverse rotation order, have no idea why but
	   the animation works like in 3dsmax then.

	***/
}


#define CMP_OBJECTNAME(s1,s2)	((unsigned long*)(s1))[0]==((unsigned long*)(s2))[0]&&	\
								((unsigned long*)(s1))[1]==((unsigned long*)(s2))[1]&&	\
								((unsigned long*)(s1))[2]==((unsigned long*)(s2))[2]&&	\
								((unsigned long*)(s1))[3]==((unsigned long*)(s2))[3]&&	\
								((unsigned long*)(s1))[4]==((unsigned long*)(s2))[4]&&	\
								((unsigned long*)(s1))[5]==((unsigned long*)(s2))[5]





glSceneObject::glSceneObject()
{
	int i;

	ID=-1;
	
	for(i=0;i<OBJECTNAME_SIZE+1;i++)
		name[i]=0;

	flags=GLSO_ACTIVE|GLSO_VISIBLE;
	matrix=Midentity;
	offset=Midentity;

	position=NULL;
	rotation=NULL;
	scale=NULL;

	path=NULL;
	path_pos=0.0f;
	path_axis=-1;

	target=NULL;
	target_roll=0.0f;

	child_count=0;
	child=NULL;
	parent=NULL;
}





glSceneObject::~glSceneObject()
{
	int i;

	for(i=0;i<child_count;i++)
		delete child[i];

	delete position;
	delete rotation;
	delete scale;
	delete [] child;
}


MATRIX glSceneObject::getmatrix()
{
	MATRIX m=matrix;

	if(parent)
		m=parent->getmatrix()*m;

	if(path&&path->type()==GLSO_SHAPE)
	{
		m.p=((glShape*)path)->getvertex(0,path_pos);
		
		if(path_axis>=0)
		{
			switch(path_axis)
			{
			case 0:
				m=MtargetVx(m.p,m.p+((glShape*)path)->gettangent(0,path_pos))*m.scale();
				break;
			case 1:
				m=MtargetVxflipped(m.p,m.p+((glShape*)path)->gettangent(0,path_pos))*m.scale();
				break;
			case 2:
				m=MtargetVy(m.p,m.p+((glShape*)path)->gettangent(0,path_pos))*m.scale();
				break;
			case 3:
				m=MtargetVyflipped(m.p,m.p+((glShape*)path)->gettangent(0,path_pos))*m.scale();
				break;
			case 4:
				m=MtargetVz(m.p,m.p+((glShape*)path)->gettangent(0,path_pos))*m.scale();
				break;
			case 5:
				m=MtargetVzflipped(m.p,m.p+((glShape*)path)->gettangent(0,path_pos))*m.scale();
				break;
			}
		}
	}

	if(target)
		m=Mtarget(m.p,target->getmatrix().p)*Mrotate(target_roll,-Vy)*m.scale();

	return m;
}


static glSceneObject *getPOINTERfromID(int ID,glSceneObject *obj)
{
	int i;
	glSceneObject *r;
	
	if(ID>=0)
	{
		if(obj->ID==ID)
			return obj;
		
		for(i=0; i<obj->child_count; i++)
			if(r=getPOINTERfromID(ID,obj->child[i]))
				return r;
	}

	return NULL;
}

static glSceneObject *getPOINTERfromname(const char *name,glSceneObject *obj)
{
	int i;
	glSceneObject *r;
	
	if(*name)
	{
		if(CMP_OBJECTNAME(name,obj->name))
			return obj;
		
		for(i=0; i<obj->child_count; i++)
			if(r=getPOINTERfromname(name,obj->child[i]))
				return r;
	}

	return NULL;
}


glSceneObject *glSceneObject::getobject(int i)
{
	return getPOINTERfromID(i,this);
}


glSceneObject *glSceneObject::getobject(const char *n)
{
	char tmp[OBJECTNAME_SIZE+1];
	int i;

	for(i=0;i<OBJECTNAME_SIZE,n[i];i++)
		tmp[i]=tolower(n[i]);
	while(i<OBJECTNAME_SIZE+1)
		tmp[i++]=0;

	return getPOINTERfromname(tmp,this);
}


void glSceneObject::update(float time)
{
	MATRIX p,r,s;

	if((flags&GLSO_ACTIVE))
	{
		if(flags&GLSO_USE_CONTROLLERS)
		{
			if(position)
			{
				position->update(time);			
				p=Mtranslate(position->value.vector);
				path_pos=position->value.scalar;
			}
			else
				p=matrix.position();
			

			if(rotation)
			{
				rotation->update(time);
				r=rotation->value.rotation;
				target_roll=rotation->value.scalar;
			}
			else
				r=matrix.rotation();

			if(scale)
			{
				scale->update(time);
				s=Mscale(scale->value.vector);
			}
			else
				s=matrix.scale();

			matrix=p*r*s;
		}

		for(int i=0;i<child_count;i++)
			child[i]->update(time);

	}
}



void glSceneObject::render(int pass, int renderflags)
{
	if((flags&GLSO_VISIBLE)&&child_count>0)
		for(int i=0;i<child_count;i++)
			child[i]->render(pass,renderflags);
}






static char *readword(IOStream *s)
{
	static char buf[MAX_PATH];
	int c,i=0,end;

	buf[0]=0;

	while((c=s->get())==' '||c=='\n');

	if(c=='\"')
	{
		end=c;
		c=s->get();
	}
	else
		end=' ';
	
	while(i<MAX_PATH-1&&c!=end)
	{
		if(c==EOF)
			break;
		
		buf[i++]=c;
		c=s->get();
	}

	buf[i]=0;
	
	return buf;
}

#define GET_FLOAT(s) ((float)atof(readword(s)))
#define GET_INT(s) atoi(readword(s))
#define GET_STRING(s) readword(s)



static void readmaterial(IOStream *s,MATERIAL *m)
{
	char *texture_name;

	m->flags=0;
	if(GET_INT(s))
		m->flags|=GLM_FACETED;
	if(GET_INT(s))
		m->flags|=GLM_TWOSIDED;
	if(GET_INT(s))
		m->flags|=GLM_WIREFRAME;

	m->ambient.r=GET_FLOAT(s);
	m->ambient.g=GET_FLOAT(s);
	m->ambient.b=GET_FLOAT(s);
	m->diffuse.r=GET_FLOAT(s);
	m->diffuse.g=GET_FLOAT(s);
	m->diffuse.b=GET_FLOAT(s);
	m->specular.r=GET_FLOAT(s);
	m->specular.g=GET_FLOAT(s);
	m->specular.b=GET_FLOAT(s);
	m->emission.r=GET_FLOAT(s);
	m->emission.g=GET_FLOAT(s);
	m->emission.b=GET_FLOAT(s);
	m->shininess=GET_FLOAT(s);
	
	if((m->ambient.a=m->diffuse.a=m->specular.a=m->emission.a=GET_FLOAT(s))<1.0f)
		m->flags|=GLM_TRANSLUCENT;
	
	
	texture_name=fnname(GET_STRING(s));

	if(GET_INT(s))
	{
		m->mapping=GL_SPHERE_MAP;
		GET_STRING(s);
	}
	else
		switch(GET_INT(s))
		{
		default:
			m->mapping=0;
			break;
		case 2:
			m->mapping=GL_OBJECT_LINEAR;
			break;
		case 3:
			m->mapping=GL_EYE_LINEAR;
			break;
		}
			
	if(texture_name[0]&&glTextureListGet(texture_name,&m->texture_rgb,GL_RGB))
		m->texture_rgb=0;

	if(texture_name[0]&&glTextureListGet(texture_name,&m->texture_luminance,GL_LUMINANCE))
		m->texture_luminance=0;
	
}


static void readmesh(IOStream *s,MESH *m)
{
	int i;

	m->vertex_count=GET_INT(s);
	m->face_count=GET_INT(s);


	m->vertex=new VERTEX[m->vertex_count];
	m->vnormal=new VECTOR[m->vertex_count];

	for(i=0;i<m->vertex_count;i++)
	{
		m->vertex[i].x=GET_FLOAT(s);		
		m->vertex[i].y=GET_FLOAT(s);
		m->vertex[i].z=GET_FLOAT(s);
		m->vnormal[i]=0.0f;
	}

	m->face=new FACE[m->face_count];
		
	for(i=0;i<m->face_count;i++)
	{
		m->face[i].vindex[0]=GET_INT(s);
		m->face[i].texcoord[0].x=GET_FLOAT(s);
		m->face[i].texcoord[0].y=GET_FLOAT(s);
		m->face[i].vindex[1]=GET_INT(s);
		m->face[i].texcoord[1].x=GET_FLOAT(s);
		m->face[i].texcoord[1].y=GET_FLOAT(s);
		m->face[i].vindex[2]=GET_INT(s);
		m->face[i].texcoord[2].x=GET_FLOAT(s);
		m->face[i].texcoord[2].y=GET_FLOAT(s);		
		
		m->face[i].fnormal=(m->vertex[m->face[i].vindex[2]]-m->vertex[m->face[i].vindex[0]]).cross(m->vertex[m->face[i].vindex[1]]-m->vertex[m->face[i].vindex[0]]);
		m->face[i].fnormal/=m->face[i].fnormal.length();
			
		m->vnormal[m->face[i].vindex[0]]+=m->face[i].fnormal;
		m->vnormal[m->face[i].vindex[1]]+=m->face[i].fnormal;
		m->vnormal[m->face[i].vindex[2]]+=m->face[i].fnormal;
	}

	for(i=0;i<m->vertex_count;i++)
		m->vnormal[i]/=m->vnormal[i].length();
}


static glMeshObject *readmeshobject(IOStream *s,glRootObject *r) 
{
	glMeshObject *mobj;
	MATERIAL *material;
	MESH *mesh;
	SKIN *skin;
	int bone_count, weight_count_max;
	int i,j;
	

	if((i=GET_INT(s))>=0)
		material=&r->material[i];
	else
		material=NULL;
		
	if((i=GET_INT(s))>=0)
		mesh=&r->mesh[i];
	else
		mesh=NULL;

	bone_count=GET_INT(s);

	if(bone_count>0)
	{
		weight_count_max=GET_INT(s);

		skin=new SKIN(mesh->face_count,mesh->vertex_count,bone_count,weight_count_max);	
		
		for(i=0;i<skin->bone_count;i++)
			skin->bone[i].matrixowner=(glSceneObject*)GET_INT(s);

		for(i=0;i<skin->vertex_count;i++)
		{
			skin->weight_count[i]=GET_INT(s);
			
			for(j=0;j<skin->weight_count[i];j++)
			{
				skin->bone_index[i*skin->weight_count_max+j]=GET_INT(s);
				skin->weight_value[i*skin->weight_count_max+j]=GET_FLOAT(s);	
			}
			
		}
	}
	else
		skin=NULL;
	
	mobj=new glMeshObject(material,mesh,skin);
	mobj->setclip();
		
	return mobj;
}


static glShape *readshape(IOStream *s) 
{
	int spline_count;
	SPLINE *spline;
	int i,j;

	spline_count=GET_INT(s);
	spline=new SPLINE[spline_count];

	for(i=0;i<spline_count;i++)
	{
		spline[i].closed=GET_INT(s);
		spline[i].vertex_count=GET_INT(s);
		spline[i].vertex=new VERTEX[spline[i].vertex_count];
		spline[i].vin=new VECTOR[spline[i].vertex_count];
		spline[i].vout=new VECTOR[spline[i].vertex_count];

		for(j=0;j<spline[i].vertex_count;j++)
		{
			spline[i].vertex[j].x=GET_FLOAT(s);
			spline[i].vertex[j].y=GET_FLOAT(s);
			spline[i].vertex[j].z=GET_FLOAT(s);
			spline[i].vin[j].x=GET_FLOAT(s);
			spline[i].vin[j].y=GET_FLOAT(s);
			spline[i].vin[j].z=GET_FLOAT(s);
			spline[i].vout[j].x=GET_FLOAT(s);
			spline[i].vout[j].y=GET_FLOAT(s);
			spline[i].vout[j].z=GET_FLOAT(s);
		}
	}

	return new glShape(spline_count,spline);
}


static glLight *readlight(IOStream *s) 
{
	float multiplier;
	RGBACOLOR ambient,diffuse,specular;
	int spot;
	float spot_cutoff, spot_exponent;
	float constant_attenuation,linear_attenuation,quadric_attenuation;


	spot=GET_INT(s);
	spot_cutoff=GET_FLOAT(s);
	spot_exponent=GET_FLOAT(s);

	ambient.r=GET_FLOAT(s);
	ambient.g=GET_FLOAT(s);
	ambient.b=GET_FLOAT(s);
	diffuse.r=GET_FLOAT(s);
	diffuse.g=GET_FLOAT(s);
	diffuse.b=GET_FLOAT(s);
	specular.r=GET_FLOAT(s);
	specular.g=GET_FLOAT(s);
	specular.b=GET_FLOAT(s);
	
	constant_attenuation=GET_FLOAT(s);
	linear_attenuation=GET_FLOAT(s);
	quadric_attenuation=GET_FLOAT(s);
	multiplier=GET_FLOAT(s);

	return new glLight(multiplier,ambient,diffuse,specular,spot,spot_cutoff,spot_exponent,constant_attenuation,linear_attenuation,quadric_attenuation);
}


static glCamera *readcamera(IOStream *s) 
{
	float fov;
	float znear, zfar;

	fov=GET_FLOAT(s);
	znear=GET_FLOAT(s); 
	zfar=GET_FLOAT(s);

	return new glCamera(fov,znear,zfar);
}


static glBoxGizmo *readboxgizmo(IOStream *s) 
{
	float length;
	float width;
	float height;
	int seed;

	length=GET_FLOAT(s);
	width=GET_FLOAT(s);
	height=GET_FLOAT(s);
	seed=GET_INT(s);

	return new glBoxGizmo(length,width,height,seed);
}


static glSphereGizmo *readspheregizmo(IOStream *s) 
{
	float radius;
	int seed;

	radius=GET_FLOAT(s);
	seed=GET_INT(s);

	return new glSphereGizmo(radius,seed);
}

 
static glCylinderGizmo *readcylindergizmo(IOStream *s)
{
	float radius;
	float height;
	int seed;

	radius=GET_FLOAT(s);
	height=GET_FLOAT(s);
	seed=GET_INT(s);

	return new glCylinderGizmo(radius,height,seed);
}


static Controller *readscalarcontroller(IOStream *s)
{
	ScalarController *sc;
	int key_count,i;
	
	key_count=GET_INT(s);

	if(key_count)
	{
		sc=new ScalarController(key_count);
			
		for(i=0;i<key_count;i++)
		{
			sc->key[i].time=GET_FLOAT(s);
			sc->key[i].value=GET_FLOAT(s);
			sc->key[i].tin=GET_FLOAT(s);
			sc->key[i].tout=GET_FLOAT(s);
			sc->key[i].smooth=GET_INT(s);
		}
	}
	else
		sc=NULL;

	return sc;
}


static Controller *readvectorcontroller(IOStream *s)
{
	VectorController *vc;
	int key_count,i;
	
	key_count=GET_INT(s);

	if(key_count)
	{
		vc=new VectorController(key_count);
			
		for(i=0;i<key_count;i++)
		{
			vc->key[i].time=GET_FLOAT(s);
			vc->key[i].value.x=GET_FLOAT(s);
			vc->key[i].value.y=GET_FLOAT(s);
			vc->key[i].value.z=GET_FLOAT(s);
			vc->key[i].tin.x=GET_FLOAT(s);
			vc->key[i].tin.y=GET_FLOAT(s);
			vc->key[i].tin.z=GET_FLOAT(s);
			vc->key[i].tout.x=GET_FLOAT(s);
			vc->key[i].tout.y=GET_FLOAT(s);
			vc->key[i].tout.z=GET_FLOAT(s);
			vc->key[i].smooth=GET_INT(s);
		}
	}
	else
		vc=NULL;
	
	return vc;
}


static Controller *readrotationcontroller(IOStream *s)
{
	RotationController *rc;
	int key_count,i;
	
	key_count=GET_INT(s);

	if(key_count)
	{
		rc=new RotationController(key_count);
			
		for(i=0;i<key_count;i++)
		{
			rc->key[i].time=GET_FLOAT(s);
			rc->key[i].value.x=GET_FLOAT(s);
			rc->key[i].value.y=GET_FLOAT(s);
			rc->key[i].value.z=GET_FLOAT(s);
			rc->key[i].angle=GET_FLOAT(s);
		}
	}
	else
		rc=NULL;
	
	return rc;
}


static glSceneObject *readsceneobject(IOStream *s,glRootObject *r)
{
	int ID, pathID,targetID;
	float path_pos, target_roll;
	int path_axis;
	MATRIX matrix;
	MATRIX offset;
	Controller *position,*rotation, *scale;
	char name[OBJECTNAME_SIZE],*type;
	glSceneObject *obj;
	int i;
	


	ID=GET_INT(s);	// ID
	memcpy(name,GET_STRING(s),OBJECTNAME_SIZE);	// name

	// transform matrix
	matrix.x.x=GET_FLOAT(s);
	matrix.x.y=GET_FLOAT(s);
	matrix.x.z=GET_FLOAT(s);
	matrix.x_w=0.0f;
	matrix.y.x=GET_FLOAT(s);
	matrix.y.y=GET_FLOAT(s);
	matrix.y.z=GET_FLOAT(s);
	matrix.y_w=0.0f;
	matrix.z.x=GET_FLOAT(s);
	matrix.z.y=GET_FLOAT(s);
	matrix.z.z=GET_FLOAT(s);
	matrix.z_w=0.0f;
	matrix.p.x=GET_FLOAT(s);
	matrix.p.y=GET_FLOAT(s);
	matrix.p.z=GET_FLOAT(s);
	matrix.p_w=1.0f;
	
	// object offset matrix
	offset.x.x=GET_FLOAT(s);
	offset.x.y=GET_FLOAT(s);
	offset.x.z=GET_FLOAT(s);
	offset.x_w=0.0f;
	offset.y.x=GET_FLOAT(s);
	offset.y.y=GET_FLOAT(s);
	offset.y.z=GET_FLOAT(s);
	offset.y_w=0.0f;
	offset.z.x=GET_FLOAT(s);
	offset.z.y=GET_FLOAT(s);
	offset.z.z=GET_FLOAT(s);
	offset.z_w=0.0f;
	offset.p.x=GET_FLOAT(s);
	offset.p.y=GET_FLOAT(s);
	offset.p.z=GET_FLOAT(s);
	offset.p_w=1.0f;

	if(ID==3)
	{
		ID=ID;
	}

	pathID=GET_INT(s);
	path_pos=GET_FLOAT(s);
	path_axis=GET_INT(s);
	
	if(pathID>=0)
		position=readscalarcontroller(s);
	else
		position=readvectorcontroller(s);

	targetID=GET_INT(s);
	target_roll=GET_FLOAT(s);

	if(targetID>=0)
		rotation=readscalarcontroller(s);
	else
		rotation=readrotationcontroller(s);

	scale=readvectorcontroller(s);



	// object type
	type=GET_STRING(s);	

	if(strcmpi(type,"MeshObject")==0)
		obj=readmeshobject(s,r);
	else 
	if(strcmpi(type,"Shape")==0)
		obj=readshape(s);
	else 
	if(strcmpi(type,"Light")==0)
		obj=readlight(s);
	else 
	if(strcmpi(type,"Camera")==0)
		obj=readcamera(s);
	else 
	if(strcmpi(type,"BoxGizmo")==0)
		obj=readboxgizmo(s);
	else 
	if(strcmpi(type,"SphereGizmo")==0)
		obj=readspheregizmo(s);
	else 
	if(strcmpi(type,"CylinderGizmo")==0)
		obj=readcylindergizmo(s);
	else
		obj=new glSceneObject;

	
	// copy temporary data
	obj->ID=ID;
	for(i=0;i<OBJECTNAME_SIZE,name[i];i++)
		obj->name[i]=tolower(name[i]);
	while(i<OBJECTNAME_SIZE+1)
		obj->name[i++]=0;
	obj->matrix=matrix;
	obj->offset=offset;
	obj->path=(glSceneObject*)pathID;
	obj->path_axis=path_axis;
	obj->path_pos=path_pos;
	obj->position=position;
	obj->target=(glSceneObject*)targetID;
	obj->target_roll=target_roll;
	obj->rotation=rotation;
	obj->scale=scale;

	if(position||rotation||scale)
		obj->flags|=GLSO_USE_CONTROLLERS;


	// number of children
	obj->child_count=GET_INT(s);	

	// read childrens
	if(obj->child_count>0)
	{
		obj->child=new glSceneObject*[obj->child_count];
		
		for(i=0;i<obj->child_count;i++)
		{
			obj->child[i]=readsceneobject(s,r);
			obj->child[i]->parent=obj;
		}
	}
	else
		obj->child=NULL;
	
	return obj;
}





static void path_and_target_fixIDtoPOINTER(glSceneObject *obj,glRootObject *r)
{
	int i;

	// turn target id�s into pointers
	obj->path=getPOINTERfromID((int)obj->path,r);
	obj->target=getPOINTERfromID((int)obj->target,r);
	
	for(i=0; i<obj->child_count; i++)
		path_and_target_fixIDtoPOINTER(obj->child[i],r);
}


static void bone_fixIDtoPOINTER(glSceneObject *obj,glRootObject *r)
{
	int i;

	// turn bone id�s into pointers
	if(obj->type()==GLSO_MESHOBJECT)
	{
		glMeshObject *mobj=(glMeshObject*)obj;
		if(mobj->skin)
		{
			for(i=0;i<mobj->skin->bone_count;i++)
			{
				mobj->skin->bone[i].matrixowner=getPOINTERfromID((int)mobj->skin->bone[i].matrixowner,r);
				mobj->skin->bone[i].restmatrix=mobj->skin->bone[i].matrixowner->gettransform();
			}
			mobj->skin->restmatrix=mobj->gettransform();
		}

	}

	for(i=0; i<obj->child_count; i++)
		bone_fixIDtoPOINTER(obj->child[i],r);
}


glRootObject *glLoadScene(const char *filename)
{
	IOStream *s;
	int i;
	glRootObject *root;
	


	if((s=fopen(filename))==NULL)
		return NULL;

	if(strcmpi(GET_STRING(s),"RootObject"))
	{
		delete s;
		return NULL;
	}

	root=new glRootObject;

	root->ID=0;
	root->flags=GLSO_ACTIVE|GLSO_VISIBLE;
	root->matrix=Midentity;
	root->offset=Midentity;

	root->position=NULL;
	root->rotation=NULL;
	root->scale=NULL;

	root->parent=NULL;

	root->path=(glSceneObject*)-1;
	root->path_axis=-1;	
	root->path_pos=0.0f;

	root->target=(glSceneObject*)-1;	
	root->target_roll=0.0f;	

	root->material_count=GET_INT(s);
	root->mesh_count=GET_INT(s);
	root->child_count=GET_INT(s);
	
	if(root->material_count>0)
	{
		root->material=new MATERIAL[root->material_count];
		
		for(i=0;i<root->material_count;i++)
			readmaterial(s,&root->material[i]);
	}
	else
		root->material=NULL;

	if(root->mesh_count>0)
	{
		root->mesh=new MESH[root->mesh_count];
	
		for(i=0;i<root->mesh_count;i++)
			readmesh(s,&root->mesh[i]);
	}
	else
		root->mesh=NULL;

	if(root->child_count>0)
	{
		root->child=new glSceneObject*[root->child_count];
		
		for(i=0;i<root->child_count;i++)
		{
			root->child[i]=readsceneobject(s,root);
			root->child[i]->parent=root;
		}
	}
	else
		root->child=NULL;

	path_and_target_fixIDtoPOINTER(root,root);
	bone_fixIDtoPOINTER(root,root);

	delete s;

	return root;
}



void glMeshObject::setclip()
{
	int i;
	VECTOR *v;
	MESH *m;

	if(skin)
		m=skin;
	else
		m=mesh;
	
	clip.max=m->vertex[0];
	clip.min=m->vertex[0];

	for(i=1;i<m->vertex_count;i++)
	{
		v=&m->vertex[i];

		if(v->x>clip.max.x)
			clip.max.x=v->x;
		else
			if(v->x<clip.min.x)
				clip.min.x=v->x;

		if(mesh->vertex[i].y>clip.max.y)
			clip.max.y=v->y;
		else
			if(v->y<clip.min.y)
				clip.min.y=v->y;

		if(v->z>clip.max.z)
			clip.max.z=v->z;
		else
			if(v->z<clip.min.z)
				clip.min.z=v->z;
	}
}










void glMeshObject::render(int pass,int renderflags)
{
	if(flags&GLSO_VISIBLE)
	{
		MESH *m;
		MATRIX w;
		VECTOR p[8];
		int i,j;
		float t;
		
		
		for(i=0;i<child_count;i++)
			child[i]->render(pass,renderflags);	

		
		switch(pass)
		{
		default:
			return;

		case 0:
			if(material&&material->flags&GLM_TRANSLUCENT)
				return;
			else
				break;

		case 1:
			if(material==NULL||!(material->flags&GLM_TRANSLUCENT))
				return;
			else
				break;
		}		
		
		
		if(skin)
		{
			if(!(renderflags&GLRF_KEEP_SKIN))
				glSkin(skin,mesh);

			setclip();

			m=skin;
		}
		else
			m=mesh;


		w=glCam->wmatrix*gettransform();
		
		clip.getpoints(p);

		for(i=0;i<8;i++)
			p[i]*=w;

		for(i=0;i<6;i++)
		{
			if(	(glCam->frustrum[i].substitute(p[0])<-FLT_EPSILON)&&
				(glCam->frustrum[i].substitute(p[1])<-FLT_EPSILON)&&
				(glCam->frustrum[i].substitute(p[2])<-FLT_EPSILON)&&
				(glCam->frustrum[i].substitute(p[3])<-FLT_EPSILON)&&
				(glCam->frustrum[i].substitute(p[4])<-FLT_EPSILON)&&
				(glCam->frustrum[i].substitute(p[5])<-FLT_EPSILON)&&
				(glCam->frustrum[i].substitute(p[6])<-FLT_EPSILON)&&
				(glCam->frustrum[i].substitute(p[7])<-FLT_EPSILON))
					return;
		}

/*
		int ist[54]={0,1, 1,2, 2,3, 3,0, 4,5, 5,6, 6,7, 0,4, 1,5, 2,6, 3,7};//    0,2, 1,3,  4,6, 5,7,   0,5, 1,4, 3,6, 2,7,   0,7, 3,4,  1,6,  2,5,   0,6, 1,7,  2,4, 3,5};
		int is_clipped=1;

	
		for(i=0;i<8;i++)
			if(glCam->frustrum.clip(p[i])==0)
			{
				is_clipped=0;
				break;
			}
		
		if(is_clipped)
			for(i=0;i<24;i+=2)
				for(j=0;j<6;j++)
				{
					RAY ray=p[ist[i]]>>p[ist[i+1]];

					t=glCam->frustrum[j].intersect(ray);

					if(t>=0.0f&&t<=1.0f)
						if(glCam->frustrum.clip(ray(t))==0)
						{
							is_clipped=0;
								break;
						}
				}

		
		if(is_clipped)
			return;*/

		if((w.z.cross(w.y)).dot(w.x)<0.0f)
			glCullFace(GL_FRONT);

		glMaterial(material);
		glPushMatrix();	
		glLoadMatrixf((float*)&w);

		if(glMat->mapping)
		{
			float s;
			
			s=1.0f/((clip.max-clip.min)*w.scale()).length();

			glMatrixMode(GL_TEXTURE);
			glPushMatrix();
			glScalef(s,s,s);
			glMesh(m);
			glPopMatrix();
			glMatrixMode(GL_MODELVIEW);
		}
		else
			glMesh(m);

		glPopMatrix();
		glCullFace(GL_BACK);

		OPF++;
	}
}







glCamera glDefaultCam;
glCamera *glCam=&glDefaultCam;



void glSetCamera(glSceneObject *c)
{	
	if(c->type()==GLSO_CAMERA)
	{
		float w,h;
		float a;
		float xp,yp;
		float r,r2;
		
		glCam=(glCamera*)c;
		
		glOrthoViewOff();

		w=float(glView->w>0?glView->w:glScreenWidth);
		h=float(glView->h>0?glView->h:glScreenHeight);
		
		if(glView->a<=0.0f)
			a=h/w;
		else
			a=glView->a;
		
		r=RAD(glCam->fov*0.5f);
		xp=glCam->znear*tanf(r);
		yp=xp*a;
		r2=atanf(yp/glCam->znear);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glFrustum(-xp, xp, -yp, yp, glCam->znear, glCam->zfar);
	

	
		glMatrixMode(GL_MODELVIEW);
		
		
		glCam->wmatrix=Mcamera(glCam->gettransform());



		glCam->frustrum[0]=PLANE(VECTOR(-cosf(r),0.0f,-sinf(r)),0.0f);						// right x-plane
		glCam->frustrum[1]=PLANE(VECTOR(cosf(r),0.0f,-sinf(r)),0.0f);						// left x-plane
		glCam->frustrum[2]=PLANE(VECTOR(0.0f,-cosf(r2),-sinf(r2)),0.0f);					// top y-plane
		glCam->frustrum[3]=PLANE(VECTOR(0.0f,cosf(r2),-sinf(r2)),0.0f);						// bottom y-plane
		glCam->frustrum[4]=PLANE(VECTOR(0.0f,0.0f,-1.0f),VECTOR(0.0f,0.0f,-glCam->znear));	// near z-plane
		glCam->frustrum[5]=PLANE(VECTOR(0.0f,0.0f,1.0f),VECTOR(0.0f,0.0f,-glCam->zfar));	// far z-plane


		glLoadMatrixf((float*)&glCam->wmatrix);

		glSetViewPort(glView);
	}
}




#define MAX_LIGHTS		8

glLight *_gl_light_list[MAX_LIGHTS]={0,0,0,0,0,0,0,0};


int glAddLight(glLight *l,int i)
{
	if(i==-1)
	{
		for(int i=0;i<MAX_LIGHTS;i++)
			if(_gl_light_list[i]==NULL)	
			{
				_gl_light_list[i]=l;
				return i;
			}

		return -1;
	}
	else
		if(i>=0&&i<MAX_LIGHTS)
		{
			_gl_light_list[i]=l;
			glDisable(GL_LIGHT0+i);
			
			return i;
		}
		else
			return -1;
}


void glAddAllLights(glSceneObject *r)
{
	if(r->type()==GLSO_LIGHT&&glAddLight((glLight*)r)==-1)
		return;

	for(int i=0;i<r->child_count;i++)
		glAddAllLights(r->child[i]);
}


void glApplyLight(int i)
{
	if(i>=0&&i<MAX_LIGHTS&&_gl_light_list[i]) 
	{
		glLight *l=_gl_light_list[i];
		MATRIX m;

		m=l->gettransform();
		if(l->spot)
		{
			glLightfv(GL_LIGHT0+i,GL_POSITION,(float*)&m.p);
			glLightfv(GL_LIGHT0+i,GL_SPOT_DIRECTION,(float*)&-m.y);		// -y is the direction of an object (3dsmax style)
			glLightf(GL_LIGHT0+i,GL_SPOT_EXPONENT,l->spot_exponent);
			glLightf(GL_LIGHT0+i,GL_SPOT_CUTOFF,l->spot_cutoff);
		}
		else
			glLightfv(GL_LIGHT0+i,GL_POSITION,(float*)&m.y); // inverse direction? dunno why it should be like that. but it works alot better ;) *tired*
		
		glLightfv(GL_LIGHT0+i,GL_AMBIENT,(float*)&(glCam->getcolor(l->ambient)*l->multiplier));
		glLightfv(GL_LIGHT0+i,GL_DIFFUSE,(float*)&(glCam->getcolor(l->diffuse)*l->multiplier));
		glLightfv(GL_LIGHT0+i,GL_SPECULAR,(float*)&(glCam->getcolor(l->specular)*l->multiplier));

		glLightf(GL_LIGHT0+i,GL_CONSTANT_ATTENUATION,l->constant_attenuation);
		glLightf(GL_LIGHT0+i,GL_LINEAR_ATTENUATION,l->linear_attenuation);
		glLightf(GL_LIGHT0+i,GL_QUADRATIC_ATTENUATION,l->quadric_attenuation);
		
		if(l->flags&GLSO_ACTIVE)
			glEnable(GL_LIGHT0+i);
		else
			glDisable(GL_LIGHT0+i);

	}
}


void glApplyAllLights()
{
	for(int i=0;i<MAX_LIGHTS;i++)
		glApplyLight(i);
}


void glRemoveLight(int i)
{
	if(i>=0&&i<MAX_LIGHTS)
	{
		_gl_light_list[i]=0;
		glDisable(GL_LIGHT0+i);
	}
}


void glRemoveAllLights()
{
	for(int i=0;i<MAX_LIGHTS;i++)
	{
		_gl_light_list[i]=0;
		glDisable(GL_LIGHT0+i);
	}
}


glLight *glGetLight(int i)
{
	if(i>=0&&i<MAX_LIGHTS)
		return _gl_light_list[i];
	else
		return NULL;
}


int glGetLightID(glLight *l)
{
	for(int i=0;i<MAX_LIGHTS;i++)
		if(_gl_light_list[i]==l)
			return i;

	return -1;
}




VERTEX glShape::getvertex(int i,float p) 
{
	int c,j,k;

	if(i<0||i>=spline_count)
		return 0.0f;
		
	if(p<0.0f)
		p=0.0f;
	else
		if(p>100.0f)
			p=100.0f;

	c=spline[i].vertex_count-(spline[i].closed?0:1);
		
	j=float((p*c)/100.0f);
			
	if(j==c)
		j--;
		
	k=j+1;

	if(k==spline[i].vertex_count)
		k=0;


	p=((p*c)/100.0f)-float(j);

	return	(
			spline[i].vertex[j]*((1.0f-p)*(1.0f-p)*(1.0f-p))+
			spline[i].vertex[k]*(p*p*p)+
			spline[i].vout[j]*(3.0f*p*(1.0f-p)*(1.0f-p))+
			spline[i].vin[k]*(3.0f*p*p*(1.0f-p))
			)*gettransform();		
}

VERTEX glShape::gettangent(int i,float p) 
{
	int c,j,k;

	if(i<0||i>=spline_count)
		return 0.0f;
		
	if(p<0.0f)
		p=0.0f;
	else
		if(p>100.0f)
			p=100.0f;

	c=spline[i].vertex_count-(spline[i].closed?0:1);
		
	j=float((p*c)/100.0f);
			
	if(j==c)
		j--;
		
	k=j+1;

	if(k==spline[i].vertex_count)
		k=0;

	p=((p*c)/100.0f)-float(j);


	return	(
			spline[i].vertex[j]*(-3.0f+6.0f*p-3.0f*p*p)+
			spline[i].vertex[k]*(3.0f*p*p)+
			spline[i].vout[j]*(3.0f-12.0f*p+9.0f*p*p)+
			spline[i].vin[k]*(6.0f*p-9.0f*p*p)
			)*gettransform().scaledrotation();		
}






