#ifndef __PS_HELIX__
#define __PS_HELIX__

#include "glBase.h"
#include "particle.h"

class PS_HELIX {
public:
	VECTOR ha;	// size (x,y,z)
	VECTOR hb;	// riktningsvektor
	//VECTOR normal; // normal
	int varv;	//antal varv i helixen =P
	int density;	// antal partiklar
	
	void Init();
	void Render();
	void Live();
protected:
	PGroup pg;
};

#endif