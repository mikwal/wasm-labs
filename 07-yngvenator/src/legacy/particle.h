#ifndef __particle_h__
#define __particle_h__

/*
	particle.h
	---
	deps: math3d.h
*/

#include "glBase.h"

inline void pPreRend(GLuint texture) {
		glBindTexture(&texture);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE);
		glPushMatrix();
		glLoadIdentity();
		glZBufOff();
		glBlendOn();
	};

inline void pPostRend() {
		glPopMatrix();
	};


class Particle {
public:
	VERTEX pos, posB;
	RGBACOLOR color,colorB;
	VECTOR size, velocity, velocityB, dV; // velocity 1 och 2.. anv�nds i olika system
	float dRandVel;		// velocitydase? alven har gl�mt vad det h�r �r :)
	float dLife;		// flyttal mellan 0 och 1 unikt f�r varje partikel
protected:
	bool tPos, tColor;
};

class PGroup {
public:
	//vars
	int pCount;
	int pMax;
	Particle *list;
	VECTOR gravity;
	VECTOR n;
	unsigned int mode;
	VERTEX g;
	TEXTURE texture;
	float gMod;

	//funktioner
	~PGroup();
	bool Remove(int _n);
	bool Add(	VERTEX *_pos,			// positio
				VERTEX *_posB,			// position B
				RGBACOLOR *_color,		// color
				RGBACOLOR *_colorB,		// color B
				VECTOR *_size,			// storlek
				VECTOR *_velocity
			);
	bool Add();

	void Init(VERTEX *_pos,			// positio
		 VERTEX *_posB,			// position B
		 RGBACOLOR *_color,		// color
		 RGBACOLOR *_colorB,	// color B
		 VECTOR *_gravity,		// gravity
		 VECTOR *_n,			// normal
		 VECTOR *_size,			// storleksfanskap
		 int _pMax				// max parts
		 );
	void Init(int _pMax);

	inline void SetG(VERTEX *_v) { g = *_v; }

	inline bool SetPosv(int _n,		// antal partiklar
						VECTOR *_v) // pekare till vektor med positionsvektorer =P
	{
		if(pCount<1)
			return false;
		for(int i=0; i<_n; i++) {
			list[i].pos = _v[i];
		}
		return true;
	}

	inline bool SetPosBv(int _n,		// antal partiklar
						 VECTOR *_v)	// pekare till vektor med positionsvektorer =P
	{
		if(pCount<1)
			return false;
		for(int i=0; i<_n; i++) {
			list[i].posB = _v[i];
		}
		return true;
	}
	
	inline bool SetColorBv(int _n,		//antal partiklar
							RGBACOLOR *_v)	//pekare till f�rger
	{
		if(pCount<1)
			return false;
		for(int i=0; i<_n; i++) {
			list[i].colorB = _v[i];
		}
		return true;
	}

	void Render();

	#define PG_STARFIELD 0
	#define PG_CIRCLE 1
	
	void Live(int _m);
	
	inline VECTOR _ranVel() {
		return VECTOR(float(rand() % 100)/1000.0f-0.05f, float(rand() % 100)/1000.0f-0.05f, float(rand() % 100)/1000.0f-0.05f);
	}

		// Particle Standard Vars
	VECTOR size;
	VERTEX pos, posB;
	RGBACOLOR color,colorB;


protected:

	inline float _randVel(float a) {
		return randf(a);
	}

	inline float _ranLife() {
		return float(rand() % 100)/100.0f;
	}

};

#endif