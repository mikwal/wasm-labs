#ifndef __tumm1__
#define __tumm1__

#include "glBase.h"
#include "math3d.h"

class TEffect
{
	public:
		void Init(glSceneObject * _obj);
		void Draw();
		void Update();
		void SetTranslate(VECTOR _pos);

		glSceneObject *obj;
		float splinetime, splinetimedir;
		bool active;

	private:


};

#endif