#include "glBase.h"
#include "tummeffekt1.h"

#define RSPEED 0.005f
#define MSPEED 0.005f
#define NUM_TUMM 3
////////////////////////////////

GL_NEW_SCENE(tumm01_Scene);


////////////////////////////////
static TEffect teffect1[NUM_TUMM];

static glCamera cam[2];
static int cam_i=0;
static float time = 0.0f, timedir = SECVAL(2.0f), splinetime = 0.0f, splinetimedir = SECVAL(PI/8);
static int rot = 1;

static glFont font1;
static VECTOR t(0.0f,2.0f,0.0f);
//glRootObject * buh; 
static glSceneObject * c, * buh;

static TEXTURE fsys, texApor, test2;

extern FSOUND_STREAM *bgmusic;
extern glScene Scene04;

void tumm01_Scene_load()
{
	glTextureListGet("fixedsys.tga", &fsys);
	buh = glLoadScene("sce.dat|boxar.sce");
	
	c = buh->getobject("camera01");
	
}


void tumm01_Scene_init()
{
	
	glResetEnviroment();
	glAddAllLights(buh);
	
	for (int count =0;count<NUM_TUMM;count++)
	{
	teffect1[count].Init(buh);
	teffect1[count].active = 0;
	
	}
	teffect1[0].active=1;
	glLightOn();
	cam[0].zfar=1000.0f;
	cam[1].zfar=1000.0f;

	font1.width=3.0f;
	font1.height=4.5f;
	font1.texture=fsys;
	font1.color = (0.0f, 0.0f, 0.0f, 1.0f);
	glView->light = RGBACOLOR(0.5, 0.5, 0.5, 1);



		
}

void tumm01_Scene_free()
{
	
	glFreeTexture(&fsys);
	glFreeTexture(&texApor);
	glFreeTexture(&test2);
}




int tumm01_Scene_update()
{	
	time+=timedir;

	for (int count =0;count<NUM_TUMM;count++)
	{
	teffect1[count].Update();
	}
	c->update(time*2);
	
	if(FSOUND_Stream_GetTime(bgmusic)>1000*272)
	{
		glScene::current=&Scene04;
		glScene::current->init();
		glClear(GL_COLOR_BUFFER_BIT);
		glSwapBuffer();
		glClear(GL_COLOR_BUFFER_BIT);
	}

	return 0;
}





void tumm01_Scene_render()
{

	glResetEnviroment();
	glBlendOn();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glOrthoViewOn();
	font1.color = RGBACOLOR(0.2f, 1.0f, 0.2f, 1.0f);
	font1.printf(VECTOR(10.0f, 10.0f, 0.0f), "%f", (50+sin(teffect1[0].splinetime - (2*PI/4))*50.0f));
	glOrthoViewOff();

	glView->color.a = 0.2f;
	glView->color.r = 1.0f;
	glView->color.g = 1.0f;
	glView->color.b = 1.0f;
	glView->clear();
	glSetCamera(c);
	glApplyAllLights();
	
	
	glLineSmoothOff();
	glZBufOff();
	glBlendOn();
		teffect1[0].Draw();
		teffect1[1].SetTranslate(VECTOR(-40.0f, 50.0f, 400.0f));
		teffect1[1].Draw();
	
	
	glSwapBuffer();
}

