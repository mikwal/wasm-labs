#include "wall.h"

void Wall::Init(VECTOR *_origin, VECTOR *_p1, VECTOR *_p2, float _height, float _lenght)
{
	this->pO = *_origin;
	this->p1 = *_p1;
	this->p2 = *_p2;
	this->p1 /= this->p1.length();
	this->p2 /= this->p2.length();
	
	this->height = _height;
	this->lenght = _lenght;
	
	
	

}

void Wall::Update(void)
{
	//Hitta normal fr�n hj�lpvektorer
	this->pN = this->p1.cross(this->p2); 
	this->pN /= this->pN.length();
	this->p1 /= this->p1.length();
	this->p2 /= this->p2.length();;
	
	//Hitta en punkt i planet
	this->pP.x = (pO.x + 5 * this->p1.x + 5*this->p2.x);
	this->pP.y = (pO.y + 5 * this->p1.y + 5*this->p2.y);
	this->pP.z = (pO.z + 5 * this->p1.z + 5*this->p2.z);

	//S�tt in den i en "vanlig" ekvation (F�r collissiondase)
	this->plane.A = this->pN.x;
	this->plane.B = this->pN.y;
	this->plane.C = this->pN.z;
	this->plane.D = -(this->pP.x + this->pP.y + this->pP.z);
	this->plane.n = this->pN;
	
}

void Wall::Draw()
{
	//glPushMatrix();
	glTranslatef(this->pO.x, this->pO.y, this->pO.z);
	glBegin(GL_QUADS);
		//tempor�r fulkod:
		//glColor4f(0.9f, 0.9f, 0.9f, 1.0f);

		//Hitta punkterna
		/*
		this->a1 = this->p1 * this->lenght;
		this->a2 = (this->p1 * this->lenght) + (this->p2 * this->height);
		this->a3 = this->p2 * this->lenght;
		*/
		this->a1 = this->p1 * this->lenght;
		this->a2 = (this->p1 * this->lenght) + (this->p2 * this->height);
		this->a3 = this->p2 * this->lenght;
		/*
		glNormal3f(this->plane.n.x, this->plane.n.y, this->plane.n.z);				// Normal Pointing Towards Viewer
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0	   , 0.0	   ,		0.0);	// Point 1 (Front)
		glTexCoord2f(1.0f, 0.0f); glVertex3f(this->a1.x, this->a1.y, this->a1.z);	// Point 2 (Front)
		glTexCoord2f(1.0f, 1.0f); glVertex3f(this->a2.x, this->a2.y, this->a2.z);	// Point 3 (Front)
		glTexCoord2f(0.0f, 1.0f); glVertex3f(this->a3.x, this->a3.y, this->a3.z);	// Point 4 (Front)
		*/
		glNormal3f(this->plane.n.x, this->plane.n.y, this->plane.n.z);				// Normal Pointing Towards Viewer
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0	   , 0.0	   ,		0.0);	// Point 1 (Front)
		glTexCoord2f(1.0f, 0.0f); glVertex3f(this->a1.x, this->a1.y, this->a1.z);	// Point 2 (Front)
		glTexCoord2f(1.0f, 1.0f); glVertex3f(this->a2.x, this->a2.y, this->a2.z);	// Point 3 (Front)
		glTexCoord2f(0.0f, 1.0f); glVertex3f(this->a3.x, this->a3.y, this->a3.z);	// Point 4 (Front)

	glEnd();
	//glPopMatrix();
}


