#include "glBase.h"

#include "liquidsurface.h"
#include "ScreenFilter.h"
#include "fmodFileCallbacks.h"

#include "fmodSpectrum.h"



float time=0;
glRootObject *test03_sce;
glCamera *cam;
LSurface *surf;

static glFont fsys;

static ScreenFilter sf,noise;


static float a;


extern glScene tumm02_Scene;

extern FSOUND_STREAM *bgmusic;

void test03_load()
{
	test03_sce=glLoadScene("sce.dat|test03.sce");
	
	surf=new LSurface("yngve_bg.bmp",40,40,1,0.95f,0);

	noise.init(SF_NOISE,GL_LUMINANCE,128);
	noise.scale=2.0f;
	noise.color.a=0.25f;
	noise.speed=1.0f;

	sf.init(SF_VLINES,GL_ALPHA,256);
	sf.color=RGBACOLOR(1.0,1.0,1.0,0.25);
	sf.min=0.5f;
	sf.max=1.0f;
	sf.scale=3.0f;
	sf.speed=2.0f;


	fmodSpectrumOn();
	
}


void test03_free()
{
	sf.free();
	noise.free();
	delete test03_sce;
	delete surf;

}



void test03_init()
{
	glResetEnviroment();
	glAddAllLights(test03_sce);

	cam=(glCamera*)test03_sce->getobject("Camera05");
	surf->matrix=test03_sce->getobject("Point01")->matrix;

	fsys=glFont("fixedsys.tga",4,4);

	glView->setbackground("yngve.tga");

	fmodSetPeakCheckerFreq(20,200);

}



int test03_update()
{
	time+=SECVAL(10);
	
	if(time>100)
	{
		cam=(glCamera*)test03_sce->getobject("Camera03");
		time=0.0f;
	}

	test03_sce->update(time);



	time+=SECVAL(5.0f);


	int x,y,l;

	y=((surf->h*surf->lod)>>1);
	l=(surf->w*surf->lod);
	x=((surf->w*surf->lod)>>1);
			
			

	a=fmodUpdatePeakChecker();

	if(a>0.0f)
		surf->map[y*l+x]=-a;



	if(a>fmodMaxPeak*0.5f)
	{
		cam->target_roll+=rand2f(PI/6.0f)*a;
		cam->fov+=rand2f(a*0.25f);
	}
	else
		a=0.0f;



	noise.update();
	sf.update();
	
	surf->update(0);

	if(FSOUND_Stream_GetTime(bgmusic)>1000*216)
	{
		glScene::current=&tumm02_Scene;
		glScene::current->init();
		glClear(GL_COLOR_BUFFER_BIT);
		glSwapBuffer();
		glClear(GL_COLOR_BUFFER_BIT);
	}

	return 0;
}


void test03_render()
{
	glView->x=400-256;
	glView->y=300-256;
	glView->w=512;
	glView->h=512;
	glView->color=RGBACOLOR(0.55f,0.55f,1.0f,0.25f);
	glView->fog_mode=GL_EXP;
	glView->fog_dens=0.0025f;
	glSetViewPort(glView);

	glView->clear();
	glSetCamera(cam);

	glApplyAllLights();
	glLightOn();
	
	test03_sce->render(0);
	surf->render(0);


	
	glSwapBuffer();
}