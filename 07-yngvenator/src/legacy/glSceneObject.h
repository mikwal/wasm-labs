#ifndef GL_SCENEOBJECT
#define GL_SCENEOBJECT



#define GLM_TWOSIDED		0x01
#define GLM_WIREFRAME		0x02
#define GLM_FACETED			0x04
#define GLM_TRANSLUCENT		0x08




struct MATERIAL {
	int flags;
	RGBACOLOR ambient;
	RGBACOLOR diffuse;
	RGBACOLOR specular;
	RGBACOLOR emission;
	float shininess;
	TEXTURE texture_rgb;
	TEXTURE texture_luminance;
	int mapping;
};

extern MATERIAL glDefaultMat;
extern MATERIAL *glMat;
void glMaterial(MATERIAL *m);

struct FACE {
	int vindex[3];
	TEXCOORD texcoord[3];
	VECTOR fnormal;
};


struct MESH {
	int face_count;
	FACE *face;
	int vertex_count;
	VERTEX *vertex;
	VECTOR *vnormal;

	inline MESH() {face_count=vertex_count=0;face=NULL;vertex=vnormal=NULL;}
	inline MESH(int _face_count,int _vertex_count) {face_count=_face_count; vertex_count=_vertex_count; face=new FACE[_face_count]; vertex=new VERTEX[_vertex_count]; vnormal= new VECTOR[_vertex_count];}
	inline ~MESH() {delete [] face; delete [] vertex; delete [] vnormal;}
};

void glMesh(MESH *m);


struct BONE {
	class glSceneObject *matrixowner;
	MATRIX restmatrix;
	MATRIX tempmatrix;
};


struct SKIN : MESH {
	int bone_count;
	BONE *bone;
	MATRIX restmatrix;
	int weight_count_max;
	int *weight_count;
	float *weight_value;
	int *bone_index;
		

	inline SKIN() : MESH() {bone_count=0; bone=NULL; weight_count_max=0; weight_count=NULL; weight_value=NULL; bone_index=NULL;}
	inline SKIN(int _face_count,int _vertex_count,int _bone_count,int _weight_count_max) : MESH(_face_count,_vertex_count) {bone_count=_bone_count; bone=new BONE[_bone_count]; weight_count_max=_weight_count_max; weight_count=new int[vertex_count]; weight_value=new float[vertex_count*_weight_count_max]; bone_index=new int[vertex_count*_weight_count_max];}
	inline ~SKIN() {delete [] bone; delete [] weight_count; delete [] weight_value; delete [] bone_index;}
};

void glSkin(SKIN *s,MESH *m);


 

struct SPLINE {
	int vertex_count;
	VERTEX *vertex;
	VECTOR *vin;
	VECTOR *vout;
	int closed;

	inline SPLINE() {vertex_count=0; vertex=vin=vout=NULL; closed=0;}
	inline SPLINE(int _vertex_count) {vertex_count=_vertex_count; vertex=new VERTEX[_vertex_count]; vin=new VECTOR[_vertex_count]; vout=new VECTOR[_vertex_count]; closed=0;}
	inline ~SPLINE() {delete [] vertex; delete [] vin; delete [] vout;}
};




struct SCALARKEY {
	float time;
	float value;
	float tin;
	float tout;
	int smooth;
};

struct VECTORKEY {
	float time;
	VECTOR value;
	VECTOR tin;
	VECTOR tout;
	int smooth;
};

struct ROTATIONKEY {
	float time;
	VECTOR value;
	float angle;
};


struct CONTROLLERVALUE {
	float scalar;
	VECTOR vector;
	MATRIX rotation;
};

class Controller {
public:
	CONTROLLERVALUE value;
	
	inline Controller() {value.scalar=0.0f;value.vector=0.0f;value.rotation=Midentity;}
	inline virtual ~Controller() {}
	inline virtual void update(float time) {}
};

class ScalarController : public Controller {
public:	
	int key_count;
	SCALARKEY *key;

	inline ScalarController(int _key_count) : Controller() {key_count=_key_count;key=new SCALARKEY[_key_count];}
	inline ~ScalarController() {delete [] key;}
	void update(float time);
};

class VectorController : public Controller {
public:	
	int key_count;
	VECTORKEY *key;

	inline VectorController(int _key_count) : Controller() {key_count=_key_count;key=new VECTORKEY[_key_count];}
	inline ~VectorController() {delete [] key;}
	void update(float time);
};

class RotationController : public Controller {
public:	
	int key_count;
	ROTATIONKEY *key;
	
	inline RotationController(int _key_count) : Controller() {key_count=_key_count;key=new ROTATIONKEY[_key_count];}
	inline ~RotationController() {delete [] key;}
	void update(float time);
};










#define GLSO_DUMMY			0
#define GLSO_ROOTOBJECT		1
#define GLSO_MESHOBJECT		2
#define GLSO_CAMERA			3
#define GLSO_LIGHT			4
#define GLSO_SHAPE			5
#define GLSO_SPHEREGIZMO	6
#define GLSO_BOXGIZMO		7
#define GLSO_CYLINDERGIZMO	8


#define	GLSO_ACTIVE				0x0001	// object and its childrens are active, have different or no meaning on different object types
#define	GLSO_VISIBLE			0x0002	// object and its childrens are visible, ie. are handled by a call to render
#define GLSO_USE_CONTROLLERS	0x0004	// object uses its controllers to update matrix


#define GLRF_KEEP_SKIN			0x0001	// dont update skin before rendering, just to optimize multiple rendering of skinned meshobjects



#define	OBJECTNAME_SIZE			24



class glSceneObject {
public:
	int ID;							// unique node ID in hierachy
	char name[OBJECTNAME_SIZE+1];	// object name, should always be padded by zeros
	int flags;						// object flags
	MATRIX matrix;					// relative transform matrix
	MATRIX offset;					// transform offset

	Controller *position;		// position animation keyframes
	Controller *rotation;		// rotation animation keyframes
	Controller *scale;			// scaling animation keyframes

	int child_count;			// number of children
	glSceneObject **child;		// array of pointers to childrens
	glSceneObject *parent;		// pointer to objects parent
	
	glSceneObject *path;		// pointer to path
	float path_pos;				// position on path
	int path_axis;				// path direction axis type	(0 = x, 1 = -x, 2 = y, 3 = -y, 4 = z, 5 = - z , -1 = no axis)
	
	glSceneObject *target;		// pointer to target
	float target_roll;			// roll around target direction


	glSceneObject();
	virtual ~glSceneObject();

	inline virtual int type() {return GLSO_DUMMY;}

	MATRIX getmatrix();		// get absolute matrix
	inline MATRIX gettransform() {return getmatrix()*offset;}	// get absolute transform

	glSceneObject *getobject(int i);
	glSceneObject *getobject(const char *name);

	virtual void update(float time);
	virtual void render(int pass,int renderflags=0);	// render object, pass 0 == all normal objects, pass 1 == all translucent objects
};




class glRootObject : public glSceneObject  {
public:	
	int material_count;
	MATERIAL *material;
	int mesh_count;
	MESH *mesh;
	
	glRootObject() : glSceneObject() {material_count=0; material=NULL; mesh_count=0; mesh=NULL;}
	glRootObject(int _material_count,MATERIAL *_material,int _mesh_count,MESH *_mesh) : glSceneObject() {material_count=_material_count; material=_material; mesh_count=_mesh_count; mesh=_mesh;}
	~glRootObject() {delete [] material; delete [] mesh;}

	int type() {return GLSO_ROOTOBJECT;}
};

glRootObject *glLoadScene(const char *filename);



class glMeshObject : public glSceneObject  {
public:
	MATERIAL *material;
	MESH *mesh;
	SKIN *skin;
	CLIPBOX clip;
	
	inline glMeshObject() : glSceneObject() {material=NULL;mesh=NULL;skin=NULL; clip=CLIPBOX(0.0f,0.0f);}
	inline glMeshObject(MATERIAL *_material,MESH *_mesh,SKIN *_skin) : glSceneObject() {material=_material;mesh=_mesh;skin=_skin; clip=CLIPBOX(0.0f,0.0f);}
	inline ~glMeshObject() {delete skin;}

	int type() {return GLSO_MESHOBJECT;}

	void setclip();
	void render(int pass,int renderflags=0);
};
			



class glShape : public glSceneObject  {
public:
	int spline_count;
	SPLINE *spline;

	inline glShape() : glSceneObject() {spline_count=0;spline=NULL;}
	inline glShape(int _spline_count,SPLINE *_spline) : glSceneObject() {spline_count=_spline_count;spline=_spline;}
	inline ~glShape() {delete [] spline;}

	int type() {return GLSO_SHAPE;}

	VERTEX getvertex(int i,float p); 
	VERTEX gettangent(int i,float p);
};



class glLight : public glSceneObject {
public:
	float multiplier;

	// rgba values
	RGBACOLOR ambient;
	RGBACOLOR diffuse;
	RGBACOLOR specular;

	int spot;				// is spotlight
	float spot_cutoff;		// spotlight spread angle
	float spot_exponent;	// spotlight hotspot	

	// attenuation values
	float constant_attenuation;
	float linear_attenuation;
	float quadric_attenuation;

	inline glLight() : glSceneObject() {multiplier=1.0f;ambient=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);diffuse=RGBACOLOR(0.8f,0.8f,0.8f,1.0f);specular=RGBACOLOR(0.2f,0.2f,0.2f,1.0f);spot=0;spot_cutoff=180.0f;spot_exponent=0.0f;constant_attenuation=1.0f;linear_attenuation=0.0f;quadric_attenuation=0.0f;}
	inline glLight(float _multiplier,const RGBACOLOR &_ambient,const RGBACOLOR &_diffuse,const RGBACOLOR &_specular,int _spot,float _spot_cutoff,float _spot_exponent,float _constant_attenuation,float _linear_attenuation,float _quadric_attenuation) : glSceneObject() {multiplier=_multiplier;ambient=_ambient;diffuse=_diffuse;specular=_specular;spot=_spot;spot_cutoff=_spot_cutoff;spot_exponent=_spot_exponent;constant_attenuation=_constant_attenuation;linear_attenuation=_linear_attenuation;quadric_attenuation=_quadric_attenuation;}
	
	int type() {return GLSO_LIGHT;}
};



// there is a internal list of GL_MAX_LIGHT lights in gl
int glAddLight(glLight *l,int i=-1);		// add light l to list, use i to force an light to the list on the i�th position
void glAddAllLights(glSceneObject *r);		// add all lights in r to list
void glApplyLight(int i);					// apply all parameters for the i�th light in the list
void glApplyAllLights();					// apply all parameters for all lights in list
void glRemoveLight(int i);					// remove i�th light from the list
void glRemoveAllLights();					// remove all lights from the list
glLight *glGetLight(int i);					// get light from index
int glGetLightID(glLight *l);				// get light index in the list
inline void glApplyLight(glLight *l) {glApplyLight(glGetLightID(l));}
inline void glRemoveLight(glLight *l) {glRemoveLight(glGetLightID(l));}




class glCamera : public glSceneObject {
public:
	float fov;		// field of view
	float znear;	// near clipping plane
	float zfar;		// far clipping plane

	int color_conversion;	// type of color conversion, 0 or GL_LUMINANCE
	RGBCOLOR color_bias;	// rgb base
	RGBCOLOR color_scale;	// rgb scale

	// world matrix and frustum are precalculated at glSetCamera (to optimize to-world-coordinates transformation)
	MATRIX wmatrix;
	FRUSTRUM frustrum;

	inline glCamera() : glSceneObject() {fov=45.0f;znear=0.25f;zfar=500.0f;color_conversion=0;color_bias=0.0f;color_scale=1.0f;}
	inline glCamera(float _fov,float _znear,float _zfar) : glSceneObject() {fov=_fov;znear=_znear;zfar=_zfar;color_conversion=0;color_bias=0.0f;color_scale=1.0f;}

	int type() {return GLSO_CAMERA;}

	// get relative color values from the camera
	inline RGBCOLOR getcolor(const RGBCOLOR &c) {if(color_conversion==GL_LUMINANCE) {float l=c.r*0.299f+c.g*0.587f+c.b*0.114f; return RGBCOLOR(color_bias.r+l*color_scale.r,color_bias.g+l*color_scale.g,color_bias.b+l*color_scale.b);} else return RGBCOLOR(color_bias.r+c.r*color_scale.r,color_bias.g+c.g*color_scale.g,color_bias.b+c.b*color_scale.b);}
	inline RGBACOLOR getcolor(const RGBACOLOR &c) {if(color_conversion==GL_LUMINANCE){float l=c.r*0.299f+c.g*0.587f+c.b*0.114f;return RGBACOLOR(color_bias.r+l*color_scale.r,color_bias.g+l*color_scale.g,color_bias.b+l*color_scale.b,c.a);}else return RGBACOLOR(color_bias.r+c.r*color_scale.r,color_bias.g+c.g*color_scale.g,color_bias.b+c.b*color_scale.b,c.a);}	
};

extern glCamera glDefaultCam;				// default camera
extern glCamera *glCam;						// pointer to current camera

// set camera as current and loads the camera matrix into gl (also reloads the current viewport into gl)
void glSetCamera(glSceneObject *c); 

// helpers for setting the current color relative to the current camera
inline void glColor(const RGBCOLOR &c) {glColor3fv((float*)&glCam->getcolor(c));}
inline void glColor(const RGBACOLOR &c) {glColor4fv((float*)&glCam->getcolor(c));}
inline void glColor(float r,float g,float b) {glColor3fv((float*)&glCam->getcolor(RGBCOLOR(r,g,b)));}
inline void glColor(float r,float g,float b,float a) {glColor4fv((float*)&glCam->getcolor(RGBACOLOR(r,g,b,a)));}







class glBoxGizmo : public glSceneObject {
public:
	float length;
	float width;
	float height;
	unsigned int seed;

	inline glBoxGizmo() : glSceneObject() {length=width=height=1.0f;seed=0;}
	inline glBoxGizmo(float _length,float _width,float _height,unsigned int _seed) : glSceneObject() {length=_length;width=_width;height=_height;seed=_seed;}
	
	int type() {return GLSO_BOXGIZMO;}
};


class glSphereGizmo : public glSceneObject {
public:	
	float radius;
	unsigned int seed;

	inline glSphereGizmo() : glSceneObject() {radius=1.0f;seed=0;}
	inline glSphereGizmo(float _radius,unsigned int _seed) : glSceneObject() {radius=_radius;seed=_seed;}

	
	int type() {return GLSO_SPHEREGIZMO;}
};


class glCylinderGizmo : public glSceneObject {
public:
	float radius;
	float height;
	unsigned int seed;

	inline glCylinderGizmo() : glSceneObject() {radius=height=1.0f;seed=0;}
	inline glCylinderGizmo(float _radius,float _height,unsigned int _seed) : glSceneObject() {radius=_radius;height=_height;seed=_seed;}
	int type() {return GLSO_CYLINDERGIZMO;}
};






#endif
