#include "bezier.h"
#include "glFont.h"

glFont bez;

VERTEX BEZIER::_GenPoint(float t, VECTOR *P) {
	//	P1*t^3 + P2*3*t^2*(1-t) + P3*3*t*(1-t)^2 + P4*(1-t)^3 = Pnew 
	VERTEX a,b,c,d;
	a = P[0] * (powf(t,3));
	b = P[1] * (3*powf(t,2)*(1-t));
	c = P[2] * (3*t*powf((1-t),2));
	d = P[3] * (powf((1-t),3));
	return (a+b+c+d);
}

BEZIER::~BEZIER() {
	delete [] list;
}

void BEZIER::Init(){
	bez.setsize(1.0f,1.0f);
	pw = (w+1);
	ph = h+1;
	list = new VERTEX [pw*ph];
	tx = 1.0f/float(w);
	ty = 1.0f/float(h);
	p[0] = VERTEX(0,0,0);
	p[1] = VERTEX(2,-2,0);
	p[2] = VERTEX(4,3,0);
	p[3] = VERTEX(6,0,0);

	p[0] = VERTEX(0,0,0);
	p[1] = VERTEX(0,0,0);
	p[2] = VERTEX(0,0,0);
	p[3] = VERTEX(0,0,0);

}

void BEZIER::Generate() {
	int A;
	for(int dy=0;dy<ph;dy++) {
		for(int dx=0;dx<pw;dx++) {
			A = pw*dy+dx;
			list[A] = pos + VECTOR(size.x/pw*dx,size.y/ph*dy,0);
		}
	}
}

void BEZIER::GenPatch(VECTOR *P, int dase) {
	int A;
	float u=0;
	for(int dy=0;dy<h;dy++) {
		for(int dx=0;dx<w;dx++) {
			A = w*dy+dx;
			if(dase) {
				u = float(dx/float(w));
			} else {
				u = float(dy/float(h));
			}
			list[A] += _GenPoint(u, P) * Mscale(0,0,1);	// generera punkter
		}
	}
}

void BEZIER::Render() {
	//glPolygonMode(GL_FRONT,GL_LINE);
	int dy,a,A;
	float s,t;
	glBindTexture(&texture);
	glColor(1,1,1,1);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glCullFaceOff();
	glSetTextureWrap(GL_CLAMP);
	glBlendOn();
	glZBufMask(0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_AUTO_NORMAL);
	for(dy=0;dy<h;dy++) {
		a = dy*ph;
		glBegin(GL_TRIANGLE_STRIP);
		//glBegin(GL_POINTS);
		for(int dx=0;dx<w;dx+=2) {
			A = a+dx;

			s = float(dx)/float(w);
			t = float(dy)/float(h);
	
			// startpunkt
			glTexCoord2f(s,t); 
			glVertex3f(list[A].x, list[A].y, list[A].z);	// 0,0
			
			A += pw;// ovanf�r
			glTexCoord2f(s,t+ty); 
			glVertex3f(list[A].x, list[A].y, list[A].z);	// 0,1
			
		}

		A -= pw-1; // ner sen till h�ger
		glTexCoord2f(1,t); 
		glVertex3f(list[A].x, list[A].y, list[A].z);	// 0,0
			
		A += pw;	// ovanf�r
		glTexCoord2f(1,t+ty); 
		glVertex3f(list[A].x, list[A].y, list[A].z);	// 0,1

		glEnd();
	}


#ifdef DEBUG
	for(dy=0;dy<h;dy++) {
		a = dy*h;
		
		//glBegin(GL_POINTS);
		for(int dx=0;dx<w;dx++) {
			A = a+dx;
			// startpunkt
			bez.printf(list[A], "%i", A);			
			A += w;	// ovanf�r
			bez.printf(list[A], "%i", A);
			A -= w-1; // ner sen till h�ger
			bez.printf(list[A], "%i", A);
			A += w; // ovanf�r
			bez.printf(list[A], "%i", A);
		}
	}
#endif
	glDisable(GL_AUTO_NORMAL);
	glZBufMask(1);	
	
}

float _flagga() {
	static float a;
	a+=0.01f;
	return sinf(a);
}


void BEZIER::Live() {
	float a;

	switch (mode) {
	case BEZIER_STILL:
		a = 0;
		break;
	case BEZIER_FLAGGA:
		a = _flagga();
		break;
	}
	p[0] = VECTOR(0,0,0);
	p[1] = VECTOR(2,0,-0.7f*sinf(a));
	p[2] = VECTOR(4,0,0.4f*sinf(a));
	p[3] = VECTOR(6,0,0);

	q[0] = VECTOR(0,0,0);
	q[1] = VECTOR(0,2,2*sinf(a));
	q[2] = VECTOR(0,6,-3*sinf(a));
	q[3] = VECTOR(0,10,0);
	
	Generate();
	GenPatch(p,0);
	GenPatch(q,1);
}

