#ifndef __particle_flare_h__
#define __particle_flare_h__

#include "glBase.h"
#include "particle.h"

class Flare : public Particle {
public:
	GLuint texture;
	Flare();
	~Flare();
	void Init(VECTOR *_pos, VECTOR *_size, RGBACOLOR *_basecolor, float _speed);
	void Live(float f);
	void Render();

	inline void SetTexture(TEXTURE _texture) {texture = _texture;}
	inline float GetAmount() {return amount;}
	inline TEXTURE *GetTexture() {return &texture;}
private:
	float speed;
	float amount;
	RGBACOLOR basecolor;
};


#endif