#include "glBase.h"

#include "Tunnel.h"
#include "textlines.h"

static glViewPort vw;

static Tunnel *tu;
static float t=0;

extern glScene test03;
extern TEXTURE monitor;
extern int use_monitor;
signed char tunnel_sync(FSOUND_STREAM *stream, void *buff, int len, int param);
extern FSOUND_STREAM *bgmusic;

extern glScene test03;

#define N_POET 3

static textline tl[N_POET];


void tunnel_scene_load()
{
	tu = new Tunnel("A149.bmp",80,30);
	for(int i=0;i<N_POET;i++) {
		glTextureListGet("fontPres.tga", &tl[i].texture0);
	}
}

void tunnel_scene_free()
{
	delete tu;
}

void tunnel_scene_init()
{
	FSOUND_Stream_SetSynchCallback(bgmusic, tunnel_sync, 0);

	tl[0].Init("IT'S NOT WHAT YOU THINK");
	tl[0].font.setsize(5,5);
	tl[0].pos = VECTOR(30,70,0);
	tl[0].valign0 = GLF_LEFT;

	tl[1].Init("HIDDEN MESSAGE?");
	tl[1].font.setsize(5,5);
	tl[1].pos = VECTOR(30,64,0);
	tl[1].valign0 = GLF_LEFT;
	
	tl[2].Init("HE FEELS YOU");
	tl[2].font.setsize(5,5);
	tl[2].pos = VECTOR(30,59,0);
	tl[2].valign0 = GLF_LEFT;

}


int tunnel_scene_update()
{
	t+=SECVAL(5.0f);
	tu->update(t);
	
	if(use_monitor)
	{
		vw.w=64;
		vw.h=64;
		vw.color.a=1;
	}
	else
	{
		vw.w=800;
		vw.h=600;
		if(vw.color.a>0.0f)
			vw.color.a-=SECVAL(0.5f);
	}

	glDefaultCam.matrix=Mtarget(VECTOR(0.0f,0.0f,-1.0f),VECTOR(0.0f,0.0f,1.0f));
	glDefaultCam.zfar=1000.0f;


	if(FSOUND_Stream_GetTime(bgmusic)>100*1383) {
		tl[0].active = 1;
	}
	if(FSOUND_Stream_GetTime(bgmusic)>100*1679) {
		tl[1].active = 1;
	}
	if(FSOUND_Stream_GetTime(bgmusic)>100*(1200+573)) {
		tl[2].active = 1;
	}

	for(int i=0;i<N_POET;i++) {
		if(tl[i].active) {
			tl[i].Update();
		}
	}

	if(FSOUND_Stream_GetTime(bgmusic)>1000*180)
	{
		glScene::current=&test03;
		glScene::current->init();
		glClear(GL_COLOR_BUFFER_BIT);
		glSwapBuffer();
		glClear(GL_COLOR_BUFFER_BIT);
	}

	return 0;
}


void tunnel_scene_render()
{

	glSetCamera(&glDefaultCam);
	glSetViewPort(&vw);
	
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	vw.clear();
	tu->render(0);

		
	if(use_monitor)
	{
		glBindTexture(&monitor);
		glBltBufferToTexture(0,0,0,0,64,64);
		vw.clear();
		glSetViewPort(&glDefaultView);
	}
	else
	{	
		glBlendOn();
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glOrthoViewOn();
		glBindTexture("frame.tga");
		glBltTexture(0,0,1,1,0,0,100,100);
		glColor(1,1,1,0.4+randf(0.1));
		glBindTexture("yngve_bg.bmp");
		glBltTexture(0,0,1,1,0,0,100,100);
		
		glBlendOn();
		glBlendFunc(GL_ONE, GL_ZERO);
		for(int i=0;i<N_POET;i++) {
			if(tl[i].active) {
				tl[i].Render();
			}
		}	
		
		glOrthoViewOff();
		glSwapBuffer();
		glBlendOff();
	}

}


signed char tunnel_sync(FSOUND_STREAM *stream, void *buff, int len, int param) {
	if(buff) {
		if(!strcmp((const char *) buff, (const char *) "s4start"))
			glScene::current = &test03;
			glScene::current->init();

			return 0;
	}
				return 0;
}