#include "psystems.h"



void FireFlies::Init(VERTEX *_pos,
					 float _domain,		// size of area where the flies are :) maxdistans fr�n pos
					 VECTOR *_size,			// size of flies
					 RGBACOLOR *_color,		// color (pulsates by adding color.a with sin(x)/2 or whatever
					 int n_flies) {

	pos = *_pos;
	posB = 0;
	color = *_color;
	colorB = 0;
	gravity = 0;	// no gravity
	n = 0;	// no lightning (no normal needed)
	size = *_size;
	pMax = n_flies;
	domain = _domain;
	pCount = 3;
	time = 0;	// b�rja fr�n b�rjan =D Anv�nds till sinuskurvan
	

	//fixa minne f�r pMax partiklar
	list = new Particle [pMax];
	for(int i=0;i<n_flies;i++) {
		Add();
	}

}

void FireFlies::Update() {
	for(int i=0;i<pMax;i++) {
		list[i].color.a = 1-0.6f*sinf(time+list[i].dRandVel);
		VECTOR newpos = list[i].pos;

		newpos += list[i].velocity;
		list[i].pos = newpos;
		
		list[i].velocity *= sinf(time);
	}
	time+=0.01f;	// hur fort ska flugdasarna blinka?
}


void Rain::Init(VERTEX *_pos,
				VECTOR *_size,
				VECTOR *_cloud,			// VECTOR som b�rjar p� pos och best�mmer omr�det d�r regnet kan falla
				RGBACOLOR *_color,
				VECTOR *_grav,
				PLANE *_ground,
				int density)
{
	pos = *_pos;
	posB = *_pos;
	color = *_color;
	colorB = 0;
	gravity = *_grav;	// no gravity
	n = 0;	// no lightning (no normal needed)
	size = *_size;
	pMax = density;
	ground = *_ground;
	cloud = *_cloud;
	
	//fixa minne f�r pMax partiklar
	list = new Particle [pMax];
	p0 = new VECTOR [pMax];
	for(int i=0;i<density;i++) {
		// randomize particle starting positions and save in p0 array

		p0[i].x = float(rand() % 100)/100.0f * cloud.x;
		p0[i].y = float(rand() % 100)/100.0f * cloud.y;
		p0[i].z = float(rand() % 100)/100.0f * cloud.z;

		p0[i] += pos;

		Add();
	}
}

Rain::~Rain() {
	delete [] p0;
}

void Rain::Update() {
	/*
		Flytta partikel och checka om den skurit sig med ground-planet
	*/
	float t;
	for(int i=0; i<pMax ;i++) {
		list[i].posB += gravity * (16 * (1+list[i].dLife));
		t = ground.intersect(RAY(list[i].pos >> list[i].posB));	
		size.y = (1 + list[i].velocity.y) * 1.5f;
		if(t<0) {
			// reset particle
			list[i].pos = list[i].posB = p0[i];
		} else {
			list[i].pos = list[i].posB;
		}
	}
}