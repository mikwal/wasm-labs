#ifndef FMODSPECTRUM_H
#define FMODSPECTRUM_H

inline void fmodSpectrumOn() {FSOUND_DSP_SetActive(FSOUND_DSP_GetFFTUnit(),1);}
inline void fmodSpectrumOff() {FSOUND_DSP_SetActive(FSOUND_DSP_GetFFTUnit(),0);}

float fmodGetSpectrumAmplitude(float start_freq,float end_freq);


extern float fmodMaxPeak;
void fmodSetPeakCheckerFreq(float start_freq,float end_freq);
float fmodUpdatePeakChecker();



#endif


