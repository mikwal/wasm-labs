#ifndef GL_SCENE_H
#define GL_SCENE_H


#define GL_MAX_SCENES	100


class glScene {
public:
	glScene(void (*loadproc)(),void (*freeproc)(),void (*initproc)(),int (*updateproc)(),void (*renderproc)());
	static int list_count;
	static glScene *list[GL_MAX_SCENES];
	static glScene *first,*current;

	void (*load)();
	void (*free)();
	void (*init)();
	int (*update)();
	void (*render)();
};

#define GL_NEW_SCENE(name)	\
void name##_load();			\
void name##_free();			\
void name##_init();			\
int name##_update();		\
void name##_render();		\
glScene name(name##_load, name##_free, name##_init, name##_update, name##_render); \


#define GL_FIRST_SCENE(name)		\
glScene *glScene::first=&name;	\
glScene *glScene::current=&name;	\

#endif