#include "glBase.h"


//GL_NEW_SCENE(test02);
//GL_FIRST_SCENE(test02);


extern glScene test01;


extern glRootObject *sce;


SPHERE boll[11];
VECTOR boll_d[11],boll_p[11];
RGBCOLOR boll_c[11];


void test02_load()
{


}


void test02_free()
{

}


void test02_init()
{
	glResetEnviroment();
	for(int i=0;i<10;i++)
	{
		boll[i]=SPHERE(100,i*10+5,43.0,3);
		boll_d[i]=VECTOR(-SECVAL(200)/i,0,0);
		boll_c[i]=1.0f;
		boll_p[i]=0.0f;
	}

		boll[10]=SPHERE(10,90,0,10);
		boll_d[10]=VECTOR(SECVAL(20),SECVAL(-15),0);
		boll_c[10]=1.0f;
		boll_p[10]=0.0f;
}


int test02_update()
{
	
	glRemoveAllLights();
	glAddAllLights(sce);
	sce->update(0);
	for(int i=0;i<11;i++)
	{
		boll[i].pos+=boll_d[i];
		boll_p[i]+=boll_d[i];
	}

	if(glKey['C'])
	{
		glScene::current=&test01;
		glScene::current->init();
	}

	return 0;
}




void render2Dsphere(SPHERE &s)
{
	int i;
	
	glBegin(GL_LINE_STRIP);
	for(i=-30;i<30;i++)
	{
		float x,y;

		x=(float(i)/30.0f)*s.radius+s.pos.x;
		y=sqrtf(s.radius*s.radius - (x - s.pos.x)*(x - s.pos.x)) + s.pos.y;

		glVertex2f(x,y);
	}
	glVertex2f(s.pos.x+s.radius,s.pos.y);
	glEnd();
	glBegin(GL_LINE_STRIP);
	for(i=-30;i<30;i++)
	{
		float x,y;

		x=(float(i)/30.0f)*s.radius+s.pos.x;
		y=-sqrtf(s.radius*s.radius - (x - s.pos.x)*(x - s.pos.x)) + s.pos.y;

		glVertex2f(x,y);
	}
	glVertex2f(s.pos.x+s.radius,s.pos.y);
	glEnd();
}

extern void test02_render();













void test02_render()
{
	VECTOR p(2,2,0),a(1,-3,0),b(7,3,0);
	VECTOR c=(a>>b).closest(p);
	

	glLightOn();
	glView->clear();
	glSetCamera(sce->getobject("Camera01"));
	glPushAttrib();
	glApplyAllLights();
	sce->render(0);
	glPopAttrib();
	
	TRIPLANE pq(VECTOR(50,30,0),VECTOR(60,83,0),VECTOR(30,30,0));

	glOrthoViewOn();
	glLightOff();
	glTextureOff();
	for(int i=0;i<10;i++)
	{
		glColor(0,1,0);
		glBegin(GL_LINE_STRIP);
		glVertex2fv((float*)(&pq.p1));
		glVertex2fv((float*)(&pq.p2));
		glVertex2fv((float*)(&pq.p3));
		glVertex2fv((float*)(&pq.p1));
		glEnd();

		if(pq.intersect(boll[i],boll[i].pos+VECTOR(0,0,-50),&p)>=0.0f&&pq.intersect(boll[i],boll[i].pos+VECTOR(0,0,-50),&p)<=1.0f)
		{
		//if(boll[i].intersect(VECTOR(50,0,0),VECTOR(0,100,0))>0.0f&&boll[i].intersect(VECTOR(50,0,0),VECTOR(0,100,0))<=1.0f)
			boll_c[i]=RGBCOLOR(0,0,1);
			boll_d[i]=0;
			boll_p[i]=VECTOR(p.x,p.y,1);
			//boll_d[i]=0.0f;
			
		}

		if(boll[i].intersect(boll_d[i],boll[10],boll_d[10],&p)>=0.0f&&boll[i].intersect(boll_d[i],boll[10],boll_d[10],&p)<=1.0f)
		{
			
			//boll_d[i]=boll_d[10];


		
			boll_c[i]=RGBCOLOR(1,0,0);
		//	boll_p[i]=VECTOR(p.x,p.y,1);
		}

		if(boll_p[i].z!=0.0f)
		{
			glColor(1,1,0);
			glBegin(GL_LINE_STRIP);
			glVertex2f(boll_p[i].x-0.5,boll_p[i].y+0.5);
			glVertex2f(boll_p[i].x+0.5,boll_p[i].y+0.5);
			glVertex2f(boll_p[i].x+0.5,boll_p[i].y-0.5);
			glVertex2f(boll_p[i].x-0.5,boll_p[i].y-0.5);
			glVertex2f(boll_p[i].x-0.5,boll_p[i].y+0.5);
			glEnd();
		}
		/*else
		{
			
			float col=boll[10].intersect(VECTOR(1,0,0),boll[i],boll_d[i]);
				
			if(col>=0.0f&&col<1.0f)
			
				glColor(0,0,1);
			else

				glColor(1,1,1);
		}*/

		//if(boll[i].intersect(VECTOR(0,0,0)>>VECTOR(100,100,0))>=0.0f&&boll[i].intersect(VECTOR(0,0,0)>>VECTOR(100,100,0))<=1.0f)
		//	boll_c[i]=RGBCOLOR(0,0,1);

		glColor(boll_c[i]);
		render2Dsphere(boll[i]);	
	}
	
	glColor(boll_c[10]);
	render2Dsphere(boll[10]);
	glLightOn();
	glOrthoViewOff();
	glTextureOn();

	glSwapBuffer();
}






