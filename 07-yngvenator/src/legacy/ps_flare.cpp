#include "ps_flare.h"

extern TEXTURE texParticle1, texParticle2, texParticle3;


PS_Flare::PS_Flare() {
	nDensity = 3;
	flare = new Flare [nDensity];
}

PS_Flare::~PS_Flare() {
	delete [] flare;
}

void PS_Flare::Init(VECTOR *_pos, 
				   VECTOR *_size, 
				   RGBACOLOR *_basecolor) {

	pos = *_pos;
	size = *_size;
	basecolor = *_basecolor;
	
	flare[0].SetTexture(texParticle1);
	flare[1].SetTexture(texParticle2);
	flare[2].SetTexture(texParticle3);

	for(int i=0;i<nDensity;i++) {
		flare[i].Init(	&pos,
						&size,
						&( basecolor-float( (rand() % 100 ))/500.0f ),
						float( (rand() % 100)-50 )/5000.0f);
	}

}

void PS_Flare::Live() {
	for(int i=0;i<nDensity;i++) {
		flare[i].Live(speed);
	}
}

void PS_Flare::Render() {

	glZBufOff();
	glBlendOn();
	glAlphaOn();
	glAlphaFunc(GL_GREATER,0);
	glColor(RGBACOLOR(basecolor));
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glTextureOn();

	for(int i=0;i<nDensity;i++) {
		glBindTexture(flare[i].GetTexture());
		flare[i].Render();
	}
}
