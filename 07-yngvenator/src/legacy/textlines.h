#ifndef __textlines_h__
#define __textlines_h__

//#include "glFont.h"
#include "glBase.h"

class textline {
public:
	bool active;
	char *text;
	int len;
	glFont font;
	float timer;
	
	textline();
	~textline();
	void Reset();
	void Render();
	void Init(char *_text);
	void Update();

	// std vars
	float vspace0,hspace0;
	float width0, height0;
	int valign0, halign0;
	VECTOR pos, spacing;
	RGBACOLOR color0;
	TEXTURE texture0;
	
	VECTOR *vel;	// en vektor som varje boktsav r�r sig i
	RGBACOLOR *col;	// en f�rg som varje bokstav har
	RGBACOLOR *fade;	// en f�rg som varje bokstav har
};

#endif