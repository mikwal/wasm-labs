#include "glBase.h"

#include "liquidsurface.h"


LSurface::LSurface(const char *texture_name,int _w,int _h,int _lod,float _damp,int mapping)
{
	int i;

	w=_w; 
	h=_h;
	lod=_lod;
	damp=_damp;

	material.flags=GLM_TRANSLUCENT;
	material.ambient=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);
	material.diffuse=RGBACOLOR(0.2f,0.2f,0.8f,1.0f);
	material.specular=RGBACOLOR(0.25f,0.25f,1.0f,1.0f);;
	material.emission=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);;
	material.shininess=10.0f;
	material.mapping=mapping;
	material.texture_rgb=0;
	material.texture_luminance=0;	


	glTextureListGet(texture_name,&material.texture_rgb,GL_RGB);
	glTextureListGet(texture_name,&material.texture_luminance,GL_LUMINANCE);
	
	int wlod=w*lod;
	int hlod=h*lod;

	src_map=new float[wlod*hlod];
	map=new float[wlod*hlod];
	
	for(i=0;i<wlod*hlod;i++)
		map[i]=src_map[i]=0.0f;

	normal=(new VECTOR[w*h]);
	
	for(i=0;i<w*h;i++)
		normal[i]=VECTOR(0.0f,1.0f,0.0f);
}



void LSurface::update(float time)
{	
	float *tmp;

	glSceneObject::update(time);

	tmp=src_map;
	src_map=map;
	map=tmp;

	int i,j,x,y;

	int wlod=w*lod;
	int hlod=h*lod;	

	for(j=1;j<wlod-1;j++)
	{
		src_map[0*wlod+j]=src_map[1*wlod+j];
		src_map[(hlod-1)*wlod+j]=src_map[(hlod-2)*wlod+j];
	}

	for(i=1;i<hlod-1;i++)
	{
		src_map[i*wlod]=src_map[i*wlod+1];
		src_map[i*wlod+wlod-1]=src_map[i*wlod+wlod-2];
	}

	for(i=1;i<hlod-1;i++)
		for(j=1;j<wlod-1;j++)
		{
			map[i*wlod+j]=((
							src_map[i*wlod+j+1]+
							src_map[i*wlod+j-1]+
							src_map[(i+1)*wlod+j]+
							src_map[(i-1)*wlod+j] 
							)*0.5f)- map[i*wlod+j];

			map[i*wlod+j]*=damp;
		}

	for(i=0;i<w*h;i++)
		normal[i]=0.0f;

	for(i=1,y=lod;i<h;i++,y+=lod)
		for(j=1,x=lod;j<w;j++,x+=lod)
		{
			VECTOR p1(j,map[(y)*wlod+x],i);
			VECTOR p2(j-1,map[(y)*wlod+x-lod],i);
			VECTOR p3(j,map[(y-lod)*wlod+x],i-1);
			VECTOR p4(j-1,map[(y-lod)*wlod+j-lod],i-1);
			VECTOR n1,n2;
		
			n1=(p3-p1).cross(p2-p1);
			n1/=n1.length();
			n2=(p4-p3).cross(p2-p3);
			n2/=n2.length();

			normal[(i)*w+j]+=n1;
			normal[(i)*w+j-1]+=n1+n2;
			normal[(i-1)*w+j]+=n1+n2;
			normal[(i-1)*w+j-1]+=n2;
		}
}




void LSurface::render(int pass,int renderflags)
{
	glSceneObject::render(pass,renderflags);
	
	MATRIX wm=glCam->wmatrix*gettransform();


	glMaterial(&material);
	glPushMatrix();	
	glLoadMatrixf((float*)&wm);
		
	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	
	if(material.mapping)
		glScalef(1.0f/wm.scale().x.length(),1.0f/wm.scale().y.length(),1.0f);
	
	glScalef(1.0f/w,1.0f/h,1.0f);
	
	int i,j,x,y;

	int wlod=w*lod;
	int hlod=h*lod;	
	float cx=-0.5f*w;
	float cz=-0.5f*h;

	for(i=1,y=lod;i<h;i++,y+=lod)
		for(j=1,x=lod;j<w;j++,x+=lod)
		{

			
			glBegin(GL_TRIANGLE_STRIP);	
				
			glTexCoord2i(j,i); 
			glNormal3fv((float*)&normal[(i)*w+j]);
			glVertex3f(j+cx,map[(y)*wlod+x],i+cz);					// Top Right
			
			glTexCoord2i(j-1,i); 	
			glNormal3fv((float*)&normal[(i)*w+j-1]);			
			glVertex3f(j+cx-1,map[(y)*wlod+x-lod],i+cz);				// Top Left

			glTexCoord2i(j,i-1); 
			glNormal3fv((float*)&normal[(i-1)*w+j]);
			glVertex3f(j+cx,map[(y-lod)*wlod+x],i+cz-1);				// Bottom Right

			glTexCoord2i(j-1,i-1);
			glNormal3fv((float*)&normal[(i-1)*w+j-1]);
			glVertex3f(j+cx-1,map[(y-lod)*wlod+x-lod],i+cz-1);			// Bottom Left
				
			glEnd();
		}

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

}