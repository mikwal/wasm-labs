#include <mlib.h>



//static buffer for returning result
static char _fname_buf[MAX_PATH];


// inline helper for finding the last path seperator
inline const char *_last_path_seperator(const char *filepath)
{
	const char *s=NULL;

	while(*filepath)
	{
#ifdef PATH_SEPERATOR2
		if(*filepath==PATH_SEPERATOR||*filepath==PATH_SEPERATOR2||*filepath==ARCHIVE_SEPERATOR)
#else
		if(*filepath==PATH_SEPERATOR||*filepath==ARCHIVE_SEPERATOR)
#endif
			s=filepath;

		filepath++;
	}

	return s;
}



char *fnpath(const char *filepath,char *buf)
{
	const char *s;
	int l=0;

	if(buf==NULL)
		buf=_fname_buf;

	if(s=_last_path_seperator(filepath))
	{
		if(*s==ARCHIVE_SEPERATOR)
			s++;
		l=int(s-filepath);
		strncpy(buf,filepath,l);
	}
	
	buf[l]=0;

	return buf;
}



char *fnname(const char *filepath,char *buf)
{
	const char *s;

	if(buf==NULL)
		buf=_fname_buf;

	if(s=_last_path_seperator(filepath))
	{
		if(s[0]==ARCHIVE_SEPERATOR&&s[1]==0)
			return fnname(fnreal(filepath),buf);
		s++;
	}
	else
		s=filepath;
	
	strcpy(buf,s);

	return buf;
}


char *fnext(const char *filepath,char *buf)
{
	const char *s;

	if(buf==NULL)
		buf=_fname_buf;

	if(s=_last_path_seperator(filepath))
		s++;
	else
		s=filepath;
	
	const char *t;
	for(t=NULL;*s;s++)
		if(*s==EXT_SEPERATOR)
			t=s+1;
	
	if(t)
		strcpy(buf,t);
	else
		*buf=0;

	return buf;
}


char *fnreal(const char *filepath,char *buf)
{	
	char *s;
	
	if(buf==NULL)
		buf=_fname_buf;

	s=buf;

	for(;;)
	{
		if(*filepath==ARCHIVE_SEPERATOR)
		{
			*s=0;
			break;
		}

		if((*s=*filepath)==0)
			break;
		
		filepath++;
		s++;
	}

	return buf;
}


char *fnarchive(const char *filepath,char *buf)
{
	if(buf==NULL)
		buf=_fname_buf;

	*buf=0;
	
	while(*filepath)
		if(*(filepath++)==ARCHIVE_SEPERATOR)
		{
			strcpy(buf,filepath);
			break;
		}

	return buf;
}


int fnisabsolut(const char *filepath)
{
#ifdef DRIVE_SEPERATOR
	if(filepath[1]==DRIVE_SEPERATOR)
#else
	if(filepath[0]==PATH_SEPERATOR)
#endif
		return 1;
	
	return 0;
}


int fnisarchive(const char *filepath)
{
	while(*filepath)
		if(*(filepath++)==ARCHIVE_SEPERATOR)
			return 1;

	return 0;
}
