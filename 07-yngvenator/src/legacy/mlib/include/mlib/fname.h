
#ifndef FNAME_H
#define FNAME_H



// routines for parsing parts out of ha filepath.
// all the "char *fn..." routines uses the same static buffer
// when returning the result


char *fnpath(const char *filepath,char *buf=NULL);
char *fnname(const char *filepath,char *buf=NULL);
char *fnext(const char *filepath,char *buf=NULL);

char *fnreal(const char *filepath,char *buf=NULL);
char *fnarchive(const char *filepath,char *buf=NULL);


int fnisabsolut(const char *filepath);
inline int fnisrelative(const char *filepath) {return !fnisabsolut(filepath);}

int fnisarchive(const char *filepath);
inline int fnisreal(const char *filepath) {return !fnisarchive(filepath);}


#endif
