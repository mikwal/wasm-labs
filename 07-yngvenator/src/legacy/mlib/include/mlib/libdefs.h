
#ifndef LIBDEFS_H
#define LIBDEFS_H



// version numbers, low byte == mayor versionnumber, mayor byte == minor versionnumber
#define MLIB_VERSION				HWORD(0,2)
#define MLIB_ARCHIVE_VERSION		HWORD(1,0)


// check platform
#ifdef WIN32
#define MLIB_WIN
#ifdef _DEBUG
#define DEBUG
#endif
#ifndef _CONSOLE	// include winmain to main wrapper if WIN32 gui app
#include <mlib/WIN32/winmain.h>
#endif
#else
#ifdef unix
#ifdef DJGPP
#define MLIB_DJGPP
#else
#define MLIB_UNIX
#endif
#else
#error unsupported plattform (not WIN32, DJGPP or unix)
#endif
#endif



// include debuglogging header case user wants DEBUG
#include <mlib/debug.h>



// define some platform dependent stuff like the path (and drive) seperator(s)

#define PATH_SEPERATOR '/'
#define EXT_SEPERATOR '.'
#if defined(MLIB_WIN) || defined(MLIB_DJGPP)
#define PATH_SEPERATOR2 '\\'
#define DRIVE_SEPERATOR ':'
#endif
#define ARCHIVE_SEPERATOR '|'	// define the "file in archive" seperator



//define NULL if not defined
#ifndef NULL	
#define NULL 0
#endif


// 4 byte make WORD and 2 byte make HWORD
#define WORD(b1,b2,b3,b4) (char(b1)|(char(b2)<<8)|(char(b3)<<16)|(char(b4)<<24))
#define HWORD(b1,b2) (char(b1)|(char(b2)<<8))


// define maximum length of a filepath if not defined
#ifndef MAX_PATH
#define MAX_PATH 260
#endif


#endif