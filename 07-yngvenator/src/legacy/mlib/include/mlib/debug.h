
#ifndef DEBUG_H
#define DEBUG_H



#ifdef DEBUG
#ifndef DEBUG_FILE
#define DEBUG_FILE "debug.log"
#endif
extern char *_debug_cur_file;
extern int _debug_cur_line;
int _debug_output(char *msg,...);
#define DBG _debug_cur_file=__FILE__,_debug_cur_line=__LINE__,_debug_output
#else 
#define DBG
#endif



#endif