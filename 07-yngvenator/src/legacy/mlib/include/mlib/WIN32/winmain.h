

#ifndef _MWINMAIN_H_
#define _MWINMAIN_H_

#ifdef _INC_WINDOWS	// if we are planing to use windows api
//the WinMain arguments accessible as globals    
extern HINSTANCE WinMain_hInstance;			// handle to current instance
extern HINSTANCE WinMain_hPrevInstance;		// handle to previous instance
extern char *WinMain_lpCmdLine;				// pointer to command line
extern int WinMain_nCmdShow;				// show state of window
#endif

#endif
