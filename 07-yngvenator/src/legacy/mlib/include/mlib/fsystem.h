
#ifndef FSYSTEM_H
#define FSYSTEM_H


#include <fcntl.h>
#include <sys/stat.h>


#ifdef MLIB_WIN
#define S_IRUSR S_IREAD
#define S_IWUSR S_IWRITE
#include <io.h>
#include <direct.h>
#endif

#ifdef MLIB_UNIX
#define O_BINARY 0
#include <unistd.h>
inline int mkdir(const char *dir) {return mkdir(dir,S_IRUSR|S_IWUSR);}
#endif

#ifdef MLIB_DJGPP
#include <unistd.h>
inline int mkdir(const char *dir) {return mkdir(dir,S_IRUSR|S_IWUSR);}
#endif

int _mlib_chdir(const char *);
char * _mlib_getcwd(char *, int);
#define chdir(s)	_mlib_chdir(s)
#define getcwd(s,m)	_mlib_getcwd(s,m)


// mode which the file be opened in
#define F_MODE				0x00FF	// the bits which describes the mode
#define F_READ				0x0000	// open a file in buffered readonly mode
#define F_WRITE				0x0001	// open a file in buffered writeonly mode
#define F_RAW				0x0002	// open a file in unbuffered read/write mode

// extra flags, F_APPEND and F_COMPRESS are only for the F_WRITE mode
// and F_NOCREATE are for the F_WRITE and F_RAW mode
#define F_FLAGS				0xFF00	// the bits which describes the extra flags
#define F_APPEND			0x0100	// if a file already exist, append data to it (should not be used with F_COMPRESS)
#define F_NOCREATE			0x0200	// dont create a file if it doesnt exist
#define F_COMPRESS			0x0400	// compress  data with the lzss algorithm


// open a file, on disk or in archive and decompress it if it is compressed
IOStream *createFileStream(const char *filename,int mode);

inline IOStream *fopen(const char *filename,int mode=F_READ) {return createFileStream(filename,mode);}
inline void fclose(IOStream *f) {delete f;}



#define MAX_ENTRY	24	// max chars in archive entryname

// the mlib archive header structure (the last 20 bytes in the file)
struct __attribute__((packed, aligned(2))) ARCHIVEHEADER {
	unsigned long id;			// archive ID ("ARCH")
	unsigned long cid;			// compression ID ("LZSS" or 0x00000000)
	unsigned short version;		// archive version
	unsigned short nentry;		// number of entries
	unsigned long offset;		// offset of the first archive entry structure
};

// the mlib archive fileentry structure
struct __attribute__((packed, aligned(4))) ARCHIVEENTRY {
	unsigned long type;			// type of entry ("DIR" or "FILE")
	char name[MAX_ENTRY];		// the name of the file or dir (unused bytes should always be padded with zeros)
	unsigned long _reserved;	// should always be 0
	unsigned long offset;		// offset to the file or the first entry in a dir
	unsigned long size;			// the size of the file or number of entries in dir
};

// routine for finding a specific archiveentry in a stream (which contains a archive)
// and retriving the ARCHIVEHEADER and the ARCHIVEENTRY structures
// returns 0 on success, -1 if the entry wasnt found, 1 if the stream doesnt contain a archive
int getentry(IOStream *s,const char *entry,ARCHIVEHEADER *ah,ARCHIVEENTRY *ae);




#endif