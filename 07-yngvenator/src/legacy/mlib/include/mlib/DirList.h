
#ifndef DIRLIST_H
#define DIRLIST_H


// define states for the _state member in Dir
#define DIRLIST_STATE_VALID		0x0001	// a dir is opened and entries can be listed from it
#define DIRLIST_STATE_ARCHIVE	0x0002	// the dir is an archivedir (mostly for internal use)


// flags for the fattr member in the Dir class
#define ATTR_RONLY		0x01	// read-only file
#define ATTR_HIDDEN		0x02	// hidden file 
#define ATTR_SYSTEM		0x04	// system file
#define ATTR_DIR		0x10	// directory
#define ATTR_ARCH		0x20	// archive file




class DirList {
private:
	int _state;	// the state of the Dir class
	IOStream *_shandle;	// IOStream handle for a archive dir
	int _entry_left;	// number of entries left to read
	unsigned long _archive_time; // timestamp of the archive
	
public:
	// this fields will be filled in by the first and next calls
	char fname[MAX_PATH];	// the name of the file/dir
	unsigned int fattr;		// the attributes
	unsigned long fsize;	// the size
	unsigned long ftime;	// timestamp


	inline DirList() {_state=0;}
	inline DirList(const char *dirname) {_state=0;first(dirname);}
	inline ~DirList() {close();}

	int first(const char *dirname);	// begin the listing process
	int next();					// continue the listing process
	void close();				// close the listing process

	inline int state() {return _state;}	// returns the _state member
};


#endif