
#ifndef IOSTREAM_H
#define IOSTREAM_H



#ifndef EOF
#define EOF -1
#endif

#ifndef SEEK_SET
#define SEEK_SET    0
#define SEEK_CUR    1
#define SEEK_END    2
#endif



#define IOSTREAM_EOF		0x0001
#define IOSTREAM_ERR		0x0002

#define IOSTREAM_MODE		0xFF00
#define IOSTREAM_READ		0x0100
#define IOSTREAM_WRITE		0x0200
#define IOSTREAM_BUFFERED	0x0400
#define IOSTREAM_COMPRESSED	0x0800
#define IOSTREAM_PARTIAL	0x1000



// pure virtual base class for IO streams
class IOStream {
public:
	virtual ~IOStream() {}
	virtual unsigned int read(void *addr,unsigned int nbyte) {return 0;}
    virtual unsigned int write(const void *addr,unsigned int nbyte) {return 0;}
    virtual int get() {return 0;}
	virtual void put(int c) {}
	virtual long seek(long offset,int origin) {return 0;}
    virtual long tell() {return 0;}
	virtual long size() {return 0;}
	virtual int eof() {return 0;}
	virtual int error() {return 0;}
	virtual int mode() {return 0;}
};



// buffer size for buffered file strems
#ifndef IOSTREAM_BUFSIZE	
#define IOSTREAM_BUFSIZE	4096
#endif



// buffered read-from-file stream
class ReadFileStream : public IOStream {
private:
	int _file;
	int _flags;

	unsigned char _buf[IOSTREAM_BUFSIZE];
	unsigned char *_buf_pos;
	int _buf_size;
	int _fill_buf();

public:
	ReadFileStream(int file);
	~ReadFileStream();
	unsigned int read(void *addr,unsigned int nbyte);
    inline int get() {return ((--_buf_size>=0)?*_buf_pos++:_fill_buf());}
	long seek(long offset,int origin);
    long tell();
	long size();
	inline int mode() {return _flags&IOSTREAM_MODE;}
	inline int eof() {return _flags&IOSTREAM_EOF;}
	inline int error() {return _flags&IOSTREAM_ERR;}
	inline int detach() {int f=_file;_file=-1;return f;}
};



// buffered read-from-file stream
// where the file is an archive filentry (at 'offset' with 'size' bytes) 
class ReadPartialFileStream : public IOStream {
private:
	int _file;
	int _flags;

	unsigned long _offset;
	unsigned long _size;
	unsigned long _cnt;

	unsigned char _buf[IOSTREAM_BUFSIZE];
	unsigned char *_buf_pos;
	int _buf_size;
	int _fill_buf();
	
public:
	ReadPartialFileStream(int file,unsigned long offset,unsigned long size);
	~ReadPartialFileStream();
	unsigned int read(void *addr,unsigned int nbyte);
    inline int get() {return ((--_buf_size>=0)?*_buf_pos++:_fill_buf());}
	long seek(long offset,int origin);
    inline long tell() {return (_size-_cnt)-_buf_size;}
	inline long size() {return _size;}
	inline int mode() {return _flags&IOSTREAM_MODE;}
	inline int eof() {return _flags&IOSTREAM_EOF;}
	inline int error() {return _flags&IOSTREAM_ERR;}
	inline int detach() {int f=_file;_file=-1;return f;}
};




#define RBUFSIZE	4096	// size of ring buffer
#define MINLEN		2		// min length of a match, that is MINLEN < X
#define MAXLEN		18		// min length of a match, that is X <= MAXLEN,



// LZSS decompression stream put on top of another stream
class LZSSUnpackStream : public IOStream {
private:
	IOStream *_stream;         		// underlying stream
	unsigned long _startpos;		// LZSSUnPackStream position in the underlying IOStream
	unsigned long _pack_pos;		// the actual position in the compressed data
	unsigned long _size;			// size of the compressed data

	unsigned char _rbuf[RBUFSIZE];	// dictionary ringbuffer
	unsigned int _flags;			// a bitmask to keep track of encoded and uncoded bytes
	int _pos;						// position of string in the ringbuffer
	int _len;						// lenght of the string
	int _i;							// current position in the decoding process
	int _bp;						// current position in the ringbuffer

	unsigned char _buf[IOSTREAM_BUFSIZE];
	unsigned char *_buf_pos;
	int _buf_size;
	int _fill_buf();

public:
	LZSSUnpackStream(IOStream *stream);
	~LZSSUnpackStream() { delete _stream; }
	unsigned int read(void *addr,unsigned int nbyte);
    inline int get() {return ((--_buf_size>=0)?*_buf_pos++:_fill_buf());}
	long seek(long offset,int origin);
    inline long tell() {return _pack_pos-_buf_size;}
	inline long size() {return _size;}
	inline int mode() {return (_stream->mode()&~IOSTREAM_WRITE)|IOSTREAM_COMPRESSED;}
	inline int eof() {return _stream->eof();}
	inline int error() {return _stream->error();}
	inline IOStream *detach() {IOStream *s=_stream;_stream=NULL;return s;}
};



#endif