#include <mlib.h>



extern char _mlib_cwd[MAX_PATH];

IOStream* createFileStream(const char *filename,int mode)
{
	int file=-1,fmode=0;
	char tmp[MAX_PATH];

	if(_mlib_cwd[0]&&!fnisabsolut(filename))
	{
		strcpy(tmp,_mlib_cwd);
		strcat(tmp,filename);
		filename=tmp;		
	}

	switch(mode&0x00FF)
	{

	// opened in read mode
	case F_READ:
		fmode=O_RDONLY;

		if(fnisarchive(filename)) // file is inside an archive
		{
			if((file=::open(fnreal(filename),fmode))>=0)
			{
				ARCHIVEHEADER ah;
				ARCHIVEENTRY ae;
				char entry[MAX_PATH];
				ReadFileStream *archive=new ReadFileStream(file);
				
				fnarchive(filename,entry);
				
				if(getentry(archive,entry,&ah,&ae)==0&&ae.type==WORD('F','I','L','E'))
				{

					IOStream *stream=new ReadPartialFileStream(archive->detach(),ae.offset,ae.size);
					if(ah.cid==WORD('L','Z','S','S'))
						stream=new LZSSUnpackStream(stream);

					delete archive;

					return stream;
				}
			}
		}
		else	// if file is not inside an archive
			if((file=::open(filename,fmode))>=0)		
			{
				unsigned long id;
	
				
				IOStream *stream=new ReadFileStream(file);

				if(::read(file,&id,4)!=4)
					id=0;
					
				if(id==WORD('L','Z','S','S'))	// file is LZSS compressed
					stream=new LZSSUnpackStream(stream); // so put an unpackstream on top of the readstream 
				else
					::lseek(file,0,SEEK_SET);	// else rewind filepointer
					
				return stream;
			}

		break;
	}
	
	return NULL;
}







#define CMP_ENTRY(s1,s2)	((unsigned long*)s1)[0]==((unsigned long*)s2)[0]&&	\
							((unsigned long*)s1)[1]==((unsigned long*)s2)[1]&&	\
							((unsigned long*)s1)[2]==((unsigned long*)s2)[2]&&	\
							((unsigned long*)s1)[3]==((unsigned long*)s2)[3]&&	\
							((unsigned long*)s1)[4]==((unsigned long*)s2)[4]&&	\
							((unsigned long*)s1)[5]==((unsigned long*)s2)[5]




static int _findentry(const char *entry,char *cmp,IOStream *s,int n,ARCHIVEENTRY *ae)
{

	int i=0;

	while(*entry)
	{
#ifdef PATH_SEPERATOR2
		if(*entry==PATH_SEPERATOR||*entry==PATH_SEPERATOR2)
#else
		if(*entry==PATH_SEPERATOR)
#endif
		{
			if(*(++entry)==0)
				return -1;

			break;
		}

		if(i>=MAX_ENTRY)
			return -1;

		cmp[i++]=*entry++;
	}

	while(i<MAX_ENTRY)
		cmp[i++]=0;
	
	for(i=0;i<n;i++)
	{
		s->read(ae,sizeof(ARCHIVEENTRY));
		if(CMP_ENTRY(cmp,ae->name))
			if(*entry)
			{
				s->seek(ae->offset,SEEK_SET);
				if(ae->type==WORD('D','I','R',0))
					return _findentry(entry,cmp,s,ae->size,ae);
				else
					break;
			}
			else
				return 0;
	}

	return -1;
}


int getentry(IOStream *s,const char *entry,ARCHIVEHEADER *ah,ARCHIVEENTRY *ae)
{	
	char cmpbuf[MAX_ENTRY];

	s->seek(-int(sizeof(ARCHIVEHEADER)),SEEK_END);
	s->read(ah,sizeof(ARCHIVEHEADER));

	if(ah->id!=WORD('A','R','C','H'))
		return 1;

	if(*entry==0)
	{
		ae->type=WORD('D','I','R',0);
		memset(ae->name,0,MAX_ENTRY);
		ae->_reserved=0;
		ae->offset=ah->offset;
		ae->size=ah->nentry;

		return 0;
	}

	s->seek(ah->offset,SEEK_SET);

	return _findentry(entry,cmpbuf,s,ah->nentry,ae);
}




