#include <mlib.h>


int wmatch(const char *str,const char *wpat)
{
	int match;
	
	for(;*wpat;wpat++,str++)
	{
		if(*wpat=='*')
		{
			while(*++wpat=='*');

			if(*wpat==0)
				return 0;
		
			while(*str)
				if((match=wmatch(str++,wpat))!=-1)
					return match;

			return 1;
		}
		else
		{
			if(*str==0)
				return 1;

			if(*wpat!='?'&&*wpat!=*str)
				return -1;
		}
	}

	return *str?-1:0;
}



int wmatchi(const char *str,const char *wpat)
{
	int match;

	for(;*wpat;wpat++,str++)
	{
		if(*wpat=='*')
		{
			while(*++wpat=='*');

			if(*wpat==0)
				return 0;
		
			while(*str)
				if((match=wmatchi(str++,wpat))!=-1)
					return match;

			return 1;
		}
		else
		{
			if(*str==0)
				return 1;

			if(*wpat!='?'&&tolower(*wpat)!=tolower(*str))
				return -1;
		}
	}

	return *str?-1:0;
}