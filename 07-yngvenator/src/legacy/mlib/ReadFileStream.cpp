#include <mlib.h>



int ReadFileStream::_fill_buf()
{
	_buf_pos=_buf;
	_buf_size=::read(_file,_buf,IOSTREAM_BUFSIZE);
	
	if(_buf_size>0)
	{
		_buf_size--;
		return *_buf_pos++;
	}

	if(_buf_size<0)
	{
		_flags|=IOSTREAM_ERR;
		_buf_size=0;
	}

	if(_buf_size==0)
		_flags|=IOSTREAM_EOF;

	return EOF;
}



ReadFileStream::ReadFileStream(int file)
{
	_file=file;
	_flags=IOSTREAM_READ|IOSTREAM_BUFFERED;
	_buf_size=0;
}



ReadFileStream::~ReadFileStream()
{	
	if(_file>=0)
		::close(_file);
	_file=-1;
}



unsigned int ReadFileStream::read(void *addr,unsigned int nbyte)
{
	unsigned char *d=(unsigned char*)addr;
	unsigned int b;

	
	for(b=0; b<nbyte; b++)
		if(--_buf_size>=0)
			*d++=*_buf_pos++;
		else 
		{
			int c;

			if((c=_fill_buf())==EOF)
				return b;
			*d++=c;
		}

	return b;
}



long ReadFileStream::seek(long offset,int origin)
{
	if(origin==SEEK_CUR)
		offset-=_buf_size;

	_buf_size=0;
	_flags&=~IOSTREAM_EOF;
	return ::lseek(_file,offset,origin);
}



long ReadFileStream::tell()
{
	long r;

	if((r=::lseek(_file,0,SEEK_CUR))>=0)
		return r-_buf_size;
	else
		return r;
}


long ReadFileStream::size()
{
	struct stat buf;

	fstat(_file,&buf);

	return buf.st_size;
}
