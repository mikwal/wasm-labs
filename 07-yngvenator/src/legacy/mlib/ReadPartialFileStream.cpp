#include <mlib.h>



int ReadPartialFileStream::_fill_buf()
{
	unsigned int s;

	s=(IOSTREAM_BUFSIZE<_cnt?IOSTREAM_BUFSIZE:_cnt);	
	_cnt-=s;

	_buf_pos=_buf;
	_buf_size=::read(_file,_buf,s);
	
	if(_buf_size>0)
	{
		_buf_size--;
		return *_buf_pos++;
	}

	if(_buf_size<0)
	{
		_flags|=IOSTREAM_ERR;
		_buf_size=0;
	}

	if(_buf_size==0)
		_flags|=IOSTREAM_EOF;
			
	return EOF;
}



ReadPartialFileStream::ReadPartialFileStream(int file,unsigned long offset,unsigned long size)
{
	_file=file;
	_flags=IOSTREAM_READ|IOSTREAM_BUFFERED;
	_buf_size=0;

	_offset=::lseek(file,offset,SEEK_SET);
	if(_offset!=offset)
	{
		_flags|=(IOSTREAM_EOF|IOSTREAM_ERR);
		_cnt=_size=0;
	}
	else
		_cnt=_size=size;
}



ReadPartialFileStream::~ReadPartialFileStream()
{
	if(_file>=0)
		::close(_file);
	_file=-1;
}



unsigned int ReadPartialFileStream::read(void *addr,unsigned int nbyte)
{
	unsigned char *d=(unsigned char*)addr;
	unsigned int b;

	
	for(b=0; b<nbyte; b++)
		if(--_buf_size>=0)
			*d++=*_buf_pos++;
		else 
		{
			int c;

			if((c=_fill_buf())==EOF)
				return b;
			*d++=c;
		}

	return b;
}



long ReadPartialFileStream::seek(long offset,int origin)
{
	long r;

	if(origin==SEEK_CUR)
		offset+=(_size-_cnt)-_buf_size;	
	else
		if(origin==SEEK_END)
			offset+=_size;

	if(offset<0)
		offset=0;

	if((unsigned)offset>_size)
		offset=_size;	

	_cnt=_size-offset;
	_buf_size=0;
	_flags&=~IOSTREAM_EOF;

	if((r=::lseek(_file,offset+_offset,SEEK_SET))<0)
		return r;
	else
		return r-_offset;
}

