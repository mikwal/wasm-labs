#include <mlib.h>




char _mlib_cwd[MAX_PATH]="";

#undef chdir
#undef getcwd


static int _archive_dir_exists(const char *dirname)
{
	IOStream *s;

	if(s=createFileStream(fnreal(dirname),F_READ))
	{
		ARCHIVEHEADER ah;
		ARCHIVEENTRY ae;
		char entry[MAX_PATH];
		int r;

		fnarchive(dirname,entry);
		if(getentry(s,entry,&ah,&ae)==0&&ae.type==WORD('D','I','R',0))
			r=1;
		else
			r=0;

		delete s;

		return r;
	}

	return 0;		
}


int _mlib_chdir(const char *dir)
{
	const char sep[2]={PATH_SEPERATOR,0};

	if(fnisabsolut(dir))
		_mlib_cwd[0]=0;

	if(_mlib_cwd[0])
	{
		char tmp[MAX_PATH];
		
		strcpy(tmp,_mlib_cwd);
		strcat(tmp,dir);
		
		if(_archive_dir_exists(tmp))
		{
			strcpy(_mlib_cwd,tmp);
			strcat(_mlib_cwd,sep);
			return 0;
		}
		else
			return 1;
	}
	else
		if(fnisarchive(dir))
		{
			int i;
			
			if(_archive_dir_exists(dir))
			{
				if(fnisabsolut(dir))
					strcpy(_mlib_cwd,dir);
				else
				{
					getcwd(_mlib_cwd,MAX_PATH);
					i=strlen(_mlib_cwd)-1;
#ifdef PATH_SEPERATOR2
					if(_mlib_cwd[i]!=PATH_SEPERATOR&&_mlib_cwd[i]!=PATH_SEPERATOR2)
#else
					if(_mlib_cwd[i]!=PATH_SEPERATOR)
#endif
					strcat(_mlib_cwd,sep);
					strcat(_mlib_cwd,dir);
				}	
				
				i=strlen(_mlib_cwd)-1;
				if(_mlib_cwd[i]!=ARCHIVE_SEPERATOR)
				{
					_mlib_cwd[++i]=PATH_SEPERATOR;
					_mlib_cwd[++i]=0;
				}

				return 0;			
			}
			else
				return 1;
		}
		else
			return chdir(dir);

}

char * _mlib_getcwd(char *buf, int size)
{
	if(_mlib_cwd[0])
	{
		int i;
		for(i=0;i<size;i++)
			if((buf[i]=_mlib_cwd[i])==0)
				break;
		i--;
		if(buf[i]!=ARCHIVE_SEPERATOR)
			buf[i]=0;

		return buf;
	}
	else
		return getcwd(buf,size);
}















