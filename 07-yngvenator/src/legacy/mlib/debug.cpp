#ifdef _DEBUG
#include <mlib.h>


char *_debug_cur_file=NULL;
int _debug_cur_line=0;



void _debug_output(char *msg,...)
{
	static IOStream *s=getFileStream(DEBUG_FILE,F_WRITE);
	static int refnum=0;

	s->printf("%04x [%s:%d] ",refnum++,_debug_cur_file,_debug_cur_line); 
	s->vprintf(msg,(const char**)&msg+1);
	s->printf("\n");
	s->flush();
}



#endif