/*
note:

                     LZSS compression routines

   This compression algorithm is based on the ideas of Lempel and Ziv,
   with the modifications suggested by Storer and Szymanski. The algorithm 
   is based on the use of a ring buffer, which initially contains zeros. 
   We read several characters from the file into the buffer, and then 
   search the buffer for the longest string that matches the characters 
   just read, and output the length and position of the match in the buffer.

   With a buffer size of 4096 bytes, the position can be encoded in 12
   bits. If we represent the match length in four bits, the <position,
   length> pair is two bytes long. If the longest match is no more than
   two characters, then we send just one character without encoding, and
   restart the process with the next letter. We must send one extra bit
   each time to tell the decoder whether we are sending a <position,
   length> pair or an unencoded character, and these flags are stored as
   an eight bit mask every eight items.

   This implementation uses binary trees to speed up the search for the
   longest match.

   Original code (lzss.cpp) by Haruhiko Okumura, 4/6/1989.
   12-2-404 Green Heights, 580 Nagasawa, Yokosuka 239, Japan.
   

   Modified for use as a IOStream class
   by: Mikael Waltersson
   files: LZSSPackStream.cpp, LZSSUnpackStream.cpp
   copyright: Bongo Productions, 2001
   
*/

#include <mlib.h>


// define LZSS compression stuff
#define LZSS_RBUFSIZE	4096	// size of ring buffer
#define LZSS_MINLEN		2		// min length of a match, that is MINLEN < X
#define LZSS_MAXLEN		18		// min length of a match, that is X <= MAXLEN,


int LZSSUnpackStream::_fill_buf()
{
	unsigned char *ptr;
	unsigned int n;
	int c;
	

	ptr=_buf;				// start of read buffer
	n=IOSTREAM_BUFSIZE;		// bytes left to read

	if(_i>0)		// if last call returned inside an position-length loop
		goto inside_pos_len_loop;	// the go there

	for(;;)
	{
		_flags>>=1;

		if((_flags&256)==0)
		{
			if((c=_stream->get())==EOF)
				goto end;
			
			_flags=c|0xFF00; //use higher byte to count 8 bits
		}

		if(_flags&1)		// 1 means unencoded byte
		{
			if((c=_stream->get())==EOF)
				goto end;
			
			_rbuf[_bp++]=c;		// fill the ringbuffer
			_bp&=(LZSS_RBUFSIZE-1);	// buffer position should always be < RBUFSIZE
			
			*ptr++=c;
			if((--n)==0)		// if bytes remaining is zero, return
				goto end;
		}
		else	// 0 means encoded byte
		{
			if((_pos=_stream->get())==EOF)
				goto end;
			if((_len=_stream->get())==EOF)
				goto end;

			_pos|=(_len&0xF0)<<4;		// first 12 bits are position
			_len=(_len&0x0F)+LZSS_MINLEN+1;	// last 4 bits are length

inside_pos_len_loop:
			while(_i<_len)
			{
				c=_rbuf[(_pos+_i)&(LZSS_RBUFSIZE-1)];
				
				_rbuf[_bp++]=c;		// fill the ringbuffer
				_bp&=(LZSS_RBUFSIZE-1);	// buffer position should always be < RBUFSIZE
				_i++;

				*ptr++=c;
				if((--n)==0)	// if bytes remaining is zero, goto end
					goto end;
			}

			_i=0;
		}
	}


end:
	_buf_pos=_buf;
	_buf_size=IOSTREAM_BUFSIZE-n;	// set _buf_size to number of bytes decompressed
	
	if(_buf_size<=0)	// reached EOF on _stream
		return EOF;
		
	_pack_pos+=_buf_size;	// increase position 
	_buf_size--;

	return *_buf_pos++;
}



LZSSUnpackStream::LZSSUnpackStream(IOStream* stream)
{
	int i;

	_stream=stream;
	_startpos=stream->tell();
	_stream->read(&_size,sizeof(unsigned long));
	_pack_pos=0;
	_buf_size=0;

	for(i=0; i<LZSS_RBUFSIZE-LZSS_MAXLEN; i++)	// clear the ringbuffer
		_rbuf[i]=' ';							// with any character that will appear often 
	
	_bp=LZSS_RBUFSIZE-LZSS_MAXLEN;			 // set ringbuffer position
	_flags=0;
	_i=0;
}



unsigned int LZSSUnpackStream::read(void *addr,unsigned int nbyte)
{
	unsigned char *d=(unsigned char*)addr;
	unsigned int b;

	
	for(b=0; b<nbyte; b++)
		if(--_buf_size>=0)
			*d++=*_buf_pos++;
		else 
		{
			int c;

			if((c=_fill_buf())==EOF)
				return b;
			*d++=c;
		}

	return b;
}



long LZSSUnpackStream::seek(long offset,int origin)
{

	if(origin==SEEK_CUR&&offset>=0)
	{
seek_cur:
		while(offset--)
			if(--_buf_size>=0)
				*_buf_pos++;
			else 
				if(_fill_buf()==EOF)
					break;
		
		return _pack_pos-_buf_size;
	}
	else
	{
		if(origin==SEEK_SET)
			offset-=_pack_pos-_buf_size;
		else
			if(origin==SEEK_END)
				offset+=_size-_pack_pos-_buf_size;

		if(offset<0)
		{
			int i;

			offset+=_pack_pos-_buf_size;
			if(offset<0)
				offset=0;
			
			_stream->seek(_startpos+sizeof(unsigned long),SEEK_SET);
			_pack_pos=0;
			_buf_size=0;

			for(i=0; i<LZSS_RBUFSIZE-LZSS_MAXLEN; i++)	// clear the ringbuffer
				_rbuf[i]=' ';							// with any character that will appear often 
			
			_bp=LZSS_RBUFSIZE-LZSS_MAXLEN;			 // set ringbuffer position
			_flags=0;
			_i=0;
		}
		
		goto seek_cur;
	}
}


