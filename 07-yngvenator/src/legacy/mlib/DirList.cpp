#include <mlib.h>


extern char _mlib_cwd[MAX_PATH];



int DirList::first(const char *dirname)
{
	int _ffhandle;
	char tmp[MAX_PATH];


	close();

	if(_mlib_cwd[0]&&!fnisabsolut(dirname))
	{
		strcpy(tmp,_mlib_cwd);
		strcat(tmp,dirname);
		dirname=tmp;
	}

	if(fnisarchive(dirname))
	{
		if(_shandle=createFileStream(fnreal(dirname), F_READ))
		{					
			ARCHIVEHEADER ah;
			ARCHIVEENTRY ae;
			char entry[MAX_PATH];
								
			fnarchive(dirname,entry);
									
			if(getentry(_shandle,entry,&ah,&ae)==0&&ae.type==WORD('D','I','R',0))
			{
				struct stat s;
								
				stat(fnreal(dirname),&s);

				_entry_left=ae.size;
				_archive_time=s.st_mtime;

				_shandle->seek(ae.offset,SEEK_SET);

				return next();
			}

			_shandle=NULL;
		}

		return -1;
	}
}



int DirList::next()
{
	if(_shandle)
	{
		if(_entry_left>0)
		{
			ARCHIVEENTRY ae;
						
			_entry_left--;
			_shandle->read(&ae,sizeof(ae));

			strcpy(fname,ae.name);
			fattr=ATTR_RONLY|(ae.type==WORD('D','I','R',0)?ATTR_DIR:0);
			ftime=_archive_time;
			fsize=(ae.type==WORD('F','I','L','E')?ae.size:0);

			return 0;
		}

		
		close();
	}


	return -1;
}



void DirList::close()
{
	delete _shandle;
	_shandle=NULL;
}



