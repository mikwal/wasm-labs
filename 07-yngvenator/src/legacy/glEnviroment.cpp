#include "glBase.h"
#include <mmsystem.h>


static struct {
	__int64 freq;
	float res;
	unsigned long mmt_start;       
	unsigned long mmt_elapsed;
	bool pt;    
	__int64 pt_start;
	__int64 pt_elapsed;
} _timer;	



void glInitTimer()
{
     memset(&_timer,0,sizeof(_timer));   

     if(!QueryPerformanceFrequency((LARGE_INTEGER*)&_timer.freq))
     {
          _timer.pt=false;
          _timer.mmt_start=timeGetTime();
          _timer.res=1.0f/1000.0f;
          _timer.freq=1000;   
          _timer.mmt_elapsed=_timer.mmt_start;
     }
     else
     {
          QueryPerformanceCounter((LARGE_INTEGER*)&_timer.pt_start);
          _timer.pt=true;
          _timer.res=(float)(((double)1.0f)/((double)_timer.freq));
          _timer.pt_elapsed=_timer.pt_start;
     }
}



float glTime()
{
     __int64 time;        

     if (_timer.pt)
     {
          QueryPerformanceCounter((LARGE_INTEGER*)&time);
          return glLastTimeValue=(((float)(time-_timer.pt_start)*_timer.res)*1000.0f);
     }
     else
          return glLastTimeValue=(((float)(timeGetTime()-_timer.mmt_start)*_timer.res)*1000.0f);
}

float glLastTimeValue=0;
float glFrameTime=1000.0f/30.0f;













glViewPort::glViewPort()
{
	x=0;
	y=0;
	w=0;
	h=0;

	a=0.0f;

	scissor_x=0;
	scissor_y=0;
	scissor_w=0;
	scissor_h=0;

	background_rgb=0;
	background_luminance=0;

	color=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);

	light=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);
	
	fog_dens=2.0f;
	fog_start=8.0f;
	fog_end=64.0f;
	fog_mode=0;
}

void glViewPort::clear()
{
	TEXTURE background=(glCam->color_conversion==GL_LUMINANCE?background_luminance:background=background_rgb);;

	glDepthMask(1);
	glColorMask(1,1,1,1);

	

	if(x==0&&y==0&&w==0&&h==0&&color.a>=1.0f&&background==0)
	{
		RGBACOLOR c=glCam->getcolor(color);

		glClearColor(c.r,c.g,c.b,c.a);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|(glFlags&GLW_STENCILBUF?GL_STENCIL_BUFFER_BIT:0));
	}
	else
	{
		glClear(GL_DEPTH_BUFFER_BIT|(glFlags&GLW_STENCILBUF?GL_STENCIL_BUFFER_BIT:0));
		
		if(color.a<1.0f)
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		}
		else
			glDisable(GL_BLEND);

		if(background)
			glBindTexture(&background);
		else
			glDisable(GL_TEXTURE_2D);

		glDisable(GL_ALPHA_TEST);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		
		glDepthMask(0);
	
		glOrthoViewOn();
		glColor(color);
		glBegin(GL_TRIANGLE_STRIP);	
		glTexCoord2i(1,1);
		glVertex2f(100.0f,100.0f);
		glTexCoord2i(0,1);
		glVertex2f(0.0f,100.0f);
		glTexCoord2i(1,0);
		glVertex2f(100.0f,0.0f);	
		glTexCoord2i(0,0);
		glVertex2f(0.0f,0.0f);	
		glEnd();
		glOrthoViewOff();
	
		glDepthMask(1);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		
		glDisable(GL_BLEND);
	}
}

void glViewPort::setbackground(const char *texture_name)
{
	background_rgb=0;
	background_luminance=0;

	glTextureListGet(texture_name,&glView->background_rgb,GL_RGB);
	glTextureListGet(texture_name,&glView->background_luminance,GL_LUMINANCE);
}


glViewPort glDefaultView;
glViewPort *glView=&glDefaultView;

void glSetViewPort(glViewPort *v)
{
	glView=v;

	if(glView->x==0&&glView->y==0&&glView->w==0&&glView->h==0)
		glViewport(0,0,glScreenWidth,glScreenHeight);
	else
		glViewport(glView->x,glView->y,glView->w,glView->h);

	glScissor(glView->scissor_x,glView->scissor_y,glView->scissor_w,glView->scissor_h);
	if(glView->scissor_x==0&&glView->scissor_y==0&&glView->scissor_w==0&&glView->scissor_h==0)
		glDisable(GL_SCISSOR_TEST);
	else
		glEnable(GL_SCISSOR_TEST);
		
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,(float*)&glCam->getcolor(glView->light));
		
	glFogfv(GL_FOG_COLOR,(float*)&glCam->getcolor(glView->color));
	glFogf(GL_FOG_DENSITY,glView->fog_dens);
	glFogf(GL_FOG_START,glView->fog_start);
	glFogf(GL_FOG_END,glView->fog_end);
	glFogi(GL_FOG_MODE,glView->fog_mode);
	if(glView->fog_mode)
		glEnable(GL_FOG);
	else
		glDisable(GL_FOG);
}








static bool _gl_ortho_view=false;


void glOrthoViewOn(float x_scale,float y_scale,float z_scale)
{
	if(!_gl_ortho_view)
	{	
		glDisable(GL_DEPTH_TEST);
		
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0.0f,x_scale,0.0f,y_scale,0.0f,z_scale);
		
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

		_gl_ortho_view=true;
	}
}

void glOrthoViewOff()
{
	if(_gl_ortho_view)
	{	
		glEnable(GL_DEPTH_TEST);

		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();

		_gl_ortho_view=false;
	}
}



// void glPolygonMask(const unsigned char *mask)
// {
// 	unsigned char buf[128],c,b;
// 	int i=0;
	
// 	if(mask)
// 		while(i<128)
// 		{
// 			b=0x80;
// 			c=0;

// 			while(b)
// 			{
// 				if(*mask++)
// 					c|=b;
// 				b>>=1;
// 			}
// 			buf[i++]=c;
// 		}
// 	else
// 		memset(buf,0xFF,128);
	
// 	glPolygonStipple(buf);
// }





void glResetEnviroment()
{
	int i;

	glView=&(glDefaultView=glViewPort());
	glSetCamera(&(glDefaultCam=glCamera()));	
	glRemoveAllLights();

	glDefaultMat.flags=GLM_FACETED;
	glDefaultMat.ambient=RGBACOLOR(0.2f,0.2f,0.2f,1.0f);
	glDefaultMat.diffuse=RGBACOLOR(0.8f,0.8f,0.8f,1.0f);
	glDefaultMat.specular=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);
	glDefaultMat.emission=RGBACOLOR(0.0f,0.0f,0.0f,1.0f);
	glDefaultMat.shininess=0.0f;
	glDefaultMat.texture_rgb=0;
	glDefaultMat.texture_luminance=0;
	glDefaultMat.mapping=0;
	glMaterial(&glDefaultMat);

	glOrthoViewOff();
	
	glShadeModel(GL_SMOOTH);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,1);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,0);

	glDepthMask(1);
	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
	glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
	glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);
	glHint(GL_FOG_HINT,GL_NICEST);
	
	glAlphaFunc(GL_GREATER,0);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	
	if(glFlags&GLW_STENCILBUF)
	{
		glStencilFunc(GL_NEVER,1,0);
		glStencilOp(GL_REPLACE,GL_KEEP,GL_KEEP);
	}

	glEnable(GL_ALPHA_TEST);
	glDisable(GL_BLEND);
	for(i=0;i<GL_MAX_CLIP_PLANES;i++)
	{
		glDisable(GL_CLIP_PLANE0+i);
		glClipPlane(i,0,0,0,0);
	}
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_DITHER);
	glDisable(GL_FOG);
	glDisable(GL_LIGHTING);
	glEnable(GL_POINT_SMOOTH);
	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_POLYGON_SMOOTH);
	glDisable(GL_POLYGON_STIPPLE);
	// glPolygonMask(NULL);
	glDisable(GL_SCISSOR_TEST);
	glDisable(GL_STENCIL_TEST);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_TEXTURE_GEN_T);
	glDisable(GL_TEXTURE_GEN_S);
}




