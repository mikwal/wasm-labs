#ifndef FMODFILECALLBACKS_H
#define FMODFILECALLBACKS_H



unsigned int _memfile_open(const char *name);
void _memfile_close(unsigned int handle);
int _memfile_read(void *buf,int size,unsigned int handle);
int _memfile_seek(unsigned int handle,int pos,signed char mode);
int _memfile_tell(unsigned int handle);

unsigned int _stream_open(const char *name);
void _stream_close(unsigned int handle);
int _stream_read(void *buf,int size,unsigned int handle);
int _stream_seek(unsigned int handle,int pos,signed char mode);
int _stream_tell(unsigned int handle);


#define FSOUND_FILE_CALLBACKS_LOAD_TO_MEMORY		_memfile_open,_memfile_close,_memfile_read,_memfile_seek,_memfile_tell
#define FSOUND_FILE_CALLBACKS_STREAM_FROM_FILE		_stream_open,_stream_close,_stream_read,_stream_seek,_stream_tell


#endif