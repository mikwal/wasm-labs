#ifndef __ps_flare_h__
#define __ps_flare_h__

#include "glBase.h"
#include "particle_flare.h"

class PS_Flare {
public:
	PS_Flare();
	~PS_Flare();
	void Init(VECTOR *_pos, VECTOR *_size, RGBACOLOR *_basecolor);
	void Live();
	void Render();
	inline void SetSpeed(float f) {speed = f;}
private:
	VECTOR pos;
	VECTOR size;
	RGBACOLOR basecolor;
	int nDensity;
	Flare *flare;
	float speed;
};

#endif