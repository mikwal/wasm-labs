#include "glBase.h"
#include "ps_ye.h"

#define _MAP_H 64
#define _MAP_W 64

PS_YE::PS_YE() {
	
};

PS_YE::~PS_YE() {
	for(int i=0;i<NUM_MAPS;i++) {
		delete [] map[i].pPosPlane;
		delete [] map[i].pPosCircle;
		delete [] map[i].pPosSquare;
		delete [] map[i].pPosHelix;
	}
};


bool PS_YE::_loadMaps() {
	//LoadTGA(IOStream *s,unsigned char **data,int *w,int *h,int *bpp)
	IOStream *s;
	unsigned char *data;	
	int i;
	
	pg.Init(_MAP_W*_MAP_H);
	pg.gMod = 1;

	// MAP 0
	if((s=fopen("gfx.dat|partmap1.tga",F_READ))==NULL)
		return false;

	LoadTGA(s, &data, &map[0].w, &map[0].h, &map[0].bpp);

	map[0].pColor = new RGBACOLOR [map[0].w*map[0].h];
	map[0].pPosPlane = new VECTOR [map[0].w*map[0].h];
	map[0].pPosCircle = new VECTOR [map[0].w*map[0].h];
	map[0].pPosHelix = new VECTOR [map[0].w*map[0].h];

	for(i=0;i<map[0].w*map[0].h;i++) {
		map[0].pColor[i].r = float(data[i*4])*(1.0f/255.0f);
		map[0].pColor[i].g = float(data[i*4+1])*(1.0f/255.0f);
		map[0].pColor[i].b = float(data[i*4+2])*(1.0f/255.0f);
		map[0].pColor[i].a = float(data[i*4+3])*(1.0f/255.0f);
	}

	
	// MAP 1
	if((s=fopen("gfx.dat|partmap1.tga",F_READ))==NULL)
		return false;

	LoadTGA(s, &data, &map[1].w, &map[1].h, &map[1].bpp);

	map[1].pColor = new RGBACOLOR [map[1].w*map[1].h];
	map[1].pPosPlane = new VECTOR [map[1].w*map[1].h];
	map[1].pPosCircle = new VECTOR [map[1].w*map[1].h];
	map[1].pPosHelix = new VECTOR [map[1].w*map[1].h];
	
	for(i=0;i<map[1].w*map[1].h;i++) {
		map[1].pColor[i].r = float(data[i*4])*(1.0f/255.0f);
		map[1].pColor[i].g = float(data[i*4+1])*(1.0f/255.0f);
		map[1].pColor[i].b = float(data[i*4+2])*(1.0f/255.0f);
		map[1].pColor[i].a = float(data[i*4+3])*(1.0f/255.0f);
	}


	
	// s�tt current map
	cMap = nMap = &map[0];
	pCount = cMap->w*cMap->h;

	// genererar positioner
	float a;
	int j;
	int dx=0,dy=0;
	size = VECTOR(10,10,0);

	for(i=0;i<NUM_MAPS;i++) {
		dx=dy=0;
		float da=0.5f;
		for(j=0;j<pCount;j++) {
			a = float((j*2*PI)/pCount);
			map[i].pPosCircle[j].x = sinf(a) + float(rand() % 100)/100.0f*da-da/2;
			map[i].pPosCircle[j].y = cosf(a) + float(rand() % 100)/100.0f*da-da/2;
			map[i].pPosCircle[j].z = 20 + float(rand() % 100)/100.0f*da-da/2;

			if(dx==map[i].w) {
				dy++;
				dx=0;
			}
			map[i].pPosPlane[j].y=dy*(size.y/map[i].h) - size.y/2 + float(rand() % 100)/150.0f*da-da/2;		//risk f�r 0division?
			map[i].pPosPlane[j].x=dx*(size.x/map[i].w) - size.x/2 + float(rand() % 100)/150.0f*da-da/2;		//risk f�r 0division?
			map[i].pPosPlane[j].z=20 + float(rand() % 100)/150.0f*da-da/2;
			dx++;
			
			static float helixdepth = 1.0f;
			map[i].pPosHelix[j].x = sinf(a) + float(rand() % 100)/100.0f*da-da/2;
			map[i].pPosHelix[j].y = cosf(a) + float(rand() % 100)/100.0f*da-da/2;
			map[i].pPosHelix[j].z = 20 + j*helixdepth/pCount + float(rand() % 100)/100.0f*da-da/2;
		}

	}

	return true;
}

void PS_YE::Init(TEXTURE *_texture) {

	_loadMaps();
	pg.Init(&VERTEX(0,0,0),			// positio
		 &VERTEX(0,0,0),			// position B
		 &RGBACOLOR(1,1,1,1),		// color
		 &RGBACOLOR(1,1,1,1),	// color B
		 &VERTEX(0,0,-0.1f),		// gravity
		 &VERTEX(0,0,0),			// normal
		 &VECTOR(0.2f,0.2f,0.2f),		//size
		 pCount
		 );
	pg.texture = *_texture;
	for(int i=0; i<pCount; i++) {
		pg.Add(&cMap->pPosPlane[i], &cMap->pPosPlane[i], &cMap->pColor[i], &cMap->pColor[i],&VECTOR(1,1,0),&VECTOR(0,0,0));
		pg.list[i].dRandVel/=4;
	}
}

void PS_YE::Render() {
	pg.Render();
}

void PS_YE::Live() {
	if(_fade())cMap = nMap;

	static float x;
	x+=0.1f;
	pg.SetG(&VERTEX(sinf(x),cosf(x),20-sinf(x)));
	pg.Live(0);
}

void PS_YE::SetMoveType(int _type) {
	move_type = _type;
	switch(move_type) {
	case 1:
		//cirkel
		pg.SetPosBv(pCount, cMap->pPosCircle);
		break;
	case 2:
		//fyrkant
		
		break;
	case 3:
		//helix
		pg.SetPosBv(pCount, cMap->pPosHelix);
		break;
	default:
		//case0: ett plan
		pg.SetPosBv(pCount, cMap->pPosPlane);
		break;
	}
}

bool PS_YE::_fade() {
	if(cMap!=nMap) {
		
		pg.SetColorBv(pCount, nMap->pColor);
		return true;
	}
	return false;
}