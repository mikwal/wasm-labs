#include "glBase.h"
#include <mlib.h>



int LoadBMP(IOStream *s,unsigned char **data,int *w,int *h,int *bpp)
{
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;
	PALETTEENTRY pal[256];
	int c, i;


	s->read(&bmfh,sizeof(bmfh));
	s->read(&bmih,sizeof(bmih));

	if(bmfh.bfType!=0x4D42||bmih.biCompression!=0)
		return 1;

	*w=bmih.biWidth;
	*h=bmih.biHeight;
	*bpp=bmih.biBitCount;
	*data=new unsigned char[(*w)*(*h)*3];

	switch(*bpp)
	{
	case 8:
		s->read(pal,sizeof(pal));
		for(i=0;i<(*w)*(*h);i++)
		{
			c=s->get();
			(*data)[i*3+2]=pal[c].peRed;
			(*data)[i*3+1]=pal[c].peGreen;
			(*data)[i*3]=pal[c].peBlue;
		}
		*bpp=24;
		return 0;

	case 24:
		for(i=0;i<(*w)*(*h);i++)
		{
			(*data)[i*3+2]=s->get();
			(*data)[i*3+1]=s->get();
			(*data)[i*3]=s->get();
		}
		*bpp=24;
		return 0;

	default:
		delete [] (*data);
		*data=NULL;

		return 1;
	}
}



int LoadTGA(IOStream *s,unsigned char **data,int *w,int *h,int *bpp)
{
	unsigned char tgah[18];
	int i;

	s->read(tgah,18);
	
	if(tgah[2]!=2)
		return 1;

	*w=tgah[12]|(tgah[13]<<8);
	*h=tgah[14]|(tgah[15]<<8);
	*bpp=tgah[16];
	*data=new UCHAR[(*w)*(*h)*(*bpp/8)];


	switch(*bpp)
	{
	case 24:
		for(i=0;i<(*w)*(*h);i++)
		{
			(*data)[i*3+2]=s->get();
			(*data)[i*3+1]=s->get();
			(*data)[i*3]=s->get();
		}
		return 0;

	case 32:
		for(i=0;i<(*w)*(*h);i++)
		{
			(*data)[i*4+2]=s->get();
			(*data)[i*4+1]=s->get();
			(*data)[i*4]=s->get();
			(*data)[i*4+3]=s->get();
		}
		return 0;


	default:
		delete [] (*data);
		*data=NULL;

		return 1;
	}
}




int glLoadTextureFilter=GL_LINEAR_MIPMAP_NEAREST;


int glLoadTexture(const char *filename,TEXTURE *textures,int *formats,int ncopy)
{
	IOStream *s;
	unsigned char *data;	
	int w,h,bpp,type;

	switch(*((unsigned long*)strupr(fnext(filename))))
	{
	case WORD('T','G','A',0):
		if((s=fopen(filename,F_READ))==NULL)
			return 1;

		if(LoadTGA(s,&data,&w,&h,&bpp))
		{
			delete s;
			return 1;
		}
		break;

	case WORD('B','M','P',0):
		if((s=fopen(filename,F_READ))==NULL)
			return 1;

		if(LoadBMP(s,&data,&w,&h,&bpp))
		{
			delete s;
			return 1;
		}
		break;

	default:
			return 1;
	}

	delete s;

	for(int i=0;i<ncopy;i++)
	{
		unsigned char *format_data,*tmp=NULL;

		glFreeTexture(&textures[i]);

		switch(formats[i])
		{
		case GL_LUMINANCE:		
			unsigned char *src,*dest;
			int c,a;
			int src_c,dest_c;

			dest_c=bpp>>4;
			src_c=bpp>>3;

			c=w*h;
			tmp=new unsigned char[c*dest_c];
			src=data;
			dest=tmp;
			
			if(dest_c>1)
			{
			RGBA_to_LUMINANCE_ALPHA:
				a=src[0]*299;	// red
				a+=src[1]*587;	// green
				a+=src[2]*114;	// blue
				dest[0]=a/1000;	// to luminance
				dest[1]=src[3]; // alpha to alpha
				src+=src_c;
				dest+=dest_c;
				if(--c)
					goto RGBA_to_LUMINANCE_ALPHA;
				type=GL_LUMINANCE_ALPHA;
			}
			else
			{
			RGB_to_LUMINANCE:
				a=src[0]*299;	// red
				a+=src[1]*587;	// green
				a+=src[2]*114;	// blue
				dest[0]=a/1000;	// to luminance
				src+=src_c;
				dest+=dest_c;
				if(--c)
					goto RGB_to_LUMINANCE;

				type=GL_LUMINANCE;
			}
			format_data=tmp;
			break;
	

			case GL_RGB:				
				if(bpp==32)
					type=GL_RGBA;
				else 
					type=GL_RGB;
				format_data=data;
				break;

	
	
			default:
				formats[i]=-1;
				format_data=NULL;
				break;
		}

		if(format_data)
			switch(glLoadTextureFilter)
			{
			case GL_NEAREST_MIPMAP_NEAREST:
			case GL_NEAREST_MIPMAP_LINEAR:
			case GL_LINEAR_MIPMAP_NEAREST:
			case GL_LINEAR_MIPMAP_LINEAR:	
				glCreateMipmaps(&textures[i],w,h,type,format_data);
				glSetTextureFilter(glLoadTextureFilter);
				break;	
			
			default:
				glCreateTexture(&textures[i],w,h,type,format_data);
				glSetTextureFilter(glLoadTextureFilter);
				break;
			}
		
		delete [] tmp;
	}

	delete [] data;

	return 0;
}


int glCreateTexture(TEXTURE *texture,int w,int h,int type)
{
	unsigned char *tmp=NULL;
	int tmp_size;
	int new_w,new_h;
	
	new_w=round2(w);
	new_h=round2(h);

	tmp_size=new_w*new_h*4;
	tmp=new unsigned char[tmp_size];
	memset(tmp,0,tmp_size);

	glGenTextures(1,texture);
	glBindTexture(GL_TEXTURE_2D,*texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D,0,type,new_w,new_h,0,type,GL_UNSIGNED_BYTE,tmp);

	delete [] tmp;

	return 0;
}


int glCreateTexture(TEXTURE *texture,int w,int h,int type,const void *data)
{
	unsigned char *tmp=NULL;
	int tmp_size;
	int new_w,new_h;
	
	new_w=round2(w);
	new_h=round2(h);

	if(new_w!=w||new_h!=h)
	{
		tmp_size=new_w*new_h*4;
		tmp=new unsigned char[tmp_size];

		gluScaleImage(type,w,h,GL_UNSIGNED_BYTE,data,new_w,new_h,GL_UNSIGNED_BYTE,tmp);
		data=tmp;
	}

	glGenTextures(1,texture);
	glBindTexture(GL_TEXTURE_2D,*texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D,0,type,new_w,new_h,0,type,GL_UNSIGNED_BYTE,data);
	
	delete [] tmp;

	return 0;
}


int glCreateMipmaps(TEXTURE *texture,int w,int h,int type,const void *data)
{
	glGenTextures(1,texture);
	glBindTexture(GL_TEXTURE_2D,*texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	gluBuild2DMipmaps(GL_TEXTURE_2D,type,w,h,type,GL_UNSIGNED_BYTE,data);

	return 0;
}




void glSetTextureFilter(int filter)
{
	int magfilter;

	switch(filter)
	{
	case GL_NEAREST_MIPMAP_NEAREST:
	case GL_NEAREST_MIPMAP_LINEAR:
		magfilter=GL_NEAREST;
		break;

	case GL_LINEAR_MIPMAP_NEAREST:
	case GL_LINEAR_MIPMAP_LINEAR:
		magfilter=GL_LINEAR;
		break;
		
	default:
		magfilter=filter;
		break;
	}
	
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,magfilter);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,filter);
}


void glSetTextureWrap(int wrap)
{
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,wrap);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,wrap);
}









struct TEXTURELIST {
	TEXTURELIST *next;
	int ntextures;
	int ncopy;
	int	*formats;
	
	char *name;
	TEXTURE *texture;
};

static TEXTURELIST *_texture_list=NULL;


#define TEXTURENAME_SIZE	24
#define CMP_TEXTURENAME(s1,s2)	((unsigned long*)(s1))[0]==((unsigned long*)(s2))[0]&&	\
								((unsigned long*)(s1))[1]==((unsigned long*)(s2))[1]&&	\
								((unsigned long*)(s1))[2]==((unsigned long*)(s2))[2]&&	\
								((unsigned long*)(s1))[3]==((unsigned long*)(s2))[3]&&	\
								((unsigned long*)(s1))[4]==((unsigned long*)(s2))[4]&&	\
								((unsigned long*)(s1))[5]==((unsigned long*)(s2))[5]

static int _tl_get_index(const char *name,TEXTURELIST **retlist)
{
	int i;
	char *n,cmp[TEXTURENAME_SIZE];
	

	for(i=0;i<TEXTURENAME_SIZE;i++)
		if((cmp[i]=toupper(name[i]))==0)
			break;
	for(;i<TEXTURENAME_SIZE;i++)
		cmp[i]=0;

	if(_texture_list)
	{
		TEXTURELIST *list=_texture_list;
		do {
			n=((char*)list) + sizeof(TEXTURELIST) + list->ncopy*sizeof(int);

			for(i=0;i<list->ntextures;i++,n+=TEXTURENAME_SIZE)
				if(CMP_TEXTURENAME(n,cmp))
				{
					if(retlist)
						*retlist=list;
					
					return i;
				}

		} while(list=list->next);
	}
	
	return -1;
}



static int _tl_add_dir(const char *dirname,int *formats,int ncopy)
{
	TEXTURELIST *list=NULL;
	DirList d;
	int ntextures;
	int nsubdirs;
	int i;
	char tmp[MAX_PATH];
		
	// scan dir
	
	ntextures=0;
	nsubdirs=0;


	if(d.first(dirname))
		return 0;

	do {
		if(d.fattr&ATTR_DIR)
		{
			if(!(d.fname[0]=='.'&&d.fname[1]==0)&&!(d.fname[0]=='.'&&d.fname[1]=='.'&&d.fname[2]==0))
				nsubdirs++;	// count subdirs
		}
		else
			switch(*((unsigned long*)strupr(fnext(d.fname))))
			{
			case WORD('T','G','A',0):
			case WORD('B','M','P',0):
				ntextures++;	// and count imagefiles
				break;
			}

	} while(!d.next());
	
	
	if(ntextures)
	{
		// allocate list
		i=sizeof(TEXTURELIST)+ncopy*sizeof(int)+ntextures*(TEXTURENAME_SIZE+ncopy*sizeof(TEXTURE));
		list=(TEXTURELIST*)malloc(i);
		memset(list,0,i);
		
		// create list
		list->next=NULL;
		list->ncopy=ncopy;
		list->formats=(int*)(((char*)list)+sizeof(TEXTURELIST));
		
		for(i=0;i<ncopy;i++)
			list->formats[i]=formats[i];	

		list->name=((char*)list) + sizeof(TEXTURELIST) + ncopy*sizeof(int);
		list->texture=(TEXTURE*)(((char*)list) + sizeof(TEXTURELIST) + ncopy*sizeof(int) + ntextures*TEXTURENAME_SIZE);	
	}
	else
		if(nsubdirs==0)
			return 0;

	ntextures=0;
	nsubdirs=0;

	if(d.first(dirname))
		return 0;

	getcwd(tmp,MAX_PATH);
	chdir(dirname);

	do {
		if(d.fattr&ATTR_DIR)
		{
			if(!(d.fname[0]=='.'&&d.fname[1]==0)&&!(d.fname[0]=='.'&&d.fname[1]=='.'&&d.fname[2]==0))
				nsubdirs+=_tl_add_dir(d.fname,formats,ncopy);	// count files loaded in subdirs
		}
		else
			if(list&&_tl_get_index(d.fname,NULL)<0)
				if(glLoadTexture(d.fname,&list->texture[ntextures*ncopy],list->formats,ncopy)==0)
				{
					strcpy(&list->name[ntextures*TEXTURENAME_SIZE],strupr(d.fname));	
					ntextures++;	// count succesfully loaded textures
				}
	} while(!d.next());

	chdir(tmp);

	if(list)
		if(list->ntextures=ntextures)	// update the lists ntextures to the number actually loaded
		{
			if(_texture_list)		// add list
			{
				TEXTURELIST *plist=_texture_list;

				while(plist->next)
					plist=plist->next;
				
				plist->next=list;
			}
			else
				_texture_list=list;
		}
		else
			free(list);
		
	return nsubdirs+ntextures;
}


int glTextureListLoad(const char *dirname,int *formats,int ncopy)
{
	int i,j;

	// check formats
	for(i=0,j=0;i<ncopy;i++)
		switch(formats[i])
		{
		case GL_LUMINANCE:
			formats[j++]=formats[i];
			break;

		case GL_RGB:
			formats[j++]=formats[i];
			break;
		}

	ncopy=j;

	while(j<ncopy)
		formats[j++]=-1;

	if(ncopy)
		return _tl_add_dir(dirname,formats,ncopy);
	else
		return 0;
}


int glTextureListGet(const char *name,TEXTURE *texture,int format)
{
	TEXTURELIST *list;
	int i,j;
	
	i=_tl_get_index(name,&list);

	if(i<0)
		return 1;

	for(j=0;j<list->ncopy;j++)
		if(format==list->formats[j])
		{
			*texture=list->texture[(i*list->ncopy)+j];
			return 0;
		}
	
	return 1;
}


void glTextureListFreeAll()
{
	if(_texture_list)
	{
		TEXTURELIST *list=_texture_list,*prev;
		int i;

		do {
			for(i=0;i<list->ntextures*list->ncopy;i++)
				glFreeTexture(&list->texture[i]);
			prev=list;
			list=prev->next;
			free(prev);
		} while(list);
	}		
}