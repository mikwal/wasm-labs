#include "glBase.h"
#include "ScreenFilter.h"


signed char data_sync(FSOUND_STREAM *stream, void *buff, int len, int param);
extern FSOUND_STREAM *bgmusic;


glRootObject *data_sce;

static glCamera *cam;


ScreenFilter sf;

static TEXTURE frame,pil;
TEXTURE monitor;
int use_monitor=0;

static float time=0.0f;
static float screen_blend=0.0f;

static glFont font3;

extern glScene tunnel_scene;

static char popup_text[][40]=
{
	"",						
	"This is a coder",
	"This is a coder",
	"This is a coder",
	"This is a musician",
	"And this is a demo"
};

static float popup_time[]=
{
	0.0f,
	85.0f,95.0f,
	130.0f,140.0f,
	165.0f,175.0f,
	242.0f,252.0f,
	450.0f,480.0f,
	-1.0f
};

static float popup_speed[]=
{
	0.0,
	1.0f,-1.0f,
	1.0f,-1.0f,
	1.0f,-1.0f,
	1.0f,-1.0f,
	1.0f,-1.0f,
	-1.0f
};

static float popup_alpha=0;
static int popup_index=0;



void data_scenen_load()
{
	data_sce=glLoadScene("sce.dat|data_scenen.sce");

	sf.init(SF_HLINES,GL_ALPHA,64);

	glCreateTexture(&monitor,64,64,GL_RGB);
}


void data_scenen_free()
{
	sf.free();
	glFreeTexture(&monitor);
	delete data_sce;
}



void data_scenen_init()
{

	FSOUND_Stream_SetSynchCallback(bgmusic, data_sync, 0);
	glResetEnviroment();
	glAddAllLights(data_sce);

	glDefaultView.x=0;
	glDefaultView.y=80;
	glDefaultView.w=800;
	glDefaultView.h=440;
	glDefaultView.color=RGBACOLOR(0.55f,0.55f,1.0f,0.45f);
	glDefaultView.fog_mode=GL_EXP;
	glDefaultView.fog_dens=0.0035f;
	glSetViewPort(&glDefaultView);

	

	
	cam=(glCamera*)data_sce->getobject("Camera01");
	cam->color_bias=RGBCOLOR(0.25f,0.0f,0.1f);
	cam->color_scale=RGBCOLOR(0.75f,0.75f,1.0f);
	
	((glMeshObject*)data_sce->getobject("viktig_grej"))->material->texture_rgb=monitor;

	sf.color=RGBACOLOR(1.0f,1.0f,1.0f,0.1f);
	sf.min=0.1f;
	sf.max=1.0f;
	sf.scale=18.0f;
	sf.speed=0.8f;

	glTextureListGet("blue_frame.tga",&frame,GL_RGB);
	glSetTextureWrap(GL_CLAMP);
	glTextureListGet("pil.tga",&pil,GL_RGB);
	glSetTextureWrap(GL_CLAMP);

	font3=glFont("tahoma.bold.emboss.tga",60.0f,50.0f);
	font3.setspace(0.65f,0.65f);
	font3.setcolor(1,1,0,0);
}



int data_scenen_update()
{
	if(time==0.0f)
	{
		glClear(GL_COLOR_BUFFER_BIT);
		glSwapBuffer();
		glClear(GL_COLOR_BUFFER_BIT);
	}


	if(time>popup_time[popup_index+1]&&popup_time[popup_index+1]>=0.0f)
		popup_index++;


	popup_alpha+=SECVAL(popup_speed[popup_index]);
	if(popup_alpha>1.0f)
		popup_alpha=1.0f;
	else
		if(popup_alpha<0.0f)
			popup_alpha=0.0f;





	if(time<5.0f)
		screen_blend=(1.0f-time/5.0f)*(1.0f-time/5.0f);
	else
		if(time>495.0f)
		{
			float u=((time-495.0f)/5.0f)*((time-495.0f)/5.0f);
			
			screen_blend=u;
			glDefaultView.y=80*(1.0f-u);
			glDefaultView.h=440+160*u;
		}
		else
			screen_blend=0.0f;


	time+=SECVAL(6.0f);


	data_sce->update(time);


	sf.update();


	if(time>350)
		use_monitor=1;
	
	if(time>=495.0f)
		use_monitor=0;
	
	if(time>=500.0f)
	{
		glScene::current=&tunnel_scene;
		glScene::current->init();
	}
	
	tunnel_scene.update();



	return 0;
}


void data_scenen_render()
{
	if(use_monitor)	
			tunnel_scene.render();

	glSetCamera(cam);
	
	glView->clear();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glBlendOn();

	sf.render();

	glBlendOff();
	
	glApplyAllLights();
	glLightOn();

	data_sce->render(0);


	data_sce->render(1);

	glBlendOff();
	glLightOff();
	glOrthoViewOn(800,440,1);
	glBindTexture(&frame);
	glColor(1,1,1,1);
	
	glBltTexture(1,1,-1,-1,0,0,800,20);
	glBltTexture(0,0,1,1,0,420,800,20);

	glBlendOn();
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);


	font3.color.a=popup_alpha;
	font3.printf(50,390,popup_text[(popup_index+1)/2]);
	glBindTexture(&pil);
	glBltTexture(0,0,1,1,80,240,100,100);

	if(screen_blend>0.0f)
	{	
		glTextureOff();
		glColor4f(1,1,1,screen_blend);
		glBltTexture(0,0,1,1,0,0,800,440);
		glTextureOn();
	}


	glOrthoViewOff();
	glLightOn();
	glBlendOff();
	


	glSwapBuffer();
}

signed char data_sync(FSOUND_STREAM *stream, void *buff, int len, int param)
{
	if(buff) {                                     
		if(!strcmp((const char *) buff, (const char *) "s2firstbass")) {
			//f�rsta basslag
			screen_blend=1;
		}
		else if(!strcmp((const char *) buff, (const char *) "s2secbass")) {
			//andra (ensamma) basslag
			screen_blend=1;
		}
		else if(!strcmp((const char *) buff, (const char *) "s2bass")) {
			//phat beats startar		
			screen_blend=1;
		}
		else if(!strcmp((const char *) buff, (const char *) "s2fade")) {
			// b�rja utfadning o in�kning i sk�rmen
		}
		else if(!strcmp((const char *) buff, (const char *) "s3start")) {
			//use_monitor = 0;
			//glScene::current = &tunnel_scene;
			//glScene::current->init();
		}

	} else {
	
	}
	return true;
}