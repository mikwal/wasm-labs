#ifndef MATH3D_H
#define MATH3D_H

#include <stdlib.h>
#include <math.h>
#include <float.h>


#define PI			3.141592f
#define RAD_CIRCLE	6.283184f
#define DEG_CIRCLE	360.0f
#define RAD(d) (d*(RAD_CIRCLE/DEG_CIRCLE))
#define DEG(r) (r*(DEG_CIRCLE/RAD_CIRCLE))

#include "_sin_table.inc"
#define sinf(a) _sin_table[int(float((a))*(float(_SIN_TABLE_SIZE)/RAD_CIRCLE))&(_SIN_TABLE_SIZE-1)]
#define cosf(a) _sin_table[int(float((a)+PI/2.0f)*(float(_SIN_TABLE_SIZE)/RAD_CIRCLE))&(_SIN_TABLE_SIZE-1)]
#define tanf(a) (sinf(a)/cosf(a))

inline float randf(float max) {return float(rand())*(1.0f/RAND_MAX)*max;}
inline float rand2f(float max) {return -max+float(rand())*(2.0f/RAND_MAX)*max;}

inline int round2(int n) {int m; for(m=1; m<n; m<<=1); if(m-n<=n-(m>>1))return m; else return m>>1;}

#define ABS(s)				((s)<0?(-s):(s))
#define MIN(s1,s2)			((s1)>(s2)?(s2):(s1))
#define MAX(s1,s2)			((s1)>(s2)?(s1):(s2))
#define LIMIT(s,min,max)	((s)>(max)? (max):((s)<(min)?(min):(s)))

inline bool QUADRIC_EQUATION(float a,float b,float c,float &r1,float &r2) {float q = b*b - 4*a*c; if(q>=0) {float sq = sqrtf(q); float d = 1 / (2*a); r1 = ( -b + sq ) * d; r2 = ( -b - sq ) * d; return true; } else {return false;}}



struct VECTOR2D {
	union {float x,s;};
	union {float y,t;};

	inline VECTOR2D() {}
	inline VECTOR2D(float _all) {x=y=_all;}
	inline VECTOR2D(float _x,float _y) {x=_x; y=_y;}

	inline float operator()(int i) const {return (&x)[i];}
	inline float length() const {return sqrtf(x*x+y*y);}
	inline float dot(const VECTOR2D &v) const {return x*v.x+y*v.y;}
	inline float angle(const VECTOR2D &v) const {return acosf(dot(v)/(length()*v.length()));}
	inline float angle() const {return atan2f(y,x);}

	inline float &operator[](int i) {return (&x)[i];}
	inline VECTOR2D &operator+=(const VECTOR2D &v) {x+=v.x;	y+=v.y; return *this;}
	inline VECTOR2D &operator-=(const VECTOR2D &v) {x-=v.x; y-=v.y; return *this;}
	inline VECTOR2D &operator*=(float s) {x*=s; y*=s; return *this;}
	inline VECTOR2D &operator/=(float s) {x/=s; y/=s; return *this;}
	inline VECTOR2D &rotate(float r) {float s=sinf(r), c=cosf(r); return *this=VECTOR2D(x*c-y*s,y*c+x*s);}
};

inline VECTOR2D operator+(const VECTOR2D &v) {return v;}
inline VECTOR2D operator-(const VECTOR2D &v) {return VECTOR2D(-v.x,-v.y);}
inline VECTOR2D operator+(const VECTOR2D &v1,const VECTOR2D &v2) {return VECTOR2D(v1.x+v2.x,v1.y+v2.y);}
inline VECTOR2D operator-(const VECTOR2D &v1,const VECTOR2D &v2) {return VECTOR2D(v1.x-v2.x,v1.y-v2.y);}
inline VECTOR2D operator*(const VECTOR2D &v,float s) {return VECTOR2D(v.x*s,v.y*s);}
inline VECTOR2D operator/(const VECTOR2D &v,float s) {return VECTOR2D(v.x/s,v.y/s);}

inline int operator==(const VECTOR2D &v1,const VECTOR2D &v2) {return ((v1.x*v1.x+v1.y*v1.y)==(v2.x*v2.x+v2.y*v2.y));}
inline int operator!=(const VECTOR2D &v1,const VECTOR2D &v2) {return ((v1.x*v1.x+v1.y*v1.y)!=(v2.x*v2.x+v2.y*v2.y));}
inline int operator>(const VECTOR2D &v1,const VECTOR2D &v2) {return ((v1.x*v1.x+v1.y*v1.y)>(v2.x*v2.x+v2.y*v2.y));}
inline int operator>=(const VECTOR2D &v1,const VECTOR2D &v2) {return ((v1.x*v1.x+v1.y*v1.y)>=(v2.x*v2.x+v2.y*v2.y));}
inline int operator<(const VECTOR2D &v1,const VECTOR2D &v2) {return ((v1.x*v1.x+v1.y*v1.y)<(v2.x*v2.x+v2.y*v2.y));}
inline int operator<=(const VECTOR2D &v1,const VECTOR2D &v2) {return ((v1.x*v1.x+v1.y*v1.y)<=(v2.x*v2.x+v2.y*v2.y));}

typedef VECTOR2D TEXCOORD;





struct VECTOR3D {
	union {float x,r;};
	union {float y,g;};
	union {float z,b;};

	inline VECTOR3D() {}
	inline VECTOR3D(float _all) {x=y=z=_all;}
	inline VECTOR3D(float _x,float _y,float _z) {x=_x; y=_y; z=_z;}
	
	inline float operator()(int i) const {return (&x)[i];}
	inline float length() const {return sqrtf(x*x+y*y+z*z);}
	inline float dot(const VECTOR3D &v) const {return x*v.x+y*v.y+z*v.z;}
	inline VECTOR3D cross(const VECTOR3D &v) const {return VECTOR3D((y*v.z)-(z*v.y),(z*v.x)-(x*v.z),(x*v.y)-(y*v.x));}
	inline float angle(const VECTOR3D &v) const {return acosf(dot(v)/(length()*v.length()));}
	
	inline float &operator[](int i) {return (&x)[i];}
	inline VECTOR3D &operator+=(const VECTOR3D &v) {x+=v.x; y+=v.y; z+=v.z; return *this;}
	inline VECTOR3D &operator-=(const VECTOR3D &v) {x-=v.x; y-=v.y; z-=v.z; return *this;}
	inline VECTOR3D &operator*=(float s) {x*=s;y*=s;z*=s;return *this;}
	inline VECTOR3D &operator*=(const struct MATRIX4D &m);
	inline VECTOR3D &operator/=(float s) {x/=s; y/=s; z/=s; return *this;}
	inline VECTOR3D &rotate(float r,const VECTOR3D &v);
};

inline VECTOR3D operator+(const VECTOR3D &v) {return v;}
inline VECTOR3D operator-(const VECTOR3D &v) {return VECTOR3D(-v.x,-v.y,-v.z);}
inline VECTOR3D operator+(const VECTOR3D &v1,const VECTOR3D &v2) {return VECTOR3D(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z);}
inline VECTOR3D operator-(const VECTOR3D &v1,const VECTOR3D &v2) {return VECTOR3D(v1.x-v2.x,v1.y-v2.y,v1.z-v2.z);}
inline VECTOR3D operator*(const VECTOR3D &v,float s){return VECTOR3D(v.x*s,v.y*s,v.z*s);}
inline VECTOR3D operator/(const VECTOR3D &v,float s){return VECTOR3D(v.x/s,v.y/s,v.z/s);}

inline int operator==(const VECTOR3D &v1,const VECTOR3D &v2) {return ((v1.x*v1.x+v1.y*v1.y+v1.z*v1.z)==(v2.x*v2.x+v2.y*v2.y+v2.z*v2.z));}
inline int operator!=(const VECTOR3D &v1,const VECTOR3D &v2) {return ((v1.x*v1.x+v1.y*v1.y+v1.z*v1.z)!=(v2.x*v2.x+v2.y*v2.y+v2.z*v2.z));}
inline int operator>(const VECTOR3D &v1,const VECTOR3D &v2) {return ((v1.x*v1.x+v1.y*v1.y+v1.z*v1.z)>(v2.x*v2.x+v2.y*v2.y+v2.z*v2.z));}
inline int operator>=(const VECTOR3D &v1,const VECTOR3D &v2) {return ((v1.x*v1.x+v1.y*v1.y+v1.z*v1.z)>=(v2.x*v2.x+v2.y*v2.y+v2.z*v2.z));}
inline int operator<(const VECTOR3D &v1,const VECTOR3D &v2) {return ((v1.x*v1.x+v1.y*v1.y+v1.z*v1.z)<(v2.x*v2.x+v2.y*v2.y+v2.z*v2.z));}
inline int operator<=(const VECTOR3D &v1,const VECTOR3D &v2) {return ((v1.x*v1.x+v1.y*v1.y+v1.z*v1.z)<=(v2.x*v2.x+v2.y*v2.y+v2.z*v2.z));}

#define Vx		VECTOR3D(1.0f,0.0f,0.0f)
#define Vy		VECTOR3D(0.0f,1.0f,0.0f)
#define Vz		VECTOR3D(0.0f,0.0f,1.0f)

typedef VECTOR3D VECTOR, VERTEX, RGBCOLOR;







struct VECTOR4D {
	union {float x,r;};
	union {float y,g;};
	union {float z,b;};
	union {float w,a;};

	inline VECTOR4D() {}
	inline VECTOR4D(float _all) {x=y=z=w=_all;}
	inline VECTOR4D(float _x,float _y,float _z,float _w) {x=_x;y=_y;z=_z;w=_w;}

	inline float operator()(int i) const {return (&x)[i];}

	inline float &operator[](int i) {return (&x)[i];}
	inline VECTOR4D &operator+=(const VECTOR4D &v) {x+=v.x; y+=v.y; z+=v.z; w+=v.w; return *this;}
	inline VECTOR4D &operator-=(const VECTOR4D &v) {x-=v.x; y-=v.y; z-=v.z; w-=v.w; return *this;}
	inline VECTOR4D &operator*=(float s) {x*=s; y*=s; z*=s; w*=s; return *this;}
	inline VECTOR4D &operator/=(float s) {x/=s; y/=s; z/=s; w/=s; return *this;}
};

inline VECTOR4D operator+(const VECTOR4D &v) {return v;}
inline VECTOR4D operator-(const VECTOR4D &v) {return VECTOR4D(-v.x,-v.y,-v.z,-v.w);}
inline VECTOR4D operator+(const VECTOR4D &v1,const VECTOR4D &v2) {return VECTOR4D(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z,v1.w+v2.w);}
inline VECTOR4D operator-(const VECTOR4D &v1,const VECTOR4D &v2) {return VECTOR4D(v1.x-v2.x,v1.y-v2.y,v1.z-v2.z,v1.w-v2.w);}
inline VECTOR4D operator*(const VECTOR4D &v,float s) {return VECTOR4D(v.x*s,v.y*s,v.z*s,v.w*s);}
inline VECTOR4D operator/(const VECTOR4D &v,float s) {return VECTOR4D(v.x/s,v.y/s,v.z/s,v.w/s);}

typedef VECTOR4D RGBACOLOR;







struct MATRIX4D {
	union {
		struct {
			float e00, e01, e02, e03;
			float e10, e11, e12, e13;
			float e20, e21, e22, e23;
			float e30, e31, e32, e33;
		};
		struct {
			VECTOR3D x; float x_w;
			VECTOR3D y; float y_w;
			VECTOR3D z; float z_w;
			VECTOR3D p; float p_w;
		};
	};

	inline MATRIX4D() {}
	inline MATRIX4D(float _all) {e00=e01=e02=e03=e10=e11=e12=e13=e20=e21=e22=e23=e30=e31=e32=e33=_all;}
	inline MATRIX4D(float _e00,float _e10,float _e20,float _e30,float _e01,float _e11,float _e21,float _e31,float _e02,float _e12,float _e22,float _e32,float _e03,float _e13,float _e23,float _e33) {e00=_e00;e01=_e01;e02=_e02;e03=_e03;e10=_e10;e11=_e11;e12=_e12;e13=_e13;e20=_e20;e21=_e21;e22=_e22;e23=_e23;e30=_e30;e31=_e31;e32=_e32;e33=_e33;}
	inline MATRIX4D(const VECTOR3D &_x,const VECTOR3D &_y,const VECTOR3D &_z,const VECTOR3D &_p) {x=_x; x_w=0.0f;y=_y; y_w=0.0f;z=_z; z_w=0.0f;p=_p; p_w=1.0f;}
	
	inline float operator()(int i, int j) const {return (&e00)[i*4+j];}

	inline float *operator[](int i) {return &((&e00)[i*4]);}
	inline MATRIX4D operator*=(const MATRIX4D &m);
	
	inline MATRIX4D position() {return MATRIX4D(Vx,Vy,Vz,p);}
	inline MATRIX4D rotation() {return MATRIX4D(x/x.length(),y/y.length(),z/z.length(),0.0f);}
	inline MATRIX4D	scale()	{return MATRIX4D(x.length(),0.0f,0.0f,0.0f,0.0f,y.length(),0.0f,0.0f,0.0f,0.0f,z.length(),0.0f,0.0f,0.0f,0.0f,1.0f);}
	inline MATRIX4D scaledrotation() {return MATRIX4D(x,y,z,0.0f);}
};

inline MATRIX4D operator*(const MATRIX4D &m1,const MATRIX4D &m2) {return MATRIX4D(m1.e00*m2.e00+m1.e10*m2.e01+m1.e20*m2.e02+m1.e30*m2.e03,m1.e00*m2.e10+m1.e10*m2.e11+m1.e20*m2.e12+m1.e30*m2.e13,m1.e00*m2.e20+m1.e10*m2.e21+m1.e20*m2.e22+m1.e30*m2.e23,m1.e00*m2.e30+m1.e10*m2.e31+m1.e20*m2.e32+m1.e30*m2.e33,m1.e01*m2.e00+m1.e11*m2.e01+m1.e21*m2.e02+m1.e31*m2.e03,m1.e01*m2.e10+m1.e11*m2.e11+m1.e21*m2.e12+m1.e31*m2.e13,m1.e01*m2.e20+m1.e11*m2.e21+m1.e21*m2.e22+m1.e31*m2.e23,m1.e01*m2.e30+m1.e11*m2.e31+m1.e21*m2.e32+m1.e31*m2.e33,m1.e02*m2.e00+m1.e12*m2.e01+m1.e22*m2.e02+m1.e32*m2.e03,m1.e02*m2.e10+m1.e12*m2.e11+m1.e22*m2.e12+m1.e32*m2.e13,m1.e02*m2.e20+m1.e12*m2.e21+m1.e22*m2.e22+m1.e32*m2.e23,m1.e02*m2.e30+m1.e12*m2.e31+m1.e22*m2.e32+m1.e32*m2.e33,m1.e03*m2.e00+m1.e13*m2.e01+m1.e23*m2.e02+m1.e33*m2.e03,m1.e03*m2.e10+m1.e13*m2.e11+m1.e23*m2.e12+m1.e33*m2.e13,m1.e03*m2.e20+m1.e13*m2.e21+m1.e23*m2.e22+m1.e33*m2.e23,m1.e03*m2.e30+m1.e13*m2.e31+m1.e23*m2.e32+m1.e33*m2.e33);}

#define Midentity MATRIX4D(Vx,Vy,Vz,0.0f)
inline MATRIX4D Mtranslate(const VECTOR &v) {return MATRIX4D(Vx,Vy,Vz,v);}
inline MATRIX4D Mtranslate(float x,float y,float z) {return Mtranslate(VECTOR3D(x,y,z));}
inline MATRIX4D Mscale(const VECTOR &v) {return MATRIX4D(v.x,0.0f,0.0f,0.0f,0.0f,v.y,0.0f,0.0f,0.0f,0.0f,v.z,0.0f,0.0f,0.0f,0.0f,1.0f);}
inline MATRIX4D Mscale(float x,float y,float z) {return Mscale(VECTOR3D(x,y,z));}
inline MATRIX4D Mrotate(float r,const VECTOR3D &v) {float l=v.length(); float x=v.x/l, y=v.y/l, z=v.z/l; float s=sinf(r), c=cosf(r), c2=1.0f-c; return MATRIX4D(x*x*c2+c,x*y*c2-z*s,x*z*c2+y*s,0.0f,y*x*c2+z*s,y*y*c2+c,y*z*c2-x*s,0.0f,z*x*c2-y*s,z*y*c2+x*s,z*z*c2+c,0.0f,0.0f,0.0f,0.0f,1.0f);}
inline MATRIX4D Mrotate(float r,float x,float y,float z) {return Mrotate(r,VECTOR3D(x,y,z));}
inline MATRIX4D Minverse(const MATRIX4D &m) {return Mscale(1.0f/(m.x.dot(m.x)),1.0f/(m.y.dot(m.y)),1.0f/(m.z.dot(m.z)))*MATRIX4D(m.e00,m.e01,m.e02,-(m.e30*m.e00 +m.e31*m.e01 +m.e32*m.e02),m.e10,m.e11,m.e12,-(m.e30*m.e10 +m.e31*m.e11 +m.e32*m.e12),m.e20,m.e21,m.e22,-(m.e30*m.e20 +m.e31*m.e21 +m.e32*m.e22),0.0f,0.0f,0.0f,1.0f);}
inline MATRIX4D Mcamera(const MATRIX4D &m) {return MATRIX4D(m.x.x,m.x.y,m.x.z,-m.x.x*m.p.x-m.x.y*m.p.y-m.x.z*m.p.z,m.z.x,m.z.y,m.z.z,-m.z.x*m.p.x-m.z.y*m.p.y-m.z.z*m.p.z,m.y.x,m.y.y,m.y.z,-m.y.x*m.p.x-m.y.y*m.p.y-m.y.z*m.p.z,0.0f,0.0f,0.0f,1.0f);}
inline MATRIX4D Mtarget(const VECTOR3D &p,const VECTOR3D &t) {VECTOR3D dir,up,right; if(p.x==t.x&&p.y==t.y&&p.z==t.z) return Mtranslate(p); dir=t-p; dir/=dir.length(); if(dir.x==0.0f&&dir.z==0.0f) {up=VECTOR3D(-dir.y,0.0f,0.0f); up/=up.length();} else up=Vy; right=dir.cross(up); right/=right.length(); up=right.cross(dir); up/=up.length();return MATRIX4D(-right,-dir,up,p);}
inline MATRIX4D MtargetVx(const VECTOR3D &p,const VECTOR3D &t) {VECTOR3D dir,up,right; if(p.x==t.x&&p.y==t.y&&p.z==t.z) return Mtranslate(p); dir=t-p; dir/=dir.length(); if(dir.x==0.0f&&dir.z==0.0f) {up=VECTOR3D(-dir.y,0.0f,0.0f); up/=up.length();} else up=Vy; right=dir.cross(up); right/=right.length(); up=right.cross(dir); up/=up.length();return MATRIX4D(-dir,up,-right,p);}
inline MATRIX4D MtargetVxflipped(const VECTOR3D &p,const VECTOR3D &t) {VECTOR3D dir,up,right; if(p.x==t.x&&p.y==t.y&&p.z==t.z) return Mtranslate(p); dir=t-p; dir/=dir.length(); if(dir.x==0.0f&&dir.z==0.0f) {up=VECTOR3D(-dir.y,0.0f,0.0f); up/=up.length();} else up=Vy; right=dir.cross(up); right/=right.length(); up=right.cross(dir); up/=up.length();	return MATRIX4D(-dir,up,-right,p);}
inline MATRIX4D MtargetVy(const VECTOR3D &p,const VECTOR3D &t) {VECTOR3D dir,up,right; if(p.x==t.x&&p.y==t.y&&p.z==t.z) return Mtranslate(p); dir=t-p; dir/=dir.length(); if(dir.x==0.0f&&dir.z==0.0f) {up=VECTOR3D(-dir.y,0.0f,0.0f); up/=up.length();} else up=Vy; right=dir.cross(up); right/=right.length(); up=right.cross(dir); up/=up.length();	return MATRIX4D(-up,-dir,-right,p);}
inline MATRIX4D MtargetVyflipped(const VECTOR3D &p,const VECTOR3D &t) {VECTOR3D dir,up,right; if(p.x==t.x&&p.y==t.y&&p.z==t.z) return Mtranslate(p); dir=t-p; dir/=dir.length(); if(dir.x==0.0f&&dir.z==0.0f) {up=VECTOR3D(-dir.y,0.0f,0.0f); up/=up.length();} else up=Vy; right=dir.cross(up); right/=right.length(); up=right.cross(dir); up/=up.length();return MATRIX4D(-up,-dir,-right,p);}
inline MATRIX4D MtargetVz(const VECTOR3D &p,const VECTOR3D &t) {VECTOR3D dir,up,right; if(p.x==t.x&&p.y==t.y&&p.z==t.z) return Mtranslate(p); dir=t-p; dir/=dir.length(); if(dir.x==0.0f&&dir.z==0.0f) {up=VECTOR3D(-dir.y,0.0f,0.0f); up/=up.length();} else up=Vy; right=dir.cross(up); right/=right.length(); up=right.cross(dir); up/=up.length();return MATRIX4D(right,up,-dir,p);}
inline MATRIX4D MtargetVzflipped(const VECTOR3D &p,const VECTOR3D &t) {VECTOR3D dir,up,right; if(p.x==t.x&&p.y==t.y&&p.z==t.z) return Mtranslate(p); dir=t-p; dir/=dir.length(); if(dir.x==0.0f&&dir.z==0.0f) {up=VECTOR3D(-dir.y,0.0f,0.0f); up/=up.length();} else up=Vy; right=dir.cross(up); right/=right.length(); up=right.cross(dir); up/=up.length();	return MATRIX4D(right,up,-dir,p);}

typedef MATRIX4D MATRIX;



inline VECTOR3D operator*(const VECTOR3D &v,const MATRIX4D &m) {return VECTOR3D(v.x*m.e00+v.y*m.e10+v.z*m.e20+m.e30,v.x*m.e01+v.y*m.e11+v.z*m.e21+m.e31,v.x*m.e02+v.y*m.e12+v.z*m.e22+m.e32);}
inline VECTOR3D &VECTOR3D::operator*=(const MATRIX4D &m) {return (*this)=(*this)*m;}
inline VECTOR3D &VECTOR3D::rotate(float r,const VECTOR3D &v) {VECTOR3D n=v/v.length(); float s=dot(n); return (*this)=((*this)-n*s)*cosf(r)+cross(n)*sinf(r)+n*s;}
inline MATRIX4D MATRIX4D::operator*=(const MATRIX4D &m) {return (*this)=(*this)*m;}















struct CLIPBOX {
	VECTOR3D max,min;

	inline CLIPBOX() {}
	inline CLIPBOX(const VECTOR3D &_min,const VECTOR3D &_max) {min=_min; max=_max;}
	
	inline void getpoints(VECTOR3D *p) {p[3]=min; p[0].x=min.x; p[0].y=max.y; p[0].z=min.z; p[1].x=max.x; p[1].y=max.y; p[1].z=min.z; p[2].x=max.x; p[2].y=min.y; p[2].z=min.z; p[5]=max; p[4].x=min.x; p[4].y=max.y; p[4].z=max.z; p[6].x=max.x; p[6].y=min.y; p[6].z=max.z; p[7].x=min.x; p[7].y=min.y; p[7].z=max.z;}
};




struct RAY
{
	VECTOR3D pos,dir;
	
	inline RAY() {}
	inline RAY(const VECTOR3D &p1,const VECTOR3D &p2) {pos=p1; dir=p2-p1;}
	
	inline VECTOR3D operator()(float u) const {return pos+dir*u;} 
	inline VECTOR3D closest(const VECTOR3D &p) const {float d = dir.length(); VECTOR c = p - pos; VECTOR v = dir/d; float t = v.dot(c); return t<-FLT_EPSILON?pos:(t>d?pos+dir:pos+v*t);		}
};

inline RAY operator>>(const VECTOR3D &p1,const VECTOR3D &p2) {return RAY(p1,p2);}
inline RAY operator<<(const VECTOR3D &p2,const VECTOR3D &p1) {return RAY(p1,p2);}



struct SPHERE {
	VECTOR3D pos;
	float radius;

	inline SPHERE() {}
	inline SPHERE(const VECTOR3D &_pos,float _radius) {pos=_pos;radius=_radius;}
	inline SPHERE(float x,float y,float z,float _radius) {pos=VECTOR3D(x,y,z);radius=_radius;}

	inline float intersect(const RAY &ray) const {float a = ray.dir.x*ray.dir.x + ray.dir.y*ray.dir.y + ray.dir.z*ray.dir.z; float b = (2.0f*ray.dir.x*(ray.pos.x-pos.x) + 2.0f*ray.dir.y*(ray.pos.y-pos.y) + 2.0f*ray.dir.z*(ray.pos.z-pos.z))/a; float c = ((ray.pos.x-pos.x)*(ray.pos.x-pos.x) + (ray.pos.y-pos.y)*(ray.pos.y-pos.y) + (ray.pos.z-pos.z)*(ray.pos.z-pos.z) - radius*radius)/a; float det = b*b*0.25f-c; if(det<-FLT_EPSILON) return -1.0f; else {det=sqrtf(det); if(-b*0.5f-det>=0.0f) return (-b*0.5f-det); else return (-b*0.5f+det);}}
	inline float intersect(const VECTOR3D &new_pos,const SPHERE &sphere2,const VECTOR3D &sphere2_new_pos,VECTOR3D *collision=NULL) const {float u1,u2; VECTOR ab=sphere2.pos - pos; VECTOR vab = sphere2_new_pos - new_pos; float r = radius + sphere2.radius; float a = vab.dot(vab); float b = 2*vab.dot(ab); float c = ab.dot(ab) - r*r; if(ab.dot(ab) <= r*r ) return 0.0f; if(QUADRIC_EQUATION(a,b,c,u1,u2)) {u1=(u1>u2?u2:u1); if(collision) {VECTOR v1=pos+new_pos*u1;VECTOR v2=(sphere2.pos+sphere2_new_pos*u1)-v1;*collision=v1+(v2*radius)/v2.length();}return u1;}else return -1.0f;}
};


struct PLANE {
	union {struct {float A,B,C;}; struct {VECTOR3D n;};};
	union {float d,D;};

	inline PLANE() {}
	inline PLANE(float _A,float _B,float _C,float _D) {A=_A;B=_B;C=_C;D=_D;}
	inline PLANE(const VECTOR3D &_n,const VECTOR3D &_p) {n=_n; d=_n.dot(_p);}
	
	inline float substitute(const VECTOR3D &v) const {return A*v.x+B*v.y+C*v.z-D;}

	inline float intersect(const RAY &ray) const {float dot=ray.dir.dot(n); if(dot<FLT_EPSILON&&dot>-FLT_EPSILON) return -1.0f; else return n.dot(n*d-ray.pos)/dot;}
	inline float intersect(const SPHERE &sphere,const VECTOR &sphere_new_pos,VECTOR3D *collision=NULL) const {VECTOR3D sn=-(n/n.length())*sphere.radius; float u; u=intersect(sphere.pos>>sphere.pos+sn); if(u>=0.0f&&u<=1.0f) return 0.0f; else {RAY ray=sphere.pos+sn>>sphere_new_pos+sn; u=intersect(ray); if(collision) *collision=ray(u); return u;}}
};


struct TRIPLANE  {
	PLANE plane;
	PLANE edge1,edge2,edge3;
	VECTOR p1,p2,p3;

	inline TRIPLANE() {}
	inline TRIPLANE(const VECTOR3D &_p1,const VECTOR3D &_p2,const VECTOR3D &_p3) {plane=PLANE(((_p3-_p1).cross(_p2-_p1))/((_p3-_p1).cross(_p2-_p1)).length(),_p1);edge1=PLANE(((_p2-_p1).cross(plane.n))/((_p2-_p1).cross(plane.n)).length(),_p1);edge2=PLANE(((_p3-_p2).cross(plane.n))/((_p3-_p2).cross(plane.n)).length(),_p2);edge3=PLANE(((_p1-_p3).cross(plane.n))/((_p1-_p3).cross(plane.n)).length(),_p3);p1=_p1;p2=_p2;p3=_p3;}

	float intersect(const RAY &ray) const {float u=plane.intersect(ray);if(u>=0.0f) {VECTOR3D v=ray(u);if(edge1.substitute(v)>=0.0f&&edge2.substitute(v)>=0.0f&&edge3.substitute(v)>=0.0f) return u; else return -1.0f;} else return -1.0f;}
	inline float intersect(const SPHERE &sphere,const VECTOR &sphere_new_pos,VECTOR3D *collision=NULL) const {VECTOR3D sn=-(plane.n/plane.n.length())*sphere.radius; float u; u=intersect(sphere.pos>>sphere.pos+sn); if(u>=0.0f&&u<=1.0) return 0.0f; RAY ray=sphere.pos+sn>>sphere_new_pos+sn; u=plane.intersect(ray); VECTOR3D v=ray(u); if(u>=0.0f) {if(edge1.substitute(v)>=0.0f&&edge2.substitute(v)>=0.0f&&edge3.substitute(v)>=0.0f) {if(collision) *collision=v;return u;} else {VECTOR3D c1=(p1>>p2).closest(v); VECTOR3D c2=(p2>>p3).closest(v); VECTOR3D c3=(p3>>p1).closest(v); v+=MIN(MIN(c1-v,c2-v),c3-v);ray=v>>v-(sphere_new_pos-sphere.pos);u=sphere.intersect(ray);if(collision) *collision=ray(-u); return u;}} else return -1.0f;}
};








struct FRUSTRUM {
	PLANE plane[6];

	inline PLANE &operator[](int i) {return plane[i];}
	inline int clip(const VECTOR3D &v) {return (plane[0].substitute(v)<-FLT_EPSILON||plane[1].substitute(v)<-FLT_EPSILON||plane[2].substitute(v)<-FLT_EPSILON||plane[3].substitute(v)<-FLT_EPSILON||plane[4].substitute(v)<-FLT_EPSILON||plane[5].substitute(v)<-FLT_EPSILON)?1:0;}
};











#endif