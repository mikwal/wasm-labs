#include "particle_flare.h"

Flare::Flare() {
}

Flare::~Flare() {
}

void Flare::Init(VECTOR *_pos, VECTOR *_size, RGBACOLOR *_basecolor, float _speed) {
	pos = *_pos;
	size = *_size;
	basecolor = *_basecolor;
	speed = _speed;
	amount=0;
}

void Flare::Live(float f) {
	amount+=speed*f;
}

void Flare::Render() {
	VECTOR _pos;
	
	glPushMatrix();
	glLoadIdentity();
	
	VECTOR c1,c2;

	// top right
	c1.x=size.x;
	c1.y=size.y;
	c1.z=0;
	c1.rotate(amount, VECTOR3D(0,0,1));

	// bottom right
	c2.x=size.x;
	c2.y=-size.y;
	c2.z=0;
	c1.rotate(amount, VECTOR3D(0,0,1));

	
	_pos = pos * Mcamera(glCam->matrix);
	glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(1,1); glVertex3f(_pos.x + c1.x, _pos.y + c1.y, _pos.z); // Top Right
		glTexCoord2f(0,1); glVertex3f(_pos.x - c2.x, _pos.y - c2.y, _pos.z); // Top Left
		glTexCoord2f(1,0); glVertex3f(_pos.x + c2.x, _pos.y + c2.y, _pos.z); // Bottom Right
		glTexCoord2f(0,0); glVertex3f(_pos.x - c1.x, _pos.y - c1.y, _pos.z); // Bottom Left
	glEnd();
	// special f�r flare
	glPopMatrix();

}