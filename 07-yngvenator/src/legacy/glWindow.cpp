#include "glBase.h"



HWND glhWnd=NULL;
HINSTANCE glhInstance=NULL;
HDC glhDC=NULL;
HGLRC glhRC=NULL; 


int glScreenWidth=0;
int glScreenHeight=0;
int glScreenBpp=0;
int glFlags=0;

unsigned char glKey[0xFF];
 

static WNDPROC _gl_user_wndproc=NULL;
static DEVMODE _display;


LRESULT CALLBACK _wndproc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam)
{
	switch (msg)
	{
	// case WM_ACTIVATE:
	// 	if(LOWORD(wparam))
	// 	{
	// 		glFlags|=GLW_ACTIVE;
	// 		if(glFlags&GLW_FULLSCREEN)
	// 			if(ChangeDisplaySettings(&_display,CDS_FULLSCREEN))
	// 				return glDestroyWindow("The requested fullscreen mode is not supported");
	// 	}	
	// 	else
	// 	{
	// 		glFlags&=~GLW_ACTIVE;
	// 		if(glFlags&GLW_FULLSCREEN)
	// 			ChangeDisplaySettings(NULL,0);
	// 	}
	// 	break;

	// case WM_KEYDOWN:
	// 	glKey[wparam]=0x01;				
	// 	break;

	// case WM_KEYUP:
	// 	glKey[wparam]=0x00;
	// 	break;

	// case WM_SYSCOMMAND:
	// 	switch (wparam)	
	// 	{
	// 	case SC_SCREENSAVE:
	// 	case SC_MONITORPOWER:
	// 		return 0;
	// 	}
	// 	break;

	// case WM_CLOSE:
	// 	return 0;
	}
	
	if(_gl_user_wndproc)
		return _gl_user_wndproc(hwnd,msg,wparam,lparam);
	else
		return DefWindowProc(hwnd,msg,wparam,lparam);
}





#define GL_CLASSNAME "Bongo Application"

int glCreateWindow(const char *title,int screen_w ,int screen_h ,int screen_bpp,int flags,WNDPROC wndproc)
{
	WNDCLASS wclass;
	DWORD wstyle;
	RECT wrect;
	PIXELFORMATDESCRIPTOR pfd;
	int pf;


	// glhInstance=GetModuleHandle(NULL);	
	glScreenWidth=screen_w;
	glScreenHeight=screen_h;
	glScreenBpp=screen_bpp;
	glFlags=flags&0xFF00;
	
	_gl_user_wndproc=wndproc;
	

	// wclass.style=CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	// wclass.lpfnWndProc=(WNDPROC)_wndproc;
	// wclass.cbClsExtra=0;
	// wclass.cbWndExtra=0;
	// wclass.hInstance=glhInstance;
	// wclass.hIcon=LoadIcon(NULL,IDI_WINLOGO);
	// wclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	// wclass.hbrBackground=NULL;
	// wclass.lpszMenuName=NULL;
	// wclass.lpszClassName=GL_CLASSNAME;

	// if(!RegisterClass(&wclass))
	// 	return glDestroyWindow("Failed to register the window class.");
	
	// if(glFlags&GLW_FULLSCREEN)
	// {
	// 	memset(&_display,0,sizeof(_display));
	// 	_display.dmSize=sizeof(_display);
	// 	_display.dmPelsWidth=glScreenWidth;
	// 	_display.dmPelsHeight=glScreenHeight;
	// 	_display.dmBitsPerPel=glScreenBpp;
	// 	_display.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

	// 	wstyle=WS_POPUP|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;
	// }
	// else
	// 	wstyle=WS_OVERLAPPED|WS_CAPTION|WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	// SetRect(&wrect,0,0,glScreenWidth,glScreenHeight);
	// AdjustWindowRect(&wrect,wstyle,0);

	// if((glhWnd=CreateWindow(GL_CLASSNAME,title,wstyle,CW_USEDEFAULT,CW_USEDEFAULT,
	// 	wrect.right-wrect.left,wrect.bottom-wrect.top,NULL,NULL,glhInstance,NULL))==NULL)
	// 	return glDestroyWindow("Cant create window.");

	// if((glhDC=GetDC(glhWnd))==NULL)
	// 	return glDestroyWindow("Cant create device context.");
	
	// memset(&pfd,0,sizeof(pfd));
    // pfd.nSize=sizeof(pfd); 
    // pfd.nVersion=1; 
    // pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	// pfd.iPixelType=PFD_TYPE_RGBA;
    // pfd.dwLayerMask=PFD_MAIN_PLANE; 
    // pfd.cColorBits=glScreenBpp; 
    // pfd.cDepthBits=glScreenBpp; 
	// if(GLW_STENCILBUF)
	// 	pfd.cStencilBits=1;
	
	// if((pf=ChoosePixelFormat(glhDC,&pfd))==0)
	// 	return glDestroyWindow("Cant find a suitable pixel format.");

	// if(SetPixelFormat(glhDC,pf,&pfd)==0)
	// 	return glDestroyWindow("Cant set pixel format.");

	// glhRC=wglCreateContext(glhDC);
	// if(glhRC==NULL)
	// 	return glDestroyWindow("Cant create rendering context.");

	// if(wglMakeCurrent(glhDC,glhRC)==0)
	// 	return glDestroyWindow("Cant activate rendering context.");

	// ShowWindow(glhWnd,SW_SHOW);
	// SetForegroundWindow(glhWnd);
	// SetFocus(glhWnd);

	SwapBuffers(glhDC);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	SwapBuffers(glhDC);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glPixelStorei(GL_PACK_ALIGNMENT,1); 

	return 0;
}



int glDestroyWindow(const char *msg)
{
	// if(glFlags&GLW_FULLSCREEN)
	// 	ChangeDisplaySettings(NULL,0);

	if(glhRC)
	{
		// wglMakeCurrent(NULL,NULL);
		// wglDeleteContext(glhRC);
		glhRC=NULL;
	}

	if(glhDC)
	{
		// ReleaseDC(glhWnd,glhDC);
		glhDC=NULL;
	}

	if(glhWnd)
	{
		// DestroyWindow(glhWnd);
		glhWnd=NULL;
	}

	// UnregisterClass(GL_CLASSNAME,glhInstance);

	glScreenWidth=0;
	glScreenHeight=0;
	glScreenBpp=0;
	glFlags=0;
	glhInstance=NULL;
	_gl_user_wndproc=NULL;


	// if(msg)
	// {
	// 	MessageBox(NULL,msg,"ERROR",MB_OK|MB_ICONEXCLAMATION);
	// 	return -1;
	// }
	// else
		return 0;
}

int OPF=0;
int PPF=0;