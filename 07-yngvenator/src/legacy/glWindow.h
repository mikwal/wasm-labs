#ifndef GLWINDOW_H
#define GLWINDOW_H




extern HWND glhWnd;
extern HINSTANCE glhInstance;
extern HDC glhDC;
extern HGLRC glhRC;


extern int glScreenWidth;
extern int glScreenHeight;
extern int glScreenBpp;

extern int glFlags;


extern unsigned char glKey[0xFF];

#define GLW_ACTIVE		0x0001
#define GLW_FULLSCREEN	0x0100
#define GLW_STENCILBUF	0x0200


int glCreateWindow(const char *title,int screen_w ,int screen_h ,int screen_bpp,int flags,WNDPROC wndproc=NULL);
int glDestroyWindow(const char *msg=NULL);


inline void glSwapBuffer() {SwapBuffers(glhDC);/*OPF=PPF=0;*/}


#endif