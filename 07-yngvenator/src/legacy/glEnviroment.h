#ifndef GLENVIROMENT_H
#define GLENVIROMENT_H


void glInitTimer();
float glTime();
extern float glLastTimeValue;
extern float glFrameTime;
inline void glSetFPS(float fps) {glFrameTime=1000.0f/fps;}
#define SECVAL(v) ((v)*(glFrameTime*0.001f))





class glViewPort {
public:
	glViewPort();

	int x;	// viewport box, (0,0,0,0) is the whole screen
	int y;
	int w;
	int h;
	
	float a;	// w -> h ascpect ratio, zero means disabled

	int scissor_x;	// scissor box, if not all parameters = 0 scissor is enabled by activate call
	int scissor_y;
	int scissor_w;
	int scissor_h;

	TEXTURE background_rgb;
	TEXTURE background_luminance;

	RGBACOLOR color;// background color

	RGBACOLOR light; // ambient light
	
	float fog_dens;	// fog parameters, if fog_dens > 0 the fog is enabled by activate call
	float fog_start;
	float fog_end;
	int fog_mode;	// GL_LINEAR, GL_EXP or GL_EXP2
	
	void clear();	// clear the viewport to color
	void setbackground(const char *texture_name);
};

extern glViewPort glDefaultView;
extern glViewPort *glView;

// set viewport as current and load it into gl
void glSetViewPort(glViewPort *v);





void glOrthoViewOn(float x_scale,float y_scale,float z_scale);
void glOrthoViewOff();
inline void glOrthoViewOn() {glOrthoViewOn(100.0f,100.0f,100.0f);}

inline void glClipPlane(unsigned int i,float A,float B,float C,float D) {double v[4]={A,B,C,D};glClipPlane(GL_CLIP_PLANE0+i,v);}
inline void glClipPlane(unsigned int i,const PLANE &p) {double v[4]={p.A,p.B,p.C,p.D};glClipPlane(GL_CLIP_PLANE0+i,v);}

inline void glZBufMask(int mask) {glDepthMask(mask);}

// void glPolygonMask(const unsigned char *mask);

void glResetEnviroment();	// reset all enviroment

inline void glPushAttrib() {glPushAttrib(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_ENABLE_BIT|GL_FOG_BIT|GL_LIGHTING_BIT|GL_POLYGON_BIT|GL_TEXTURE_BIT);}


#define glAlphaOn() glEnable(GL_ALPHA_TEST)
#define glAlphaOff() glDisable(GL_ALPHA_TEST)

#define glBlendOn() glEnable(GL_BLEND)
#define glBlendOff() glDisable(GL_BLEND)

#define glClipPlaneOn(i) glEnable(GL_CLIP_PLANE0+i)
#define glClipPlaneOff(i) glDisable(GL_CLIP_PLANE0+i)

#define glCullFaceOn() glEnable(GL_CULL_FACE)
#define glCullFaceOff() glDisable(GL_CULL_FACE)

#define glZBufOn() glEnable(GL_DEPTH_TEST)
#define glZBufOff() glDisable(GL_DEPTH_TEST)

#define glDitherOn() glEnable(GL_DITHER)
#define glDitherOff() glDisable(GL_DITHER)

#define glFogOn() glEnable(GL_FOG)
#define glFogOff() glDisable(GL_FOG)

#define glLightOn() (glEnable(GL_LIGHTING),glEnable(GL_NORMALIZE))
#define glLightOff() (glDisable(GL_LIGHTING),glDisable(GL_NORMALIZE))

#define glLineSmoothOn() glEnable(GL_LINE_SMOOTH)
#define glLineSmoothOff() glDisable(GL_LINE_SMOOTH)

#define glPointSmoothOn() glEnable(GL_POINT_SMOOTH)
#define glPointSmoothOff() glDisable(GL_POINT_SMOOTH)

#define glPolygonSmoothOn() glEnable(GL_POLYGON_SMOOTH)
#define glPolygonSmoothOff() glDisable(GL_POLYGON_SMOOTH)

// #define glPolygonMaskOn() glEnable(GL_POLYGON_STIPPLE)
// #define glPolygonMaskOff() glDisable(GL_POLYGON_STIPPLE)

#define glScissorOn() glEnable(GL_SCISSOR_TEST)
#define glScissorOff() glDisable(GL_SCISSOR_TEST)

#define glStencilOn() glEnable(GL_STENCIL_TEST)
#define glStencilOff() glDisable(GL_STENCIL_TEST)

#define glTextureOn() glEnable(GL_TEXTURE_2D)
#define glTextureOff() glDisable(GL_TEXTURE_2D)

#define glTexGenOn() (glEnable(GL_TEXTURE_GEN_S),glEnable(GL_TEXTURE_GEN_T))
#define glTexGenOff() (glDisable(GL_TEXTURE_GEN_S),glDisable(GL_TEXTURE_GEN_T))






#endif