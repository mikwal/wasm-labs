#include "textlines.h"

textline::textline() {
		width0 = 5;
		height0 = 5;
		vspace0 = 2;
		hspace0 = 3;
		color0 = RGBACOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		active=0;
		pos.x = 30;
		pos.y = 80;
		halign0 = GLF_CENTER;
}

textline::~textline() {
	delete [] vel;
	delete [] col;
	delete [] fade;
}

void textline::Init(char *_text) {
		text = _text;
		len = strlen(text);
		vel = new VECTOR [len];
		col = new RGBACOLOR [len];
		fade = new RGBACOLOR [len];
		Reset();
}

void textline::Reset() {
	float f;
	font.color = color0;
	
	font.height = height0;
	font.width = width0;
	
	font.hspace = hspace0;
	font.vspace = vspace0;
	
	font.valign = valign0;
	font.halign = halign0;

	font.texture = texture0;
	for(int i=0;i<len;i++) {
		f = float(62838)/float(len) * i;
		vel[i].y = sinf(f)/1000;
		vel[i].x = cosf(f)/1000;
		vel[i].z = 0;
		col[i] = 1;
		fade[i].r = float((rand() % 100) + 50)/10000.0f;
		fade[i].g = float((rand() % 100) + 50)/10000.0f;
		fade[i].b = float((rand() % 100) + 50)/10000.0f;
		fade[i].a = float((rand() % 100) + 50)/10000.0f;
	}
	spacing = VECTOR(hspace0,0,0);
}

void textline::Update() {
	for(int i=0;i<len;i++) {
		vel[i] = (vel[i]/vel[i].length())*(vel[i].length()+0.03f);
		col[i] -= fade[i];
		spacing.x+=0.001f;
		pos.x-=0.01f;
	}
}

void textline::Render() {
	font.bind();
	for(int i=0;i<len;i++) {
		font.put(pos + vel[i] + VECTOR(spacing*float(i)), &col[i], text[i]);
	}
}