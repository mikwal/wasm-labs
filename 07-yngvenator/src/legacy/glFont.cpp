#include <stdio.h>
#include "glBase.h"






void glFont::_print(float x,float y,float z,char *text)
{
	int l,i;
	char c;
	float tx,ty;
	float w2,h2;
	float dx;


	l=strlen(text);

	if(halign==GLF_CENTER)
		x-=(float)l*width*0.5f*hspace;
	else 
		if(halign==GLF_LEFT)
			x-=(float)l*width*hspace;

	dx=hspace*width;
	x+=dx/2.0f;
	w2=(float)width*0.5f;
	h2=(float)height*0.5f;
	
	for(i=0;i<l;i++,x+=dx)
	{
		c=text[i];

		tx=(c%16)*0.0625f+0.001f;
		ty=1.0f-(c/16)*0.0625f-0.001f;

		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(tx+0.0615f,ty);
		glVertex3f(x+w2,y+h2,z);
		glTexCoord2f(tx,ty);
		glVertex3f(x-w2,y+h2,z);
		glTexCoord2f(tx+0.0615f,ty-0.0615f);
		glVertex3f(x+w2,y-h2,z);
		glTexCoord2f(tx,ty-0.0615f);
		glVertex3f(x-w2,y-h2,z);		
		glEnd();
	}
}


void glFont::put(float x,float y,int c)
{
	float w2=width*0.5f ,h2=height*0.5f;
	float tx,ty;

	glColor(color);

	if(valign==GLF_CENTER)
		y+=height*0.5f*vspace;
	else 
		if(valign==GLF_BOTTOM)
			y+=height*vspace;

	if(halign==GLF_CENTER)
		x-=width*0.5f*hspace;
	else 
		if(halign==GLF_LEFT)
			x-=width*hspace;

	tx=(c%16)*0.0625f+0.001f;
	ty=1.0f-(c/16)*0.0625f-0.001f;

	glBegin(GL_TRIANGLE_STRIP);
	glTexCoord2f(tx+0.0615f,ty);
	glVertex3f(x+w2,y+h2,0.0f);
	glTexCoord2f(tx,ty);
	glVertex3f(x-w2,y+h2,0.0f);
	glTexCoord2f(tx+0.0615f,ty-0.0615f);
	glVertex3f(x+w2,y-h2,0.0f);
	glTexCoord2f(tx,ty-0.0615f);
	glVertex3f(x-w2,y-h2,0.0f);		
	glEnd();
}


void glFont::put(const VECTOR &p, RGBACOLOR *col,int c)
{
	float w2=width*0.5f ,h2=height*0.5f;
	float x=p.x, y=p.y;
	float tx,ty;

	if(valign==GLF_CENTER)
		y+=height*0.5f*vspace;
	else 
		if(valign==GLF_BOTTOM)
			y+=height*vspace;

	if(halign==GLF_CENTER)
		x-=width*0.5f*hspace;
	else 
		if(halign==GLF_LEFT)
			x-=width*hspace;
	glColor(*col);
	tx=(c%16)*0.0625f+0.001f;
	ty=1.0f-(c/16)*0.0625f-0.001f;

	glBegin(GL_TRIANGLE_STRIP);
	glTexCoord2f(tx+0.0615f,ty);
	glVertex3f(x+w2,y+h2,p.z);
	glTexCoord2f(tx,ty);
	glVertex3f(x-w2,y+h2,p.z);
	glTexCoord2f(tx+0.0615f,ty-0.0615f);
	glVertex3f(x+w2,y-h2,p.z);
	glTexCoord2f(tx,ty-0.0615f);
	glVertex3f(x-w2,y-h2,p.z);		
	glEnd();
}



void glFont::printf(float x,float y,char *text,...)
{
	char buf[512],buf2[512];
	va_list vlist;
	int n_line;
	int l;
	int i,wi;
	float dy;


	va_start(vlist,text);
	vsprintf(buf,text,vlist);
	va_end(vlist);

	glBindTexture(&texture);
	glColor(color);
	
	l=strlen(buf);
	for(i=0,n_line=1;i<l;i++)
		if(buf[i]==10)
			n_line++;
	
	if(valign==GLF_CENTER)
		y+=(float)n_line*height/2.0f*vspace;
	else 
		if(valign==GLF_BOTTOM)
			y+=(float)n_line*height*vspace;
	
	dy=-height*vspace;
	y+=dy/2.0f;

	for(i=0,wi=0;i<=l;i++)
		if(buf[i]==10||buf[i]==0)
		{
			if(i>wi)
			{
				strncpy(buf2,(char*)(buf+wi),i-wi);
				buf2[i-wi]=0;
				_print(x,y,0,buf2);
			}
			y+=dy;
			wi=i+1;
			if(buf[i]==0)
				break;
		}	
}

void glFont::printf(const VECTOR &p,char *text,...)
{
	char buf[512],buf2[512];
	va_list vlist;
	int n_line;
	int l;
	int i,wi;
	float dy;
	VECTOR v(0.0f);


	va_start(vlist,text);
	vsprintf(buf,text,vlist);
	va_end(vlist);

	glBindTexture(&texture);
	glColor(color);
	
	l=strlen(buf);
	for(i=0,n_line=1;i<l;i++)
		if(buf[i]==10)
			n_line++;
	
	if(valign==GLF_CENTER)
		v.y+=(float)n_line*height/2.0f*vspace;
	else 
		if(valign==GLF_BOTTOM)
			v.y+=(float)n_line*height*vspace;
	
	dy=-height*vspace;
	v.y+=dy/2.0f;

	for(i=0,wi=0;i<=l;i++)
		if(buf[i]==10||buf[i]==0)
		{
			if(i>wi)
			{
				strncpy(buf2,(char*)(buf+wi),i-wi);
				buf2[i-wi]=0;
				_print(v.x+p.x,v.y+p.y,v.z+p.z,buf2);
			}
			v.y+=dy;
			wi=i+1;
			if(buf[i]==0)
				break;
		}	
}