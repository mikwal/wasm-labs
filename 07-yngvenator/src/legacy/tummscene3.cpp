#include "glBase.h"
#include "math3d.h"
#include "wall.h"

#define RSPEED 0.115f
#define MSPEED 0.115f

////////////////////////////////

GL_NEW_SCENE(tumm03_Scene);

//GL_FIRST_SCENE(tumm03_Scene);


////////////////////////////////

static glCamera cam[2];
static int cam_i=0;
static float time = 0.0f, timedir = SECVAL(2.0f), splinetime = 0.0f, splinetimedir = SECVAL(PI/8);
static int rot = 1;

static glFont font1;
static VECTOR t(0.0f,2.0f,0.0f);
//glRootObject * buh; 

static TEXTURE fsys, texApor, test2;

Wall wall1;
Wall wall2;
Wall wall3;
Wall wall4;


void tumm03_Scene_load()
{
	glLoadTexture("rutn\xE4t.tga", &texApor);
	glLoadTexture("fixedsys.tga", &fsys);
}


void tumm03_Scene_init()
{
	wall1.Init(&VECTOR(-100.0f, -40.0f, 40.0f),		 // Golv
		       &VECTOR(1.0f, 0.0f, 0.0f), 
			   &VECTOR(0.0f, 0.0f, 1.0f), 
			   200.0f, 
			   200.0f);
	
	wall2.Init(&VECTOR(100.0f, -40.0f, 40.0f),		// H�ger V�gg
		       &VECTOR(0.0f, 1.0f, 0.0f), 
			   &VECTOR(0.0f, 0.0f, 1.0f), 
			   200.0f, 
			   200.0f);
	
	wall3.Init(&VECTOR(-100.0f, -40.0f, 40.0f),		// V�nster V�gg
		       &VECTOR(0.0f, 0.0f, 1.0f), 
			   &VECTOR(0.0f, 1.0f, 0.0f), 
			   200.0f, 
			   200.0f);
	
	wall4.Init(&VECTOR(-100.0f, -40.0f, 240.0f),	//V�gg rakt fram
		       &VECTOR(1.0f, 0.0f, 0.0f), 
			   &VECTOR(0.0f, 1.0f, 0.0f), 
			   200.0f, 
			   200.0f);
	
	
	
	glResetEnviroment();

	
	//glLightOn();
	cam[0].zfar=5000.0f;
	cam[1].zfar=5000.0f;

	font1.width=3.0f;
	font1.height=4.5f;
	font1.texture=fsys;
	font1.color = (0.0f, 0.0f, 0.0f, 1.0f);
	glView->light = RGBACOLOR(0.5, 0.5, 0.5, 1);
	cam[0].matrix *= Mrotate(PI/2, 0, 0, 1);



		
}

void tumm03_Scene_free()
{
	
	glFreeTexture(&fsys);
	glFreeTexture(&texApor);
	glFreeTexture(&test2);
}




int tumm03_Scene_update()
{
// if(glKey[VK_UP])
// 	cam[0].matrix*=Mtranslate(0.0f,-MSPEED*5,0.0f);
// if(glKey[VK_DOWN])
// 	cam[0].matrix*=Mtranslate(0.0f,MSPEED*5, 0.0f);
// if(glKey[VK_INSERT])
// 	cam[0].matrix*=Mrotate(RSPEED,Vz);
// if(glKey[VK_DELETE])
// 	cam[0].matrix*=Mrotate(-RSPEED,Vz);

// if(glKey[VK_PRIOR])
// 	cam[0].matrix*=Mrotate(RSPEED,Vx);
// if(glKey[VK_NEXT])
// 	cam[0].matrix*=Mrotate(-RSPEED,Vx);
// if(glKey[VK_LEFT])
// 	cam[0].matrix*=Mrotate(-RSPEED,Vy);
// if(glKey[VK_RIGHT])
// 	cam[0].matrix*=Mrotate(RSPEED,Vy);

if(glKey['Z'])	{
//	glScene::current = &tumm02_Scene;
}

	if(glKey['1'])
		cam_i=0;

	if(glKey['2'])
		cam_i=1;
	
	time+=timedir;


	wall1.Update();
	wall2.Update();
	wall3.Update();
	wall4.Update();


	
	return 0;
}


#define PLANE(p1,p2,p3,p4) glBegin(GL_TRIANGLE_STRIP),glVertex3fv((float*)&p1),glVertex3fv((float*)&p2),glVertex3fv((float*)&p3),glVertex3fv((float*)&p4),glEnd();


void tumm03_Scene_render()
{

	glResetEnviroment();
	glBlendOn();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glOrthoViewOn();
	font1.color = RGBACOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	font1.printf(VECTOR(10.0f, 10.0f, 0.0f), "%f - %f", wall1.height, cam[0].matrix.e01);
	glOrthoViewOff();

	glView->color.a = 1.0f;
	glView->color.r = 0.0f;
	glView->color.g = 0.0f;
	glView->color.b = 0.0f;
	glView->clear();
	//glSetCamera(c);
	glSetCamera(&cam[0]);
	//glApplyAllLights();
	
	//glView->light = RGBACOLOR(0.5f, 0.5f, 0.5f, 1.0f):
	
	
	glPushMatrix();
		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, texApor);
		wall1.Draw();
	glPopMatrix();
	glPushMatrix();
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, texApor);
		wall2.Draw();
	glPopMatrix();
	glPushMatrix();
		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, texApor);
		wall3.Draw();
	glPopMatrix();
	glPushMatrix();
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, texApor);
		wall4.Draw();
	glPopMatrix();
	
		
	glLineSmoothOff();
	glZBufOff();
	glBlendOn();
		
	
	glSwapBuffer();
}

