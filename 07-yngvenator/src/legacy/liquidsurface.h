#ifndef LIQUIDSURFACE_H
#define LIQUIDSURFACE_H







class LSurface : public glSceneObject{
public:
	MATERIAL material;
	int w,h,lod;
	float damp;
	float *map, *src_map;
	VECTOR *normal;
		
	LSurface() : glSceneObject() {map=src_map=NULL;normal=NULL;}
	LSurface(const char *texture_name,int _w,int _h,int _lod, float _damp,int mapping=GL_SPHERE_MAP);
	~LSurface() {delete [] map; delete [] src_map; delete [] normal;}

	void update(float time);
	void render(int pass,int renderflags=0);
};








#endif