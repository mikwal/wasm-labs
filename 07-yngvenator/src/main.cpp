#include <browser.h>
#include <cstdint>
#include <cstdlib>
#include <gl/gl.h>
#include "stubs/include/windows.h"
#include "legacy/glWindow.h"

int WinMain(int hInstance,int hPrevInstance,void *lpCmdLine,int nCmdShow);
long _wndproc(int, unsigned int, unsigned int, long);

float fps;

extern "C" void __show_fps(double fps) __attribute__((__import_module__("app"), __import_name__("show_fps")));



int window_new_width = 0;
int window_new_height = 0;
void on_resize_window(int new_width, int new_height) __attribute__((__export_name__("on_resize_window"))) {
    window_new_width = new_width;
    window_new_height = new_height;
}

void glClear(GLbitfield mask) {
    __proxy_batch_mode(true);
    browser::wait_for_animation_frame();
    rglClear(mask);
}
 
void SwapBuffers(int ) {

    __show_fps(fps);
    __proxy_batch_mode(false);

    if (window_new_width && window_new_height) {
        _wndproc(0, WM_SIZE, 0, (window_new_width & 0xFFFF) | ((window_new_height & 0xFFFF) << 16));
        window_new_width = window_new_height = 0;
    }
}

double timeGetTime() {
    timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
	return t.tv_sec*1000.0+t.tv_nsec/1000000.0;
}

int main(int argc, char **argv) {
    uint32_t width = 800;
    uint32_t height = 600;
    if (argc >= 2) {
        width = atoi(argv[0]);
        height = atoi(argv[1]);
    }
    RegalMakeCurrent((RegalSystemContext)1);
    browser::load_file_async("/gfx.dat");
    browser::load_file_async("/sce.dat");
    browser::load_file_async("/music.wav");
    browser::wait_for_async();
    glViewport(0, 0, width, height);
    glCreateWindow("", width, height, 32, 0, NULL);
    return WinMain(0, 0, NULL, 0);
}