#ifndef STUBS_MMSYSTEM_DLL_H
#define STUBS_MMSYSTEM_DLL_H

#include <windows.h>

typedef long long __int64;
typedef long long LARGE_INTEGER;

BOOL QueryPerformanceFrequency(LARGE_INTEGER *lpFrequency);
BOOL QueryPerformanceCounter(LARGE_INTEGER *lpPerformanceCount);

#endif