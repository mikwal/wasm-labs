#ifndef STUBS_FMOD_H
#define STUBS_FMOD_H

#define FSOUND_FREE (-1)
#define FSOUND_OUTPUT_WINMM (0)
#define FSOUND_MIXER_QUALITY_FPU (0)
#define FSOUND_STEREO (2)
#define FSOUND_LOOP_NORMAL (0)
#define FSOUND_DSP_DEFAULTPRIORITY_USER (0)


struct FSOUND_STREAM {};
struct FSOUND_DSPUNIT {};
typedef unsigned char FSOUND_BOOL;
typedef signed char (*FSOUND_STREAM_CALLBACK)(FSOUND_STREAM *stream, void *buf, int len, int param);

void FSOUND_Init(int sampleRate, int numChannels, int);
void FSOUND_SetOutput(int output);
void FSOUND_SetDriver(int driver);
int FSOUND_GetMixer();

#define FSOUND_File_SetCallbacks(...)

FSOUND_STREAM *FSOUND_Stream_OpenFile(const char *filename, int flags, int );
void FSOUND_Stream_SetSynchCallback(FSOUND_STREAM *stream, FSOUND_STREAM_CALLBACK callback, int);
double FSOUND_Stream_GetTime(FSOUND_STREAM *stream);
void FSOUND_Stream_SetPosition(FSOUND_STREAM *stream, double position);
void FSOUND_Stream_Play(int channel, FSOUND_STREAM *stream);
void FSOUND_Stream_Close(FSOUND_STREAM *stream);

FSOUND_DSPUNIT *FSOUND_DSP_GetFFTUnit();
int FSOUND_DSP_GetBufferLength();
int FSOUND_DSP_GetBufferLengthTotal();
float* FSOUND_DSP_GetSpectrum();
FSOUND_BOOL FSOUND_DSP_SetActive(FSOUND_DSPUNIT *dsp, FSOUND_BOOL active);
void FSOUND_DSP_Free(FSOUND_DSPUNIT *dsp);

void FSOUND_Close();

#endif