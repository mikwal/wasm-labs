#ifndef STUBS_WINDOWS_H
#define STUBS_WINDOWS_H

#include <cstdint>
#include <cstdlib>
#include <cctype>
#include <cstdarg>
#include <memory.h>
#include <fmod_legacy.h>

typedef uint8_t BOOL;
typedef uint8_t UCHAR;
typedef uint32_t UINT;
typedef int32_t LONG;
typedef uint32_t ULONG;

typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;

typedef struct __attribute__((packed, aligned(2))) tagBITMAPFILEHEADER {
    WORD bfType;
    DWORD bfSize;
    WORD bfReserved1;
    WORD bfReserved2;
    DWORD bfOffBits;
} BITMAPFILEHEADER, *LPBITMAPFILEHEADER, *PBITMAPFILEHEADER;

typedef struct __attribute__((packed, aligned(2))) tagBITMAPINFOHEADER {
    DWORD biSize;
    LONG biWidth;
    LONG biHeight;
    WORD biPlanes;
    WORD biBitCount;
    DWORD biCompression;
    DWORD biSizeImage;
    LONG biXPelsPerMeter;
    LONG biYPelsPerMeter;
    DWORD biClrUsed;
    DWORD biClrImportant;
} BITMAPINFOHEADER, *LPBITMAPINFOHEADER, *PBITMAPINFOHEADER;

typedef struct tagPALETTEENTRY {
    BYTE peRed;
    BYTE peGreen;
    BYTE peBlue;
    BYTE peFlags;
} PALETTEENTRY, *PPALETTEENTRY, *LPPALETTEENTRY;

#define WINAPI
#define CALLBACK

#define PM_NOREMOVE 0x0000

#define WM_SIZE 0x0005
#define WM_QUIT 0x0012

#define VK_ESCAPE 0x1B
#define VK_SPACE 0x20

#define LOWORD(dw) ((dw) & 0x0000FFFF)
#define HIWORD(dw) (((dw) & 0xFFFF0000) >> 16)

#define MAX_PATH 260

typedef int HINSTANCE;
typedef int HWND;
typedef int HDC;
typedef int HGLRC;
typedef int WNDCLASS;
typedef unsigned int UINT_PTR;
typedef long LONG_PTR;
typedef LONG_PTR LRESULT;
typedef UINT_PTR WPARAM;
typedef LONG_PTR LPARAM;
typedef LRESULT CALLBACK (*WNDPROC)(HWND hWnd,UINT Msg,WPARAM wParam,LPARAM lParam);
typedef struct { int x; int y; int w; int h; } RECT;
typedef struct { } PIXELFORMATDESCRIPTOR;
typedef void *LPSTR;
typedef struct { int wParam; int message; } MSG;
inline int PeekMessage(MSG *msg, void*, int, int, int) { return msg->wParam = msg->message = 0; }
inline int GetMessage(MSG *msg, void*, int, int) { return msg->wParam = msg->message = 0; }
inline void TranslateMessage(MSG *) {}
inline void DispatchMessage(MSG *) {}
inline void PostQuitMessage(int code) { exit(code); }
inline LRESULT CALLBACK DefWindowProc(HWND hwnd,UINT msg,WPARAM wparam,LPARAM lparam) { return 0; }

double timeGetTime();

char *strupr(const char* str);

typedef bool DEVMODE;

extern float fps;

#endif