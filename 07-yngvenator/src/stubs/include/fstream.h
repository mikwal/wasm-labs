#ifndef STUBS_FSTREAM_H
#define STUBS_FSTREAM_H

#include <fstream>

typedef std::ifstream ifstream;
typedef std::ofstream ofstream;

#endif