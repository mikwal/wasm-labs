#include <fmod.h>
#include <fmod_legacy.h>

void FSOUND_Init(int sampleRate, int numChannels, int) {

}

void FSOUND_SetOutput(int output) {

}

void FSOUND_SetDriver(int driver) {

}

int FSOUND_GetMixer() {

}


FSOUND_STREAM *FSOUND_Stream_OpenFile(const char *filename, int flags, int ) {
    return new FSOUND_STREAM();
}

void FSOUND_Stream_SetSynchCallback(FSOUND_STREAM *stream, FSOUND_STREAM_CALLBACK callback, int) {

}

double FSOUND_Stream_GetTime(FSOUND_STREAM *stream) {
    return 0;
}

void FSOUND_Stream_SetPosition(FSOUND_STREAM *stream, double position) {

}

void FSOUND_Stream_Play(int channel, FSOUND_STREAM *stream) {

}

void FSOUND_Stream_Close(FSOUND_STREAM *stream) {
    delete stream; 
}

FSOUND_DSPUNIT *FSOUND_DSP_GetFFTUnit() {
    return new FSOUND_DSPUNIT();
}

int FSOUND_DSP_GetBufferLength() {

}

int FSOUND_DSP_GetBufferLengthTotal() {

}

static float dsp[4096];
float* FSOUND_DSP_GetSpectrum() {
    return dsp;
}

FSOUND_BOOL FSOUND_DSP_SetActive(FSOUND_DSPUNIT *dsp, FSOUND_BOOL active) {
    return false;
}

void FSOUND_DSP_Free(FSOUND_DSPUNIT *dsp) {
    delete dsp;
}

void FSOUND_Close() {
    
}