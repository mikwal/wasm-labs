#include <cctype>
#include <cstdlib>
#include <cstring>

char *strupr(const char* str) {
    static int buf_len = 256;
    static char *buf = (char *)malloc(buf_len);

    int len = strlen(str);
    if (len + 1 > buf_len) {
        buf_len *= 2;
        free(buf);
        buf = (char *)malloc(buf_len);
    }

    int i;
    for (i = 0; str[i]; i++) {
        buf[i] = toupper(str[i]); 
    }
    buf[i] = 0;

    return buf;
}
