import { WASIModule } from '/js/wasi';

declare var XMPlayer: {
    init(): void;
    load(buffer: ArrayBuffer): void;
    play(): void;
    stop(): void;
}

export async function init(programUrl: string) {

    const canvas = document.getElementById('screen')! as HTMLCanvasElement;
    
    const fpsCounter = document.getElementById('fps-counter')!;
    const showFps = new URL(document.location.href).searchParams.has('showfps');
    const showFpsCallback = showFps 
        ? (fps: number | bigint) => { fpsCounter.innerText = `FPS=${Number(fps).toFixed(0)}`; }
        : () => {};

    console.log(`Loading and compiling '${programUrl}'...`);

    const module = new WASIModule();
    try {
        await module.load(programUrl, { 
            imports: {
                app: {
                    'show_fps': (fps: number) => {
                        showFpsCallback(fps);
                    },
                    'xm_init': () => {
                        XMPlayer.init();
                    },
                    'xm_load': (filename: number) => {
                        XMPlayer.load(module.getFileContent(module.memory.readNullTerminatedString(filename as number)!)!);
                    },
                    'xm_play': () => {
                        XMPlayer.play();
                    },
                    'xm_stop': () => {
                        XMPlayer.stop();
                    }
                }
            },
            plugins: [
                ['/js/GLES2', { canvas }]
            ],
            proxyBatchSize: 50000
        });
    }
    catch (error) {
        console.error(`Failed to load and compile '${programUrl}':`, error);
        return;
    }

    console.log(`Done.`);

    const splash = document.getElementById('splash')!;
    const startButton = splash.querySelector('button')!;

    startButton.focus();
    startButton.onclick = async () => {
        startButton.onclick = null;
        splash.classList.add('disabled');
        
        resizeCanvas();
        window.addEventListener('resize', () => {
            resizeCanvas();
            module.exports.on_resize_window(canvas.width, canvas.height);
        });

        canvas.classList.add('enabled');
        if (showFps) {
            fpsCounter.classList.add('enabled');
        }
        
        console.log(`Running program...`);
        
        module.argv = [canvas.width.toString(), canvas.height.toString()];
        const code = await module.start();
    
        console.log(`Program exited with code ${code}.`);
    };

    function resizeCanvas() {
        
        const borders = 20;

        let width = Math.max(160, window.innerWidth - borders * 2) 
        let height = Math.max(120, window.innerHeight - borders * 2);

        if (width * 3 > height * 4) {
            const extra = Math.round((width * 3 - height * 4) / 3);
            width -= extra;
        }
        else if (height * 4 > width * 3) {
            const extra = Math.round((height * 4 - width * 3) / 4);
            height -= extra;
        }

        canvas.width = width;
        canvas.height = height;
        canvas.style.width = `${width}px`;
        canvas.style.height = `${height}px`;
        canvas.style.left = `${Math.round((window.innerWidth - width) / 2)}px`;
        canvas.style.top = `${Math.round((window.innerHeight - height) / 2)}px`;
    }
}


